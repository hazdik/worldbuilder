VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAtmosphere"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarHeight As Long 'local copy
Private mvarLower As CRGBColor 'local copy
Private mvarUpper As CRGBColor 'local copy
Private mvarSky As CRGBColor 'local copy
Private mvarCloudMap As String 'local copy
Private mvarCloudHeight As Long 'local copy
Private mvarCloudSpeed As Long 'local copy
Private mvarSunset As CRGBColor 'local copy

Public Property Set Sunset(ByVal vData As CRGBColor)
    Set mvarSunset = vData
End Property


Public Property Get Sunset() As CRGBColor
    Set Sunset = mvarSunset
End Property



Public Property Let CloudSpeed(ByVal vData As Long)
    mvarCloudSpeed = vData
End Property


Public Property Get CloudSpeed() As Long
    CloudSpeed = mvarCloudSpeed
End Property



Public Property Let CloudHeight(ByVal vData As Long)
    mvarCloudHeight = vData
End Property


Public Property Get CloudHeight() As Long
    CloudHeight = mvarCloudHeight
End Property



Public Property Let CloudMap(ByVal vData As String)
    mvarCloudMap = vData
End Property


Public Property Get CloudMap() As String
    CloudMap = mvarCloudMap
End Property



Public Property Set Sky(ByVal vData As CRGBColor)
    Set mvarSky = vData
End Property


Public Property Get Sky() As CRGBColor
    Set Sky = mvarSky
End Property




Public Property Set Upper(ByVal vData As CRGBColor)
    Set mvarUpper = vData
End Property


Public Property Get Upper() As CRGBColor
    Set Upper = mvarUpper
End Property



Public Property Set Lower(ByVal vData As CRGBColor)
    Set mvarLower = vData
End Property


Public Property Get Lower() As CRGBColor
    Set Lower = mvarLower
End Property



Public Property Let Height(ByVal vData As Long)
    mvarHeight = vData
End Property


Public Property Get Height() As Long
    Height = mvarHeight
End Property


Public Sub Generate(StarType As String, SpectralType As String, RadiusM As Double)

  If SpectralType Like "EoGaian" Or SpectralType Like "Europan" _
    Or SpectralType Like "LithicGelidian" Or SpectralType Like "Panthalassic" _
    Or SpectralType Like "Gaian" Or SpectralType Like "MesoGaian" _
    Or SpectralType Like "Cytherian" Or SpectralType Like "Moist Greenhouse" _
    Or StarType Like "Jovian" Or StarType Like "Brown Dwarf" Then
    
    If SpectralType Like "EoGaian" Or SpectralType Like "Europan" _
      Or SpectralType Like "LithicGelidian" Or SpectralType Like "Panthalassic" _
      Or SpectralType Like "Gaian" Then
      
      mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM

      
      mvarLower.Red = 0.43
      mvarLower.Green = 0.52
      mvarLower.Blue = 0.65
      
      mvarUpper.Red = 0.26
      mvarUpper.Green = 0.47
      mvarUpper.Blue = 0.84
      
      mvarSky.Red = 0.4
      mvarSky.Green = 0.6
      mvarSky.Blue = 1#
      
      mvarSunset.Red = 1#
      mvarSunset.Green = 0.6
      mvarSunset.Blue = 0.2
      
      mvarCloudHeight = 7
      mvarCloudSpeed = 65
      mvarCloudMap = "earth-clouds.*"
        
    ElseIf SpectralType Like "MesoGaian" Then
      mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
      
      mvarLower.Red = 0.43
      mvarLower.Green = 0.52
      mvarLower.Blue = 0.65
      
      mvarUpper.Red = 0.26
      mvarUpper.Green = 0.47
      mvarUpper.Blue = 0.84
      
      mvarSky.Red = 0.4
      mvarSky.Green = 0.6
      mvarSky.Blue = 1#
      
      mvarSunset.Red = 1#
      mvarSunset.Green = 0.6
      mvarSunset.Blue = 0.2
      
      mvarCloudHeight = 7
      mvarCloudSpeed = 65
      mvarCloudMap = "venus.jpg"
              
    ElseIf SpectralType Like "Cytherian" Or SpectralType Like "Moist Greenhouse" Then
      mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
      mvarLower.Red = 0.8
      mvarLower.Green = 0.8
      mvarLower.Blue = 0.5
      
      mvarUpper.Red = 0.6
      mvarUpper.Green = 0.6
      mvarUpper.Blue = 0.6
      
      mvarSky.Red = 0.8
      mvarSky.Green = 0.8
      mvarSky.Blue = 0.5
      
      mvarCloudMap = "venus.jpg"
      mvarCloudHeight = 50
      mvarCloudSpeed = 90
      

    ElseIf SpectralType Like "Arean" Then
      mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
      
      mvarLower.Red = 0.8
      mvarLower.Green = 0.6
      mvarLower.Blue = 0.6
      
      mvarUpper.Red = 0.7
      mvarUpper.Green = 0.3
      mvarUpper.Blue = 0.3
      
      mvarSky.Red = 0.83
      mvarSky.Green = 0.75
      mvarSky.Blue = 0.65
      
      mvarSunset.Red = 0.7
      mvarSunset.Green = 0.7
      mvarSunset.Blue = 0.8
    End If
  End If
  
  If StarType Like "Jovian" Or StarType Like "Brown Dwarf" Then
  
    mvarHeight = (0.002442188 + randn * 0.000233374) * RadiusM
    
    mvarLower.Red = 0.8
    mvarLower.Green = 0.75
    mvarLower.Blue = 0.65
    
    mvarUpper.Red = 0.6
    mvarUpper.Green = 0.55
    mvarUpper.Blue = 0.45
    
    mvarSky.Red = 0.8
    mvarSky.Green = 0.8
    mvarSky.Blue = 0.5
      
  End If
  
  If StarType Like "Star" Then
    mvarHeight = RadiusM / 2
    
    If SpectralType Like "O*" Then
      mvarLower.Red = 0.8
      mvarLower.Green = 0.8
      mvarLower.Blue = 0.96
    
    ElseIf SpectralType Like "B*" Then
      mvarLower.Red = 0.91
      mvarLower.Green = 0.99
      mvarLower.Blue = 1
    
    ElseIf SpectralType Like "A*" Then
      mvarLower.Red = 1
      mvarLower.Green = 1
      mvarLower.Blue = 1
    
    ElseIf SpectralType Like "F*" Then
      mvarLower.Red = 0.99
      mvarLower.Green = 0.99
      mvarLower.Blue = 0.8
    
    ElseIf SpectralType Like "G*" Then
      mvarLower.Red = 1
      mvarLower.Green = 0.96
      mvarLower.Blue = 0.59
    
    ElseIf SpectralType Like "K*" Then
      mvarLower.Red = 0.98
      mvarLower.Green = 0.84
      mvarLower.Blue = 0.59
    
    ElseIf SpectralType Like "M*" Then
      mvarLower.Red = 0.89
      mvarLower.Green = 0.44
      mvarLower.Blue = 0.45
    
    End If
    
    If StarType Like "White Dwarf" Then
      mvarLower.Red = 1
      mvarLower.Green = 1
      mvarLower.Blue = 1
    End If
    
    mvarUpper.Red = 0
    mvarUpper.Green = 0
    mvarUpper.Blue = 0
    
    mvarSky.Red = mvarLower.Red
    mvarSky.Green = mvarLower.Green
    mvarSky.Blue = mvarLower.Blue
  End If
End Sub

Private Sub Class_Initialize()
  Set mvarLower = New CRGBColor
  Set mvarUpper = New CRGBColor
  Set mvarSky = New CRGBColor
  Set mvarSunset = New CRGBColor
End Sub

Private Sub Class_Terminate()
  Set mvarLower = Nothing
  Set mvarUpper = Nothing
  Set mvarSky = Nothing
  Set mvarSunset = Nothing
End Sub
