VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCollageFractal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"CComposition"
Option Explicit

' Creates random heightmaps by the collage method or the fractal method

Private mvarZ() As Integer

Private mvarZCoast As Long
Private mvarZShelf As Long
Private mvarCComposition As CComposition


Public Property Get CComposition() As CComposition
    If mvarCComposition Is Nothing Then
        Set mvarCComposition = New CComposition
    End If


    Set CComposition = mvarCComposition
End Property


Public Property Set CComposition(vData As CComposition)
    Set mvarCComposition = vData
End Property
Private Sub Class_Terminate()
    Set mvarCComposition = Nothing
End Sub




Public Property Let ZCoast(vData As Long)
  mvarZCoast = vData
End Property
Public Property Get ZCoast() As Long
  ZCoast = mvarZCoast
End Property

Public Property Let ZShelf(vData As Long)
  mvarZShelf = vData
End Property
Public Property Get ZShelf() As Long
  ZShelf = mvarZShelf
End Property



Public Function GetHiResDEM() As Integer()
  GetHiResDEM = mvarZ
End Function


Public Function CollageTerrain(RDimIn As Long, CDimIn As Long, ZDelta As Long, _
  MaxR As Long, MaxAlt As Long, ZMin As Long, Optional UseExistingZ As Boolean = False) As Integer()
  
  Dim i As Long
  Dim Z() As Integer
  Dim RDim As Long, CDim As Long
  Dim newR As Long, newC As Long, radR As Long, radC As Long, altZ As Long
  Dim ro As Long, co As Long
  Dim newZ() As Long
  Dim r As Long, c As Long
  
  RDim = RDimIn
  CDim = CDimIn
  
  If UseExistingZ = True Then
    Z = mvarZ
  Else
    ReDim Z(RDim, CDim) As Integer
  End If
  
  i = 0
  Do While i < ZDelta 'MaxMax(Z) - MinMin(Z) < ZDelta
    i = i + 1
    
    newR = ceil(randn * RDim / 2 + RDim / 2)
    newC = ceil(randn * CDim / 2 + CDim / 2)
    radR = ceil(Rnd * MaxR)
    radC = ceil(Rnd * MaxR)
    altZ = randn * MaxAlt
    
    ReDim newZ(RDim, CDim) As Long
    
    For ro = (newR - radR) To (newR + radR)
      For co = (newC - radC) To (newC + radC)
        If ro >= 0 And ro <= RDim And co >= 0 And co <= CDim Then
            
          r = ro - newR
          c = co - newC
          
          newZ(ro, co) = radR / MaxR * (1 - (r ^ 2 / radR ^ 2) - (c ^ 2 / radC ^ 2))
          If newZ(ro, co) < 0 Then newZ(ro, co) = 0
          If Z(ro, co) + altZ * newZ(ro, co) + ZMin > 32767 Then
            Z(ro, co) = 32767
          Else
            Z(ro, co) = Z(ro, co) + altZ * newZ(ro, co) + ZMin
          End If
          If Z(ro, co) < 0 Then Z(ro, co) = 0
        
        End If
      Next co
    Next ro
    DoEvents
  Loop
  
  mvarZ = Z
  CollageTerrain = Z
End Function

Private Function getdata(X As Long, Y As Long, MX As Long, MY As Long) As Single
  ' Get an array element
  Dim L As Single
  
  If Y > MY Then
    L = mvarZ(MX + 1 - Y + 1, MX - X + 1)
  Else
    L = mvarZ(Y + 1, X + 1)
  End If
  getdata = L
End Function


Public Sub FractalTerrainInterp(BaseArray() As Integer, Levels As Long, Optional NoWrap As Boolean = False)
  
  'Dim OutputArray() As Long
  
  Dim i As Long, j As Long
  Dim n As Long
  
  Dim ratio As Long
  Dim mountainscale As Single
  Dim baseRows As Long
  Dim baseCols As Long
  Dim newRows As Long, newCols As Long
  Dim stepsize As Long
  Dim altdev As Long
  Dim tempZ As Long
  Dim sealevel As Double
  Dim thisRowHi As Long
  Dim thisColHi As Long
  Dim thisRowLo As Long
  Dim thisColLo As Long
  
  Dim maxZ As Long, minZ As Long
  
  baseRows = UBound(BaseArray, 1)
  baseCols = UBound(BaseArray, 2)
  
  ratio = 2 ^ Levels
  
  newRows = baseRows * 2 ^ Levels
  newCols = baseCols * 2 ^ Levels
  
  ReDim mvarZ(newRows, newCols) As Integer
  
  mountainscale = MAXMOUNTAINHEIGHT * 100# / 255
  altdev = mountainscale
  sealevel = mvarZCoast * mountainscale
  
  maxZ = 0
  minZ = 1000000#
  
  ' Copy the base array into the new array, and scale it into feet
  For i = 0 To baseRows
    For j = 0 To baseCols
      If CLng(BaseArray(i, j)) * CLng(altdev) <= 32767 Then
        mvarZ(i * ratio, j * ratio) = CLng(BaseArray(i, j)) * CLng(altdev)
        If CLng(BaseArray(i, j)) * CLng(altdev) < minZ Then minZ = CLng(BaseArray(i, j)) * CLng(altdev)
        If CLng(BaseArray(i, j)) * CLng(altdev) > maxZ Then maxZ = CLng(BaseArray(i, j)) * CLng(altdev)
      Else
        mvarZ(i * ratio, j * ratio) = 32767
        If 32767 < minZ Then minZ = 32767
        If 32767 > maxZ Then maxZ = 32767
      End If
    Next j
  Next i
  
  ' setup the basics
  stepsize = ratio
  
  frmTec.ProgressBar1.Value = 0
  If Levels > 0 Then
    frmTec.ProgressBar1.max = Levels
  Else
    frmTec.ProgressBar1.max = 1
  End If
  
  For n = 1 To Levels
    ' cut the altitude deviation in half
    'altdev = altdev / 2
    
    ' interpolate main rows
    For i = 0 To newRows Step stepsize
      For j = stepsize / 2 To newCols Step stepsize
        thisColLo = j - stepsize / 2
        thisColHi = j + stepsize / 2
        
        If thisColLo >= 0 And thisColHi <= newCols Then
        
          mvarZ(i, j) = Int((CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, thisColHi))) / 2)
        
        ElseIf thisColLo < 0 Then ' wrap around
        
          If NoWrap = False Then
            mvarZ(i, j) = Int((CLng(mvarZ(i, newCols)) + CLng(mvarZ(i, thisColHi))) / 2)
          Else
            mvarZ(i, j) = mvarZ(i, thisColHi)
          End If
        
        Else ' wrap around the other way
        
          If NoWrap = False Then
            mvarZ(i, j) = Int((CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, 0))) / 2)
          Else
            mvarZ(i, j) = mvarZ(i, thisColLo)
          End If
        End If
        
        ' introduce a random element
        If mvarZ(i, j) >= sealevel Then
          tempZ = mvarZ(i, j) + randn * altdev
          If tempZ > 32767 Then tempZ = 32767
          mvarZ(i, j) = tempZ
        End If
        If mvarZ(i, j) < 0 Then
          mvarZ(i, j) = 0
        End If
        'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
        
        If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
        If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
      Next j
    Next i
    
    ' interpolate main cols
    For i = stepsize / 2 To newRows Step stepsize
      thisRowLo = i - stepsize / 2
      thisRowHi = i + stepsize / 2
      
      For j = 0 To newCols Step stepsize
        
        If thisRowLo >= 0 And thisRowHi <= newRows Then
          mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowHi, j))) / 2)
        ElseIf thisRowLo < 0 Then
          mvarZ(i, j) = mvarZ(0, j)
        ElseIf thisRowHi > newRows Then
          mvarZ(i, j) = mvarZ(newRows, j)
        End If
        
        ' introduce a random element
        If mvarZ(i, j) >= sealevel Then
          tempZ = mvarZ(i, j) + randn * altdev
          If tempZ > 32767 Then tempZ = 32767
          mvarZ(i, j) = tempZ
        End If
        If mvarZ(i, j) < 0 Then
          mvarZ(i, j) = 0
        End If
        'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
        If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
        If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
      Next j
    Next i
    
    ' interpolate diagonally
    For i = stepsize / 2 To newRows Step stepsize
      thisRowLo = i - stepsize / 2
      thisRowHi = i + stepsize / 2
      
      If thisRowLo >= 0 And thisRowHi <= newRows Then
      
        For j = stepsize / 2 To newCols Step stepsize
          thisColLo = j - stepsize / 2
          thisColHi = j + stepsize / 2
          
          If thisColLo >= 0 And thisColHi <= newCols Then
            ' eight-way
            mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, thisColLo)) + CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
              CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, thisColLo)) + _
              CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 8)
          ElseIf thisColLo < 0 Then
            If NoWrap = False Then
              ' eight-way across left edge
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, newCols)) + CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                CLng(mvarZ(i, newCols)) + CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, newCols)) + _
                CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 8)
            Else
              ' five-way across left edge
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                 CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 5)
            End If
          Else
            If NoWrap = False Then
              ' eight-way across right edge
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                CLng(mvarZ(i, 0)) + CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, 0)) + _
                CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 8)
            Else
              ' five-way across right edge
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                 CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 5)
            End If
          End If
        
          ' introduce a random element
          If mvarZ(i, j) >= sealevel Then
            tempZ = mvarZ(i, j) + randn * altdev
            If tempZ > 32767 Then tempZ = 32767
            mvarZ(i, j) = tempZ
          End If
          
          If mvarZ(i, j) < 0 Then
            mvarZ(i, j) = 0
          End If
          'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
          If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
          If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
        Next j
      
      ElseIf thisRowLo < 0 Then
        For j = stepsize / 2 To newCols Step stepsize
          thisColLo = j - stepsize / 2
          thisColHi = j + stepsize / 2
          If thisColLo >= 0 And thisColHi <= newCols Then
            ' five-way across top edge
            mvarZ(i, j) = Int((CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, thisColLo)) + _
              CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 5)
          ElseIf thisColLo < 0 Then
            If NoWrap = False Then
              'five-way across top left corner
              mvarZ(i, j) = Int((CLng(mvarZ(i, newCols)) + CLng(mvarZ(i, thisColHi)) + CLng(mvarZ(thisRowHi, newCols)) + _
                CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 5)
            Else
              'five-way across top left corner
              mvarZ(i, j) = Int((CLng(mvarZ(i, thisColHi)) + _
                CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, thisColHi))) / 3)
            End If
          Else
            If NoWrap = False Then
              ' five-way across top right corner
              mvarZ(i, j) = Int((CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, 0)) + CLng(mvarZ(thisRowHi, thisColLo)) + _
                CLng(mvarZ(thisRowHi, j)) + CLng(mvarZ(thisRowHi, 0))) / 5)
            Else
              ' five-way across top right corner
              mvarZ(i, j) = Int((CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(thisRowHi, thisColLo)) + _
                CLng(mvarZ(thisRowHi, j))) / 3)
            End If
          End If
        
          ' introduce a random element
          If mvarZ(i, j) >= sealevel Then
            tempZ = mvarZ(i, j) + randn * altdev
            If tempZ > 32767 Then tempZ = 32767
            mvarZ(i, j) = tempZ
          End If
          
          
          If mvarZ(i, j) < 0 Then
            mvarZ(i, j) = 0
          End If
          'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
          If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
          If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
        Next j
      
      Else
        For j = stepsize / 2 To newCols Step stepsize
          thisColLo = j - stepsize / 2
          thisColHi = j + stepsize / 2
          If thisColLo >= 0 And thisColHi <= newCols Then
            ' five-way across bottom edge
            mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, thisColLo)) + CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
              CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, thisColHi))) / 5)
          ElseIf thisColLo < 0 Then
            If NoWrap = False Then
              ' five-way across bottem left corner
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, newCols)) + CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                CLng(mvarZ(i, newCols)) + CLng(mvarZ(i, thisColHi))) / 5)
            Else
              ' five-way across bottem left corner
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, thisColHi)) + _
                CLng(mvarZ(i, thisColHi))) / 3)
            End If
          Else
            If NoWrap = False Then
              'five-way across bottom right corner
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, thisColLo)) + CLng(mvarZ(thisRowLo, j)) + CLng(mvarZ(thisRowLo, 0)) + _
                CLng(mvarZ(i, thisColLo)) + CLng(mvarZ(i, 0))) / 5)
            Else
              'five-way across bottom right corner
              mvarZ(i, j) = Int((CLng(mvarZ(thisRowLo, thisColLo)) + CLng(mvarZ(thisRowLo, j)) + _
                CLng(mvarZ(i, thisColLo))) / 3)
            End If
          End If
          
        
          ' introduce a random element
          If mvarZ(i, j) >= sealevel Then
            tempZ = mvarZ(i, j) + randn * altdev
            If tempZ > 32767 Then tempZ = 32767
            mvarZ(i, j) = tempZ
          End If
          If mvarZ(i, j) < 0 Then
            mvarZ(i, j) = 0
          End If
          'If mvarZ(i, j) > MAXMOUNTAINHEIGHT * 100# Then mvarZ(i, j) = MAXMOUNTAINHEIGHT * 100#
          If mvarZ(i, j) < minZ Then minZ = mvarZ(i, j)
          If mvarZ(i, j) > minZ Then minZ = mvarZ(i, j)
        Next j
      End If
    
    Next i
    
    
    
    stepsize = stepsize / 2 ' do it at the end because we need to skip the points we've already got
    
    frmTec.ProgressBar1.Value = n
    DoEvents
  Next n
  
  
  frmTec.StatusBar1.Panels(4).Text = "Max elevation = " & Format(maxZ - (mvarZCoast * altdev), "##,##0") & " ft"
  
  
End Sub

Public Sub DrawHiRes()
  
  Dim i As Long
  Dim j As Long
  Dim MaxAlt As Long
  For i = 0 To UBound(mvarZ, 1)
    For j = 0 To UBound(mvarZ, 2)
      If mvarZ(i, j) > MaxAlt Then MaxAlt = mvarZ(i, j)
    Next j
  Next i
  
  frmTec.StatusBar1.Panels(4).Text = "Max elevation = " & Format(MaxAlt, "##,##0") & " ft"
  'frmTec.lblMinVal.Caption = "0 ft"
  'frmTec.lblMaxVal.Caption = "34,000 ft"
  
  draw_ DRAW_TEC, mvarZ
End Sub


'Private Sub assign_colors()
'  Dim i As Long
'
'  FillX11ColorTable
'
'  color_values(0) = &H0 'black
'  color_values(1) = &HFFFFFF 'white
'  color_values(2) = &HBFBFBF 'grey75
'  color_values(3) = &H7F7F7F 'grey50
'  color_values(4) = &HAB82FF 'MediumPurple1
'  color_values(5) = &H551A8B 'purple4
'  color_values(6) = &H7D26CD 'purple3
'  color_values(7) = &H912CEE 'purple2
'  color_values(8) = &H9B30FF 'purple1
'  color_values(9) = RGB(0, 0, &H8B) 'blue4
'  color_values(10) = RGB(0, 0, &HCD) 'blue3
'  color_values(11) = RGB(0, 0, &HEE) 'blue2
'  color_values(12) = RGB(0, 0, &HFF)     'blue1
'  color_values(13) = RGB(0, &H64, 0) 'DarkGreen
'  color_values(14) = RGB(0, &H8B, 0) 'green4
'  color_values(15) = RGB(0, &HCD, 0) 'green3
'  color_values(16) = RGB(0, &HEE, 0) 'green2
'  color_values(17) = RGB(0, &HFF, 0) 'green1
'  color_values(18) = &H8B6508 'DarkGoldenrod4
'  color_values(19) = &H8B8B00 'yellow4
'  color_values(20) = &HCDCD00 'yellow3
'  color_values(21) = &HEEEE00 'yellow2
'  color_values(22) = &HFFFF00 'yellow1
'  color_values(23) = &H8B5A00 'orange4
'  color_values(24) = &HCD8500 'orange3
'  color_values(25) = &HEE9A00 'orange2
'  color_values(26) = &HFFA500 'orange1
'  color_values(27) = &H8B2323 'brown4
'  color_values(28) = &H8B0000 'red4
'  color_values(29) = &HCD0000 'red3
'  color_values(30) = &HEE0000 'red2
'  color_values(31) = &HFF0000 'red1
'
'  climcols(0) = color_values(9)
'  climcols(1) = color_values(20)
'  climcols(2) = color_values(18)
'  climcols(3) = color_values(2)
'  climcols(4) = color_values(3)
'  climcols(5) = color_values(18)
'  climcols(6) = color_values(22)
'  climcols(7) = color_values(26)
'  climcols(8) = color_values(14)
'  climcols(9) = color_values(17)
'  climcols(10) = color_values(12)
'
'  landcols(0) = color_values(11)
'  landcols(1) = color_values(28)
'  landcols(2) = color_values(3)
'
'
'  For i = 0 To 255
'    'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
'    greycols(i) = RGB(i, i, i)
'  Next i
'
'  For i = 1 To MAXPLATE
'    platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
'  Next i
'
'  ReDim tecols(MAXMOUNTAINHEIGHT) As Long
'  ' deep ocean
'  For i = 0 To mvarZShelf
'    tecols(i) = RGB(0, 0, 139) ' dark blue
'  Next i
'
'  ' continental shelf
'  For i = mvarZShelf To mvarZCoast
'    tecols(i) = RGB(128, 255, 255) ' light blue
'  Next i
'
'  ' Greens for land
'  For i = mvarZCoast To mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255)
'    'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
'    If i >= UBound(tecols) Then Exit For
'    tecols(i) = RGB(0, i * 2 + 95, 0)
'  Next i
'
'  ' Brown for above tree line
'  For i = mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255)
'    If i >= UBound(tecols) Then Exit For
'    tecols(i) = RGB(128, i, i)
'  Next i
'
'  ' White for above snow line
'  For i = mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
'    If i >= UBound(tecols) Then Exit For
'    'tecols(i) = color_values(1)
'    tecols(i) = RGB(150 + i / 2, 150 + i / 2, 150 + i / 2)
'  Next i
'
'  For i = mvarZCoast + 200 To UBound(tecols)
'    If i >= UBound(tecols) Then Exit For
'    'tecols(i) = color_values(1)
'    tecols(i) = RGB(255, 255, 255)
'  Next i
'
'
'
'End Sub





Private Sub draw_(ctype As DrawType_E, cra() As Integer)
  
  Dim i As Long, j As Long, k As Long, X As Long
  Dim lut() As Long
  Dim XSize As Long, YSize As Long
  Dim clrinx As Long
  
  XSize = UBound(cra, 1)
  YSize = UBound(cra, 2)

  Debug.Print CStr(XSize) & " x " & CStr(YSize)

  assign_colors mvarZShelf, mvarZCoast
  Select Case ctype
    Case DRAW_GREY
      lut = greycols
    Case DRAW_LAND
      lut = landcols
    Case DRAW_CLIM
      lut = climcols
    Case DRAW_TEC
      lut = tecols
    Case DRAW_PLATE
      lut = platecols
    Case DRAW_JET
      lut = heatcols
  End Select
  
  frmTec.ProgressBar1.Value = 0
  frmTec.ProgressBar1.max = YSize
  For j = 0 To YSize
    For i = 0 To XSize - 1
      clrinx = cra(i, j) / (MAXMOUNTAINHEIGHT * 100# / 255)
      X = CLng(lut(clrinx))
      k = i + 1
      
      Do While ((CLng(lut(cra(k, j) / (MAXMOUNTAINHEIGHT * 100# / 255))) = X) And (k < XSize - 1))
        k = k + 1
      Loop
      
      frmTec.Picture1.Line _
        (i * SIZE * frmTec.Picture1.ScaleWidth / XSize, _
        j * SIZE * frmTec.Picture1.ScaleHeight / YSize _
        )-Step((k - i) * SIZE * frmTec.Picture1.ScaleWidth / XSize, _
        SIZE * frmTec.Picture1.ScaleHeight / YSize), _
        X, BF
      
      i = k - 1
      
    Next i
  
    frmTec.ProgressBar1.Value = j
    DoEvents
  Next j
  

  DrawGrid
  frmTec.DrawColorBar lut
End Sub


Sub LEFT(i, j, X, Optional ByVal col = 0)
  setXYWinScale
  If col = PR_LOW Then col = 0
  If col = PR_HIGH Then col = RGB(255, 255, 255)
  If col = PR_HEQ Then col = 0
  frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale)-(i * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
End Sub
   
Sub ABOVE(i, j, X, Optional ByVal col = 0)
  setXYWinScale
  If col = PR_LOW Then col = 0
  If col = PR_HIGH Then col = RGB(255, 255, 255)
  If col = PR_HEQ Then col = 0
  frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale)-((i + 1) * SIZE * XWinScale, j * SIZE * YWinScale), col
End Sub
   
Sub UPLEFT(i, j, X, Optional ByVal col = 0)
  setXYWinScale
  If col = PR_LOW Then col = 0
  If col = PR_HIGH Then col = RGB(255, 255, 255)
  If col = PR_HEQ Then col = 0
  frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale)-((i + 1) * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
End Sub
   
Sub UPRIGHT(i, j, X, Optional ByVal col = 0)
  setXYWinScale
  If col = PR_LOW Then col = 0
  If col = PR_HIGH Then col = RGB(255, 255, 255)
  If col = PR_HEQ Then col = 0
  frmTec.Picture1.Line ((i + 1) * SIZE * XWinScale, j * SIZE * YWinScale)-(i * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
End Sub

Private Sub setXYWinScale()
  XWinScale = frmTec.Picture1.ScaleWidth / UBound(mvarZ, 1)
  YWinScale = frmTec.Picture1.ScaleHeight / UBound(mvarZ, 2)
End Sub

Private Sub Class_Initialize()
  mvarZShelf = 8
  mvarZCoast = 16
End Sub
