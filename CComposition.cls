VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CComposition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit

Private mvarCarbon As Single 'local copy
Private mvarHydrogen As Single 'local copy
Private mvarFayalite As Single 'local copy
Private mvarNickel As Single 'local copy
Private mvarIron As Single 'local copy
Private mvarIce As Single 'local copy
Private mvarMethane As Single 'local copy
Private mvarNickelIron As Single 'local copy
Private mvarNickelIronRatio As Single 'local copy

Public Property Let NickelIronRatio(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NickelIronRatio = 5
    mvarNickelIronRatio = vData
End Property


Public Property Get NickelIronRatio() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NickelIronRatio
    NickelIronRatio = mvarNickelIronRatio
End Property



Public Property Let NickelIron(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NickelIron = 5
    mvarNickelIron = vData
End Property


Public Property Get NickelIron() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NickelIron
    NickelIron = mvarNickelIron
End Property





Public Property Let Methane(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Methane = 5
    mvarMethane = vData
End Property


Public Property Get Methane() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Methane
    Methane = mvarMethane
End Property



Public Property Let Ice(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Ice = 5
    mvarIce = vData
End Property


Public Property Get Ice() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Ice
    Ice = mvarIce
End Property



Public Property Let Iron(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Iron = 5
    mvarIron = vData
End Property


Public Property Get Iron() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Iron
    Iron = mvarIron
End Property



Public Property Let Nickel(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Nickel = 5
    mvarNickel = vData
End Property


Public Property Get Nickel() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Nickel
    Nickel = mvarNickel
End Property



Public Property Let Fayalite(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Fayalite = 5
    mvarFayalite = vData
End Property


Public Property Get Fayalite() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Fayalite
    Fayalite = mvarFayalite
End Property



Public Property Let Hydrogen(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Hydrogen = 5
    mvarHydrogen = vData
End Property


Public Property Get Hydrogen() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Hydrogen
    Hydrogen = mvarHydrogen
End Property



Public Property Let Carbon(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Carbon = 5
    mvarCarbon = vData
End Property


Public Property Get Carbon() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Carbon
    Carbon = mvarCarbon
End Property



Public Function CalculateMass(RadiusM As Double) As Double
  
  CalculateMass = (mvarCarbon * CDensityKgM3 + mvarFayalite * FayaliteDensityKgM3 + _
    mvarHydrogen * HDensityKgM3 + mvarIce * IceDensityKgM3 + mvarIron * FeDensityKgM3 + _
    mvarMethane * (CDensityKgM3 + 4 * HDensityKgM3) + mvarNickel * NiDensityKgM3 + _
    mvarNickelIron * (NiDensityKgM3 * mvarNickelIronRatio + FeDensityKgM3 * (100 - mvarNickelIronRatio))) _
    * 4 / 3 * PI * RadiusM ^ 3
End Function

