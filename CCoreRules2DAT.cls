VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCoreRules2DAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

' Opens Core Rules 2.0 Expansion user database files


Private Type CEncounterOb
  bytFiller As Byte
End Type

Private Type CEncounterMonsterPackageOb
  bytFiller As Byte
End Type

Private Type CEncounterMonHpOb
  bytFiller As Byte
End Type


' EncPlat
Private Type CEncTemplate
  bytFiller As Byte
End Type

Private Type CEncTemplateMonsterPropertiesOb
  bytFiller As Byte
End Type

Private Type CMonsterTypeOb
  ' 01 80 is the start of a new record
  bytNameLen As Byte
  strMonsterName As String
  bytFileListNameLen As Byte
  strFileListName As String
  ' empty integer
  bytImageListIndex As Byte
  ' 00 00 00 00 00 00 00 07
  bytFileNameLen As Byte
  strFileName As String
End Type


Private Type CMonsterTypeObU
  ' 01 80 is the start of a new record
  bytNameLen As Byte
  strMonsterName As String
  bytImageListIndex As Byte ' should be 00
  bytFileNameLen As Byte
  strFileName As String
  bytPathLen As Byte
  strPathName As String
  bytLongFileNameLen As Byte
  strLongFileName As String
End Type

Private Type CMonsterOb
  ' Separated by 64 00 00 00 64 00 00 00 64 00 00 00 64 00 00 00
  bytFiller As Byte
End Type

Private Type CClassOb
  bytFiller As Byte
End Type

Private Type CSpellPointsOb
  bytFiller As Byte
End Type

Private Type CClassesAllowedOb
  bytFiller As Byte
End Type

Private Type CPartKitBonusOb
  bytFiller As Byte
End Type

Private Type CClassesAllowedHelperOb
  bytFiller As Byte
End Type


Private Type CEncounterFreqTableOb
  bytFiller As Byte
End Type

Private Type CPart
  bytFiller As Byte
End Type

Private Type CPsionicPowerOb
  bytFiller As Byte
End Type

Private Type CRaceOb
  bytFiller As Byte
End Type

Private Type CSpellsOb
  bytFiller As Byte
End Type

Private Type CTrapOb
  bytFiller As Byte
End Type

Private Type CTrasureOb
  bytFiller As Byte
End Type

Private Type CTreasureSingleTableOb
  bytFiller As Byte
End Type

Private Type CTreasureTableEntry
  bytFiller As Byte
End Type

Private Type CMonsterAttackOb
  bytFiller As Byte
End Type

Private Type CPhraseSpacingOb
  bytFiller As Byte
End Type


