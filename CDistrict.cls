VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDistrict"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarstrType As String 'local copy
Private mvarBuildings As CBuildings 'local copy
Private mvarNPCs As CNPCs 'local copy
Private mvarstrDescription As String 'local copy
Private mvarstrFirstImpression As String 'local copy
Private mvarstrSocialClass As String 'local copy

Public Property Let strSocialClass(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSocialClass = 5
    mvarstrSocialClass = vData
End Property


Public Property Get strSocialClass() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSocialClass
    strSocialClass = mvarstrSocialClass
End Property



Public Property Let strFirstImpression(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strFirstImpression = 5
    mvarstrFirstImpression = vData
End Property


Public Property Get strFirstImpression() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strFirstImpression
    strFirstImpression = mvarstrFirstImpression
End Property



Public Property Let strDescription(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strDescription = 5
    mvarstrDescription = vData
End Property


Public Property Get strDescription() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strDescription
    strDescription = mvarstrDescription
End Property




Public Property Set NPCs(ByVal vData As CNPCs)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.NPCs = Form1
    Set mvarNPCs = vData
End Property


Public Property Get NPCs() As CNPCs
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NPCs
    Set NPCs = mvarNPCs
End Property



Public Property Set Buildings(ByVal vData As CBuildings)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Buildings = Form1
    Set mvarBuildings = vData
End Property


Public Property Get Buildings() As CBuildings
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Buildings
    Set Buildings = mvarBuildings
End Property



Public Property Let strType(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strType = 5
    mvarstrType = vData
End Property


Public Property Get strType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strType
    strType = mvarstrType
End Property



Private Sub Class_Initialize()
  Set Buildings = New CBuildings
  Set NPCs = New CNPCs
End Sub
