VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarlngValue As Long
Private mvarstrGemType As String

Public Property Let strGemType(ByVal vData As String)
  mvarstrGemType = vData
End Property


Public Property Get strGemType() As String
  strGemType = mvarstrGemType
End Property



Public Property Let lngValue(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngValue = 5
    mvarlngValue = vData
End Property


Public Property Get lngValue() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngValue
    lngValue = mvarlngValue
End Property



Public Sub RollArt()
  Dim lngRoll As Long
  Dim ddb As New CDB
  Dim bytRoll As Byte
  Dim strResult As String
  
  ddb.OpenDB "SELECT * FROM TreasureArt ORDER BY MinChance"
  
  bytRoll = d100
  strResult = ""
  If ddb.rs.State > 0 Then
    If ddb.rs.RecordCount > 0 Then
      ddb.rs.MoveFirst
      Do
        ddb.rs.MoveNext
        If ddb.rs.EOF = True Then Exit Do
      Loop Until ddb.rs!MinChance >= bytRoll
      ddb.rs.MovePrevious
      If ddb.rs.EOF = True Then ddb.rs.MoveLast
      
      mvarlngValue = dN(ddb.rs!ValNDice, ddb.rs!ValSides) * ddb.rs!ValMult
      mvarstrGemType = ddb.rs!Examples
      
    End If
    ddb.CloseDB
  End If
End Sub


Public Sub RollGem()
  Dim lngRoll As Long
  Dim ddb As New CDB
  Dim bytRoll As Byte
  Dim strResult As String
  Dim i As Long
  Dim strParsedNames() As String
  Dim strTemp As String
  
  ddb.OpenDB "SELECT * FROM TreasureGems ORDER BY MinChance"
  
  bytRoll = d100
  strResult = ""
  If ddb.rs.State > 0 Then
    If ddb.rs.RecordCount > 0 Then
      ddb.rs.MoveFirst
      Do
        ddb.rs.MoveNext
        If ddb.rs.EOF = True Then Exit Do
      Loop Until ddb.rs!MinChance >= bytRoll
      ddb.rs.MovePrevious
      If ddb.rs.EOF = True Then ddb.rs.MoveLast
      
      mvarlngValue = dN(ddb.rs!ValNDice, ddb.rs!ValSides) * ddb.rs!ValMult
      strTemp = ddb.rs!Examples
      
      strParsedNames = Split(strTemp, ";")
      i = Int(Rnd(UBound(strParsedNames)))
      mvarstrGemType = Trim(strParsedNames(i))
    End If
    ddb.CloseDB
  End If
End Sub
