VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGlobe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' This program is Copyright (c) 1991 David Allen.  It may be freely _
distributed as long as you leave my name and copyright notice on it. _
 I'd really like your comments and feedback; send e-mail to _
allen@viewlogic.com, or send us-mail to David Allen, 10 O'Moore _
Avenue, Maynard, MA 01754.

' This file contains a spinning globe animation

Private Sub SET_(n, x, y, i, j)
  m(n, (x) + n, (y) + n) = m(0, (i) + n, (j) + n)
End Sub


Dim m(21, MAXX, MAXY) As Byte

int rx() = {0, 0, 0, 0, 0}, ry() = {0, N, 0, N, 0};
int xofs, prime, xflip, sflip, sxflip, change(1), step = 0;
int MAXSTEP = 10000, PRINTMODE = PRINTMODE_SCALE, XSIZE = MAXX, YSIZE = MAXX;
int ZCOAST = 0;


Private Sub RunModel(numsteps As Long)
  Dim i As Long
  Static step As Long
  getparams s
  xofs = 45
  For i = 1 To 6
    xofs = xofs - 10
    prime = 16 - i
    xflip = i + 5
    sflip = 15 + i
    sxflip = 6 - i
    compute
  Next i
  
  For i = 1 To numsteps
    draw DRAW_TEC, LINE_NONE, m(i), 0
    i = i + 1
    step = step + 1
    If (i = 5) Then i = 6
    If (i = 16) Then i = 17
    If (i = 21) Then i = 1
  Next i
  
End Sub


Private Function mainpar(s As String) As Long
{
  int vec (AREA), index = AREA;  int i, j, k;
  
  if (CMP ("LAND")) {
    getlvec (&index, vec, 0);
    for (k=0, j=0; j<MAXX; j++) for (i=0; i<MAXX; i++, k++)
      m(0,i,j) = (vec(k) > 16) ? vec(k) : 16;
    return (1);
  }
  return (0);
End Function


Private Sub compute()
   int i, j, top; int x, y;
  
  rx(0) = xofs + N; rx(4) = rx(0); rx(2) = xofs - N;
  for (j=0; j<RADIUS; j++) {
    checkmouse ();
    top = (int)(sqrt ((double) (RADIUS * RADIUS - j * j + 0.5)));
    for (i=1-top; i<top; i++) {
      point (i, j, &x, &y);
      SET_ (prime,   i,  j,  x,  y);
      SET_ (prime,   i, -j,  x, -y);
      SET_ (xflip,  -i,  j, -x,  y);
      SET_ (xflip,  -i, -j, -x, -y);
      permute (x, y, &x, &y);
      SET_ (sxflip,  i, -j, -x,  y);
      SET_ (sxflip,  i,  j, -x, -y);
      SET_ (sflip,  -i, -j,  x,  y);
      SET_ (sflip,  -i,  j,  x, -y);
    }
  }
End Sub


Private Sub point(x As Long, y As Long, ByRef i As Long, ByRef j As Long)
  double r, theta, topx, topy, botx, boty; int bot, top;
  
  r = asin (sqrt ((double)(x * x + y * y)) / RADIUS) * 2.0 / PI;
  if (x  or  y) theta = atan2 ((double)y, (double)x);
  else theta = 0;
  if (theta <= 0) theta += TWOPI;
  
  bot = 0
  Do While (theta > HALFPI)
    bot = bot + 1
    theta = theta - HALFPI
  Loop
  
  top = bot + 1; theta = theta / HALFPI;
  mapr (bot, r, &botx, &boty); mapr (top, r, &topx, &topy);
  *i = (int)(botx + theta * (topx - botx) + 0.5);
  *j = (int)(boty + theta * (topy - boty) + 0.5);
  if (*i >= N) *i = N-1; if (*j >= N) *j = N-1;
End Sub


Private Sub mapr(n As Long, r As Double, ByRef x As Double, ByRef y As Double)
  x = r * (rx(n) - xofs) + xofs
  y = r * ry(n)
  If (x >= n) Then
    y = x - n
    x = n
    If (n = 4) Then y = -(y)
  End If
End Sub


Private Sub permute(x As Long, y As Long, ByRef i As Long, ByRef j As Long)
  If ((x >= 0) And (y >= 0)) Then
    i = n - y - 1
    j = x - n + 0
  End If
  If ((x < 0) And (y >= 0)) Then
    i = y - n + 0
    j = -n - x - 1
  End If
  If ((x >= 0) And (y < 0)) Then
    i = y + n + 0
    j = n - x - 1
  End If
  If ((x < 0) And (y < 0)) Then
    i = -n - y - 1
    j = x + n + 0
  End If
End Sub

Private Sub Class_Initialize()
  rx = Array(0, 0, 0, 0, 0)
  ry = Array(0, n, 0, n, 0)
  step = 0
  MaxStep = 10000
  PrintMode = PRINTMODE_SCALE
  XSize = MAXX
  YSize = MAXX
  ZCoast = 0

End Sub
