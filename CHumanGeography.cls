VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHumanGeography"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarPopDen() As Single
Private mvarClimate() As Byte
Private mvarElevation() As Integer
Private mvarTemperature() As Integer
Private mvarHydroPct As Byte
Private mvarXSize As Long
Private mvarYSize As Long
Private mvarMapScale As Long




Private Const CITYPOPDEN = 38400

Private mvarCities As CCities 'local copy
Private mvarsngLandArea As Single 'local copy
Private mvarsngKingdomAge As Single 'local copy
Private mvarsngAveragePopulationDensity As Single 'local copy

Public Property Let sngAveragePopulationDensity(ByVal vData As Single)
    mvarsngAveragePopulationDensity = vData
End Property
Public Property Get sngAveragePopulationDensity() As Single
    sngAveragePopulationDensity = mvarsngAveragePopulationDensity
End Property



Public Property Let sngKingdomAge(ByVal vData As Single)
    mvarsngKingdomAge = vData
End Property
Public Property Get sngKingdomAge() As Single
    sngKingdomAge = mvarsngKingdomAge
End Property




Public Property Let sngLandArea(ByVal vData As Single)
    mvarsngLandArea = vData
End Property
Public Property Get sngLandArea() As Single
    sngLandArea = mvarsngLandArea
End Property



Public Property Set Cities(ByVal vData As CCities)
    Set mvarCities = vData
End Property
Public Property Get Cities() As CCities
    Set Cities = mvarCities
End Property




Public Property Let YSize(ByVal vData As Long)
    mvarYSize = vData
End Property
Public Property Get YSize() As Long
    YSize = mvarYSize
End Property



Public Property Let XSize(ByVal vData As Long)
    mvarXSize = vData
End Property
Public Property Get XSize() As Long
    XSize = mvarXSize
End Property




Public Property Let MapScale(ByVal vData As Long)
  mvarMapScale = vData
End Property
Public Property Get MapScale() As Long
  MapScale = mvarMapScale
End Property

Public Property Let HydroPct(ByVal vData As Long)
    mvarHydroPct = vData
End Property
Public Property Get HydroPct() As Long
    HydroPct = mvarHydroPct
End Property

Public Sub SetClimate(vData() As Byte)
  mvarClimate = vData
End Sub
Public Function GetClimate() As Byte()
  GetClimate = mvarClimate
End Function

Public Sub SetElevation(vData() As Integer)
  mvarElevation = vData
End Sub
Public Function GetElevation() As Integer()
  GetElevation = mvarElevation
End Function

Public Sub SetPopDen(vData() As Single)
  mvarPopDen = vData
End Sub
Public Function GetPopDen() As Single()
  GetPopDen = mvarPopDen
End Function


Public Sub SetTemperature(vData() As Integer)
  mvarTemperature = vData
End Sub
Public Function GetTemperature() As Integer()
  GetTemperature = mvarTemperature
End Function




Public Sub DrawPopulationMap()
  DrawSingle_ DRAW_POP, mvarPopDen
End Sub

Private Sub DrawSingle_(ctype As DrawType_E, cra() As Single)
  
  Dim i As Long, j As Long, k As Long, X As Long
  Dim lut() As Long

  assign_colors 16, 32 'mvarZShelf, mvarZCoast
  Select Case ctype
    Case DRAW_GREY
      lut = greycols
    Case DRAW_LAND
      lut = landcols
    Case DRAW_CLIM
      lut = climcols
    Case DRAW_TEC
      lut = tecols
    Case DRAW_PLATE
      lut = platecols
    Case DRAW_JET
      lut = heatcols
    Case DRAW_TRUE
      lut = truecols
    Case DRAW_POP
      lut = popcols
  End Select
  
  'lut = greycols
  
  'frmTec.Picture1.Cls
  'frmTec.Picture1.Line (0, 0)-(frmTec.Picture1.ScaleWidth, frmTec.Picture1.ScaleHeight), RGB(0, 0, 255), BF
  
  For j = 0 To UBound(cra, 2)
    For i = 0 To UBound(cra, 1) - 1
    
      ' For purposes of drawing, let any population density > 255 people per square mile use 255.
      If cra(i, j) > 255 Then cra(i, j) = 255
      
      X = lut(CLng(cra(i, j)))
      k = i + 1
      
      Do While ((lut(cra(k, j)) = X) And (k < UBound(cra, 1) - 1))
        k = k + 1
      Loop
      
      If mvarClimate(i, j) <> C_OCEAN And mvarClimate(i, j) <> C_OCEANICE Then
        frmTec.Picture1.Line _
          (i * SIZE * frmTec.Picture1.ScaleWidth / UBound(cra, 1), _
          j * SIZE * frmTec.Picture1.ScaleHeight / UBound(cra, 2) _
          )-Step((k - i) * SIZE * frmTec.Picture1.ScaleWidth / UBound(cra, 1), _
          SIZE * frmTec.Picture1.ScaleHeight / UBound(cra, 2)), _
          X, BF
      Else
        frmTec.Picture1.Line _
          (i * SIZE * frmTec.Picture1.ScaleWidth / UBound(cra, 1), _
          j * SIZE * frmTec.Picture1.ScaleHeight / UBound(cra, 2) _
          )-Step((k - i) * SIZE * frmTec.Picture1.ScaleWidth / UBound(cra, 1), _
          SIZE * frmTec.Picture1.ScaleHeight / UBound(cra, 2)), _
          RGB(0, 0, &H8B), BF
      End If
      i = k - 1
      
    Next i
  Next j
  
  'DrawGrid
  frmTec.DrawColorBar lut
End Sub





Public Sub Generate()
    Dim sngKingdomPopulation As Single
    Dim sngVillagePopulation As Single
    Dim sngNumVillages As Single
    Dim sngTownPopulation As Single
    Dim sngNumTowns As Single
    Dim sngWanderingPopulation As Single
    Dim sngTotalCityPopulation As Single
    Dim sqpop As Single
    Dim sngKingdomLargestCityPop As Single
    Dim sngKingdom2ndLargestCityPop As Single
    Dim sngKingdom3rdLargestCityPop As Single
    Dim sngNumCities As Single
    Dim sngFarmlandPercent As Single
    Dim sngDistanceBetweenVillages As Single
    Dim sngDistanceBetweenTowns As Single
    Dim sngDistanceBetweenCities As Single
    Dim sngRuinedCastles As Single
    Dim sngActiveCastles As Single
    Dim sngFarmlandArea As Single
    Dim lngRemainingPopulation As Long
    Dim lngCityPopulation As Long
    
  
    Set mvarCities = New CCities
  
    
    If mvarsngLandArea < 1 Then
      MsgBox "Kingdom Area must be greater than 0."
      Exit Sub
    End If

    If mvarsngAveragePopulationDensity < 0 Then
      MsgBox "Population Density must be greater than 0"
      Exit Sub
    End If
    
    If mvarsngAveragePopulationDensity > 1000 Then
      MsgBox "Population Density must be less than 1001"
      Exit Sub
    End If
    
    If mvarsngKingdomAge < 1 Then
      MsgBox "Kingdom Age must be greater than 0."
      Exit Sub
    End If
  
    sngKingdomPopulation = FormatNumber(mvarsngLandArea * mvarsngAveragePopulationDensity, 0)
    sngVillagePopulation = FormatNumber(sngKingdomPopulation * 0.89, 0)
    sngNumVillages = FormatNumber(sngVillagePopulation / 700, 0)
    sngTownPopulation = FormatNumber(sngKingdomPopulation * 0.06, 0)
    sngNumTowns = FormatNumber(sngTownPopulation / 5000, 0)
    sngWanderingPopulation = FormatNumber(sngKingdomPopulation * 0.02, 0)
    sngTotalCityPopulation = FormatNumber(sngKingdomPopulation * 0.03, 0)
    sqpop = sngKingdomPopulation ^ 0.5
    sngKingdomLargestCityPop = FormatNumber(sqpop * (Rnd * 4 + Rnd * 4 + 10), 0)
    sngKingdom2ndLargestCityPop = FormatNumber((0.25 + Rnd * 0.5) * sngKingdomLargestCityPop, 0)
    sngKingdom3rdLargestCityPop = FormatNumber((Rnd * 0.1 + 0.8) * sngKingdom2ndLargestCityPop, 0)
    
    sngNumCities = 0
    lngRemainingPopulation = (sngTotalCityPopulation - sngKingdomLargestCityPop - sngKingdom2ndLargestCityPop - sngKingdom3rdLargestCityPop)
    lngCityPopulation = sngKingdom3rdLargestCityPop
    
    mvarCities.Add
    mvarCities(mvarCities.Count).population = sngKingdomLargestCityPop
    mvarCities.Add
    mvarCities(mvarCities.Count).population = sngKingdom2ndLargestCityPop
    mvarCities.Add
    mvarCities(mvarCities.Count).population = sngKingdom3rdLargestCityPop
    
    
    Do Until lngCityPopulation < 11000 Or lngRemainingPopulation < 12000
      sngNumCities = sngNumCities + 1
      lngCityPopulation = (1 - (Rnd * 0.3 + 0.1)) * lngCityPopulation
      
      mvarCities.Add
      mvarCities(mvarCities.Count).population = lngCityPopulation
      
      lngRemainingPopulation = lngRemainingPopulation - lngCityPopulation
    Loop
    
    If sngNumCities < 0 Then sngNumCities = 0
    sngFarmlandArea = FormatNumber((mvarsngLandArea * mvarsngAveragePopulationDensity) / 180, 0)  ' area
    sngFarmlandPercent = FormatNumber((sngFarmlandArea / mvarsngLandArea) * 100, 0) ' percentage
    
    
    sngNumTowns = 0
    lngRemainingPopulation = sngTownPopulation
    Do Until lngRemainingPopulation < 1700
      sngNumTowns = sngNumTowns + 1
      lngCityPopulation = -1
      Do Until lngCityPopulation > 0
        lngCityPopulation = 1000 + (Rnd * 7000)
      Loop
      
      mvarCities.Add
      mvarCities(mvarCities.Count).population = lngCityPopulation
      
      lngRemainingPopulation = lngRemainingPopulation - lngCityPopulation
    Loop
   
    sngNumVillages = 0
    lngRemainingPopulation = sngVillagePopulation
    Do Until lngRemainingPopulation < 20
      sngNumVillages = sngNumVillages + 1
      lngCityPopulation = Rnd * 980 + 20
      
      mvarCities.Add
      mvarCities(mvarCities.Count).population = lngCityPopulation
      
      lngRemainingPopulation = lngRemainingPopulation - lngCityPopulation
    Loop
   
    ' Use farm area for towns and villages since they will depend on a great city.
    If sngNumVillages > 0 Then
      sngDistanceBetweenVillages = FormatNumber(2 * (sngFarmlandArea / sngNumVillages) ^ 0.5, 1)
    End If
    If sngNumTowns > 0 Then
      sngDistanceBetweenTowns = FormatNumber(2 * (sngFarmlandArea / sngNumTowns) ^ 0.5, 1)
    End If
    
    ' Use total area for cities since they are independent.
    If sngNumCities > 0 Then
      sngDistanceBetweenCities = FormatNumber(2 * (mvarsngLandArea / (sngNumCities + 3)) ^ 0.5, 1)
    End If
    
    sngRuinedCastles = FormatNumber((sngKingdomPopulation / 5000000) * (mvarsngKingdomAge ^ 0.5), 0)
    sngActiveCastles = FormatNumber(sngKingdomPopulation / 50000, 0)
    
    
End Sub
