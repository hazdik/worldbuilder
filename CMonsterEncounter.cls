VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonsterEncounter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarstrName As String 'local copy
Private mvarData As CMonsterEncounterDataSet 'local copy
Private mvarsngEL As Single 'local copy

Public Property Let sngEL(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sngEL = 5
    mvarsngEL = vData
End Property


Public Property Get sngEL() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sngEL
    sngEL = mvarsngEL
End Property


Public Property Set Data(ByVal vData As CMonsterEncounterDataSet)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Data = Form1
    Set mvarData = vData
End Property


Public Property Get Data() As CMonsterEncounterDataSet
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Data
    Set Data = mvarData
End Property



Public Property Let strName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strName = 5
    mvarstrName = vData
End Property


Public Property Get strName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strName
    strName = mvarstrName
End Property


Public Sub RollEncounter()
  Dim db As New CDB
  Dim i As Long
  Dim lngNList() As Long, sngCRList() As Single
  
  Set mvarData = New CMonsterEncounterDataSet
  
  
  
  db.OpenDB "SELECT * FROM MonsterEncounterData WHERE EncounterName LIKE '" & _
    Apostrify(mvarstrName) & "' ORDER BY MonsterName"
  
  If db.rs.State > 0 Then
    If db.rs.RecordCount > 0 Then
      db.rs.MoveFirst
      Do While Not db.rs.EOF
        mvarData.Add
        mvarData(mvarData.Count).strMonsterName = db.rs!monstername
        mvarData(mvarData.Count).strMonsterTitle = db.rs!MonsterTitle
        mvarData(mvarData.Count).lngMinAppearing = db.rs!MinAppearing
        mvarData(mvarData.Count).lngMaxAppearing = db.rs!MaxAppearing
        If d100 <= db.rs!ChanceOfAppearing Then
          mvarData(mvarData.Count).lngActualAppearing = _
            ceil(Rnd * (db.rs!MaxAppearing - db.rs!MinAppearing) + db.rs!MinAppearing)
            
        Else
          mvarData(mvarData.Count).lngActualAppearing = 0
        End If
        
        db.rs.MoveNext
      Loop
    End If
    db.CloseDB
    
    ReDim lngNList(mvarData.Count) As Long
    ReDim sngCRList(mvarData.Count) As Single
    
    
    For i = 1 To mvarData.Count
      mvarData(i).RetrieveData mvarstrName
      lngNList(i) = mvarData(i).lngActualAppearing
      sngCRList(i) = mvarData(i).MonsterStatistics(1).sngCR
    Next i
    
    'calculate the EL
    mvarsngEL = GetEL(lngNList, sngCRList)
    
  End If
  
  
  
  
End Sub
