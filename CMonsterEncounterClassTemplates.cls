VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonsterEncTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarstrSubtype As String 'local copy
Private mvarstrTemplateName As String 'local copy
Private mvarlngTemplateStack As Long 'local copy
Private mvarstrClassName As String 'local copy
Private mvarlngMinClassLevel As Long 'local copy
Private mvarlngMaxClassLevel As Long 'local copy
Private mvarlngActualClassLevel As Long 'local copy
Public Property Let lngActualClassLevel(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngActualClassLevel = 5
    mvarlngActualClassLevel = vData
End Property


Public Property Get lngActualClassLevel() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngActualClassLevel
    lngActualClassLevel = mvarlngActualClassLevel
End Property



Public Property Let lngMaxClassLevel(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngMaxClassLevel = 5
    mvarlngMaxClassLevel = vData
End Property


Public Property Get lngMaxClassLevel() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngMaxClassLevel
    lngMaxClassLevel = mvarlngMaxClassLevel
End Property



Public Property Let lngMinClassLevel(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngMinClassLevel = 5
    mvarlngMinClassLevel = vData
End Property


Public Property Get lngMinClassLevel() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngMinClassLevel
    lngMinClassLevel = mvarlngMinClassLevel
End Property



Public Property Let strClassName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strClassName = 5
    mvarstrClassName = vData
End Property


Public Property Get strClassName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strClassName
    strClassName = mvarstrClassName
End Property



Public Property Let lngTemplateStack(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngTemplateStack = 5
    mvarlngTemplateStack = vData
End Property


Public Property Get lngTemplateStack() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTemplateStack
    lngTemplateStack = mvarlngTemplateStack
End Property



Public Property Let strTemplateName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strTemplateName = 5
    mvarstrTemplateName = vData
End Property


Public Property Get strTemplateName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strTemplateName
    strTemplateName = mvarstrTemplateName
End Property



Public Property Let strSubtype(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSubtype = 5
    mvarstrSubtype = vData
End Property


Public Property Get strSubtype() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSubtype
    strSubtype = mvarstrSubtype
End Property

Public Function GetCRBonus(lngHD As Long) As Single
  Dim ddb As New CDB
  If Len(mvarstrTemplateName) > 1 Then
    ddb.OpenDB "SELECT * FROM MonsterTemplates WHERE name LIKE '" & _
      Apostrify(mvarstrTemplateName) & "' AND MinHD >= " & CStr(lngHD) & _
      " AND MaxHD <= " & CStr(lngHD)
    If ddb.rs.State > 0 Then
      If ddb.rs.RecordCount > 0 Then
        ddb.rs.MoveFirst
        GetCRBonus = ddb.rs!CR * mvarlngTemplateStack
      End If
      ddb.rs.Close
    End If
  Else
    GetCRBonus = mvarlngActualClassLevel
  End If
End Function


