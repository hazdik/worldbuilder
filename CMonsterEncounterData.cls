VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonsterEncounterData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarstrMonsterName As String 'local copy
Private mvarstrMonsterTitle As String 'local copy
Private mvarlngMinAppearing As Long 'local copy
Private mvarlngMaxAppearing As Long 'local copy
Private mvarlngActualAppearing As Long 'local copy
Private mvarlngChanceOfAppearing As Long 'local copy
Private mvarlngNonCombatant As Long 'local copy
Private mvarlngExtraHD As Long 'local copy
Private mvarClassesTemplates As CMonsterEncTemplateSet 'local copy
Private mvarMonsterStatistics As CMonsterStats 'local copy



Public Property Set MonsterStatistics(ByVal vData As CMonsterStats)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.MonsterStatistics = Form1
    Set mvarMonsterStatistics = vData
End Property


Public Property Get MonsterStatistics() As CMonsterStats
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MonsterStatistics
    Set MonsterStatistics = mvarMonsterStatistics
End Property




Public Property Set ClassesTemplates(ByVal vData As CMonsterEncTemplateSet)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.ClassesTemplates = Form1
    Set mvarClassesTemplates = vData
End Property


Public Property Get ClassesTemplates() As CMonsterEncTemplateSet
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ClassesTemplates
    Set ClassesTemplates = mvarClassesTemplates
End Property



Public Property Let lngExtraHD(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngExtraHD = 5
    mvarlngExtraHD = vData
End Property


Public Property Get lngExtraHD() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngExtraHD
    lngExtraHD = mvarlngExtraHD
End Property



Public Property Let lngNonCombatant(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngNonCombatant = 5
    mvarlngNonCombatant = vData
End Property


Public Property Get lngNonCombatant() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngNonCombatant
    lngNonCombatant = mvarlngNonCombatant
End Property



Public Property Let lngChanceOfAppearing(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngChanceOfAppearing = 5
    mvarlngChanceOfAppearing = vData
End Property


Public Property Get lngChanceOfAppearing() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngChanceOfAppearing
    lngChanceOfAppearing = mvarlngChanceOfAppearing
End Property



Public Property Let lngActualAppearing(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngActualAppearing = 5
    mvarlngActualAppearing = vData
End Property


Public Property Get lngActualAppearing() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngActualAppearing
    lngActualAppearing = mvarlngActualAppearing
End Property



Public Property Let lngMaxAppearing(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngMaxAppearing = 5
    mvarlngMaxAppearing = vData
End Property


Public Property Get lngMaxAppearing() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngMaxAppearing
    lngMaxAppearing = mvarlngMaxAppearing
End Property



Public Property Let lngMinAppearing(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngMinAppearing = 5
    mvarlngMinAppearing = vData
End Property


Public Property Get lngMinAppearing() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngMinAppearing
    lngMinAppearing = mvarlngMinAppearing
End Property



Public Property Let strMonsterTitle(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strMonsterTitle = 5
    mvarstrMonsterTitle = vData
End Property


Public Property Get strMonsterTitle() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strMonsterTitle
    strMonsterTitle = mvarstrMonsterTitle
End Property



Public Property Let strMonsterName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strMonsterName = 5
    mvarstrMonsterName = vData
End Property


Public Property Get strMonsterName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strMonsterName
    strMonsterName = mvarstrMonsterName
End Property


Public Sub RetrieveData(strEncounterName As String)
  Dim db As New CDB
  Dim msdb As New CDB
  Dim sngECR As Single
  Dim i As Long, j As Long, k As Long
  
  Set mvarClassesTemplates = New CMonsterEncTemplateSet
  Set mvarMonsterStatistics = New CMonsterStats
  
  msdb.OpenDB "SELECT * FROM MonsterStats WHERE name LIKE '" & _
    Apostrify(mvarstrMonsterName) & "'"
    
  If msdb.rs.State > 0 Then
    If msdb.rs.RecordCount > 0 Then
      msdb.rs.MoveFirst
      
      
      Do While Not msdb.rs.EOF
        
        For k = 1 To mvarlngActualAppearing
          
          sngECR = msdb.rs!CR
          mvarMonsterStatistics.Add
          
          If Not IsNull(msdb.rs!Treasure) Then
            mvarMonsterStatistics(mvarMonsterStatistics.Count).strTreasure = msdb.rs!Treasure
          Else
            mvarMonsterStatistics(mvarMonsterStatistics.Count).strTreasure = "Standard"
          End If
          If Not IsNull(msdb.rs!hit_dice_num) Then
            mvarMonsterStatistics(mvarMonsterStatistics.Count).lngHitDiceNum = msdb.rs!hit_dice_num
          End If
          'If Not IsNull(msdb.rs!hit_dice_sides) Then
          '  mvarMonsterStatistics(mvarMonsterStatistics.Count).lngHitDiceSides = msdb.rs!hit_dice_sides
          'End If
          If Not IsNull(msdb.rs!Type) Then
            mvarMonsterStatistics(mvarMonsterStatistics.Count).strType = msdb.rs!Type
          End If
          
          If Not IsNull(msdb.rs!Descriptor) Then
            mvarMonsterStatistics(mvarMonsterStatistics.Count).strDescriptor = msdb.rs!Descriptor
          End If
          
          If Not IsNull(msdb.rs!STREN) Then
            If msdb.rs!STREN > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytStrength = msdb.rs!STREN
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytStrength = 0
            End If
          End If
          If Not IsNull(msdb.rs!DEX) Then
            If msdb.rs!DEX > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytDexterity = msdb.rs!DEX
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytDexterity = 0
            End If
          End If
          If Not IsNull(msdb.rs!CON) Then
            If msdb.rs!CON > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytConstitution = msdb.rs!CON
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytConstitution = 0
            End If
          End If
          If Not IsNull(msdb.rs!INTEL) Then
            If msdb.rs!INTEL > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytIntelligence = msdb.rs!INTEL
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytIntelligence = 0
            End If
          End If
          If Not IsNull(msdb.rs!WIS) Then
            If msdb.rs!WIS > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytWisdom = msdb.rs!WIS
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytWisdom = 0
            End If
          End If
          If Not IsNull(msdb.rs!CHA) Then
            If msdb.rs!CHA > 0 Then
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytCharisma = msdb.rs!CHA
            Else
              mvarMonsterStatistics(mvarMonsterStatistics.Count).bytCharisma = 0
            End If
          End If
          
          
      
          ' get the monster's CR by summing templates and class levels
          db.OpenDB "SELECT * FROM MonsterEncounterClasses WHERE EncounterName LIKE '" & _
            Apostrify(strEncounterName) & "' AND MonsterName LIKE '" & Apostrify(mvarstrMonsterName) & _
            "' AND MonsterTitle LIKE '" & Apostrify(strMonsterTitle) & "'"
          
          If db.rs.State > 0 Then
            If db.rs.RecordCount > 0 Then
              db.rs.MoveFirst
              Do While Not db.rs.EOF
                mvarClassesTemplates.Add
                With mvarClassesTemplates(mvarClassesTemplates.Count)
                  .lngMinClassLevel = db.rs!MinClassLevel
                  .lngMaxClassLevel = db.rs!MaxClassLevel
                  If .lngMinClassLevel > 0 And .lngMaxClassLevel > 0 Then
                    .lngActualClassLevel = ceil(Rnd * (.lngMaxClassLevel - .lngMinClassLevel) + .lngMinClassLevel)
                  End If
                  .strClassName = db.rs!ClassName
                  .strTemplateName = db.rs!TemplateName
                  .lngTemplateStack = db.rs!TemplateStack
                End With
                
                db.rs.MoveNext
              Loop
            End If
            db.CloseDB
          End If
          
          
          mvarMonsterStatistics(mvarMonsterStatistics.Count).sngCR = sngECR
        
        Next k
        msdb.rs.MoveNext
      Loop
      
    End If
    msdb.CloseDB
  End If
  
  
  
  For j = 1 To mvarMonsterStatistics.Count
    mvarMonsterStatistics(j).CalculateTreasure
    For i = 1 To mvarClassesTemplates.Count
      sngECR = mvarMonsterStatistics(j).sngCR + mvarClassesTemplates(i).GetCRBonus(mvarMonsterStatistics(mvarMonsterStatistics.Count).lngHitDiceNum)
    Next i
  Next j
  
  
End Sub



