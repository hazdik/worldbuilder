VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMonsterStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarstrName As String 'local copy
Private mvarsngCR As Single 'local copy
Private mvarstrType As String 'local copy
Private mvarlngHitDiceNum As Long 'local copy
Private mvarlngHitDiceSides As Long 'local copy
Private mvarstrTreasure As String 'local copy
'local variable(s) to hold property value(s)
Private mvarTreasure As CTreasureHoard 'local copy
'local variable(s) to hold property value(s)
Private mvarstrDescriptor As String 'local copy
Private mvarstrSize As String 'local copy
Private mvarbytFort As Byte 'local copy
Private mvarbytRef As Byte 'local copy
Private mvarbytWill As Byte 'local copy
Private mvarbytStrength As Byte 'local copy
Private mvarbytDexterity As Byte 'local copy
Private mvarbytConstitution As Byte 'local copy
Private mvarbytIntelligence As Byte 'local copy
Private mvarbytWisdom As Byte 'local copy
Private mvarbytCharisma As Byte 'local copy
Private mvarstrSkills As String 'local copy
Private mvarstrFeats As String 'local copy
Private mvarstrSource As String 'local copy
Private mvarblnOGL As Boolean 'local copy
Public Property Let blnOGL(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.blnOGL = 5
    mvarblnOGL = vData
End Property


Public Property Get blnOGL() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.blnOGL
    blnOGL = mvarblnOGL
End Property



Public Property Let strSource(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSource = 5
    mvarstrSource = vData
End Property


Public Property Get strSource() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSource
    strSource = mvarstrSource
End Property



Public Property Let strFeats(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strFeats = 5
    mvarstrFeats = vData
End Property


Public Property Get strFeats() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strFeats
    strFeats = mvarstrFeats
End Property



Public Property Let strSkills(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSkills = 5
    mvarstrSkills = vData
End Property


Public Property Get strSkills() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSkills
    strSkills = mvarstrSkills
End Property



Public Property Let bytCharisma(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytCharisma = 5
    mvarbytCharisma = vData
End Property


Public Property Get bytCharisma() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytCharisma
    bytCharisma = mvarbytCharisma
End Property



Public Property Let bytWisdom(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytWisdom = 5
    mvarbytWisdom = vData
End Property


Public Property Get bytWisdom() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytWisdom
    bytWisdom = mvarbytWisdom
End Property



Public Property Let bytIntelligence(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytIntelligence = 5
    mvarbytIntelligence = vData
End Property


Public Property Get bytIntelligence() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytIntelligence
    bytIntelligence = mvarbytIntelligence
End Property



Public Property Let bytConstitution(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytConstitution = 5
    mvarbytConstitution = vData
End Property


Public Property Get bytConstitution() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytConstitution
    bytConstitution = mvarbytConstitution
End Property



Public Property Let bytDexterity(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytDexterity = 5
    mvarbytDexterity = vData
End Property


Public Property Get bytDexterity() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytDexterity
    bytDexterity = mvarbytDexterity
End Property



Public Property Let bytStrength(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytStrength = 5
    mvarbytStrength = vData
End Property


Public Property Get bytStrength() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytStrength
    bytStrength = mvarbytStrength
End Property



Public Property Let bytWill(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytWill = 5
    mvarbytWill = vData
End Property


Public Property Get bytWill() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytWill
    bytWill = mvarbytWill
End Property



Public Property Let bytRef(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytRef = 5
    mvarbytRef = vData
End Property


Public Property Get bytRef() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytRef
    bytRef = mvarbytRef
End Property



Public Property Let bytFort(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytFort = 5
    mvarbytFort = vData
End Property


Public Property Get bytFort() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytFort
    bytFort = mvarbytFort
End Property



Public Property Let strSize(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strSize = 5
    mvarstrSize = vData
End Property


Public Property Get strSize() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strSize
    strSize = mvarstrSize
End Property



Public Property Let strDescriptor(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strDescriptor = 5
    mvarstrDescriptor = vData
End Property


Public Property Get strDescriptor() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strDescriptor
    strDescriptor = mvarstrDescriptor
End Property




Public Property Set Treasure(ByVal vData As CTreasureHoard)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Treasure = Form1
    Set mvarTreasure = vData
End Property


Public Property Get Treasure() As CTreasureHoard
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Treasure
    Set Treasure = mvarTreasure
End Property




Public Property Let strTreasure(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strTreasure = 5
    mvarstrTreasure = vData
End Property


Public Property Get strTreasure() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strTreasure
    strTreasure = mvarstrTreasure
End Property



Public Property Let lngHitDiceSides(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngHitDiceSides = 5
    mvarlngHitDiceSides = vData
End Property


Public Property Get lngHitDiceSides() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngHitDiceSides
    lngHitDiceSides = mvarlngHitDiceSides
End Property



Public Property Let lngHitDiceNum(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.lngHitDiceNum = 5
    mvarlngHitDiceNum = vData
End Property


Public Property Get lngHitDiceNum() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngHitDiceNum
    lngHitDiceNum = mvarlngHitDiceNum
End Property



Public Property Let strType(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strType = 5
    mvarstrType = vData
End Property


Public Property Get strType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strType
    strType = mvarstrType
End Property



Public Property Let sngCR(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sngCR = 5
    mvarsngCR = vData
End Property


Public Property Get sngCR() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sngCR
    sngCR = mvarsngCR
End Property



Public Property Let strName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strName = 5
    mvarstrName = vData
End Property


Public Property Get strName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strName
    strName = mvarstrName
End Property


Public Sub CalculateTreasure()
  'parses and rolls treasure
  Set mvarTreasure = New CTreasureHoard
  mvarTreasure.ParseMonsterTreasures mvarstrTreasure
  mvarTreasure.sngCR = mvarsngCR
  mvarTreasure.RollTreasure
  mvarTreasure.AddMonsterItems mvarstrTreasure
  
  
End Sub


