VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNPC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarstrName As String 'local copy
Private mvarstrClass As String 'local copy
Private mvarstrRace As String 'local copy
Private mvarbytLevel As Byte 'local copy
Private mvarstrGender As String

Public Property Let strGender(ByVal vData As String)
    mvarstrGender = vData
End Property


Public Property Get strGender() As String
    strGender = mvarstrGender
End Property


Public Property Let bytLevel(ByVal vData As Byte)
    mvarbytLevel = vData
End Property


Public Property Get bytLevel() As Byte
    bytLevel = mvarbytLevel
End Property



Public Property Let strRace(ByVal vData As String)
    mvarstrRace = vData
End Property


Public Property Get strRace() As String
    strRace = mvarstrRace
End Property



Public Property Let strClass(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strClass = 5
    mvarstrClass = vData
End Property


Public Property Get strClass() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strClass
    strClass = mvarstrClass
End Property



Public Property Let strName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strName = 5
    mvarstrName = vData
End Property


Public Property Get strName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strName
    strName = mvarstrName
End Property

