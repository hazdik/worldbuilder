VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COrbit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarlN As Long ' quantum number
Private mvarlK As Long ' quantum number
Private mvarEpoch As Single 'local copy
Private mvark As Single 'local copy
Private mvariDeg As Single 'local copy
Private mvarMeanMotion As Single 'local copy
Private mvarAscendingNode As Single 'local copy
Private mvarTimeOfPericenter As Single 'local copy
Private mvarPeriodYr As Single 'local copy
Private mvarPericenterDistance As Single 'local copy
Private mvarArgumentOfPericenter As Single 'local copy
Private mvarEccentricity As Single 'local copy
Private mvarSemiMajorAxisAU As Double 'local copy
Private mvarMeanAnomaly As Single 'local copy
Private mvarLongOfPericenter As Single 'local copy

Public Property Let LongOfPericenter(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.LongOfPericenter = 5
    mvarLongOfPericenter = vData
End Property


Public Property Get LongOfPericenter() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.LongOfPericenter
    LongOfPericenter = mvarLongOfPericenter
End Property





Public Property Let MeanAnomaly(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.MeanAnomaly = 5
    mvarMeanAnomaly = vData
End Property


Public Property Get MeanAnomaly() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MeanAnomaly
    MeanAnomaly = mvarMeanAnomaly
End Property




Public Property Let lN(ByVal vData As Long)
    mvarlN = vData
    If mvarlN < 1 Then mvarlN = 1
    If mvarlK >= mvarlN Then mvarlK = mvarlN - 1
End Property


Public Property Get lN() As Long
    lN = mvarlN
End Property


Public Property Let lK(ByVal vData As Long)
    mvarlK = vData
    If mvarlK < 0 Then mvarlK = 0
    If mvarlK >= mvarlN Then mvarlN = mvarlK + 1
    
End Property

Public Property Get lK() As Long
    lK = mvarlK
End Property


Public Property Let SemiMajorAxisAU(ByVal vData As Double)
    mvarSemiMajorAxisAU = vData
End Property

Public Property Get SemiMajorAxisAU() As Double
    SemiMajorAxisAU = mvarSemiMajorAxisAU
End Property


Public Property Let eccentricity(ByVal vData As Single)
    mvarEccentricity = vData
End Property

Public Property Get eccentricity() As Single
    eccentricity = mvarEccentricity
End Property


Public Property Let ArgumentOfPericenter(ByVal vData As Single)
    mvarArgumentOfPericenter = vData
End Property

Public Property Get ArgumentOfPericenter() As Single
    ArgumentOfPericenter = mvarArgumentOfPericenter
End Property


Public Property Let PericenterDistance(ByVal vData As Single)
    mvarPericenterDistance = vData
End Property

Public Property Get PericenterDistance() As Single
    PericenterDistance = mvarPericenterDistance
End Property


Public Property Let PeriodYr(ByVal vData As Single)
    mvarPeriodYr = vData
End Property

Public Property Get PeriodYr() As Single
    PeriodYr = mvarPeriodYr
End Property


Public Property Let TimeOfPericenter(ByVal vData As Single)
    mvarTimeOfPericenter = vData
End Property

Public Property Get TimeOfPericenter() As Single
    TimeOfPericenter = mvarTimeOfPericenter
End Property



Public Property Let AscendingNode(ByVal vData As Single)
    mvarAscendingNode = vData
End Property

Public Property Get AscendingNode() As Single
    AscendingNode = mvarAscendingNode
End Property



Public Property Let MeanMotion(ByVal vData As Single)
    mvarMeanMotion = vData
End Property

Public Property Get MeanMotion() As Single
    MeanMotion = mvarMeanMotion
End Property



Public Property Let iDeg(ByVal vData As Single)
    mvariDeg = vData
End Property

Public Property Get iDeg() As Single
    iDeg = mvariDeg
End Property



Public Property Let k(ByVal vData As Single)
    mvark = vData
End Property

Public Property Get k() As Single
    k = mvark
End Property



Public Property Let epoch(ByVal vData As Single)
    mvarEpoch = vData
End Property

Public Property Get epoch() As Single
    epoch = mvarEpoch
End Property


Public Function EllipsePerimeterNK(n As Long, k As Long) As Double
  ' Calculates the perimeter of an ellipse where e = k/n
  ' k and n are planetary quantum numbers

  Dim sqrn2k2 As Double
  
  If n > 0 And k >= 0 And k < n Then
    sqrn2k2 = Sqr(n ^ 2 - k ^ 2)
    EllipsePerimeterNK = (3 * PI * (sqrn2k2 - n) ^ 2) / _
      ((Sqr(14 * sqrn2k2 * n - k ^ 2 + 2 * n ^ 2) + 10 * (sqrn2k2 + n)) * n) _
      + PI * (sqrn2k2 + n) / n
  End If
  
End Function

Public Function EllipsePerimeterE(e As Single) As Double
  ' Calculates the perimeter of an ellipse where e = k/n
  ' k and n are planetary quantum numbers

  Dim sqrn2k2 As Double
  
  If e >= 0 And e < 1 Then
    sqrn2k2 = Sqr(1 - e ^ 2)
    EllipsePerimeterE = (3 * PI * (sqrn2k2 - 1) ^ 2) / _
      ((Sqr(14 * sqrn2k2 - e ^ 2 + 2) + 10 * (sqrn2k2 + 1))) _
      + PI * (sqrn2k2 + 1)
  End If
  
End Function


Public Sub OrbitDataNK(PrimaryMassMSol As Single)
  
  If mvarlN = 0 Then
    mvarlN = 1
    mvarlK = 0
  End If
  
  mvarEccentricity = mvarlK / mvarlN
  mvarPeriodYr = PrimaryMassMSol * (mvarlN * EllipsePerimeterNK(mvarlN, mvarlK) / w0) ^ 3
  mvarSemiMajorAxisAU = (PrimaryMassMSol * mvarPeriodYr ^ 2) ^ (1 / 3)
  
End Sub

Public Sub OrbitDataE(PrimaryMassMSol As Single)
  
  If mvarlN = 0 Then
    mvarlN = 1
    mvarlK = 0
  End If
  
  mvarPeriodYr = PrimaryMassMSol * (mvarlN * EllipsePerimeterE(mvarEccentricity) / w0) ^ 3
  mvarSemiMajorAxisAU = (PrimaryMassMSol * mvarPeriodYr ^ 2) ^ (1 / 3)
  
End Sub

Private Sub Class_Initialize()
  mvarlN = 1
  mvarMeanAnomaly = Rnd * 360
  mvarEpoch = 2450800.5
  mvarLongOfPericenter = Rnd * 360
End Sub
