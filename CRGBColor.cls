VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRGBColor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarRed As Single 'local copy
Private mvarGreen As Single 'local copy
Private mvarBlue As Single 'local copy
Public Property Let Blue(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Blue = 5
    mvarBlue = vData
End Property


Public Property Get Blue() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Blue
    Blue = mvarBlue
End Property



Public Property Let Green(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Green = 5
    mvarGreen = vData
End Property


Public Property Get Green() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Green
    Green = mvarGreen
End Property



Public Property Let Red(ByVal vData As Single)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Red = 5
    mvarRed = vData
End Property


Public Property Get Red() As Single
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Red
    Red = mvarRed
End Property



