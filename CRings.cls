VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarInner As Double 'local copy
Private mvarOuter As Double 'local copy
Private mvarTexture As String 'local copy
'local variable(s) to hold property value(s)
Private mvarColor As CRGBColor 'local copy
Public Property Set Color(ByVal vData As CRGBColor)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.Color = Form1
    Set mvarColor = vData
End Property


Public Property Get Color() As CRGBColor
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Color
    Set Color = mvarColor
End Property




Public Property Let Texture(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Texture = 5
    mvarTexture = vData
End Property


Public Property Get Texture() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Texture
    Texture = mvarTexture
End Property



Public Property Let Outer(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Outer = 5
    mvarOuter = vData
End Property


Public Property Get Outer() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Outer
    Outer = mvarOuter
End Property



Public Property Let Inner(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Inner = 5
    mvarInner = vData
End Property


Public Property Get Inner() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Inner
    Inner = mvarInner
End Property



Public Sub Generate(RadiusM As Double)
  Dim temp As Double
  Dim a As Integer
  
  mvarInner = Abs(randn) * 0.3717 * RadiusM / 1000 + 1.6825 * RadiusM / 1000
  mvarOuter = Abs(randn) * 0.3282 * RadiusM / 1000 + 2.54175 * RadiusM / 1000
  
  If Inner > Outer Then
    temp = Inner
    mvarInner = mvarOuter
    mvarOuter = temp
  End If
  
  a = Int(Rnd * 4) + 1
  Select Case a
    Case Is <= 1
      mvarTexture = "saturn-rings.png"
    Case 2
      mvarTexture = "neptune-rings.png"
    Case 3
      mvarTexture = "uranus-rings.png"
    Case Else
      ' do nothing
  End Select
  
  mvarColor.Blue = Rnd
  mvarColor.Red = Rnd
  mvarColor.Green = Rnd
  
End Sub

Private Sub Class_Initialize()
  Set Color = New CRGBColor
End Sub

Private Sub Class_Terminate()
  Set Color = Nothing
End Sub
