VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStarMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private StarMap() As Byte

Public Sub MakeStarMap(filename As String, MaxAppMag As Single, NStars As Long, rows As Long, cols As Long)
  Dim stars As New CStars
  Dim i As Long, j As Long, k As Long
  
  ReDim StarMap(rows, cols, 2) As Byte
  
  For i = 1 To NStars
    stars.Add
    stars(i).RollStar
    stars(i).AppMag = Rnd * MaxAppMag
    stars(i).RightAscension = Rnd * 360
    stars(i).DistanceLY = Rnd * 6000
    stars(i).RightAscension = Rnd * 360
    stars(i).DecDeg = Rnd * 180 - 90
    stars(i).DistanceLY = 32.6 * Exp(0.460517018599 * (stars(i).AppMag - stars(i).AbsMag))
  
    j = CLng(stars(i).DecDeg * rows / 90 + 90)
    k = CLng(stars(i).RightAscension * cols / 360)
  
    Select Case LEFT(stars(i).SpectralType, 1)
      Case "O"
        StarMap(j, k, 0) = 173
        StarMap(j, k, 1) = 173
        StarMap(j, k, 2) = 255
      Case "B"
        StarMap(j, k, 0) = 230
        StarMap(j, k, 1) = 230
        StarMap(j, k, 2) = 255
      Case "A"
        StarMap(j, k, 0) = 255
        StarMap(j, k, 1) = 255
        StarMap(j, k, 2) = 255
      Case "F"
        StarMap(j, k, 0) = 220
        StarMap(j, k, 1) = 255
        StarMap(j, k, 2) = 255
      Case "G"
        StarMap(j, k, 0) = 255
        StarMap(j, k, 1) = 255
        StarMap(j, k, 2) = 128
      Case "K"
        StarMap(j, k, 0) = 255
        StarMap(j, k, 1) = 175
        StarMap(j, k, 2) = 0
      Case "M"
        StarMap(j, k, 0) = 255
        StarMap(j, k, 1) = 128
        StarMap(j, k, 2) = 128
      Case Else
        StarMap(j, k, 0) = 255
        StarMap(j, k, 0) = 255
        StarMap(j, k, 0) = 255
    End Select
    
    StarMap(j, k, 0) = StarMap(j, k, 0) / 10 ^ stars(i).AppMag
    StarMap(j, k, 1) = StarMap(j, k, 1) / 10 ^ stars(i).AppMag
    StarMap(j, k, 2) = StarMap(j, k, 2) / 10 ^ stars(i).AppMag
  
  Next i
  
  ' next save the bitmap
  
End Sub
