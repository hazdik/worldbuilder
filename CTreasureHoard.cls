VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTreasureHoard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private db As New CDB

Private mvarsngCR As Single
Private mvarlngCP As Long
Private mvarlngSP As Long
Private mvarlngGP As Long
Private mvarlngPP As Long
Private mvarlngGems As Long
Private mvarlngArt As Long
Private mvarlngMundaneItems As Long
Private mvarlngMinorItems As Long
Private mvarlngMediumItems As Long
Private mvarlngMajorItems As Long
Private mvarlngEpicItems As Long

Private mvarobjGems As New CGems
Private mvarobjItems As New CGems

Private mvarlngTotalCoinValue As Long
Private mvarlngTotalGemValue As Long
Private mvarlngTotalItemValue As Long
Private mvarlngTotalHoardValue As Long

Private mvarsngCoinsMultiplier As Single
Private mvarsngGemsMultiplier As Single
Private mvarsngItemsMultiplier As Single

Public Property Let lngTotalCoinValue(ByVal vData As Long)
  mvarlngTotalCoinValue = vData
End Property
Public Property Get lngTotalCoinValue() As Long
  lngTotalCoinValue = mvarlngTotalCoinValue
End Property

Public Property Let lngTotalGemValue(ByVal vData As Long)
  mvarlngTotalGemValue = vData
End Property
Public Property Get lngTotalGemValue() As Long
  lngTotalGemValue = mvarlngTotalGemValue
End Property

Public Property Let lngTotalItemValue(ByVal vData As Long)
  mvarlngTotalItemValue = vData
End Property
Public Property Get lngTotalItemValue() As Long
  lngTotalItemValue = mvarlngTotalItemValue
End Property

Public Property Let lngTotalHoardValue(ByVal vData As Long)
  mvarlngTotalHoardValue = vData
End Property
Public Property Get lngTotalHoardValue() As Long
  lngTotalHoardValue = mvarlngTotalHoardValue
End Property




Public Property Let sngCoinsMultiplier(ByVal vData As Single)
  mvarsngCoinsMultiplier = vData
End Property
Public Property Get sngCoinsMultiplier() As Single
  sngCoinsMultiplier = mvarsngCoinsMultiplier
End Property

Public Property Let sngGemsMultiplier(ByVal vData As Single)
  mvarsngGemsMultiplier = vData
End Property
Public Property Get sngGemsMultiplier() As Single
  sngGemsMultiplier = mvarsngGemsMultiplier
End Property

Public Property Let sngItemsMultiplier(ByVal vData As Single)
  mvarsngItemsMultiplier = vData
End Property
Public Property Get sngItemsMultiplier() As Single
  sngItemsMultiplier = mvarsngItemsMultiplier
End Property

Public Property Set objItems(ByVal vData As CGems)
  Set mvarobjItems = vData
End Property
Public Property Get objItems() As CGems
  Set objItems = mvarobjItems
End Property


Public Property Set objGems(ByVal vData As CGems)
  Set mvarobjGems = vData
End Property
Public Property Get objGems() As CGems
  Set objGems = mvarobjGems
End Property

Public Property Let lngEpicItems(ByVal vData As Long)
  mvarlngEpicItems = vData
End Property
Public Property Get lngEpicItems() As Long
  lngEpicItems = mvarlngEpicItems
End Property

Public Property Let lngMundaneItems(ByVal vData As Long)
  mvarlngMundaneItems = vData
End Property
Public Property Get lngMundaneItems() As Long
  lngMundaneItems = mvarlngMundaneItems
End Property

Public Property Let lngMajorItems(ByVal vData As Long)
  mvarlngMajorItems = vData
End Property
Public Property Get lngMajorItems() As Long
  lngMajorItems = mvarlngMajorItems
End Property

Public Property Let lngMediumItems(ByVal vData As Long)
  mvarlngMediumItems = vData
End Property
Public Property Get lngMediumItems() As Long
  lngMediumItems = mvarlngMediumItems
End Property

Public Property Let lngMinorItems(ByVal vData As Long)
  mvarlngMinorItems = vData
End Property
Public Property Get lngMinorItems() As Long
  lngMinorItems = mvarlngMinorItems
End Property

Public Property Let lngArt(ByVal vData As Long)
  mvarlngArt = vData
End Property
Public Property Get lngArt() As Long
  lngArt = mvarlngArt
End Property

Public Property Let lngGems(ByVal vData As Long)
  mvarlngGems = vData
End Property
Public Property Get lngGems() As Long
  lngGems = mvarlngGems
End Property

Public Property Let lngPP(ByVal vData As Long)
  mvarlngPP = vData
End Property
Public Property Get lngPP() As Long
  lngPP = mvarlngPP
End Property

Public Property Let lnggp(ByVal vData As Long)
  mvarlngGP = vData
End Property
Public Property Get lnggp() As Long
  lnggp = mvarlngGP
End Property

Public Property Let lngSP(ByVal vData As Long)
  mvarlngSP = vData
End Property
Public Property Get lngSP() As Long
  lngSP = mvarlngSP
End Property

Public Property Let lngCP(ByVal vData As Long)
  mvarlngCP = vData
End Property
Public Property Get lngCP() As Long
  lngCP = mvarlngCP
End Property

Public Property Let sngCR(ByVal vData As Single)
  mvarsngCR = vData
End Property
Public Property Get sngCR() As Single
  sngCR = mvarsngCR
End Property



Public Sub RollTreasure()
  Dim lngroll As Long
  Dim i As Long
  
  mvarlngTotalItemValue = 0
  mvarlngTotalCoinValue = 0
  mvarlngTotalGemValue = 0
  mvarlngTotalHoardValue = 0
  
  If mvarsngCR > 42 Then
    db.OpenDB "SELECT * FROM Treasure WHERE CR = 42"
  ElseIf mvarsngCR < 1 Then
    db.OpenDB "SELECT * FROM Treasure WHERE CR = 1"
  Else
    db.OpenDB "SELECT * FROM Treasure WHERE CR = " & Int(mvarsngCR)
  End If
  
  If db.rs.State > 0 Then
    lngroll = d100
    Select Case lngroll
      Case db.rs!CPMinChance To db.rs!CPMaxChance
        mvarlngCP = dN(db.rs!CPNumDice, db.rs!CPSides) * db.rs!CPMultiplier * mvarsngCoinsMultiplier
      Case db.rs!SPMinChance To db.rs!SPMaxChance
        mvarlngSP = dN(db.rs!SPNumDice, db.rs!SPSides) * db.rs!SPMultiplier * mvarsngCoinsMultiplier
      Case db.rs!GPMinChance To db.rs!GPMaxChance
        mvarlngGP = dN(db.rs!GPNumDice, db.rs!GPSides) * db.rs!GPMultiplier * mvarsngCoinsMultiplier
      Case db.rs!PPMinChance To db.rs!PPMaxChance
        mvarlngPP = dN(db.rs!PPNumDice, db.rs!PPSides) * db.rs!PPMultiplier * mvarsngCoinsMultiplier
    End Select
    mvarlngTotalCoinValue = mvarlngCP / 100 + mvarlngSP / 10 + mvarlngGP + mvarlngPP * 10
    
    lngroll = d100
    Select Case lngroll
      Case db.rs!GemMinChance To db.rs!GemMaxChance
        mvarlngGems = dN(db.rs!GemNumDice, db.rs!GemSides) * mvarsngGemsMultiplier
      Case db.rs!ArtMinChance To db.rs!ArtMaxChance
        mvarlngArt = dN(db.rs!ArtNumDice, db.rs!ArtSides) * mvarsngGemsMultiplier
    End Select
    Set mvarobjGems = New CGems
    For i = 1 To mvarlngGems
      mvarobjGems.Add
      mvarobjGems(i).RollGem
      mvarlngTotalGemValue = mvarlngTotalGemValue + mvarobjGems(i).lngValue
    Next i
    For i = 1 To mvarlngArt
      mvarobjGems.Add
      mvarobjGems(i).RollArt
      mvarlngTotalGemValue = mvarlngTotalGemValue + mvarobjGems(i).lngValue
    Next i


    If mvarsngCR > 42 Then
      db.OpenDB "SELECT * FROM Treasure WHERE CR = 42"
    ElseIf mvarsngCR < 1 Then
      db.OpenDB "SELECT * FROM Treasure WHERE CR = 1"
    Else
      db.OpenDB "SELECT * FROM Treasure WHERE CR = " & Int(mvarsngCR)
    End If

    If db.rs.State > 0 Then
      lngroll = d100
      Select Case lngroll
        Case db.rs!MundaneMinChance To db.rs!MundaneMaxChance
          mvarlngMundaneItems = dN(db.rs!MundaneNumDice, db.rs!MundaneSides) * mvarsngItemsMultiplier
        Case db.rs!MinorMinChance To db.rs!MinorMaxChance
          mvarlngMinorItems = dN(db.rs!MinorNumDice, db.rs!MinorSides) * mvarsngItemsMultiplier
        Case db.rs!MediumMinChance To db.rs!MediumMaxChance
          mvarlngMediumItems = (dN(db.rs!MediumNumDice, db.rs!MediumSides) + db.rs!BonusMagicItems) * mvarsngItemsMultiplier
        Case db.rs!MajorMinChance To db.rs!MajorMaxChance
          mvarlngMajorItems = (dN(db.rs!MajorNumDice, db.rs!MajorSides) + db.rs!BonusMagicItems) * mvarsngItemsMultiplier
        Case db.rs!EpicMinChance To db.rs!EpicMaxChance
          mvarlngEpicItems = dN(db.rs!EpicNumDice, db.rs!EpicSides) * mvarsngItemsMultiplier
      End Select
    End If
    db.CloseDB
  End If
  
  RollMagicItems
  
  mvarlngTotalHoardValue = mvarlngTotalItemValue + mvarlngTotalCoinValue + mvarlngTotalGemValue
  
End Sub


Private Sub RollMagicItems()
  Dim i As Long
  Dim db As New CDB
  Dim lngroll As Long
  Dim strName As String
  Dim sngValue As Single
  
  Set mvarobjItems = New CGems
  
  If mvarlngMundaneItems > 0 Then
    For i = 0 To mvarlngMundaneItems - 1
      sngValue = 0
      strName = RollTreasureTableMagic("TreasureMundaneItem", "Roll", sngValue)
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
      mvarobjItems(mvarobjItems.Count).strGemType = strName
      mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
    Next i
  End If
  
  If mvarlngMinorItems > 0 Then
    For i = 0 To mvarlngMinorItems - 1
      sngValue = 0
      strName = RollTreasureTableMagic("TreasureMagicItem", "Minor", sngValue)
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
      mvarobjItems(mvarobjItems.Count).strGemType = strName
      mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
    Next i
  End If
  
  If mvarlngMediumItems > 0 Then
    For i = 0 To mvarlngMediumItems - 1
      sngValue = 0
      strName = RollTreasureTableMagic("TreasureMagicItem", "Medium", sngValue)
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
      mvarobjItems(mvarobjItems.Count).strGemType = strName
      mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
    Next i
  End If
  
  If mvarlngMajorItems > 0 Then
    For i = 0 To mvarlngMajorItems - 1
      sngValue = 0
      strName = RollTreasureTableMagic("TreasureMagicItem", "Major", sngValue)
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
      mvarobjItems(mvarobjItems.Count).strGemType = strName
      mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
    Next i
  End If
  
  If mvarlngEpicItems > 0 Then
    For i = 0 To mvarlngEpicItems - 1
      sngValue = 0
      strName = RollTreasureTableMagic("TreasureMagicItem", "Epic", sngValue)
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
      mvarobjItems(mvarobjItems.Count).strGemType = strName
      mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
    Next i
  End If

End Sub

Private Sub Class_Initialize()
  mvarsngCR = 1
  mvarsngCoinsMultiplier = 1
  mvarsngGemsMultiplier = 1
  mvarsngItemsMultiplier = 1
End Sub


Public Sub ParseMonsterTreasures(strTreasure As String)
  Select Case strTreasure
    Case "1/10 coins, 50% goods (no nonmetal or nonstone), 50% items (no nonmetal or nonstone)"
      mvarsngCoinsMultiplier = 0.1
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0.5
    Case "1/10 coins; 50% goods; 50% items"
      mvarsngCoinsMultiplier = 0.1
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0.5
    Case "1/10 coins; 50% goods; standard items"
      mvarsngCoinsMultiplier = 0.1
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 1
    Case "1/2 coins; double goods; standard items"
      mvarsngCoinsMultiplier = 0.5
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "50% coins; 50% goods; 50% items"
      mvarsngCoinsMultiplier = 0.5
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0.5
    Case "Always"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "As character"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Double standard"
      mvarsngCoinsMultiplier = 2
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 2
    Case "Double standard (nonflammables only) and +3 longspear"
      mvarsngCoinsMultiplier = 2
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 2
    Case "Double standard plus +4 half-plate armor and Gargantuan +3 adamantine warhammer"
      mvarsngCoinsMultiplier = 2
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 2
    Case "No coins, 50% goods (stone only), no items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0
    Case "No coins; 1/4 goods (honey only); no items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0.25
      mvarsngItemsMultiplier = 0
    Case "No coins; 50% goods (metal or stone only); 50% items (no scrolls)"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0.5
    Case "No coins; 50% goods (stone only); no items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0
    Case "No coins; 50% goods; 50% items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0.5
      mvarsngItemsMultiplier = 0.5
    Case "No coins; double goods; standard items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "No coins; no goods; standard items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0
      mvarsngItemsMultiplier = 1
    Case "No coins; standard goods; double items"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 2
    Case "None"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0
      mvarsngItemsMultiplier = 0
    Case "Nonstandard (just its dagger)"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0
      mvarsngItemsMultiplier = 0
    Case "Quadruple standard"
      mvarsngCoinsMultiplier = 4
      mvarsngGemsMultiplier = 4
      mvarsngItemsMultiplier = 4
    Case "Solitary"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Special (The aurumvorax�s hide, if undamaged, is worth in excess of 3,000 gp)"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 0
      mvarsngItemsMultiplier = 0
    Case "standard"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Standard (Gems only)"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 0
    Case "Standard (nonflammables only)"
      mvarsngCoinsMultiplier = 0
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 0
    Case "Standard (see text)"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "standard [except double the number of gems, and half the number of coins]"
      mvarsngCoinsMultiplier = 0.5
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins, double goods, standard items"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins; double goods (nonflammables only); standard items (nonflammables only)"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins; double goods; standard items"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins; double goods; standard items, plus +1 vorpal greatsword and +1 flaming whip"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins; double goods; standard items, plus 1d4 magic weapons"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 1
    Case "Standard coins; standard goods (gems only); standard items"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Standard coins; standard goods (nonflammables only); standard items (nonflammables only)"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Standard, plus rope and +1 flaming composite longbow (+5 Str bonus)"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 1
      mvarsngItemsMultiplier = 1
    Case "Standard; double gems, art, magic"
      mvarsngCoinsMultiplier = 1
      mvarsngGemsMultiplier = 2
      mvarsngItemsMultiplier = 2
    Case "Triple standard"
      mvarsngCoinsMultiplier = 3
      mvarsngGemsMultiplier = 3
      mvarsngItemsMultiplier = 3
  End Select
End Sub

Public Sub AddMonsterItems(strTreasure As String)
  Dim mwpncnt As Long
  Dim i As Long, j As Long
  Dim strTable As String
  Dim lngroll As Long
  Dim sngValue As Single
  Dim strName As String
  
  Select Case strTreasure
    Case "Double standard (nonflammables only) and +3 longspear"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "+3 longspear"
      mvarobjItems(mvarobjItems.Count).lngValue = 18305
      
    Case "Double standard plus +4 half-plate armor and Gargantuan +3 adamantine warhammer"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "+4 half-plate armor"
      mvarobjItems(mvarobjItems.Count).lngValue = 16750
      
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "Gargantuan +3 adamantine warhammer"
      mvarobjItems(mvarobjItems.Count).lngValue = 21012
    
    Case "Nonstandard (just its dagger)"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "dagger"
      mvarobjItems(mvarobjItems.Count).lngValue = 2
    
    Case "Special (The aurumvorax�s hide, if undamaged, is worth in excess of 3,000 gp)"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "aurumvorax hide"
      mvarobjItems(mvarobjItems.Count).lngValue = 3000
    
    Case "Standard coins; double goods; standard items, plus +1 vorpal greatsword and +1 flaming whip"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "+1 vorpal greatsword"
      mvarobjItems(mvarobjItems.Count).lngValue = 72350
      
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "+1 flaming whip"
      mvarobjItems(mvarobjItems.Count).lngValue = 8301
    
    Case "Standard coins; double goods; standard items, plus 1d4 magic weapons"
      
      mwpncnt = d4
      For j = 0 To mwpncnt - 1
      
        lngroll = d100
        Select Case lngroll
          Case Is < 39
            strTable = "TreasureWeaponCommonMelee"
          Case 39 To 92
            strTable = "TreasureWeaponUncommon"
          Case Is > 92
            strTable = "TreasureWeaponCommonRanged"
        End Select
      
        If mvarlngMinorItems > 0 Then
          For i = 0 To mvarlngMinorItems - 1
            sngValue = 0
            
            Select Case lngroll
              Case Is < 44
                strTable = "TreasureWeaponCommonMelee"
              Case 44 To 86
                strTable = "TreasureWeaponUncommon"
              Case Is > 86
                strTable = "TreasureWeaponCommonRanged"
            End Select
            
            strName = RollTreasureTableMagic(strTable, "Minor", sngValue)
            mvarobjItems.Add
            mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
            mvarobjItems(mvarobjItems.Count).strGemType = strName
            mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
          Next i
        End If
        
        If mvarlngMediumItems > 0 Then
          For i = 0 To mvarlngMediumItems - 1
            sngValue = 0
            strName = RollTreasureTableMagic(strTable, "Medium", sngValue)
            mvarobjItems.Add
            mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
            mvarobjItems(mvarobjItems.Count).strGemType = strName
            mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
          Next i
        End If
        
        If mvarlngMajorItems > 0 Then
          For i = 0 To mvarlngMajorItems - 1
            sngValue = 0
            strName = RollTreasureTableMagic(strTable, "Major", sngValue)
            mvarobjItems.Add
            mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
            mvarobjItems(mvarobjItems.Count).strGemType = strName
            mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
          Next i
        End If
        
        If mvarlngEpicItems > 0 Then
          For i = 0 To mvarlngEpicItems - 1
            sngValue = 0
            strName = RollTreasureTableMagic(strTable, "Epic", sngValue)
            mvarobjItems.Add
            mvarobjItems(mvarobjItems.Count).lngValue = CLng(sngValue)
            mvarobjItems(mvarobjItems.Count).strGemType = strName
            mvarlngTotalItemValue = mvarlngTotalItemValue + sngValue
          Next i
        End If
        
      Next j
    
    Case "Standard, plus rope and +1 flaming composite longbow (+5 Str bonus)"
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "silk rope"
      mvarobjItems(mvarobjItems.Count).lngValue = 10
      
      mvarobjItems.Add
      mvarobjItems(mvarobjItems.Count).strGemType = "+1 flaming composite longbow (+5 Str bonus)"
      mvarobjItems(mvarobjItems.Count).lngValue = 8900
  
  
  
  End Select
End Sub
