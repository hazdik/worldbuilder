VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTreasureHoards"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CTreasureHoard"
Attribute VB_Ext_KEY = "Member0" ,"CTreasureHoard"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable to hold collection
Private mCol As Collection


Private mvarlngTotalCP As Long 'local copy
Private mvarlngTotalSP As Long 'local copy
Private mvarlngTotalGP As Long 'local copy
Private mvarlngTotalPP As Long 'local copy
Private mvarlngTotalGemValue As Long 'local copy
Private mvarlngTotalCoinValue As Long 'local copy
Private mvarlngTotalMundaneItems As Long 'local copy
Private mvarlngTotalMinorItems As Long 'local copy
Private mvarlngTotalMediumItems As Long 'local copy
Private mvarlngTotalMajorItems As Long 'local copy
Private mvarlngTotalEpicItems As Long 'local copy
Private mvarlngTotalItemValue As Long 'local copy
Private mvarlngTotalArtObjects As Long 'local copy
Private mvarlngTotalGems As Long 'local copy
Private mvarlngTotalArtValue As Long 'local copy


Public Property Get lngTotalArtValue() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalArtValue
  Tabulate
    lngTotalArtValue = mvarlngTotalArtValue
End Property

Public Property Get lngTotalGems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalGems
  Tabulate
    lngTotalGems = mvarlngTotalGems
End Property


Public Property Get lngTotalArtObjects() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalArtObjects
  Tabulate
    lngTotalArtObjects = mvarlngTotalArtObjects
End Property



Public Property Get lngTotalItemValue() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalItemValue
      Tabulate
lngTotalItemValue = mvarlngTotalItemValue
End Property



Public Property Get lngTotalEpicItems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalEpicItems
  Tabulate
    lngTotalEpicItems = mvarlngTotalEpicItems
End Property



Public Property Get lngTotalMajorItems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalMajorItems
  Tabulate
    lngTotalMajorItems = mvarlngTotalMajorItems
End Property




Public Property Get lngTotalMediumItems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalMediumItems
  Tabulate
    lngTotalMediumItems = mvarlngTotalMediumItems
End Property


Public Property Get lngTotalMinorItems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalMinorItems
  Tabulate
    lngTotalMinorItems = mvarlngTotalMinorItems
End Property



Public Property Get lngTotalMundaneItems() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalMundaneItems
  Tabulate
    lngTotalMundaneItems = mvarlngTotalMundaneItems
End Property




Public Property Get lngTotalCoinValue() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalCoinValue
  Tabulate
    lngTotalCoinValue = mvarlngTotalCoinValue
End Property



Public Property Get lngTotalGemValue() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalGemValue
  Tabulate
    lngTotalGemValue = mvarlngTotalGemValue
End Property



Public Property Get lngTotalPP() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalPP
  Tabulate
    lngTotalPP = mvarlngTotalPP
End Property



Public Property Get lngTotalGP() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalGP
  Tabulate
    lngTotalGP = mvarlngTotalGP
End Property



Public Property Get lngTotalSP() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalSP
  Tabulate
    lngTotalSP = mvarlngTotalSP
End Property



Public Property Get lngTotalCP() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.lngTotalCP
  Tabulate
    lngTotalCP = mvarlngTotalCP
End Property





Public Function Add(Optional vData As CTreasureHoard, Optional sKey As String) As CTreasureHoard
    'create a new object
    Dim objNewMember As CTreasureHoard
    Set objNewMember = New CTreasureHoard

    If Not (vData Is Nothing) Then
      Set objNewMember = vData
    End If

    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If


    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CTreasureHoard
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

Public Sub Tabulate()
  Dim i As Long
  
  mvarlngTotalCP = 0
  mvarlngTotalSP = 0
  mvarlngTotalGP = 0
  mvarlngTotalPP = 0
  mvarlngTotalGemValue = 0
  mvarlngTotalCoinValue = 0
  mvarlngTotalMundaneItems = 0
  mvarlngTotalMinorItems = 0
  mvarlngTotalMediumItems = 0
  mvarlngTotalMajorItems = 0
  mvarlngTotalEpicItems = 0
  mvarlngTotalItemValue = 0
  mvarlngTotalArtObjects = 0
  mvarlngTotalGems = 0
  mvarlngTotalArtValue = 0
  
  For i = 1 To mCol.Count
    
    mvarlngTotalCP = mvarlngTotalCP + mCol(i).lngCP
    mvarlngTotalSP = mvarlngTotalSP + mCol(i).lngSP
    mvarlngTotalGP = mvarlngTotalGP + mCol(i).lnggp
    mvarlngTotalPP = mvarlngTotalPP + mCol(i).lngPP
    
    mvarlngTotalCoinValue = mvarlngTotalCoinValue + mCol(i).lngTotalCoinValue
    
    mvarlngTotalMundaneItems = mvarlngTotalMundaneItems + mCol(i).lngMundaneItems
    mvarlngTotalMinorItems = mvarlngTotalMinorItems + mCol(i).lngMundaneItems
    mvarlngTotalMediumItems = mvarlngTotalMediumItems + mCol(i).lngMediumItems
    mvarlngTotalMajorItems = mvarlngTotalMajorItems + mCol(i).lngMajorItems
    mvarlngTotalEpicItems = mvarlngTotalEpicItems + mCol(i).lngEpicItems
    
    mvarlngTotalItemValue = mvarlngTotalItemValue + mCol(i).lngTotalItemValue
    
    If Me(i).lngGems > 0 Then
      mvarlngTotalGems = mvarlngTotalGems + mCol(i).lngGems
      mvarlngTotalGemValue = mvarlngTotalGemValue + mCol(i).lngTotalGemValue
    ElseIf Me(i).lngArt > 0 Then
      mvarlngTotalArtObjects = mvarlngTotalArtObjects + mCol(i).lngArt
      mvarlngTotalArtValue = mvarlngTotalArtValue + mCol(i).lngTotalGemValue
    End If
  Next i
End Sub


Public Sub AddHoards(HoardsToAdd As CTreasureHoards)
  Dim i As Long
  For i = 1 To HoardsToAdd.Count
    Add HoardsToAdd(i)
  Next i
End Sub
