VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWaterWays"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarClimate() As Climate_E
Private mvarElevation() As Integer
Private mvarTemperature() As Integer
Private mnvarHydroPct As Byte
Private mvarXSize As Long
Private mvarYSize As Long
Private mvarMapScale As Long
Private mvarHydroPct As Long

Public Property Let YSize(ByVal vData As Long)
    mvarYSize = vData
End Property
Public Property Get YSize() As Long
    YSize = mvarYSize
End Property



Public Property Let XSize(ByVal vData As Long)
    mvarXSize = vData
End Property
Public Property Get XSize() As Long
    XSize = mvarXSize
End Property

Public Property Let MapScale(ByVal vData As Long)
  mvarMapScale = vData
End Property
Public Property Get MapScale() As Long
  MapScale = mvarMapScale
End Property

Public Property Let HydroPct(ByVal vData As Long)
    mvarHydroPct = vData
End Property
Public Property Get HydroPct() As Long
    HydroPct = mvarHydroPct
End Property

Public Sub SetClimate(vData() As Climate_E)
  mvarClimate = vData
End Sub
Public Function GetClimate() As Climate_E()
  GetClimate = mvarClimate
End Function

Public Sub SetElevation(vData() As Integer)
  mvarElevation = vData
End Sub
Public Function GetElevation() As Integer()
  GetElevation = mvarElevation
End Function

Public Sub SetTemperature(vData() As Integer)
  mvarTemperature = vData
End Sub
Public Function GetTemperature() As Integer()
  GetTemperature = mvarTemperature
End Function



' You should add one major river per 260,000 square miles (112 pixels).
' This is about 228 rivers for 70% water or 190 for 75% water.
' This is 8 rivers per 1% land for earth sized worlds.
' Rivers should be roughly 300 miles apart (6 pixels @ 48 mi).

' Try to start them in mountain zones and go downhill.
' Need to come up with a tree algorithm to make river systems.
'  - Build main river first
' Favor forest climates over desert and scrub climates.

' 1. Determine starting point.
' 2. Land? No -> goto 1
' 3. Ocean next to it? Yes -> goto 8
' 4. Downhill?  Yes -> go most downhill direction, then goto 3
' 5. Basin?  Fill in, then goto 3
' 6. Go with wetter climate type (A,C,D,E,B), then goto 3
' 7. All same elevation? Continue in same direction, then goto 3
' 8.

Public Function CountLand() As Single
  Dim i As Long
  Dim j As Long
  Dim land As Single
  land = 0
  For i = 0 To UBound(mvarClimate)
    For j = 0 To UBound(mvarClimate)
      If mvarClimate(i, j) <> C_OCEAN And mvarClimate(i, j) <> C_OCEANICE Then
        land = land + 1
      End If
    Next j
  Next i
  CountLand = land
End Function

Public Function AddFreshWater(Optional nrivers As Long = -1)
  
  Dim i As Long, j As Long
  Dim ti As Long, tj As Long
  Dim el As Long, si As Long, sj As Long
  Dim ctrcheck As Long
  Dim ctrcheck2 As Long
  
  Dim riverstep As Long
  
  If nrivers = -1 Then
    ' For the world map (512x256 @ 48 mi), this adds 348 rivers (probably too many to draw).
    ' For regional maps (1024x1024 @ 3 mi), this adds 11 rivers.
    nrivers = ((UBound(mvarElevation, 1) * UBound(mvarElevation, 2)) * (mvarMapScale ^ 2)) / 260000# * _
      CountLand / (UBound(mvarElevation, 1) * UBound(mvarElevation, 2))
  End If
  
  For riverstep = 0 To nrivers
    
    ' Randomly select a point on land
    ctrcheck2 = 0
    Do
      j = random(mvarYSize)
      i = random(mvarXSize)
      ctrcheck2 = ctrcheck2 + 1
      DoEvents
    Loop Until ctrcheck2 > 100 Or (mvarClimate(i, j) <> C_OCEAN And mvarClimate(i, j) <> C_OCEANICE And mvarClimate(i, j) <> C_DESERT)
  
    mvarClimate(i, j) = C_LAKERIVER
    
    
    Do
      si = i
      sj = j
      
      ' Find the steepest path going downwards.
      For ti = i - 1 To i + 1
        For tj = j - 1 To j + 1
          If ti >= 0 And ti < mvarXSize Then
            If tj < 0 Then tj = mvarYSize + tj
            If tj > mvarYSize Then tj = tj Mod mvarYSize
            
            If mvarElevation(ti, tj) < mvarElevation(i, j) And mvarElevation(ti, tj) < mvarElevation(si, sj) And mvarClimate(ti, tj) <> C_LAKERIVER Then
              ' this is steeper, it's going down this way
              si = ti
              sj = tj
            End If
            
          End If
        Next tj
      Next ti
    
      If si = i And sj = j Then
        ctrcheck = 0
        Do
          ti = random(2) - 1
          If ti = 0 Then ti = -1
          si = i + ti
          If si < 0 Or si > UBound(mvarElevation, 1) Then
            Exit Do ' off the map vertically, stop this river
          End If
          
          tj = random(2) - 1
          If tj = 0 Then tj = -1
          sj = j + tj
          If sj < 0 Then sj = sj + mvarYSize
          If sj >= mvarYSize Then sj = sj Mod mvarYSize
          ctrcheck = ctrcheck + 1
          DoEvents
        Loop Until mvarClimate(si, sj) <> C_LAKERIVER Or ctrcheck > 9
      End If
      
      If mvarClimate(si, sj) = C_OCEAN Or mvarClimate(si, sj) = C_OCEANICE Or mvarClimate(si, sj) = C_LAKERIVER Then
        Exit Do
      Else
        mvarClimate(si, sj) = C_LAKERIVER
        i = si
        j = sj
      End If
      DoEvents
    Loop Until mvarClimate(si, sj) = C_OCEAN
    
  Next riverstep
  
End Function

