Imports System.Drawing

Public Class Form1

    ' TODO: make a way to determine grasslands more common without going too far.  
    ' TODO: refine coefficients so that climates are more balanced.
    ' TODO: rename the buttons

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Randomize()
        Dim a As System.Drawing.Bitmap
        Dim i As Integer, j As Integer, k As Integer
        Dim imagedata(,) As Short
        Dim imagedata2(,) As Byte
        Dim tempdata(,,) As Short

        Dim MAXMOUNTAINHT As Integer = 295

        ProgressBar1.Maximum = 100
        ProgressBar1.Value = 1

        ' TODO: check the bitmap's size to make sure it's grayscale sized in powers of 2 with a 2:1 ratio.

        OpenFileDialog1.Filter = "Grayscale Bitmap (*.bmp)|*.bmp"
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Dim strRoot As String

            strRoot = OpenFileDialog1.FileName.Substring(0, _
                                OpenFileDialog1.FileName.Length - 4)

            ' TODO: use the brightness instead of the blue channel.
            a = New System.Drawing.Bitmap(OpenFileDialog1.FileName)

            PictureBox1.Load(OpenFileDialog1.FileName)

            ReDim imagedata(a.Width, a.Height)
            Application.DoEvents()

            ' copy the array
            For i = 0 To a.Width - 1
                For j = 0 To a.Height - 1
                    imagedata(i, j) = a.GetPixel(i, j).B * MAXMOUNTAINHT * 100.0# / 255
                Next
            Next

            ProgressBar1.Value = 33

            Dim b As New CClim

            ' TODO: put these into the GUI instead of hard coding them.  
            ' Use the tabs on the right side of the GUI.
            b.BarSep = 16
            b.BSize = 12
            b.eccentricity = 0.016
            b.EccPhase = 1 * System.Math.PI
            b.FetchDelta = 4
            b.FlankDelta = -50 '-24
            b.HeatEquatorDelta = 32
            b.IcebergK = 263
            b.LandDelta = -10
            b.LandTemperatureMean = 275
            b.LandTemperatureRange = 84
            b.LHMax = 20
            b.LHMin = 0
            b.LLMax = 255
            b.LLMin = 220
            b.LLThresh = 15
            b.LOThresh = 6
            b.MaxFetch = 11
            b.MaxRange = 15
            b.MaxStep = 10000
            b.MountainDelta = 32
            b.NearFlankDelta = -5 '-4
            b.NearHeatEquatorDelta = 24
            b.OceanTemperatureMean = 285
            b.OceanTemperatureRange = 50
            b.OHMax = 180
            b.OHMin = 130
            b.OLMax = 65
            b.OLMin = 40
            b.OLThresh = 2
            b.OOThresh = 11
            b.RainConst = 32
            b.Tilt = 23
            b.XSize = a.Width
            b.YSize = a.Height
            b.ZCoast = 16
            b.ZShelf = 8

            b.LetDEM(imagedata)

            b.RunModel()
            ProgressBar1.Value = 67

            tempdata = b.GetTemperatureMap
            Dim outfile As New System.Drawing.Bitmap(tempdata.GetUpperBound(1), tempdata.GetUpperBound(2))
            For k = 0 To 11
                For i = 0 To tempdata.GetUpperBound(1) - 1
                    For j = 0 To tempdata.GetUpperBound(2) - 1
                        outfile.SetPixel(i, j, heatcols(tempdata(k, i, j) / 10 - 100))
                    Next
                Next

                outfile.Save(strRoot + "-temp" + k.ToString + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp)

                PictureBox1.Load(strRoot + "-temp" + k.ToString + ".bmp")
                Application.DoEvents()
                ProgressBar1.Value = 67 + k
            Next k

            imagedata2 = b.GetDnDClimMap

            outfile = New System.Drawing.Bitmap(imagedata2.GetUpperBound(0), imagedata2.GetUpperBound(1))

            For i = 0 To imagedata2.GetUpperBound(0) - 1
                For j = 0 To imagedata2.GetUpperBound(1) - 1
                    outfile.SetPixel(i, j, dndcols(imagedata2(i, j)))
                Next
            Next

            outfile.Save(strRoot + "-dndclim.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
            PictureBox1.Load(strRoot + "-dndclim.bmp")
            Application.DoEvents()



            imagedata2 = b.GetClimateMap()

            outfile = New System.Drawing.Bitmap(imagedata2.GetUpperBound(0), imagedata2.GetUpperBound(1))

            For i = 0 To imagedata2.GetUpperBound(0) - 1
                For j = 0 To imagedata2.GetUpperBound(1) - 1
                    outfile.SetPixel(i, j, truecols(imagedata2(i, j)))
                Next
            Next

            outfile.Save(strRoot + "-truecolor.bmp", System.Drawing.Imaging.ImageFormat.Bmp)
            ProgressBar1.Value = 100

            PictureBox1.Load(strRoot + "-truecolor.bmp")
            Application.DoEvents()

            ' now load up the difference bitmap and do a hard light overlay
            Dim differencemap As New Bitmap("lightmap.bmp")
            Dim spaceview As Bitmap
            spaceview = HardLight(outfile, differencemap)
            spaceview.Save(strRoot + "-spaceview.bmp", Imaging.ImageFormat.Bmp)
            PictureBox1.Load(strRoot + "-spaceview.bmp")
            Application.DoEvents()

        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Function HardLight(ByVal image1 As Bitmap, ByVal image2 As Bitmap) As Bitmap
        Dim x As Integer
        Dim y As Integer
        Dim Width As Integer
        Dim Height As Integer
        Dim output As Bitmap
        Dim Colour1 As Color
        Dim Colour2 As Color
        Dim Red As Integer
        Dim Green As Integer
        Dim Blue As Integer
        Dim Divisor As Integer

        Width = image1.Width
        Height = image1.Height
        Divisor = 127

        output = New Bitmap(Width, Height)

        For x = 0 To Width - 1
            For y = 0 To Height - 1
                Colour1 = image1.GetPixel(x, y)
                Colour2 = image2.GetPixel(x, y)
                If Colour1.R < 128 Then
                    Red = CInt(Colour1.R) * CInt(Colour2.R) / CInt(Divisor)
                Else
                    Red = 255 - ((255 - CInt(Colour1.R)) * (255 - CInt(Colour2.R)) / Divisor)
                End If
                If Colour1.G < 128 Then
                    Green = CInt(Colour1.G) * CInt(Colour2.G) / Divisor
                Else
                    Green = 255 - ((255 - CInt(Colour1.G)) * (255 - CInt(Colour2.G)) / Divisor)
                End If
                If Colour1.B < 128 Then
                    Blue = CInt(Colour1.B) * CInt(Colour2.B) / Divisor
                Else
                    Blue = 255 - ((255 - CInt(Colour1.B)) * (255 - CInt(Colour2.B)) / Divisor)
                End If
                output.SetPixel(x, y, Color.FromArgb(Red, Green, Blue))
            Next
        Next
        HardLight = output
    End Function

End Class
