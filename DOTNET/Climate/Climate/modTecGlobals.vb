Option Strict Off
Option Explicit On

Imports VB = Microsoft.VisualBasic
Imports Microsoft.VisualBasic.Compatibility

Module modTecGlobals
	
	
	' Declares for randomad DLL
	Public Declare Sub TRandomInit Lib "randomad.dll" (ByVal seed As Integer)
	Public Declare Sub TRandomInitByArray Lib "randomad.dll" (ByRef seeds() As Integer, ByRef length As Integer)
	Public Declare Function TBRandom Lib "randomad.dll" () As Double
	Public Declare Function TIRandom Lib "randomad.dll" (ByVal min As Integer, ByVal max As Integer) As Double
	Public Declare Function TRandom Lib "randomad.dll" () As Double
	Public Declare Function TRandom2 Lib "randomad.dll" () As Double
	Public Declare Function TRandom3 Lib "randomad.dll" () As Double


	
    Public color_values(31) As Color
	
	Public MAXMOUNTAINHEIGHT As Integer ' maximum mountain height = 32,700 feet
	
	Public MAXX As Integer
	Public MAXY As Integer
	
	Public Const MAXPRESS As Short = 255
	
	Public Const SIZE As Short = 1
	Public WINDOW_HEIGHT As Integer
	Public WINDOW_WIDTH As Integer
	Public Const MAXPLATE As Short = 500
	
    Public landcols(256) As Color
    Public greycols(256) As Color
    Public tecols() As Color
    Public platecols(MAXPLATE) As Color
    Public heatcols(256) As Color
    Public popcols(256) As Color
	
	Public XWinScale As Double
	Public YWinScale As Double
	
	Enum PrintMode_E
		PRINTMODE_NONE = 0
		PRINTMODE_LONG
		PRINTMODE_SCALE
		PRINTMODE_SHORT
		PRINTMODE_GREY
		PRINTMODE_CLIM
	End Enum
	
	Enum DrawType_E
		DRAW_GREY = 1
		DRAW_LAND
		DRAW_CLIM
		DRAW_TEC
		DRAW_PLATE
		DRAW_JET
		DRAW_TRUE
		DRAW_DND
		DRAW_POP
	End Enum
	
	Enum LineType_E
		LINE_DIAG = 1
		LINE_CORN
		LINE_NONE
	End Enum
	
	
	Public Const PI As Double = 3.14159265359
	Public Const TWOPI As Double = 6.28318530718
	Public Const TWOMILLIPI As Double = 0.00628318
	Public Const FREEZING As Double = 273#
	Public Const DEG2RAD As Double = (PI / 180)
	Public n As Integer
	Public area As Integer
	Public Const RADIUS As Short = 45
	Public Const HALFPI As Double = (PI / 2#)
	
	Public Const MAXB As Short = 12 '4
	
	'Global Const LINE_DIAG = 1
	'Global Const LINE_CORN = 2
	
	Public Const LINE_0V As Short = 1
	Public Const LINE_1V As Short = 2
	Public Const LINE_0H As Short = 4
	Public Const LINE_1H As Short = 8
	
	Public Const North As Short = LINE_0V
	Public Const South As Short = LINE_1V
	Public Const East As Short = LINE_0H
	Public Const West As Short = LINE_1H
	Public Const Northwest As Boolean = North Or West
	Public Const Southwest As Boolean = South Or West
	Public Const Northeast As Boolean = North Or East
	Public Const Southeast As Boolean = South Or East
	
	
	Public Const TREELINE_FT As Short = 10000
	Public Const SNOWLINE_FT As Short = 12000
	
	Public Enum Pressure_E
		PR_LOW = 1
		PR_HIGH
		PR_HEQ
	End Enum
	
	Public Enum MainType_E
		M_MAIN = 0
		M_HEAT
		M_PRESS
		M_WIND
		M_RAIN
		M_CLIM
	End Enum
	
	Public Enum Climate_E
		C_OCEAN = 0
		C_OCEANICE
		C_SWAMP
		C_BARELAND
		C_LAKERIVER
		C_MOUNTAIN
		C_JUNGLE ' Af
		C_DECID ' Cf
		C_BOREAL ' Df
		C_TROPICWETDRY ' Aw
		C_MONSOON ' Am
		C_GRASSLAND ' Cw
		C_MEDITERRANEAN ' Cs
		C_COLDDRYWINTER ' Dw
		C_SAVANA
		C_DESERT ' Bw
		C_STEPPE
		C_TUNDRA ' Et
		C_LANDICE
	End Enum
	
	Public Enum DNDCLIMATE_E
		D_WARM_SWAMP
		D_WARM_FOREST
		D_WARM_PLAIN
		D_WARM_DESERT
		D_WARM_HILL
		D_WARM_MOUNTAIN
		D_WARM_AQUATIC
		D_TEMP_SWAMP
		D_TEMP_FOREST
		D_TEMP_PLAIN
		D_TEMP_DESERT
		D_TEMP_HILL
		D_TEMP_MOUNTAIN
		D_TEMP_AQUATIC
		D_COLD_SWAMP
		D_COLD_FOREST
		D_COLD_PLAIN
		D_COLD_DESERT
		D_COLD_HILL
		D_COLD_MOUNTAIN
		D_COLD_AQUATIC
		D_OCEANICE
	End Enum
	
	Public Enum MONTH_E
		JANUARY
		FEBRUARY
		MARCH
		APRIL
		MAY
		JUNE
		JULY
		AUGUST
		SEPTEMBER
		OCTOBER
		NOVEMBER
		DECEMBER
	End Enum
	
	Public Enum COLOR_E
		clr_black
		clr_white
		clr_grey75
		clr_grey50
		clr_MediumPurple1
		clr_purple4
		clr_purple3
		clr_purple2
		clr_purple1
		clr_blue4
		clr_blue3
		clr_blue2
		clr_blue1
		clr_DarkGreen
		clr_green4
		clr_green3
		clr_green2
		clr_green1
		clr_DarkGoldenrod4
		clr_yellow4
		clr_yellow3
		clr_yellow2
		clr_yellow1
		clr_orange4
		clr_orange3
		clr_orange2
		clr_orange1
		clr_brown4
		clr_red4
		clr_red3
		clr_red2
		clr_red1
	End Enum
	
    Public climcols(Climate_E.C_LANDICE) As Color
    Public truecols(Climate_E.C_LANDICE) As Color
    Public dndcols(DNDCLIMATE_E.D_OCEANICE) As Color
	
	Function CMP(ByRef s As String, ByRef X As String) As String
		CMP = CStr(Not StrComp(s, X))
	End Function
	
	
	
	
	
	
	
	
	
	
	
	
	Sub panic(ByRef s As String)
		'frmTec.StatusBar1.Panels(1).Text = "PANIC: " & s
		
		'Set frmTec.tec = Nothing
		
		'Unload frmTec
		'Beep
		
	End Sub
	
	
	
	
	Function random(ByRef top As Integer) As Integer
		' produces a random number from 0 to top
        random = CInt(Rnd() * top)
	End Function
	
	Sub testran1()
		Dim i As Short
		Dim an As Double
		
		TRandomInit(VB.Timer())
		
		'  Debug.Print ("Random integers in interval from 0 to 99:")
		'  For i = 1 To 40
		'    an = TIRandom(0, 99)
		'    Debug.Print Format(an, "00")
		'    If i Mod 10 = 0 Then
		'        Debug.Print
		'    End If
		'  Next i
		'  Debug.Print
		
		Debug.Print("Random floating point numbers in interval from 0 to 1:")
		For i = 1 To 32
			an = TRandom
			Debug.Print(VB6.Format(an, "0.000000 "))
			If i Mod 8 = 0 Then
				Debug.Print("")
			End If
		Next i
		Debug.Print("")
		
	End Sub
	
	
    Function randnDLL() As Single
        Static iset As Integer
        Static gset As Single
        Dim v1, fac, rsq, v2 As Single

        If iset = 0 Then
            Do
                v1 = 2.0# * TRandom - 1.0# 'pick two uniform numbers in the square extending from -1 to +1 in each direction,
                v2 = 2.0# * TRandom - 1.0#
                rsq = v1 * v1 + v2 * v2
            Loop While (rsq >= 1.0# Or rsq = 0.0#) ' and if they are not, try again.
            fac = System.Math.Sqrt(-2.0# * System.Math.Log(rsq) / rsq)
            ' Now make the Box-Muller transformation to get two normal deviates. Return one and
            ' save the other for next time.
            gset = v1 * fac
            iset = 1 ' Set flag.
            randnDLL = v2 * fac
        Else ' We have an extra deviate handy,
            iset = 0 ' so unset the flag,
            randnDLL = gset ' and return it.
        End If

    End Function
	
	'Returns a normally distributed deviate with zero mean and unit variance, using ran1(idum)
	'as the source of uniform deviates.
	Public Function randn() As Single
		
		Static gset As Single
		
		Dim v1, fac, rsq, v2 As Single
		
		Do While (rsq >= 1# Or rsq = 0#)
			v1 = 2# * Rnd() - 1# 'pick two uniform numbers in the square extending
			'from -1 to +1 in each direction,
			v2 = 2# * Rnd() - 1#
			rsq = v1 * v1 + v2 * v2 'see if they are in the unit circle,
		Loop 
		fac = System.Math.Sqrt(-2# * System.Math.Log(rsq) / rsq)
		gset = v1 * fac
		randn = v2 * fac
	End Function
	
    Function MaxMax(ByRef Z(,) As Short) As Integer
        Dim j, i, maxZ As Integer
        maxZ = -2000000000.0#

        For i = 0 To UBound(Z, 1)
            For j = 0 To UBound(Z, 2)
                If maxZ <= Z(i, j) Then maxZ = Z(i, j)
            Next
        Next
        MaxMax = maxZ

    End Function
	
    Function MinMin(ByRef Z(,) As Short) As Integer
        Dim j, i, minZ As Integer
        minZ = 2000000000.0#

        For i = 0 To UBound(Z, 1)
            For j = 0 To UBound(Z, 2)
                If minZ >= Z(i, j) Then minZ = Z(i, j)
            Next
        Next
        MinMin = minZ

    End Function
	
	
	Function ceil(ByRef n As Single) As Integer
		If n - Fix(n) > 0 Then
			ceil = Fix(n) + 1
		Else
			ceil = Fix(n)
		End If
	End Function
	
	Function ndims(ByRef L() As Integer) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndims = i - 1
		
	End Function
	
	Function ndimsB(ByRef L() As Byte) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndimsB = i - 1
		
	End Function
	
	Function ndimsI(ByRef L() As Short) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndimsI = i - 1
		
	End Function
	
	
	Function EmptySingle(ByRef rows As Integer, Optional ByRef cols As Integer = 0, Optional ByRef Z As Integer = 0) As Single()
		' creates an empty array of size (rows, cols, z)
        Dim sngO(,,) As Single
		If rows > 0 Then
			If cols > 0 Then
				If Z > 0 Then
					ReDim sngO(rows, cols, Z)
				Else
                    ReDim sngO(rows, cols, 0)
				End If
			Else
                ReDim sngO(rows, 0, 0)
			End If
		End If
		EmptySingle = VB6.CopyArray(sngO)
	End Function
	
    
	
    Function QuickGradient(ByRef f() As Short) As Short()
        ' like gradient, only returns magnitude, and assumes all h=1

        Dim i As Integer
        Dim G() As Short
        Dim n As Integer


        n = UBound(f, 1)
        ReDim G(n - 1)
        For i = 0 To n - 1
            G(i) = CSng(f(i + 1) - f(i))
        Next i

        QuickGradient = VB6.CopyArray(G)
    End Function

    Function QuickGradient2(ByRef f(,) As Short) As Integer(,)
        ' like gradient, only returns magnitude, and assumes all h=1

        Dim i As Integer
        Dim j As Integer
        Dim G(,) As Integer
        Dim dfdy, dfdx As Integer
        Dim p, n As Integer

        n = UBound(f, 1)
        p = UBound(f, 2)
        ReDim G(n - 1, p - 1)
        For i = 0 To n - 1
            For j = 0 To p - 1
                dfdx = CSng(f(i, j + 1) - f(i, j))
                dfdy = CSng(f(i + 1, j) - f(i, j))
                G(i, j) = System.Math.Sqrt(dfdx ^ 2 + dfdy ^ 2)
            Next j
        Next i

        QuickGradient2 = VB6.CopyArray(G)
    End Function

    Function QuickGradient3(ByRef f(,,) As Short) As Short(,,)
        ' like gradient, only returns magnitude, and assumes all h=1

        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim G(,,) As Short
        Dim dfdy, dfdx, dfdz As Integer
        Dim p, n, L As Integer


        n = UBound(f, 1)
        p = UBound(f, 2)
        L = UBound(f, 3)
        ReDim G(n - 1, p - 1, L - 1)
        For i = 0 To n - 1
            For j = 0 To p - 1
                For k = 0 To L - 1
                    dfdx = CSng(f(i, j + 1, k) - f(i, j, k))
                    dfdy = CSng(f(i + 1, j, k) - f(i, j, k))
                    dfdy = CSng(f(i, j, k + 1) - f(i, j, k))
                    G(i, j, k) = System.Math.Sqrt(dfdx ^ 2 + dfdy ^ 2 + dfdz ^ 2)
                Next k
            Next j
        Next i
        QuickGradient3 = VB6.CopyArray(G)
    End Function
	

	
	
	Sub assign_colors(ByRef mvarZShelf As Integer, ByRef mvarZCoast As Integer)
		Dim i As Integer

        MAXMOUNTAINHEIGHT = 327

        FillX11ColorTable()
		
        color_values(COLOR_E.clr_black) = Color.FromArgb(CInt(&H0S))
        color_values(COLOR_E.clr_white) = Color.FromArgb(&HFFFFFF)
        color_values(COLOR_E.clr_grey75) = Color.FromArgb(&HBFBFBF)
        color_values(COLOR_E.clr_grey50) = Color.FromArgb(&H7F7F7F)
        color_values(COLOR_E.clr_MediumPurple1) = Color.FromArgb(&HABS, &H82S, &HFFS)
        color_values(COLOR_E.clr_purple4) = Color.FromArgb(&H55S, &H1AS, &H8BS)
        color_values(COLOR_E.clr_purple3) = Color.FromArgb(&H7DS, &H26S, &HCDS)
        color_values(COLOR_E.clr_purple2) = Color.FromArgb(&H91S, &H2CS, &HEES)
        color_values(COLOR_E.clr_purple1) = Color.FromArgb(&H9BS, &H30S, &HFFS)
        color_values(COLOR_E.clr_blue4) = Color.FromArgb(0, 0, &H8BS)
        color_values(COLOR_E.clr_blue3) = Color.FromArgb(0, 0, &HCDS)
        color_values(COLOR_E.clr_blue2) = Color.FromArgb(0, 0, &HEES)
        color_values(COLOR_E.clr_blue1) = Color.FromArgb(0, 0, &HFFS)
        color_values(COLOR_E.clr_DarkGreen) = Color.FromArgb(0, &H64S, 0)
        color_values(COLOR_E.clr_green4) = Color.FromArgb(0, &H8BS, 0)
        color_values(COLOR_E.clr_green3) = Color.FromArgb(0, &HCDS, 0)
        color_values(COLOR_E.clr_green2) = Color.FromArgb(0, &HEES, 0)
        color_values(COLOR_E.clr_green1) = Color.FromArgb(0, &HFFS, 0)
        color_values(COLOR_E.clr_DarkGoldenrod4) = Color.FromArgb(&H8BS, &H65S, &H8S)
        color_values(COLOR_E.clr_yellow4) = Color.FromArgb(&H8BS, &H8BS, &H0S)
        color_values(COLOR_E.clr_yellow3) = Color.FromArgb(&HCDS, &HCDS, &H0S)
        color_values(COLOR_E.clr_yellow2) = Color.FromArgb(&HEES, &HEES, &H0S)
        color_values(COLOR_E.clr_yellow1) = Color.FromArgb(&HFFS, &HFFS, &H0S)
        color_values(COLOR_E.clr_orange4) = Color.FromArgb(&H8BS, &H5AS, &H0S)
        color_values(COLOR_E.clr_orange3) = Color.FromArgb(&HCDS, &H85S, &H0S)
        color_values(COLOR_E.clr_orange2) = Color.FromArgb(&HEES, &H9AS, &H0S)
        color_values(COLOR_E.clr_orange1) = Color.FromArgb(&HFFS, &HA5S, &H0S)
        color_values(COLOR_E.clr_brown4) = Color.FromArgb(&H8BS, &H23S, &H23S)
        color_values(COLOR_E.clr_red4) = Color.FromArgb(&H8BS, &H0S, &H0S)
        color_values(COLOR_E.clr_red3) = Color.FromArgb(&HCDS, &H0S, &H0S)
        color_values(COLOR_E.clr_red2) = Color.FromArgb(&HEES, &H0S, &H0S)
        color_values(COLOR_E.clr_red1) = Color.FromArgb(&HFFS, &H0S, &H0S)
		
		' Ocean
		climcols(Climate_E.C_OCEANICE) = color_values(COLOR_E.clr_grey75)
		climcols(Climate_E.C_OCEAN) = color_values(COLOR_E.clr_blue4)
		
		
		' Dry year-round
		climcols(Climate_E.C_DESERT) = color_values(COLOR_E.clr_yellow1) ' R < 25cm   (Somalia)
		climcols(Climate_E.C_SAVANA) = color_values(COLOR_E.clr_orange1) ' 25 cm < R < 50 cm  (Africa)
		climcols(Climate_E.C_STEPPE) = color_values(COLOR_E.clr_DarkGoldenrod4) ' Temperate savannah  (Colorado)
		
		' Wet/Dry and Grasslands
		climcols(Climate_E.C_MONSOON) = color_values(COLOR_E.clr_purple4)
		climcols(Climate_E.C_TROPICWETDRY) = color_values(COLOR_E.clr_yellow2)
		
		climcols(Climate_E.C_GRASSLAND) = color_values(COLOR_E.clr_green1) ' Wet summer, dry winter
		climcols(Climate_E.C_MEDITERRANEAN) = color_values(COLOR_E.clr_orange3) 'RGB(105, 112, 70)    ' Dry summer, wet winter
		
		climcols(Climate_E.C_COLDDRYWINTER) = color_values(COLOR_E.clr_green2) ' Wet summer, dry winter
		
		' Forests
		climcols(Climate_E.C_BOREAL) = color_values(COLOR_E.clr_DarkGreen) ' Coniferous
		climcols(Climate_E.C_DECID) = color_values(COLOR_E.clr_green4) ' Deciduous
		climcols(Climate_E.C_JUNGLE) = color_values(COLOR_E.clr_green3) ' Broadleaf
		
		' Bare lands
		climcols(Climate_E.C_MOUNTAIN) = color_values(COLOR_E.clr_DarkGoldenrod4)
		climcols(Climate_E.C_BARELAND) = color_values(COLOR_E.clr_yellow3)
		
		' Polar
		climcols(Climate_E.C_TUNDRA) = color_values(COLOR_E.clr_grey50)
        climcols(Climate_E.C_LANDICE) = Color.FromArgb(210, 220, 222)
		
		' Other
        climcols(Climate_E.C_SWAMP) = Color.FromArgb(128, 128, 255)
		climcols(Climate_E.C_LAKERIVER) = color_values(COLOR_E.clr_blue1)
		
		
        truecols(Climate_E.C_OCEAN) = Color.FromArgb(3, 29, 63)
        truecols(Climate_E.C_BARELAND) = Color.FromArgb(194, 164, 125)
        truecols(Climate_E.C_MOUNTAIN) = Color.FromArgb(137, 122, 92)
        truecols(Climate_E.C_OCEANICE) = Color.FromArgb(229, 229, 229)
        truecols(Climate_E.C_TUNDRA) = Color.FromArgb(109, 100, 69)
        truecols(Climate_E.C_STEPPE) = Color.FromArgb(77, 99, 45)
        truecols(Climate_E.C_DESERT) = Color.FromArgb(229, 228, 129)
        truecols(Climate_E.C_SAVANA) = Color.FromArgb(85, 102, 47)
        truecols(Climate_E.C_DECID) = Color.FromArgb(65, 86, 29)
        truecols(Climate_E.C_JUNGLE) = Color.FromArgb(50, 69, 27)
        truecols(Climate_E.C_SWAMP) = Color.FromArgb(36, 71, 58)
        truecols(Climate_E.C_LAKERIVER) = Color.FromArgb(3, 29, 63)
        truecols(Climate_E.C_TROPICWETDRY) = Color.FromArgb(160, 150, 115)
        truecols(Climate_E.C_MONSOON) = Color.FromArgb(70, 80, 37)
        truecols(Climate_E.C_GRASSLAND) = Color.FromArgb(113, 115, 63)
        truecols(Climate_E.C_MEDITERRANEAN) = Color.FromArgb(105, 112, 70)
        truecols(Climate_E.C_BOREAL) = Color.FromArgb(52, 70, 29)
        truecols(Climate_E.C_COLDDRYWINTER) = Color.FromArgb(129, 120, 86)
        truecols(Climate_E.C_LANDICE) = Color.FromArgb(210, 220, 222)
		
		landcols(0) = color_values(11)
		landcols(1) = color_values(28)
		For i = 2 To 255
			landcols(i) = color_values(3)
		Next i
		
		' Hot
		For i = 0 To 32
            heatcols(i) = Color.FromArgb(0, 0, (i * 4) + 127)
		Next i
		For i = 33 To 96
            heatcols(i) = Color.FromArgb(0, (i - 32) * 4 - 1, 255)
		Next i
		For i = 97 To 160
            heatcols(i) = Color.FromArgb((i - 96) * 4 - 1, 255, 256 - 4 * (i - 96))
		Next i
		For i = 161 To 224
            heatcols(i) = Color.FromArgb(255, 256 - (4 * (i - 160)), 0)
		Next i
		For i = 225 To 255
            heatcols(i) = Color.FromArgb(256 - (i - 224) * 4, 0, 0)
		Next i
		
		For i = 0 To 255
			'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
            greycols(i) = Color.FromArgb(i, i, i)
		Next i
        greycols(256) = Color.FromArgb(255, 255, 255)
		
		
		For i = 0 To 255
            popcols(i) = Color.FromArgb(255, 255 - i, 255 - i)
		Next i
		
		
		For i = 1 To MAXPLATE
            platecols(i) = Color.FromArgb(random(128) + 127, random(128) + 127, random(128) + 127)
		Next i
		
		ReDim tecols(MAXMOUNTAINHEIGHT)
		' deep ocean
		For i = 0 To mvarZShelf
			tecols(i) = color_values(COLOR_E.clr_blue4) ' dark blue
		Next i
		
		' continental shelf
		For i = mvarZShelf To mvarZCoast
            tecols(i) = Color.FromArgb(128, 255, 255) ' light blue
			If mvarZCoast <> mvarZShelf Then
                tecols(i) = Color.FromArgb(128.0# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 255.0# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 116.0# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf) + &H8BS)
			End If
		Next i
		
		' Greens for land
		For i = mvarZCoast To mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
			'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
			If i >= UBound(tecols) Then Exit For
            tecols(i) = Color.FromArgb(0, i * 2, 0)
		Next i
		
		' Brown for above tree line
		For i = mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
			If i >= UBound(tecols) Then Exit For
            tecols(i) = Color.FromArgb(128, (i - 73) * 40 / 16 + 40, 0)
		Next i
		
		' White for above snow line
		For i = mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
			If i >= UBound(tecols) Then Exit For
			'tecols(i) = color_values(1)
            tecols(i) = Color.FromArgb(128 + i - 110, 128 + i - 110, 128 + i - 110)
		Next i
		
		For i = mvarZCoast + 200 To UBound(tecols)
			If i >= UBound(tecols) Then Exit For
			'tecols(i) = color_values(1)
            tecols(i) = Color.FromArgb(255, 255, 255)
		Next i
		
		' minimum temperature > 18 C
        dndcols(DNDCLIMATE_E.D_WARM_SWAMP) = Color.DarkOliveGreen ' color_values(COLOR_E.clr_purple2)
        dndcols(DNDCLIMATE_E.D_TEMP_SWAMP) = Color.OliveDrab  ' color_values(COLOR_E.clr_purple3)
        dndcols(DNDCLIMATE_E.D_COLD_SWAMP) = Color.Olive   'color_values(COLOR_E.clr_purple4)

        dndcols(DNDCLIMATE_E.D_WARM_FOREST) = Color.ForestGreen ' color_values(COLOR_E.clr_green3)
        dndcols(DNDCLIMATE_E.D_TEMP_FOREST) = Color.Green  'color_values(COLOR_E.clr_green4)
        dndcols(DNDCLIMATE_E.D_COLD_FOREST) = Color.DarkGreen  'color_values(COLOR_E.clr_DarkGreen)

        dndcols(DNDCLIMATE_E.D_WARM_PLAIN) = Color.Goldenrod  ' color_values(COLOR_E.clr_orange2)
        dndcols(DNDCLIMATE_E.D_TEMP_PLAIN) = Color.PaleGoldenrod  'color_values(COLOR_E.clr_orange3)
        dndcols(DNDCLIMATE_E.D_COLD_PLAIN) = Color.LightGoldenrodYellow 'color_values(COLOR_E.clr_orange4)

        dndcols(DNDCLIMATE_E.D_WARM_DESERT) = Color.LightYellow   ' color_values(COLOR_E.clr_yellow1)
        dndcols(DNDCLIMATE_E.D_TEMP_DESERT) = Color.Yellow ' color_values(COLOR_E.clr_yellow2)
        dndcols(DNDCLIMATE_E.D_COLD_DESERT) = Color.OldLace   'color_values(COLOR_E.clr_yellow3)

        dndcols(DNDCLIMATE_E.D_WARM_HILL) = Color.Beige 'color_values(COLOR_E.clr_red2)
        dndcols(DNDCLIMATE_E.D_TEMP_HILL) = Color.Brown 'color_values(COLOR_E.clr_red3)
        dndcols(DNDCLIMATE_E.D_COLD_HILL) = Color.Chocolate  'color_values(COLOR_E.clr_red4)

        dndcols(DNDCLIMATE_E.D_WARM_MOUNTAIN) = Color.Firebrick 'Color.FromArgb(137, 122, 92)
        dndcols(DNDCLIMATE_E.D_TEMP_MOUNTAIN) = Color.FromArgb(137 + 32, 122 + 32, 92 + 32)
        dndcols(DNDCLIMATE_E.D_COLD_MOUNTAIN) = Color.Snow 'Color.FromArgb(137 + 64, 122 + 64, 92 + 64)

        dndcols(DNDCLIMATE_E.D_WARM_AQUATIC) = Color.DarkBlue   'color_values(COLOR_E.clr_blue2)
        dndcols(DNDCLIMATE_E.D_TEMP_AQUATIC) = Color.Blue  'color_values(COLOR_E.clr_blue3)
        dndcols(DNDCLIMATE_E.D_COLD_AQUATIC) = Color.LightBlue  'color_values(COLOR_E.clr_blue4)
        dndcols(DNDCLIMATE_E.D_OCEANICE) = Color.LightSteelBlue   'color_values(COLOR_E.clr_grey75)
		
		
	End Sub


    Public X11ColorTable(501) As Integer

    Sub FillX11ColorTable()
        X11ColorTable(0) = &H0S ' grey0
        X11ColorTable(1) = &H30303 ' grey1
        X11ColorTable(2) = &H50505 ' grey2
        X11ColorTable(3) = &H80808 ' grey3
        X11ColorTable(4) = &HA0A0A ' grey4
        X11ColorTable(5) = &HD0D0D ' grey5
        X11ColorTable(6) = &HF0F0F ' grey6
        X11ColorTable(7) = &H121212 ' grey7
        X11ColorTable(8) = &H141414 ' grey8
        X11ColorTable(9) = &H171717 ' grey9
        X11ColorTable(10) = &H1A1A1A ' grey10
        X11ColorTable(11) = &H1C1C1C ' grey11
        X11ColorTable(12) = &H1F1F1F ' grey12
        X11ColorTable(13) = &H212121 ' grey13
        X11ColorTable(14) = &H242424 ' grey14
        X11ColorTable(15) = &H262626 ' grey15
        X11ColorTable(16) = &H292929 ' grey16
        X11ColorTable(17) = &H2B2B2B ' grey17
        X11ColorTable(18) = &H2E2E2E ' grey18
        X11ColorTable(19) = &H303030 ' grey19
        X11ColorTable(20) = &H333333 ' grey20
        X11ColorTable(21) = &H363636 ' grey21
        X11ColorTable(22) = &H383838 ' grey22
        X11ColorTable(23) = &H3B3B3B ' grey23
        X11ColorTable(24) = &H3D3D3D ' grey24
        X11ColorTable(25) = &H404040 ' grey25
        X11ColorTable(26) = &H424242 ' grey26
        X11ColorTable(27) = &H454545 ' grey27
        X11ColorTable(28) = &H474747 ' grey28
        X11ColorTable(29) = &H4A4A4A ' grey29
        X11ColorTable(30) = &H4D4D4D ' grey30
        X11ColorTable(31) = &H4F4F4F ' grey31
        X11ColorTable(32) = &H525252 ' grey32
        X11ColorTable(33) = &H545454 ' grey33
        X11ColorTable(34) = &H575757 ' grey34
        X11ColorTable(35) = &H595959 ' grey35
        X11ColorTable(36) = &H5C5C5C ' grey36
        X11ColorTable(37) = &H5E5E5E ' grey37
        X11ColorTable(38) = &H616161 ' grey38
        X11ColorTable(39) = &H636363 ' grey39
        X11ColorTable(40) = &H666666 ' grey40
        X11ColorTable(41) = &H696969 ' grey41
        X11ColorTable(42) = &H6B6B6B ' grey42
        X11ColorTable(43) = &H6E6E6E ' grey43
        X11ColorTable(44) = &H707070 ' grey44
        X11ColorTable(45) = &H737373 ' grey45
        X11ColorTable(46) = &H757575 ' grey46
        X11ColorTable(47) = &H787878 ' grey47
        X11ColorTable(48) = &H7A7A7A ' grey48
        X11ColorTable(49) = &H7D7D7D ' grey49
        X11ColorTable(50) = &H7F7F7F ' grey50
        X11ColorTable(51) = &H828282 ' grey51
        X11ColorTable(52) = &H858585 ' grey52
        X11ColorTable(53) = &H878787 ' grey53
        X11ColorTable(54) = &H8A8A8A ' grey54
        X11ColorTable(55) = &H8C8C8C ' grey55
        X11ColorTable(56) = &H8F8F8F ' grey56
        X11ColorTable(57) = &H919191 ' grey57
        X11ColorTable(58) = &H949494 ' grey58
        X11ColorTable(59) = &H969696 ' grey59
        X11ColorTable(60) = &H999999 ' grey60
        X11ColorTable(61) = &H9C9C9C ' grey61
        X11ColorTable(62) = &H9E9E9E ' grey62
        X11ColorTable(63) = &HA1A1A1 ' grey63
        X11ColorTable(64) = &HA3A3A3 ' grey64
        X11ColorTable(65) = &HA6A6A6 ' grey65
        X11ColorTable(66) = &HA8A8A8 ' grey66
        X11ColorTable(67) = &HA9A9A9 ' darkgray
        X11ColorTable(68) = &HABABAB ' grey67
        X11ColorTable(69) = &HADADAD ' grey68
        X11ColorTable(70) = &HB0B0B0 ' grey69
        X11ColorTable(71) = &HB3B3B3 ' grey70
        X11ColorTable(72) = &HB5B5B5 ' grey71
        X11ColorTable(73) = &HB8B8B8 ' grey72
        X11ColorTable(74) = &HBABABA ' grey73
        X11ColorTable(75) = &HBDBDBD ' grey74
        X11ColorTable(76) = &HBEBEBE ' grey
        X11ColorTable(77) = &HBFBFBF ' grey75
        X11ColorTable(78) = &HC2C2C2 ' grey76
        X11ColorTable(79) = &HC4C4C4 ' grey77
        X11ColorTable(80) = &HC7C7C7 ' grey78
        X11ColorTable(81) = &HC9C9C9 ' grey79
        X11ColorTable(82) = &HCCCCCC ' grey80
        X11ColorTable(83) = &HCFCFCF ' grey81
        X11ColorTable(84) = &HD1D1D1 ' grey82
        X11ColorTable(85) = &HD3D3D3 ' lightgray
        X11ColorTable(86) = &HD4D4D4 ' grey83
        X11ColorTable(87) = &HD6D6D6 ' grey84
        X11ColorTable(88) = &HD9D9D9 ' grey85
        X11ColorTable(89) = &HDBDBDB ' grey86
        X11ColorTable(90) = &HDCDCDC ' gainsboro
        X11ColorTable(91) = &HDEDEDE ' grey87
        X11ColorTable(92) = &HE0E0E0 ' grey88
        X11ColorTable(93) = &HE3E3E3 ' grey89
        X11ColorTable(94) = &HE5E5E5 ' grey90
        X11ColorTable(95) = &HE8E8E8 ' grey91
        X11ColorTable(96) = &HEBEBEB ' grey92
        X11ColorTable(97) = &HEDEDED ' grey93
        X11ColorTable(98) = &HF0F0F0 ' grey94
        X11ColorTable(99) = &HF2F2F2 ' grey95
        X11ColorTable(100) = &HF5F5F5 ' grey96
        X11ColorTable(101) = &HF7F7F7 ' grey97
        X11ColorTable(102) = &HFAFAFA ' grey98
        X11ColorTable(103) = &HFCFCFC ' grey99
        X11ColorTable(104) = &HFFFFFF ' grey100
        X11ColorTable(105) = &H8B8989 ' snow4
        X11ColorTable(106) = &HCDC9C9 ' snow3
        X11ColorTable(107) = &HEEE9E9 ' snow2
        X11ColorTable(108) = &HFFFAFA ' snow1
        X11ColorTable(109) = &HBC8F8F ' rosybrown
        X11ColorTable(110) = &H8B6969 ' rosybrown4
        X11ColorTable(111) = &HCD9B9B ' rosybrown3
        X11ColorTable(112) = &HEEB4B4 ' rosybrown2
        X11ColorTable(113) = &HFFC1C1 ' rosybrown1
        X11ColorTable(114) = &HF08080 ' lightcoral
        X11ColorTable(115) = &HCD5C5C ' indianred
        X11ColorTable(116) = &H8B3A3A ' indianred4
        X11ColorTable(117) = &HCD5555 ' indianred3
        X11ColorTable(118) = &HEE6363 ' indianred2
        X11ColorTable(119) = &HFF6A6A ' indianred1
        X11ColorTable(120) = &HA52A2A ' brown
        X11ColorTable(121) = &H8B2323 ' brown4
        X11ColorTable(122) = &HCD3333 ' brown3
        X11ColorTable(123) = &HFF4040 ' brown1
        X11ColorTable(124) = &HEE3B3B ' brown2
        X11ColorTable(125) = &HB22222 ' firebrick
        X11ColorTable(126) = &H8B1A1A ' firebrick4
        X11ColorTable(127) = &HCD2626 ' firebrick3
        X11ColorTable(128) = &HFF3030 ' firebrick1
        X11ColorTable(129) = &HEE2C2C ' firebrick2
        X11ColorTable(130) = &H8B0000 ' darkred
        X11ColorTable(131) = &HCD0000 ' red3
        X11ColorTable(132) = &HEE0000 ' red2
        X11ColorTable(133) = &HFF0000 ' red1
        X11ColorTable(134) = &HCDB7B5 ' mistyrose3
        X11ColorTable(135) = &HFFE4E1 ' mistyrose1
        X11ColorTable(136) = &HEED5D2 ' mistyrose2
        X11ColorTable(137) = &HFA8072 ' salmon
        X11ColorTable(138) = &H8B7D7B ' mistyrose4
        X11ColorTable(139) = &HCD4F39 ' tomato3
        X11ColorTable(140) = &HEE5C42 ' tomato2
        X11ColorTable(141) = &HFF6347 ' tomato1
        X11ColorTable(142) = &H8B3626 ' tomato4
        X11ColorTable(143) = &HCD5B45 ' coral3
        X11ColorTable(144) = &H8B3E2F ' coral4
        X11ColorTable(145) = &HEE6A50 ' coral2
        X11ColorTable(146) = &HFF7256 ' coral1
        X11ColorTable(147) = &HEE8262 ' salmon2
        X11ColorTable(148) = &HCD7054 ' salmon3
        X11ColorTable(149) = &H8B4C39 ' salmon4
        X11ColorTable(150) = &HFF8C69 ' salmon1
        X11ColorTable(151) = &HE9967A ' darksalmon
        X11ColorTable(152) = &HFF7F50 ' coral
        X11ColorTable(153) = &H8B2500 ' orangered4
        X11ColorTable(154) = &HCD3700 ' orangered3
        X11ColorTable(155) = &HEE4000 ' orangered2
        X11ColorTable(156) = &HFF4500 ' orangered1
        X11ColorTable(157) = &HEE9572 ' lightsalmon2
        X11ColorTable(158) = &HFFA07A ' lightsalmon1
        X11ColorTable(159) = &H8B5742 ' lightsalmon4
        X11ColorTable(160) = &HCD8162 ' lightsalmon3
        X11ColorTable(161) = &HCD6839 ' sienna3
        X11ColorTable(162) = &HEE7942 ' sienna2
        X11ColorTable(163) = &HFF8247 ' sienna1
        X11ColorTable(164) = &HA0522D ' sienna
        X11ColorTable(165) = &H8B4726 ' sienna4
        X11ColorTable(166) = &HFFF5EE ' seashell1
        X11ColorTable(167) = &HCDC5BF ' seashell3
        X11ColorTable(168) = &HCD661D ' chocolate3
        X11ColorTable(169) = &HEE7621 ' chocolate2
        X11ColorTable(170) = &HFF7F24 ' chocolate1
        X11ColorTable(171) = &H8B4513 ' chocolate4
        X11ColorTable(172) = &HD2691E ' chocolate
        X11ColorTable(173) = &HEEE5DE ' seashell2
        X11ColorTable(174) = &H8B8682 ' seashell4
        X11ColorTable(175) = &HEECBAD ' peachpuff2
        X11ColorTable(176) = &HCDAF95 ' peachpuff3
        X11ColorTable(177) = &HF4A460 ' sandybrown
        X11ColorTable(178) = &HFFDAB9 ' peachpuff1
        X11ColorTable(179) = &H8B7765 ' peachpuff4
        X11ColorTable(180) = &HFAF0E6 ' linen
        X11ColorTable(181) = &HFFA54F ' tan1
        X11ColorTable(182) = &H8B5A2B ' tan4
        X11ColorTable(183) = &HEE9A49 ' tan2
        X11ColorTable(184) = &HCD853F ' tan3
        X11ColorTable(185) = &HEE7600 ' darkorange2
        X11ColorTable(186) = &H8B4500 ' darkorange4
        X11ColorTable(187) = &HCD6600 ' darkorange3
        X11ColorTable(188) = &HFF7F00 ' darkorange1
        X11ColorTable(189) = &HCDB79E ' bisque3
        X11ColorTable(190) = &HFFE4C4 ' bisque1
        X11ColorTable(191) = &HEED5B7 ' bisque2
        X11ColorTable(192) = &HCDC0B0 ' antiquewhite3
        X11ColorTable(193) = &HFFEFDB ' antiquewhite1
        X11ColorTable(194) = &HEEDFCC ' antiquewhite2
        X11ColorTable(195) = &H8B7355 ' burlywood4
        X11ColorTable(196) = &H8B7D6B ' bisque4
        X11ColorTable(197) = &HEEC591 ' burlywood2
        X11ColorTable(198) = &HFFD39B ' burlywood1
        X11ColorTable(199) = &HCDAA7D ' burlywood3
        X11ColorTable(200) = &HDEB887 ' burlywood
        X11ColorTable(201) = &HFF8C00 ' darkorange
        X11ColorTable(202) = &HFAEBD7 ' antiquewhite
        X11ColorTable(203) = &HD2B48C ' tan
        X11ColorTable(204) = &H8B8378 ' antiquewhite4
        X11ColorTable(205) = &HFFEBCD ' blanchedalmond
        X11ColorTable(206) = &HEECFA1 ' navajowhite2
        X11ColorTable(207) = &HFFDEAD ' navajowhite1
        X11ColorTable(208) = &H8B795E ' navajowhite4
        X11ColorTable(209) = &HCDB38B ' navajowhite3
        X11ColorTable(210) = &HFFEFD5 ' papayawhip
        X11ColorTable(211) = &HFFE4B5 ' moccasin
        X11ColorTable(212) = &HFDF5E6 ' oldlace
        X11ColorTable(213) = &H8B7E66 ' wheat4
        X11ColorTable(214) = &HF5DEB3 ' wheat
        X11ColorTable(215) = &HFFE7BA ' wheat1
        X11ColorTable(216) = &HCDBA96 ' wheat3
        X11ColorTable(217) = &HEED8AE ' wheat2
        X11ColorTable(218) = &HFFFAF0 ' floralwhite
        X11ColorTable(219) = &HEE9A00 ' orange2
        X11ColorTable(220) = &HFFA500 ' orange1
        X11ColorTable(221) = &H8B5A00 ' orange4
        X11ColorTable(222) = &HCD8500 ' orange3
        X11ColorTable(223) = &HFFB90F ' darkgoldenrod1
        X11ColorTable(224) = &H8B6508 ' darkgoldenrod4
        X11ColorTable(225) = &HCD950C ' darkgoldenrod3
        X11ColorTable(226) = &HEEAD0E ' darkgoldenrod2
        X11ColorTable(227) = &HB8860B ' darkgoldenrod
        X11ColorTable(228) = &H8B6914 ' goldenrod4
        X11ColorTable(229) = &HDAA520 ' goldenrod
        X11ColorTable(230) = &HFFC125 ' goldenrod1
        X11ColorTable(231) = &HEEB422 ' goldenrod2
        X11ColorTable(232) = &HCD9B1D ' goldenrod3
        X11ColorTable(233) = &HFFF8DC ' cornsilk1
        X11ColorTable(234) = &HEEE8CD ' cornsilk2
        X11ColorTable(235) = &HCDC8B1 ' cornsilk3
        X11ColorTable(236) = &HEEDC82 ' lightgoldenrod2
        X11ColorTable(237) = &H8B8878 ' cornsilk4
        X11ColorTable(238) = &HFFEC8B ' lightgoldenrod1
        X11ColorTable(239) = &HCDBE70 ' lightgoldenrod3
        X11ColorTable(240) = &H8B814C ' lightgoldenrod4
        X11ColorTable(241) = &HEEDD82 ' lightgoldenrod
        X11ColorTable(242) = &H8B7500 ' gold4
        X11ColorTable(243) = &HFFD700 ' gold1
        X11ColorTable(244) = &HCDAD00 ' gold3
        X11ColorTable(245) = &HEEC900 ' gold2
        X11ColorTable(246) = &HEEE9BF ' lemonchiffon2
        X11ColorTable(247) = &HCDC9A5 ' lemonchiffon3
        X11ColorTable(248) = &HFFFACD ' lemonchiffon1
        X11ColorTable(249) = &HF0E68C ' khaki
        X11ColorTable(250) = &HEEE8AA ' palegoldenrod
        X11ColorTable(251) = &H8B864E ' khaki4
        X11ColorTable(252) = &HFFF68F ' khaki1
        X11ColorTable(253) = &H8B8970 ' lemonchiffon4
        X11ColorTable(254) = &HCDC673 ' khaki3
        X11ColorTable(255) = &HEEE685 ' khaki2
        X11ColorTable(256) = &HBDB76B ' darkkhaki
        X11ColorTable(257) = &H8B8B83 ' ivory4
        X11ColorTable(258) = &HCDCDC1 ' ivory3
        X11ColorTable(259) = &HEEEEE0 ' ivory2
        X11ColorTable(260) = &HFFFFF0 ' ivory1
        X11ColorTable(261) = &HF5F5DC ' beige
        X11ColorTable(262) = &H8B8B7A ' lightyellow4
        X11ColorTable(263) = &HCDCDB4 ' lightyellow3
        X11ColorTable(264) = &HEEEED1 ' lightyellow2
        X11ColorTable(265) = &HFFFFE0 ' lightyellow1
        X11ColorTable(266) = &HFAFAD2 ' lightgoldenrodyellow
        X11ColorTable(267) = &H8B8B00 ' yellow4
        X11ColorTable(268) = &HCDCD00 ' yellow3
        X11ColorTable(269) = &HEEEE00 ' yellow2
        X11ColorTable(270) = &HFFFF00 ' yellow1
        X11ColorTable(271) = &H698B22 ' olivedrab4
        X11ColorTable(272) = &HC0FF3E ' olivedrab1
        X11ColorTable(273) = &H6B8E23 ' olivedrab
        X11ColorTable(274) = &HB3EE3A ' olivedrab2
        X11ColorTable(275) = &H9ACD32 ' olivedrab3
        X11ColorTable(276) = &H556B2F ' darkolivegreen
        X11ColorTable(277) = &HCAFF70 ' darkolivegreen1
        X11ColorTable(278) = &H6E8B3D ' darkolivegreen4
        X11ColorTable(279) = &HBCEE68 ' darkolivegreen2
        X11ColorTable(280) = &HA2CD5A ' darkolivegreen3
        X11ColorTable(281) = &HADFF2F ' greenyellow
        X11ColorTable(282) = &H7FFF00 ' chartreuse1
        X11ColorTable(283) = &H66CD00 ' chartreuse3
        X11ColorTable(284) = &H458B00 ' chartreuse4
        X11ColorTable(285) = &H76EE00 ' chartreuse2
        X11ColorTable(286) = &H7CFC00 ' lawngreen
        X11ColorTable(287) = &H838B83 ' honeydew4
        X11ColorTable(288) = &HC1CDC1 ' honeydew3
        X11ColorTable(289) = &HE0EEE0 ' honeydew2
        X11ColorTable(290) = &HF0FFF0 ' honeydew1
        X11ColorTable(291) = &H8FBC8F ' darkseagreen
        X11ColorTable(292) = &H698B69 ' darkseagreen4
        X11ColorTable(293) = &H9BCD9B ' darkseagreen3
        X11ColorTable(294) = &HB4EEB4 ' darkseagreen2
        X11ColorTable(295) = &HC1FFC1 ' darkseagreen1
        X11ColorTable(296) = &H548B54 ' palegreen4
        X11ColorTable(297) = &H7CCD7C ' palegreen3
        X11ColorTable(298) = &H90EE90 ' lightgreen
        X11ColorTable(299) = &H98FB98 ' palegreen
        X11ColorTable(300) = &H9AFF9A ' palegreen1
        X11ColorTable(301) = &H228B22 ' forestgreen
        X11ColorTable(302) = &H32CD32 ' limegreen
        X11ColorTable(303) = &H6400S ' darkgreen
        X11ColorTable(304) = &H8B00S ' green4
        X11ColorTable(305) = &HCD00S ' green3
        X11ColorTable(306) = &HEE00S ' green2
        X11ColorTable(307) = &HFF00S ' green1
        X11ColorTable(308) = &H4EEE94 ' seagreen2
        X11ColorTable(309) = &H54FF9F ' seagreen1
        X11ColorTable(310) = &H2E8B57 ' seagreen4
        X11ColorTable(311) = &H43CD80 ' seagreen3
        X11ColorTable(312) = &H3CB371 ' mediumseagreen
        X11ColorTable(313) = &HF5FFFA ' mintcream
        X11ColorTable(314) = &HEE76S ' springgreen2
        X11ColorTable(315) = &H8B45S ' springgreen4
        X11ColorTable(316) = &HCD66S ' springgreen3
        X11ColorTable(317) = &HFF7FS ' springgreen1
        X11ColorTable(318) = &HFA9AS ' mediumspringgreen
        X11ColorTable(319) = &H66CDAA ' aquamarine3
        X11ColorTable(320) = &H7FFFD4 ' aquamarine1
        X11ColorTable(321) = &H76EEC6 ' aquamarine2
        X11ColorTable(322) = &H458B74 ' aquamarine4
        X11ColorTable(323) = &H40E0D0 ' turquoise
        X11ColorTable(324) = &H20B2AA ' lightseagreen
        X11ColorTable(325) = &H48D1CC ' mediumturquoise
        X11ColorTable(326) = &H838B8B ' azure4
        X11ColorTable(327) = &HC1CDCD ' azure3
        X11ColorTable(328) = &HE0EEEE ' azure2
        X11ColorTable(329) = &HF0FFFF ' azure1
        X11ColorTable(330) = &H7A8B8B ' lightcyan4
        X11ColorTable(331) = &HB4CDCD ' lightcyan3
        X11ColorTable(332) = &HD1EEEE ' lightcyan2
        X11ColorTable(333) = &HE0FFFF ' lightcyan1
        X11ColorTable(334) = &H668B8B ' paleturquoise4
        X11ColorTable(335) = &HAFEEEE ' paleturquoise
        X11ColorTable(336) = &H96CDCD ' paleturquoise3
        X11ColorTable(337) = &HBBFFFF ' paleturquoise1
        X11ColorTable(338) = &HAEEEEE ' paleturquoise2
        X11ColorTable(339) = &H2F4F4F ' darkslategrey
        X11ColorTable(340) = &H528B8B ' darkslategray4
        X11ColorTable(341) = &H79CDCD ' darkslategray3
        X11ColorTable(342) = &H8DEEEE ' darkslategray2
        X11ColorTable(343) = &H97FFFF ' darkslategray1
        X11ColorTable(344) = &H8B8BS ' darkcyan
        X11ColorTable(345) = &HCDCDS ' cyan3
        X11ColorTable(346) = &HEEEES ' cyan2
        X11ColorTable(347) = &HFFFFS ' cyan1
        X11ColorTable(348) = &HCED1S ' darkturquoise
        X11ColorTable(349) = &H5F9EA0 ' cadetblue
        X11ColorTable(350) = &H868BS ' turquoise4
        X11ColorTable(351) = &HE5EES ' turquoise2
        X11ColorTable(352) = &HC5CDS ' turquoise3
        X11ColorTable(353) = &HF5FFS ' turquoise1
        X11ColorTable(354) = &H53868B ' cadetblue4
        X11ColorTable(355) = &H8EE5EE ' cadetblue2
        X11ColorTable(356) = &H7AC5CD ' cadetblue3
        X11ColorTable(357) = &H98F5FF ' cadetblue1
        X11ColorTable(358) = &HB0E0E6 ' powderblue
        X11ColorTable(359) = &H68838B ' lightblue4
        X11ColorTable(360) = &HADD8E6 ' lightblue
        X11ColorTable(361) = &HB2DFEE ' lightblue2
        X11ColorTable(362) = &HBFEFFF ' lightblue1
        X11ColorTable(363) = &H9AC0CD ' lightblue3
        X11ColorTable(364) = &H9ACDS ' deepskyblue3
        X11ColorTable(365) = &HBFFFS ' deepskyblue1
        X11ColorTable(366) = &H688BS ' deepskyblue4
        X11ColorTable(367) = &HB2EES ' deepskyblue2
        X11ColorTable(368) = &H87CEEB ' skyblue
        X11ColorTable(369) = &H8DB6CD ' lightskyblue3
        X11ColorTable(370) = &HA4D3EE ' lightskyblue2
        X11ColorTable(371) = &HB0E2FF ' lightskyblue1
        X11ColorTable(372) = &H607B8B ' lightskyblue4
        X11ColorTable(373) = &H87CEFA ' lightskyblue
        X11ColorTable(374) = &H6CA6CD ' skyblue3
        X11ColorTable(375) = &H87CEFF ' skyblue1
        X11ColorTable(376) = &H7EC0EE ' skyblue2
        X11ColorTable(377) = &H4A708B ' skyblue4
        X11ColorTable(378) = &H5CACEE ' steelblue2
        X11ColorTable(379) = &H4F94CD ' steelblue3
        X11ColorTable(380) = &HF0F8FF ' aliceblue
        X11ColorTable(381) = &H4682B4 ' steelblue
        X11ColorTable(382) = &H63B8FF ' steelblue1
        X11ColorTable(383) = &H36648B ' steelblue4
        X11ColorTable(384) = &H708090 ' slategrey
        X11ColorTable(385) = &H778899 ' lightslategrey
        X11ColorTable(386) = &H9FB6CD ' slategray3
        X11ColorTable(387) = &H1874CD ' dodgerblue3
        X11ColorTable(388) = &HC6E2FF ' slategray1
        X11ColorTable(389) = &H1E90FF ' dodgerblue1
        X11ColorTable(390) = &HB9D3EE ' slategray2
        X11ColorTable(391) = &H1C86EE ' dodgerblue2
        X11ColorTable(392) = &H104E8B ' dodgerblue4
        X11ColorTable(393) = &H6C7B8B ' slategray4
        X11ColorTable(394) = &H6E7B8B ' lightsteelblue4
        X11ColorTable(395) = &HA2B5CD ' lightsteelblue3
        X11ColorTable(396) = &HBCD2EE ' lightsteelblue2
        X11ColorTable(397) = &HB0C4DE ' lightsteelblue
        X11ColorTable(398) = &HCAE1FF ' lightsteelblue1
        X11ColorTable(399) = &H6495ED ' cornflowerblue
        X11ColorTable(400) = &H3A5FCD ' royalblue3
        X11ColorTable(401) = &H436EEE ' royalblue2
        X11ColorTable(402) = &H4876FF ' royalblue1
        X11ColorTable(403) = &H27408B ' royalblue4
        X11ColorTable(404) = &H4169E1 ' royalblue
        X11ColorTable(405) = &HF8F8FF ' ghostwhite
        X11ColorTable(406) = &HE6E6FA ' lavender
        X11ColorTable(407) = &H191970 ' midnightblue
        X11ColorTable(408) = &H80S ' navyblue
        X11ColorTable(409) = &H8BS ' darkblue
        X11ColorTable(410) = &HCDS ' blue3
        X11ColorTable(411) = &HEES ' blue2
        X11ColorTable(412) = &HFFS ' blue1
        X11ColorTable(413) = &H6959CD ' slateblue3
        X11ColorTable(414) = &H836FFF ' slateblue1
        X11ColorTable(415) = &H6A5ACD ' slateblue
        X11ColorTable(416) = &H473C8B ' slateblue4
        X11ColorTable(417) = &H8470FF ' lightslateblue
        X11ColorTable(418) = &H483D8B ' darkslateblue
        X11ColorTable(419) = &H7A67EE ' slateblue2
        X11ColorTable(420) = &H7B68EE ' mediumslateblue
        X11ColorTable(421) = &H5D478B ' mediumpurple4
        X11ColorTable(422) = &H9F79EE ' mediumpurple2
        X11ColorTable(423) = &H8968CD ' mediumpurple3
        X11ColorTable(424) = &H9370DB ' mediumpurple
        X11ColorTable(425) = &HAB82FF ' mediumpurple1
        X11ColorTable(426) = &H9B30FF ' purple1
        X11ColorTable(427) = &H8A2BE2 ' blueviolet
        X11ColorTable(428) = &H912CEE ' purple2
        X11ColorTable(429) = &H7D26CD ' purple3
        X11ColorTable(430) = &H551A8B ' purple4
        X11ColorTable(431) = &HA020F0 ' purple
        X11ColorTable(432) = &H68228B ' darkorchid4
        X11ColorTable(433) = &HB23AEE ' darkorchid2
        X11ColorTable(434) = &HBF3EFF ' darkorchid1
        X11ColorTable(435) = &H9932CC ' darkorchid
        X11ColorTable(436) = &H9A32CD ' darkorchid3
        X11ColorTable(437) = &H9400D3 ' darkviolet
        X11ColorTable(438) = &HB452CD ' mediumorchid3
        X11ColorTable(439) = &HD15FEE ' mediumorchid2
        X11ColorTable(440) = &HE066FF ' mediumorchid1
        X11ColorTable(441) = &H7A378B ' mediumorchid4
        X11ColorTable(442) = &HBA55D3 ' mediumorchid
        X11ColorTable(443) = &H8B7B8B ' thistle4
        X11ColorTable(444) = &HD8BFD8 ' thistle
        X11ColorTable(445) = &HCDB5CD ' thistle3
        X11ColorTable(446) = &HEED2EE ' thistle2
        X11ColorTable(447) = &HFFE1FF ' thistle1
        X11ColorTable(448) = &H8B668B ' plum4
        X11ColorTable(449) = &HCD96CD ' plum3
        X11ColorTable(450) = &HFFBBFF ' plum1
        X11ColorTable(451) = &HEEAEEE ' plum2
        X11ColorTable(452) = &HDDA0DD ' plum
        X11ColorTable(453) = &HEE82EE ' violet
        X11ColorTable(454) = &H8B008B ' darkmagenta
        X11ColorTable(455) = &HCD00CD ' magenta3
        X11ColorTable(456) = &HEE00EE ' magenta2
        X11ColorTable(457) = &HFF00FF ' magenta1
        X11ColorTable(458) = &H8B4789 ' orchid4
        X11ColorTable(459) = &HDA70D6 ' orchid
        X11ColorTable(460) = &HCD69C9 ' orchid3
        X11ColorTable(461) = &HFF83FA ' orchid1
        X11ColorTable(462) = &HEE7AE9 ' orchid2
        X11ColorTable(463) = &HD02090 ' violetred
        X11ColorTable(464) = &H8B1C62 ' maroon4
        X11ColorTable(465) = &HCD2990 ' maroon3
        X11ColorTable(466) = &HC71585 ' mediumvioletred
        X11ColorTable(467) = &HEE30A7 ' maroon2
        X11ColorTable(468) = &HFF34B3 ' maroon1
        X11ColorTable(469) = &H8B0A50 ' deeppink4
        X11ColorTable(470) = &HEE1289 ' deeppink2
        X11ColorTable(471) = &HFF1493 ' deeppink1
        X11ColorTable(472) = &HCD1076 ' deeppink3
        X11ColorTable(473) = &HFF69B4 ' hotpink
        X11ColorTable(474) = &H8B3A62 ' hotpink4
        X11ColorTable(475) = &HFF6EB4 ' hotpink1
        X11ColorTable(476) = &HEE6AA7 ' hotpink2
        X11ColorTable(477) = &H8B2252 ' violetred4
        X11ColorTable(478) = &HFF3E96 ' violetred1
        X11ColorTable(479) = &HEE3A8C ' violetred2
        X11ColorTable(480) = &HCD3278 ' violetred3
        X11ColorTable(481) = &HCD6090 ' hotpink3
        X11ColorTable(482) = &H8B8386 ' lavenderblush4
        X11ColorTable(483) = &HB03060 ' maroon
        X11ColorTable(484) = &HEEE0E5 ' lavenderblush2
        X11ColorTable(485) = &HCDC1C5 ' lavenderblush3
        X11ColorTable(486) = &HFFF0F5 ' lavenderblush1
        X11ColorTable(487) = &HFF82AB ' palevioletred1
        X11ColorTable(488) = &HDB7093 ' palevioletred
        X11ColorTable(489) = &HCD6889 ' palevioletred3
        X11ColorTable(490) = &HEE799F ' palevioletred2
        X11ColorTable(491) = &H8B475D ' palevioletred4
        X11ColorTable(492) = &H8B636C ' pink4
        X11ColorTable(493) = &HEEA9B8 ' pink2
        X11ColorTable(494) = &HCD919E ' pink3
        X11ColorTable(495) = &HFFB5C5 ' pink1
        X11ColorTable(496) = &HFFC0CB ' pink
        X11ColorTable(497) = &HFFB6C1 ' lightpink
        X11ColorTable(498) = &HEEA2AD ' lightpink2
        X11ColorTable(499) = &HCD8C95 ' lightpink3
        X11ColorTable(500) = &H8B5F65 ' lightpink4
        X11ColorTable(501) = &HFFAEB9 ' lightpink1

    End Sub
End Module