<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDemographics
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lstVillages As System.Windows.Forms.ListBox
	Public WithEvents lstTowns As System.Windows.Forms.ListBox
	Public WithEvents lstCities As System.Windows.Forms.ListBox
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents acastles As System.Windows.Forms.TextBox
	Public WithEvents rcastles As System.Windows.Forms.TextBox
	Public WithEvents dcities As System.Windows.Forms.TextBox
	Public WithEvents dtowns As System.Windows.Forms.TextBox
	Public WithEvents dvillage As System.Windows.Forms.TextBox
	Public WithEvents farmland As System.Windows.Forms.TextBox
	Public WithEvents citynum As System.Windows.Forms.TextBox
	Public WithEvents lcitypop3 As System.Windows.Forms.TextBox
	Public WithEvents lcitypop2 As System.Windows.Forms.TextBox
	Public WithEvents lcitypop As System.Windows.Forms.TextBox
	Public WithEvents citypop As System.Windows.Forms.TextBox
	Public WithEvents wandpop As System.Windows.Forms.TextBox
	Public WithEvents towns As System.Windows.Forms.TextBox
	Public WithEvents townpop As System.Windows.Forms.TextBox
	Public WithEvents villages As System.Windows.Forms.TextBox
	Public WithEvents villagepop As System.Windows.Forms.TextBox
	Public WithEvents population As System.Windows.Forms.TextBox
	Public WithEvents age As System.Windows.Forms.TextBox
	Public WithEvents density As System.Windows.Forms.TextBox
	Public WithEvents kingarea As System.Windows.Forms.TextBox
	Public WithEvents cmdCalculate As System.Windows.Forms.Button
	Public WithEvents lblDistricts3 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents lblDistricts2 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents lblDistricts As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents _Label4_16 As System.Windows.Forms.Label
	Public WithEvents _Label4_15 As System.Windows.Forms.Label
	Public WithEvents _Label4_14 As System.Windows.Forms.Label
	Public WithEvents _Label4_13 As System.Windows.Forms.Label
	Public WithEvents _Label4_12 As System.Windows.Forms.Label
	Public WithEvents _Label4_11 As System.Windows.Forms.Label
	Public WithEvents _Label4_10 As System.Windows.Forms.Label
	Public WithEvents _Label4_9 As System.Windows.Forms.Label
	Public WithEvents _Label4_8 As System.Windows.Forms.Label
	Public WithEvents _Label4_7 As System.Windows.Forms.Label
	Public WithEvents _Label4_6 As System.Windows.Forms.Label
	Public WithEvents _Label4_5 As System.Windows.Forms.Label
	Public WithEvents _Label4_4 As System.Windows.Forms.Label
	Public WithEvents _Label4_3 As System.Windows.Forms.Label
	Public WithEvents _Label4_2 As System.Windows.Forms.Label
	Public WithEvents _Label4_1 As System.Windows.Forms.Label
	Public WithEvents _Label4_0 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label4 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDemographics))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me.lstVillages = New System.Windows.Forms.ListBox
		Me.lstTowns = New System.Windows.Forms.ListBox
		Me.lstCities = New System.Windows.Forms.ListBox
		Me.acastles = New System.Windows.Forms.TextBox
		Me.rcastles = New System.Windows.Forms.TextBox
		Me.dcities = New System.Windows.Forms.TextBox
		Me.dtowns = New System.Windows.Forms.TextBox
		Me.dvillage = New System.Windows.Forms.TextBox
		Me.farmland = New System.Windows.Forms.TextBox
		Me.citynum = New System.Windows.Forms.TextBox
		Me.lcitypop3 = New System.Windows.Forms.TextBox
		Me.lcitypop2 = New System.Windows.Forms.TextBox
		Me.lcitypop = New System.Windows.Forms.TextBox
		Me.citypop = New System.Windows.Forms.TextBox
		Me.wandpop = New System.Windows.Forms.TextBox
		Me.towns = New System.Windows.Forms.TextBox
		Me.townpop = New System.Windows.Forms.TextBox
		Me.villages = New System.Windows.Forms.TextBox
		Me.villagepop = New System.Windows.Forms.TextBox
		Me.population = New System.Windows.Forms.TextBox
		Me.age = New System.Windows.Forms.TextBox
		Me.density = New System.Windows.Forms.TextBox
		Me.kingarea = New System.Windows.Forms.TextBox
		Me.cmdCalculate = New System.Windows.Forms.Button
		Me.lblDistricts3 = New System.Windows.Forms.Label
		Me.Label8 = New System.Windows.Forms.Label
		Me.lblDistricts2 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.lblDistricts = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me._Label4_16 = New System.Windows.Forms.Label
		Me._Label4_15 = New System.Windows.Forms.Label
		Me._Label4_14 = New System.Windows.Forms.Label
		Me._Label4_13 = New System.Windows.Forms.Label
		Me._Label4_12 = New System.Windows.Forms.Label
		Me._Label4_11 = New System.Windows.Forms.Label
		Me._Label4_10 = New System.Windows.Forms.Label
		Me._Label4_9 = New System.Windows.Forms.Label
		Me._Label4_8 = New System.Windows.Forms.Label
		Me._Label4_7 = New System.Windows.Forms.Label
		Me._Label4_6 = New System.Windows.Forms.Label
		Me._Label4_5 = New System.Windows.Forms.Label
		Me._Label4_4 = New System.Windows.Forms.Label
		Me._Label4_3 = New System.Windows.Forms.Label
		Me._Label4_2 = New System.Windows.Forms.Label
		Me._Label4_1 = New System.Windows.Forms.Label
		Me._Label4_0 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label4 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.Frame1.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "Kingdom Demographics"
		Me.ClientSize = New System.Drawing.Size(645, 450)
		Me.Location = New System.Drawing.Point(3, 29)
		Me.Icon = CType(resources.GetObject("frmDemographics.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDemographics"
		Me.Frame1.Text = "Cities and Towns"
		Me.Frame1.Size = New System.Drawing.Size(301, 441)
		Me.Frame1.Location = New System.Drawing.Point(340, 4)
		Me.Frame1.TabIndex = 47
		Me.Frame1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Name = "Frame1"
		Me.lstVillages.Size = New System.Drawing.Size(129, 228)
		Me.lstVillages.Location = New System.Drawing.Point(156, 196)
		Me.lstVillages.TabIndex = 50
		Me.lstVillages.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstVillages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstVillages.BackColor = System.Drawing.SystemColors.Window
		Me.lstVillages.CausesValidation = True
		Me.lstVillages.Enabled = True
		Me.lstVillages.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstVillages.IntegralHeight = True
		Me.lstVillages.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstVillages.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstVillages.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstVillages.Sorted = False
		Me.lstVillages.TabStop = True
		Me.lstVillages.Visible = True
		Me.lstVillages.MultiColumn = False
		Me.lstVillages.Name = "lstVillages"
		Me.lstTowns.Size = New System.Drawing.Size(129, 228)
		Me.lstTowns.Location = New System.Drawing.Point(8, 196)
		Me.lstTowns.TabIndex = 49
		Me.lstTowns.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstTowns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstTowns.BackColor = System.Drawing.SystemColors.Window
		Me.lstTowns.CausesValidation = True
		Me.lstTowns.Enabled = True
		Me.lstTowns.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstTowns.IntegralHeight = True
		Me.lstTowns.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstTowns.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstTowns.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstTowns.Sorted = False
		Me.lstTowns.TabStop = True
		Me.lstTowns.Visible = True
		Me.lstTowns.MultiColumn = False
		Me.lstTowns.Name = "lstTowns"
		Me.lstCities.Size = New System.Drawing.Size(281, 176)
		Me.lstCities.Location = New System.Drawing.Point(8, 16)
		Me.lstCities.TabIndex = 48
		Me.lstCities.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstCities.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstCities.BackColor = System.Drawing.SystemColors.Window
		Me.lstCities.CausesValidation = True
		Me.lstCities.Enabled = True
		Me.lstCities.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstCities.IntegralHeight = True
		Me.lstCities.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstCities.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstCities.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstCities.Sorted = False
		Me.lstCities.TabStop = True
		Me.lstCities.Visible = True
		Me.lstCities.MultiColumn = False
		Me.lstCities.Name = "lstCities"
		Me.acastles.AutoSize = False
		Me.acastles.Enabled = False
		Me.acastles.Size = New System.Drawing.Size(73, 17)
		Me.acastles.Location = New System.Drawing.Point(176, 424)
		Me.acastles.TabIndex = 40
		Me.acastles.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.acastles.AcceptsReturn = True
		Me.acastles.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.acastles.BackColor = System.Drawing.SystemColors.Window
		Me.acastles.CausesValidation = True
		Me.acastles.ForeColor = System.Drawing.SystemColors.WindowText
		Me.acastles.HideSelection = True
		Me.acastles.ReadOnly = False
		Me.acastles.Maxlength = 0
		Me.acastles.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.acastles.MultiLine = False
		Me.acastles.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.acastles.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.acastles.TabStop = True
		Me.acastles.Visible = True
		Me.acastles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.acastles.Name = "acastles"
		Me.rcastles.AutoSize = False
		Me.rcastles.Enabled = False
		Me.rcastles.Size = New System.Drawing.Size(73, 17)
		Me.rcastles.Location = New System.Drawing.Point(176, 404)
		Me.rcastles.TabIndex = 39
		Me.rcastles.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.rcastles.AcceptsReturn = True
		Me.rcastles.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.rcastles.BackColor = System.Drawing.SystemColors.Window
		Me.rcastles.CausesValidation = True
		Me.rcastles.ForeColor = System.Drawing.SystemColors.WindowText
		Me.rcastles.HideSelection = True
		Me.rcastles.ReadOnly = False
		Me.rcastles.Maxlength = 0
		Me.rcastles.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.rcastles.MultiLine = False
		Me.rcastles.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.rcastles.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.rcastles.TabStop = True
		Me.rcastles.Visible = True
		Me.rcastles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.rcastles.Name = "rcastles"
		Me.dcities.AutoSize = False
		Me.dcities.Enabled = False
		Me.dcities.Size = New System.Drawing.Size(73, 17)
		Me.dcities.Location = New System.Drawing.Point(176, 384)
		Me.dcities.TabIndex = 38
		Me.dcities.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.dcities.AcceptsReturn = True
		Me.dcities.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.dcities.BackColor = System.Drawing.SystemColors.Window
		Me.dcities.CausesValidation = True
		Me.dcities.ForeColor = System.Drawing.SystemColors.WindowText
		Me.dcities.HideSelection = True
		Me.dcities.ReadOnly = False
		Me.dcities.Maxlength = 0
		Me.dcities.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.dcities.MultiLine = False
		Me.dcities.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.dcities.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.dcities.TabStop = True
		Me.dcities.Visible = True
		Me.dcities.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.dcities.Name = "dcities"
		Me.dtowns.AutoSize = False
		Me.dtowns.Enabled = False
		Me.dtowns.Size = New System.Drawing.Size(73, 17)
		Me.dtowns.Location = New System.Drawing.Point(176, 364)
		Me.dtowns.TabIndex = 37
		Me.dtowns.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.dtowns.AcceptsReturn = True
		Me.dtowns.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.dtowns.BackColor = System.Drawing.SystemColors.Window
		Me.dtowns.CausesValidation = True
		Me.dtowns.ForeColor = System.Drawing.SystemColors.WindowText
		Me.dtowns.HideSelection = True
		Me.dtowns.ReadOnly = False
		Me.dtowns.Maxlength = 0
		Me.dtowns.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.dtowns.MultiLine = False
		Me.dtowns.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.dtowns.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.dtowns.TabStop = True
		Me.dtowns.Visible = True
		Me.dtowns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.dtowns.Name = "dtowns"
		Me.dvillage.AutoSize = False
		Me.dvillage.Enabled = False
		Me.dvillage.Size = New System.Drawing.Size(73, 17)
		Me.dvillage.Location = New System.Drawing.Point(176, 344)
		Me.dvillage.TabIndex = 36
		Me.dvillage.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.dvillage.AcceptsReturn = True
		Me.dvillage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.dvillage.BackColor = System.Drawing.SystemColors.Window
		Me.dvillage.CausesValidation = True
		Me.dvillage.ForeColor = System.Drawing.SystemColors.WindowText
		Me.dvillage.HideSelection = True
		Me.dvillage.ReadOnly = False
		Me.dvillage.Maxlength = 0
		Me.dvillage.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.dvillage.MultiLine = False
		Me.dvillage.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.dvillage.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.dvillage.TabStop = True
		Me.dvillage.Visible = True
		Me.dvillage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.dvillage.Name = "dvillage"
		Me.farmland.AutoSize = False
		Me.farmland.Enabled = False
		Me.farmland.Size = New System.Drawing.Size(73, 17)
		Me.farmland.Location = New System.Drawing.Point(176, 324)
		Me.farmland.TabIndex = 35
		Me.farmland.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.farmland.AcceptsReturn = True
		Me.farmland.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.farmland.BackColor = System.Drawing.SystemColors.Window
		Me.farmland.CausesValidation = True
		Me.farmland.ForeColor = System.Drawing.SystemColors.WindowText
		Me.farmland.HideSelection = True
		Me.farmland.ReadOnly = False
		Me.farmland.Maxlength = 0
		Me.farmland.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.farmland.MultiLine = False
		Me.farmland.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.farmland.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.farmland.TabStop = True
		Me.farmland.Visible = True
		Me.farmland.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.farmland.Name = "farmland"
		Me.citynum.AutoSize = False
		Me.citynum.Enabled = False
		Me.citynum.Size = New System.Drawing.Size(73, 17)
		Me.citynum.Location = New System.Drawing.Point(176, 304)
		Me.citynum.TabIndex = 34
		Me.citynum.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.citynum.AcceptsReturn = True
		Me.citynum.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.citynum.BackColor = System.Drawing.SystemColors.Window
		Me.citynum.CausesValidation = True
		Me.citynum.ForeColor = System.Drawing.SystemColors.WindowText
		Me.citynum.HideSelection = True
		Me.citynum.ReadOnly = False
		Me.citynum.Maxlength = 0
		Me.citynum.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.citynum.MultiLine = False
		Me.citynum.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.citynum.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.citynum.TabStop = True
		Me.citynum.Visible = True
		Me.citynum.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.citynum.Name = "citynum"
		Me.lcitypop3.AutoSize = False
		Me.lcitypop3.Enabled = False
		Me.lcitypop3.Size = New System.Drawing.Size(73, 17)
		Me.lcitypop3.Location = New System.Drawing.Point(176, 284)
		Me.lcitypop3.TabIndex = 33
		Me.lcitypop3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lcitypop3.AcceptsReturn = True
		Me.lcitypop3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.lcitypop3.BackColor = System.Drawing.SystemColors.Window
		Me.lcitypop3.CausesValidation = True
		Me.lcitypop3.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lcitypop3.HideSelection = True
		Me.lcitypop3.ReadOnly = False
		Me.lcitypop3.Maxlength = 0
		Me.lcitypop3.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.lcitypop3.MultiLine = False
		Me.lcitypop3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lcitypop3.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.lcitypop3.TabStop = True
		Me.lcitypop3.Visible = True
		Me.lcitypop3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lcitypop3.Name = "lcitypop3"
		Me.lcitypop2.AutoSize = False
		Me.lcitypop2.Enabled = False
		Me.lcitypop2.Size = New System.Drawing.Size(73, 17)
		Me.lcitypop2.Location = New System.Drawing.Point(176, 264)
		Me.lcitypop2.TabIndex = 32
		Me.lcitypop2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lcitypop2.AcceptsReturn = True
		Me.lcitypop2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.lcitypop2.BackColor = System.Drawing.SystemColors.Window
		Me.lcitypop2.CausesValidation = True
		Me.lcitypop2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lcitypop2.HideSelection = True
		Me.lcitypop2.ReadOnly = False
		Me.lcitypop2.Maxlength = 0
		Me.lcitypop2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.lcitypop2.MultiLine = False
		Me.lcitypop2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lcitypop2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.lcitypop2.TabStop = True
		Me.lcitypop2.Visible = True
		Me.lcitypop2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lcitypop2.Name = "lcitypop2"
		Me.lcitypop.AutoSize = False
		Me.lcitypop.Enabled = False
		Me.lcitypop.Size = New System.Drawing.Size(73, 17)
		Me.lcitypop.Location = New System.Drawing.Point(176, 244)
		Me.lcitypop.TabIndex = 31
		Me.lcitypop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lcitypop.AcceptsReturn = True
		Me.lcitypop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.lcitypop.BackColor = System.Drawing.SystemColors.Window
		Me.lcitypop.CausesValidation = True
		Me.lcitypop.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lcitypop.HideSelection = True
		Me.lcitypop.ReadOnly = False
		Me.lcitypop.Maxlength = 0
		Me.lcitypop.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.lcitypop.MultiLine = False
		Me.lcitypop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lcitypop.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.lcitypop.TabStop = True
		Me.lcitypop.Visible = True
		Me.lcitypop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lcitypop.Name = "lcitypop"
		Me.citypop.AutoSize = False
		Me.citypop.Enabled = False
		Me.citypop.Size = New System.Drawing.Size(93, 17)
		Me.citypop.Location = New System.Drawing.Point(176, 224)
		Me.citypop.TabIndex = 30
		Me.citypop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.citypop.AcceptsReturn = True
		Me.citypop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.citypop.BackColor = System.Drawing.SystemColors.Window
		Me.citypop.CausesValidation = True
		Me.citypop.ForeColor = System.Drawing.SystemColors.WindowText
		Me.citypop.HideSelection = True
		Me.citypop.ReadOnly = False
		Me.citypop.Maxlength = 0
		Me.citypop.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.citypop.MultiLine = False
		Me.citypop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.citypop.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.citypop.TabStop = True
		Me.citypop.Visible = True
		Me.citypop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.citypop.Name = "citypop"
		Me.wandpop.AutoSize = False
		Me.wandpop.Enabled = False
		Me.wandpop.Size = New System.Drawing.Size(93, 17)
		Me.wandpop.Location = New System.Drawing.Point(176, 204)
		Me.wandpop.TabIndex = 29
		Me.wandpop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.wandpop.AcceptsReturn = True
		Me.wandpop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.wandpop.BackColor = System.Drawing.SystemColors.Window
		Me.wandpop.CausesValidation = True
		Me.wandpop.ForeColor = System.Drawing.SystemColors.WindowText
		Me.wandpop.HideSelection = True
		Me.wandpop.ReadOnly = False
		Me.wandpop.Maxlength = 0
		Me.wandpop.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.wandpop.MultiLine = False
		Me.wandpop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.wandpop.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.wandpop.TabStop = True
		Me.wandpop.Visible = True
		Me.wandpop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.wandpop.Name = "wandpop"
		Me.towns.AutoSize = False
		Me.towns.Enabled = False
		Me.towns.Size = New System.Drawing.Size(93, 17)
		Me.towns.Location = New System.Drawing.Point(176, 184)
		Me.towns.TabIndex = 28
		Me.towns.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.towns.AcceptsReturn = True
		Me.towns.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.towns.BackColor = System.Drawing.SystemColors.Window
		Me.towns.CausesValidation = True
		Me.towns.ForeColor = System.Drawing.SystemColors.WindowText
		Me.towns.HideSelection = True
		Me.towns.ReadOnly = False
		Me.towns.Maxlength = 0
		Me.towns.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.towns.MultiLine = False
		Me.towns.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.towns.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.towns.TabStop = True
		Me.towns.Visible = True
		Me.towns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.towns.Name = "towns"
		Me.townpop.AutoSize = False
		Me.townpop.Enabled = False
		Me.townpop.Size = New System.Drawing.Size(93, 17)
		Me.townpop.Location = New System.Drawing.Point(176, 164)
		Me.townpop.TabIndex = 27
		Me.townpop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.townpop.AcceptsReturn = True
		Me.townpop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.townpop.BackColor = System.Drawing.SystemColors.Window
		Me.townpop.CausesValidation = True
		Me.townpop.ForeColor = System.Drawing.SystemColors.WindowText
		Me.townpop.HideSelection = True
		Me.townpop.ReadOnly = False
		Me.townpop.Maxlength = 0
		Me.townpop.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.townpop.MultiLine = False
		Me.townpop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.townpop.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.townpop.TabStop = True
		Me.townpop.Visible = True
		Me.townpop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.townpop.Name = "townpop"
		Me.villages.AutoSize = False
		Me.villages.Enabled = False
		Me.villages.Size = New System.Drawing.Size(93, 17)
		Me.villages.Location = New System.Drawing.Point(176, 144)
		Me.villages.TabIndex = 26
		Me.villages.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.villages.AcceptsReturn = True
		Me.villages.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.villages.BackColor = System.Drawing.SystemColors.Window
		Me.villages.CausesValidation = True
		Me.villages.ForeColor = System.Drawing.SystemColors.WindowText
		Me.villages.HideSelection = True
		Me.villages.ReadOnly = False
		Me.villages.Maxlength = 0
		Me.villages.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.villages.MultiLine = False
		Me.villages.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.villages.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.villages.TabStop = True
		Me.villages.Visible = True
		Me.villages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.villages.Name = "villages"
		Me.villagepop.AutoSize = False
		Me.villagepop.Enabled = False
		Me.villagepop.Size = New System.Drawing.Size(93, 17)
		Me.villagepop.Location = New System.Drawing.Point(176, 124)
		Me.villagepop.TabIndex = 25
		Me.villagepop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.villagepop.AcceptsReturn = True
		Me.villagepop.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.villagepop.BackColor = System.Drawing.SystemColors.Window
		Me.villagepop.CausesValidation = True
		Me.villagepop.ForeColor = System.Drawing.SystemColors.WindowText
		Me.villagepop.HideSelection = True
		Me.villagepop.ReadOnly = False
		Me.villagepop.Maxlength = 0
		Me.villagepop.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.villagepop.MultiLine = False
		Me.villagepop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.villagepop.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.villagepop.TabStop = True
		Me.villagepop.Visible = True
		Me.villagepop.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.villagepop.Name = "villagepop"
		Me.population.AutoSize = False
		Me.population.Enabled = False
		Me.population.Size = New System.Drawing.Size(93, 17)
		Me.population.Location = New System.Drawing.Point(176, 104)
		Me.population.TabIndex = 24
		Me.population.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.population.AcceptsReturn = True
		Me.population.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.population.BackColor = System.Drawing.SystemColors.Window
		Me.population.CausesValidation = True
		Me.population.ForeColor = System.Drawing.SystemColors.WindowText
		Me.population.HideSelection = True
		Me.population.ReadOnly = False
		Me.population.Maxlength = 0
		Me.population.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.population.MultiLine = False
		Me.population.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.population.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.population.TabStop = True
		Me.population.Visible = True
		Me.population.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.population.Name = "population"
		Me.age.AutoSize = False
		Me.age.Size = New System.Drawing.Size(89, 19)
		Me.age.Location = New System.Drawing.Point(176, 44)
		Me.age.TabIndex = 6
		Me.age.Text = "300"
		Me.age.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.age.AcceptsReturn = True
		Me.age.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.age.BackColor = System.Drawing.SystemColors.Window
		Me.age.CausesValidation = True
		Me.age.Enabled = True
		Me.age.ForeColor = System.Drawing.SystemColors.WindowText
		Me.age.HideSelection = True
		Me.age.ReadOnly = False
		Me.age.Maxlength = 0
		Me.age.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.age.MultiLine = False
		Me.age.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.age.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.age.TabStop = True
		Me.age.Visible = True
		Me.age.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.age.Name = "age"
		Me.density.AutoSize = False
		Me.density.Size = New System.Drawing.Size(89, 19)
		Me.density.Location = New System.Drawing.Point(176, 24)
		Me.density.TabIndex = 5
		Me.density.Text = "71"
		Me.density.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.density.AcceptsReturn = True
		Me.density.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.density.BackColor = System.Drawing.SystemColors.Window
		Me.density.CausesValidation = True
		Me.density.Enabled = True
		Me.density.ForeColor = System.Drawing.SystemColors.WindowText
		Me.density.HideSelection = True
		Me.density.ReadOnly = False
		Me.density.Maxlength = 0
		Me.density.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.density.MultiLine = False
		Me.density.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.density.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.density.TabStop = True
		Me.density.Visible = True
		Me.density.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.density.Name = "density"
		Me.kingarea.AutoSize = False
		Me.kingarea.Size = New System.Drawing.Size(89, 19)
		Me.kingarea.Location = New System.Drawing.Point(176, 4)
		Me.kingarea.TabIndex = 4
		Me.kingarea.Text = "70000"
		Me.kingarea.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.kingarea.AcceptsReturn = True
		Me.kingarea.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.kingarea.BackColor = System.Drawing.SystemColors.Window
		Me.kingarea.CausesValidation = True
		Me.kingarea.Enabled = True
		Me.kingarea.ForeColor = System.Drawing.SystemColors.WindowText
		Me.kingarea.HideSelection = True
		Me.kingarea.ReadOnly = False
		Me.kingarea.Maxlength = 0
		Me.kingarea.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.kingarea.MultiLine = False
		Me.kingarea.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.kingarea.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.kingarea.TabStop = True
		Me.kingarea.Visible = True
		Me.kingarea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.kingarea.Name = "kingarea"
		Me.cmdCalculate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdCalculate.Text = "Calculate Kingdom Details"
		Me.cmdCalculate.Size = New System.Drawing.Size(241, 25)
		Me.cmdCalculate.Location = New System.Drawing.Point(8, 72)
		Me.cmdCalculate.TabIndex = 0
		Me.cmdCalculate.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCalculate.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCalculate.CausesValidation = True
		Me.cmdCalculate.Enabled = True
		Me.cmdCalculate.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCalculate.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCalculate.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCalculate.TabStop = True
		Me.cmdCalculate.Name = "cmdCalculate"
		Me.lblDistricts3.Text = "0"
		Me.lblDistricts3.Size = New System.Drawing.Size(33, 17)
		Me.lblDistricts3.Location = New System.Drawing.Point(252, 284)
		Me.lblDistricts3.TabIndex = 46
		Me.lblDistricts3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblDistricts3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblDistricts3.BackColor = System.Drawing.SystemColors.Control
		Me.lblDistricts3.Enabled = True
		Me.lblDistricts3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDistricts3.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDistricts3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDistricts3.UseMnemonic = True
		Me.lblDistricts3.Visible = True
		Me.lblDistricts3.AutoSize = False
		Me.lblDistricts3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblDistricts3.Name = "lblDistricts3"
		Me.Label8.Text = "Districts"
		Me.Label8.Size = New System.Drawing.Size(37, 13)
		Me.Label8.Location = New System.Drawing.Point(288, 286)
		Me.Label8.TabIndex = 45
		Me.Label8.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = True
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.lblDistricts2.Text = "0"
		Me.lblDistricts2.Size = New System.Drawing.Size(33, 17)
		Me.lblDistricts2.Location = New System.Drawing.Point(252, 264)
		Me.lblDistricts2.TabIndex = 44
		Me.lblDistricts2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblDistricts2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblDistricts2.BackColor = System.Drawing.SystemColors.Control
		Me.lblDistricts2.Enabled = True
		Me.lblDistricts2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDistricts2.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDistricts2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDistricts2.UseMnemonic = True
		Me.lblDistricts2.Visible = True
		Me.lblDistricts2.AutoSize = False
		Me.lblDistricts2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblDistricts2.Name = "lblDistricts2"
		Me.Label6.Text = "Districts"
		Me.Label6.Size = New System.Drawing.Size(37, 13)
		Me.Label6.Location = New System.Drawing.Point(288, 266)
		Me.Label6.TabIndex = 43
		Me.Label6.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = True
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.lblDistricts.Text = "0"
		Me.lblDistricts.Size = New System.Drawing.Size(33, 17)
		Me.lblDistricts.Location = New System.Drawing.Point(252, 244)
		Me.lblDistricts.TabIndex = 42
		Me.lblDistricts.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblDistricts.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblDistricts.BackColor = System.Drawing.SystemColors.Control
		Me.lblDistricts.Enabled = True
		Me.lblDistricts.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDistricts.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDistricts.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDistricts.UseMnemonic = True
		Me.lblDistricts.Visible = True
		Me.lblDistricts.AutoSize = False
		Me.lblDistricts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblDistricts.Name = "lblDistricts"
		Me.Label5.Text = "Districts"
		Me.Label5.Size = New System.Drawing.Size(37, 13)
		Me.Label5.Location = New System.Drawing.Point(288, 246)
		Me.Label5.TabIndex = 41
		Me.Label5.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = True
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me._Label4_16.Text = "Number of active castles:"
		Me._Label4_16.Size = New System.Drawing.Size(120, 13)
		Me._Label4_16.Location = New System.Drawing.Point(8, 426)
		Me._Label4_16.TabIndex = 23
		Me._Label4_16.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_16.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_16.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_16.Enabled = True
		Me._Label4_16.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_16.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_16.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_16.UseMnemonic = True
		Me._Label4_16.Visible = True
		Me._Label4_16.AutoSize = True
		Me._Label4_16.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_16.Name = "_Label4_16"
		Me._Label4_15.Text = "Number of ruin sites:"
		Me._Label4_15.Size = New System.Drawing.Size(96, 13)
		Me._Label4_15.Location = New System.Drawing.Point(8, 406)
		Me._Label4_15.TabIndex = 22
		Me._Label4_15.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_15.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_15.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_15.Enabled = True
		Me._Label4_15.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_15.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_15.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_15.UseMnemonic = True
		Me._Label4_15.Visible = True
		Me._Label4_15.AutoSize = True
		Me._Label4_15.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_15.Name = "_Label4_15"
		Me._Label4_14.Text = "Avg distance between cities:"
		Me._Label4_14.Size = New System.Drawing.Size(136, 13)
		Me._Label4_14.Location = New System.Drawing.Point(8, 386)
		Me._Label4_14.TabIndex = 21
		Me._Label4_14.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_14.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_14.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_14.Enabled = True
		Me._Label4_14.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_14.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_14.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_14.UseMnemonic = True
		Me._Label4_14.Visible = True
		Me._Label4_14.AutoSize = True
		Me._Label4_14.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_14.Name = "_Label4_14"
		Me._Label4_13.Text = "Avg Distance between towns:"
		Me._Label4_13.Size = New System.Drawing.Size(142, 13)
		Me._Label4_13.Location = New System.Drawing.Point(8, 366)
		Me._Label4_13.TabIndex = 20
		Me._Label4_13.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_13.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_13.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_13.Enabled = True
		Me._Label4_13.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_13.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_13.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_13.UseMnemonic = True
		Me._Label4_13.Visible = True
		Me._Label4_13.AutoSize = True
		Me._Label4_13.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_13.Name = "_Label4_13"
		Me._Label4_12.Text = "Avg Distance between villages:"
		Me._Label4_12.Size = New System.Drawing.Size(149, 13)
		Me._Label4_12.Location = New System.Drawing.Point(8, 346)
		Me._Label4_12.TabIndex = 19
		Me._Label4_12.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_12.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_12.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_12.Enabled = True
		Me._Label4_12.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_12.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_12.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_12.UseMnemonic = True
		Me._Label4_12.Visible = True
		Me._Label4_12.AutoSize = True
		Me._Label4_12.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_12.Name = "_Label4_12"
		Me._Label4_11.Text = "Area of farmland:"
		Me._Label4_11.Size = New System.Drawing.Size(80, 13)
		Me._Label4_11.Location = New System.Drawing.Point(8, 326)
		Me._Label4_11.TabIndex = 18
		Me._Label4_11.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_11.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_11.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_11.Enabled = True
		Me._Label4_11.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_11.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_11.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_11.UseMnemonic = True
		Me._Label4_11.Visible = True
		Me._Label4_11.AutoSize = True
		Me._Label4_11.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_11.Name = "_Label4_11"
		Me._Label4_10.Text = "Number of other cities:"
		Me._Label4_10.Size = New System.Drawing.Size(106, 13)
		Me._Label4_10.Location = New System.Drawing.Point(8, 306)
		Me._Label4_10.TabIndex = 17
		Me._Label4_10.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_10.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_10.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_10.Enabled = True
		Me._Label4_10.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_10.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_10.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_10.UseMnemonic = True
		Me._Label4_10.Visible = True
		Me._Label4_10.AutoSize = True
		Me._Label4_10.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_10.Name = "_Label4_10"
		Me._Label4_9.Text = "Population of Third Largest City:"
		Me._Label4_9.Size = New System.Drawing.Size(150, 13)
		Me._Label4_9.Location = New System.Drawing.Point(8, 286)
		Me._Label4_9.TabIndex = 16
		Me._Label4_9.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_9.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_9.Enabled = True
		Me._Label4_9.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_9.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_9.UseMnemonic = True
		Me._Label4_9.Visible = True
		Me._Label4_9.AutoSize = True
		Me._Label4_9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_9.Name = "_Label4_9"
		Me._Label4_8.Text = "Population of Second Largest City:"
		Me._Label4_8.Size = New System.Drawing.Size(163, 13)
		Me._Label4_8.Location = New System.Drawing.Point(8, 266)
		Me._Label4_8.TabIndex = 15
		Me._Label4_8.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_8.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_8.Enabled = True
		Me._Label4_8.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_8.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_8.UseMnemonic = True
		Me._Label4_8.Visible = True
		Me._Label4_8.AutoSize = True
		Me._Label4_8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_8.Name = "_Label4_8"
		Me._Label4_7.Text = "Population of Largest City:"
		Me._Label4_7.Size = New System.Drawing.Size(123, 13)
		Me._Label4_7.Location = New System.Drawing.Point(8, 246)
		Me._Label4_7.TabIndex = 14
		Me._Label4_7.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_7.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_7.Enabled = True
		Me._Label4_7.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_7.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_7.UseMnemonic = True
		Me._Label4_7.Visible = True
		Me._Label4_7.AutoSize = True
		Me._Label4_7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_7.Name = "_Label4_7"
		Me._Label4_6.Text = "City Population:"
		Me._Label4_6.Size = New System.Drawing.Size(73, 13)
		Me._Label4_6.Location = New System.Drawing.Point(8, 226)
		Me._Label4_6.TabIndex = 13
		Me._Label4_6.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_6.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_6.Enabled = True
		Me._Label4_6.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_6.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_6.UseMnemonic = True
		Me._Label4_6.Visible = True
		Me._Label4_6.AutoSize = True
		Me._Label4_6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_6.Name = "_Label4_6"
		Me._Label4_5.Text = "Wandering Population:"
		Me._Label4_5.Size = New System.Drawing.Size(108, 13)
		Me._Label4_5.Location = New System.Drawing.Point(8, 206)
		Me._Label4_5.TabIndex = 12
		Me._Label4_5.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_5.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_5.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_5.Enabled = True
		Me._Label4_5.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_5.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_5.UseMnemonic = True
		Me._Label4_5.Visible = True
		Me._Label4_5.AutoSize = True
		Me._Label4_5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_5.Name = "_Label4_5"
		Me._Label4_4.Text = "Number of Towns in Kingdom:"
		Me._Label4_4.Size = New System.Drawing.Size(142, 13)
		Me._Label4_4.Location = New System.Drawing.Point(8, 186)
		Me._Label4_4.TabIndex = 11
		Me._Label4_4.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_4.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_4.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_4.Enabled = True
		Me._Label4_4.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_4.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_4.UseMnemonic = True
		Me._Label4_4.Visible = True
		Me._Label4_4.AutoSize = True
		Me._Label4_4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_4.Name = "_Label4_4"
		Me._Label4_3.Text = "Population Living in Towns:"
		Me._Label4_3.Size = New System.Drawing.Size(130, 13)
		Me._Label4_3.Location = New System.Drawing.Point(8, 166)
		Me._Label4_3.TabIndex = 10
		Me._Label4_3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_3.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_3.Enabled = True
		Me._Label4_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_3.UseMnemonic = True
		Me._Label4_3.Visible = True
		Me._Label4_3.AutoSize = True
		Me._Label4_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_3.Name = "_Label4_3"
		Me._Label4_2.Text = "Number of Villages in Kingdom:"
		Me._Label4_2.Size = New System.Drawing.Size(146, 13)
		Me._Label4_2.Location = New System.Drawing.Point(8, 146)
		Me._Label4_2.TabIndex = 9
		Me._Label4_2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_2.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_2.Enabled = True
		Me._Label4_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_2.UseMnemonic = True
		Me._Label4_2.Visible = True
		Me._Label4_2.AutoSize = True
		Me._Label4_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_2.Name = "_Label4_2"
		Me._Label4_1.Text = "Population Living in Villages:"
		Me._Label4_1.Size = New System.Drawing.Size(134, 13)
		Me._Label4_1.Location = New System.Drawing.Point(8, 126)
		Me._Label4_1.TabIndex = 8
		Me._Label4_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_1.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_1.Enabled = True
		Me._Label4_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_1.UseMnemonic = True
		Me._Label4_1.Visible = True
		Me._Label4_1.AutoSize = True
		Me._Label4_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_1.Name = "_Label4_1"
		Me._Label4_0.Text = "Kingdom Population:"
		Me._Label4_0.Size = New System.Drawing.Size(97, 13)
		Me._Label4_0.Location = New System.Drawing.Point(8, 106)
		Me._Label4_0.TabIndex = 7
		Me._Label4_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._Label4_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._Label4_0.BackColor = System.Drawing.SystemColors.Control
		Me._Label4_0.Enabled = True
		Me._Label4_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._Label4_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._Label4_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._Label4_0.UseMnemonic = True
		Me._Label4_0.Visible = True
		Me._Label4_0.AutoSize = True
		Me._Label4_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._Label4_0.Name = "_Label4_0"
		Me.Label3.Text = "Age of Kingdom:"
		Me.Label3.Size = New System.Drawing.Size(78, 13)
		Me.Label3.Location = New System.Drawing.Point(8, 47)
		Me.Label3.TabIndex = 3
		Me.Label3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = True
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.Text = "Population Density:"
		Me.Label2.Size = New System.Drawing.Size(91, 13)
		Me.Label2.Location = New System.Drawing.Point(8, 27)
		Me.Label2.TabIndex = 2
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = True
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.Text = "Kingdom Area:"
		Me.Label1.Size = New System.Drawing.Size(69, 13)
		Me.Label1.Location = New System.Drawing.Point(8, 7)
		Me.Label1.TabIndex = 1
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = True
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(Frame1)
		Me.Controls.Add(acastles)
		Me.Controls.Add(rcastles)
		Me.Controls.Add(dcities)
		Me.Controls.Add(dtowns)
		Me.Controls.Add(dvillage)
		Me.Controls.Add(farmland)
		Me.Controls.Add(citynum)
		Me.Controls.Add(lcitypop3)
		Me.Controls.Add(lcitypop2)
		Me.Controls.Add(lcitypop)
		Me.Controls.Add(citypop)
		Me.Controls.Add(wandpop)
		Me.Controls.Add(towns)
		Me.Controls.Add(townpop)
		Me.Controls.Add(villages)
		Me.Controls.Add(villagepop)
		Me.Controls.Add(population)
		Me.Controls.Add(age)
		Me.Controls.Add(density)
		Me.Controls.Add(kingarea)
		Me.Controls.Add(cmdCalculate)
		Me.Controls.Add(lblDistricts3)
		Me.Controls.Add(Label8)
		Me.Controls.Add(lblDistricts2)
		Me.Controls.Add(Label6)
		Me.Controls.Add(lblDistricts)
		Me.Controls.Add(Label5)
		Me.Controls.Add(_Label4_16)
		Me.Controls.Add(_Label4_15)
		Me.Controls.Add(_Label4_14)
		Me.Controls.Add(_Label4_13)
		Me.Controls.Add(_Label4_12)
		Me.Controls.Add(_Label4_11)
		Me.Controls.Add(_Label4_10)
		Me.Controls.Add(_Label4_9)
		Me.Controls.Add(_Label4_8)
		Me.Controls.Add(_Label4_7)
		Me.Controls.Add(_Label4_6)
		Me.Controls.Add(_Label4_5)
		Me.Controls.Add(_Label4_4)
		Me.Controls.Add(_Label4_3)
		Me.Controls.Add(_Label4_2)
		Me.Controls.Add(_Label4_1)
		Me.Controls.Add(_Label4_0)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.Frame1.Controls.Add(lstVillages)
		Me.Frame1.Controls.Add(lstTowns)
		Me.Frame1.Controls.Add(lstCities)
		Me.Label4.SetIndex(_Label4_16, CType(16, Short))
		Me.Label4.SetIndex(_Label4_15, CType(15, Short))
		Me.Label4.SetIndex(_Label4_14, CType(14, Short))
		Me.Label4.SetIndex(_Label4_13, CType(13, Short))
		Me.Label4.SetIndex(_Label4_12, CType(12, Short))
		Me.Label4.SetIndex(_Label4_11, CType(11, Short))
		Me.Label4.SetIndex(_Label4_10, CType(10, Short))
		Me.Label4.SetIndex(_Label4_9, CType(9, Short))
		Me.Label4.SetIndex(_Label4_8, CType(8, Short))
		Me.Label4.SetIndex(_Label4_7, CType(7, Short))
		Me.Label4.SetIndex(_Label4_6, CType(6, Short))
		Me.Label4.SetIndex(_Label4_5, CType(5, Short))
		Me.Label4.SetIndex(_Label4_4, CType(4, Short))
		Me.Label4.SetIndex(_Label4_3, CType(3, Short))
		Me.Label4.SetIndex(_Label4_2, CType(2, Short))
		Me.Label4.SetIndex(_Label4_1, CType(1, Short))
		Me.Label4.SetIndex(_Label4_0, CType(0, Short))
		CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Frame1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class