Option Strict Off
Option Explicit On
Friend Class frmDemographics
	Inherits System.Windows.Forms.Form
	
    'Dim Cities As New CCities
	
	Public Sub cmdCalculate_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCalculate.Click
		
		Dim karea As Single
		Dim kdensity As Single
		Dim kage As Single
		Dim pop As Single
		Dim vpop As Single
		Dim kvillages As Single
		Dim tpop As Single
		Dim ktowns As Single
		Dim kwandpop As Single
		Dim kcitypop As Single
		Dim sqpop As Single
		Dim klcitypop As Single
		Dim klcitypop2 As Single
		Dim klcitypop3 As Single
		Dim kcitynum As Single
		Dim kfarmland As Single
		Dim kdvillage As Single
		Dim kdtowns As Single
		Dim kdcities As Single
		Dim krcastles As Single
		Dim kacastles As Single
		Dim farmarea As Single
		Dim lremainpop As Integer
		Dim lngcitypop As Integer
		
		cmdCalculate.Enabled = False
		
		'Set cities = New CCities
		
		karea = CSng(kingarea.Text)
		kdensity = CSng(density.Text)
		kage = CSng(age.Text)
		
		If karea < 1 Then
			MsgBox("Kingdom Area must be greater than 0.")
			Exit Sub
		End If
		
		If kdensity < 0 Then
			MsgBox("Population Density must be greater than 0")
			Exit Sub
		End If
		
		If kdensity > 1000 Then
			MsgBox("Population Density must be less than 1001")
			Exit Sub
		End If
		
		If kage < 1 Then
			MsgBox("Kingdom Age must be greater than 0.")
			Exit Sub
		End If
		
		pop = CSng(FormatNumber(karea * kdensity, 0))
		vpop = CSng(FormatNumber(pop * 0.89, 0))
		kvillages = CSng(FormatNumber(vpop / 700, 0))
		tpop = CSng(FormatNumber(pop * 0.06, 0))
		ktowns = CSng(FormatNumber(tpop / 5000, 0))
		kwandpop = CSng(FormatNumber(pop * 0.02, 0))
		kcitypop = CSng(FormatNumber(pop * 0.03, 0))
		sqpop = pop ^ 0.5
		klcitypop = CSng(FormatNumber(sqpop * (Rnd() * 4 + Rnd() * 4 + 10), 0))
		klcitypop2 = CSng(FormatNumber((0.25 + Rnd() * 0.5) * klcitypop, 0))
		klcitypop3 = CSng(FormatNumber((Rnd() * 0.1 + 0.8) * klcitypop2, 0))
		
		lstCities.Items.Clear()
        'If gcblnOGL = False Then
        '	lstCities.Items.Add(klcitypop & Chr(9) & " (" & CStr(CInt(klcitypop / 500)) & " districts)")
        '	lstCities.Items.Add(klcitypop2 & Chr(9) & " (" & CStr(CInt(klcitypop2 / 500)) & " districts)")
        '	lstCities.Items.Add(klcitypop3 & Chr(9) & " (" & CStr(CInt(klcitypop3 / 500)) & " districts)")
        'Else
        lstCities.Items.Add(CStr(klcitypop))
        lstCities.Items.Add(CStr(klcitypop2))
        lstCities.Items.Add(CStr(klcitypop3))
        'End If

        kcitynum = 0
        lremainpop = (kcitypop - klcitypop - klcitypop2 - klcitypop3)
        lngcitypop = klcitypop3

        'Cities.Add()
        'Cities((Cities.Count)).population = klcitypop
        'Cities.Add()
        'Cities((Cities.Count)).population = klcitypop2
        'Cities.Add()
        'Cities((Cities.Count)).population = klcitypop3


        Do Until lngcitypop < 11000 Or lremainpop < 12000
            kcitynum = kcitynum + 1
            lngcitypop = (1 - (Rnd() * 0.3 + 0.1)) * lngcitypop
            'If gcblnOGL = True Then
            lstCities.Items.Add(CStr(lngcitypop))
            'Else
            'lstCities.Items.Add(lngcitypop & Chr(9) & " (" & CStr(CInt(lngcitypop / 500)) & " districts)")
            'End If

            'Cities.Add()
            'Cities((Cities.Count)).population = lngcitypop

            lremainpop = lremainpop - lngcitypop
        Loop

        If kcitynum < 0 Then kcitynum = 0
        farmarea = CSng(FormatNumber((karea * kdensity) / 180, 0)) ' area
        kfarmland = CSng(FormatNumber((farmarea / karea) * 100, 0)) ' percentage


        ktowns = 0
        lremainpop = tpop
        lstTowns.Items.Clear()
        Do Until lremainpop < 1700
            ktowns = ktowns + 1
            lngcitypop = -1
            Do Until lngcitypop > 0
                lngcitypop = 1000 + (Rnd() * 7000)
            Loop
            'lngcitypop = (Rnd * 0.1 + 0.8) * lngcitypop

            'Cities.Add()
            'Cities((Cities.Count)).population = lngcitypop

            lstTowns.Items.Add(CStr(lngcitypop))
            lremainpop = lremainpop - lngcitypop
        Loop

        kvillages = 0
        lremainpop = vpop
        lstVillages.Items.Clear()
        Do Until lremainpop < 20
            kvillages = kvillages + 1
            lngcitypop = Rnd() * 980 + 20
            lstVillages.Items.Add(CStr(lngcitypop))

            'Cities.Add()
            'Cities((Cities.Count)).population = lngcitypop

            lremainpop = lremainpop - lngcitypop
        Loop

        ' Use farm area for towns and villages since they will depend on a great city.
        If kvillages > 0 Then
            kdvillage = CSng(FormatNumber(2 * (farmarea / kvillages) ^ 0.5, 1))
            'kdvillage = FormatNumber(2 * Sqr(karea / kvillages), 1)
        End If
        If ktowns > 0 Then
            kdtowns = CSng(FormatNumber(2 * (farmarea / ktowns) ^ 0.5, 1))
            'kdtowns = FormatNumber(2 * Sqr(karea / ktowns), 1)
        End If

        ' Use total area for cities since they are independent.
        If kcitynum > 0 Then
            'kdcities = FormatNumber((farmarea / (kcitynum + 3)) ^ 0.5, 1)
            kdcities = CSng(FormatNumber(2 * (karea / (kcitynum + 3)) ^ 0.5, 1))
        End If
        krcastles = CSng(FormatNumber((pop / 5000000) * (kage ^ 0.5), 0))
        kacastles = CSng(FormatNumber(pop / 50000, 0))

        population.Text = VB6.Format(pop, "###,###,##0")
        villagepop.Text = VB6.Format(vpop, "###,###,##0")
        villages.Text = VB6.Format(kvillages, "###,###,##0")
        townpop.Text = VB6.Format(tpop, "###,###,##0")
        towns.Text = VB6.Format(ktowns, "###,###,##0")
        wandpop.Text = VB6.Format(kwandpop, "###,###,##0")
        citypop.Text = VB6.Format(kcitypop, "###,###,##0")
        lcitypop.Text = VB6.Format(klcitypop, "###,###,##0")
        lblDistricts.Text = VB6.Format(klcitypop / 500, "##0")
        lcitypop2.Text = VB6.Format(klcitypop2, "###,###,##0")
        lblDistricts2.Text = VB6.Format(klcitypop2 / 500, "##0")
        lcitypop3.Text = VB6.Format(klcitypop3, "###,###,##0")
        lblDistricts3.Text = VB6.Format(klcitypop3 / 500, "##0")
        citynum.Text = VB6.Format(kcitynum, "###,###,##0")
        farmland.Text = VB6.Format(kfarmland / 100, "##0.0#%")
        dvillage.Text = VB6.Format(kdvillage, "###,###,##0")
        dtowns.Text = VB6.Format(kdtowns, "###,###,##0")
        dcities.Text = VB6.Format(kdcities, "###,###,##0")
        rcastles.Text = VB6.Format(krcastles, "###,###,##0")
        acastles.Text = VB6.Format(kacastles, "###,###,##0")

        cmdCalculate.Enabled = True
		
	End Sub
	
	Private Sub frmDemographics_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'If gcblnOGL = True Then
        Label5.Visible = False
        Label6.Visible = False
        Label8.Visible = False
        lblDistricts.Visible = False
        lblDistricts2.Visible = False
        lblDistricts3.Visible = False
        lstCities.Items.Clear()
        'End If
	End Sub
	
	Private Sub lstCities_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstCities.DoubleClick

        VB6.ShowForm(frmSV, , Me)
        frmSV.lblPopulation.Text = VB6.GetItemString(lstCities, lstCities.SelectedIndex)
        System.Windows.Forms.Application.DoEvents()
        frmSV.CalculateSV()
    End Sub
	
	Private Sub lstTowns_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstTowns.DoubleClick
		VB6.ShowForm(frmSV,  , Me)
		frmSV.lblPopulation.Text = VB6.GetItemString(lstTowns, lstTowns.SelectedIndex)
		System.Windows.Forms.Application.DoEvents()
		frmSV.CalculateSV()
	End Sub
	
	Private Sub lstVillages_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstVillages.DoubleClick
		VB6.ShowForm(frmSV,  , Me)
		frmSV.lblPopulation.Text = VB6.GetItemString(lstVillages, lstVillages.SelectedIndex)
		System.Windows.Forms.Application.DoEvents()
		frmSV.CalculateSV()
	End Sub
End Class