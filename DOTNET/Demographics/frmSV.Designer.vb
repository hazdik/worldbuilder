<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSV
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lstBusinesses As System.Windows.Forms.ListBox
	Public WithEvents Label18 As System.Windows.Forms.Label
	Public WithEvents lblPowerCenter As System.Windows.Forms.Label
	Public WithEvents Label21 As System.Windows.Forms.Label
	Public WithEvents lblAlignment As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents lblArea As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents lblPopulation As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSV))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.lstBusinesses = New System.Windows.Forms.ListBox
		Me.Label18 = New System.Windows.Forms.Label
		Me.lblPowerCenter = New System.Windows.Forms.Label
		Me.Label21 = New System.Windows.Forms.Label
		Me.lblAlignment = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.lblArea = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.lblPopulation = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Town/Village Statistics"
		Me.ClientSize = New System.Drawing.Size(311, 449)
		Me.Location = New System.Drawing.Point(3, 29)
		Me.Icon = CType(resources.GetObject("frmSV.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSV"
		Me.lstBusinesses.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstBusinesses.Size = New System.Drawing.Size(301, 357)
		Me.lstBusinesses.Location = New System.Drawing.Point(4, 76)
		Me.lstBusinesses.TabIndex = 2
		Me.lstBusinesses.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstBusinesses.BackColor = System.Drawing.SystemColors.Window
		Me.lstBusinesses.CausesValidation = True
		Me.lstBusinesses.Enabled = True
		Me.lstBusinesses.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstBusinesses.IntegralHeight = True
		Me.lstBusinesses.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstBusinesses.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstBusinesses.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstBusinesses.Sorted = False
		Me.lstBusinesses.TabStop = True
		Me.lstBusinesses.Visible = True
		Me.lstBusinesses.MultiColumn = False
		Me.lstBusinesses.Name = "lstBusinesses"
		Me.Label18.Text = "Power Center:"
		Me.Label18.Size = New System.Drawing.Size(67, 13)
		Me.Label18.Location = New System.Drawing.Point(4, 28)
		Me.Label18.TabIndex = 9
		Me.Label18.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label18.BackColor = System.Drawing.SystemColors.Control
		Me.Label18.Enabled = True
		Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label18.UseMnemonic = True
		Me.Label18.Visible = True
		Me.Label18.AutoSize = True
		Me.Label18.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label18.Name = "Label18"
		Me.lblPowerCenter.Size = New System.Drawing.Size(105, 17)
		Me.lblPowerCenter.Location = New System.Drawing.Point(84, 28)
		Me.lblPowerCenter.TabIndex = 8
		Me.lblPowerCenter.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblPowerCenter.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPowerCenter.BackColor = System.Drawing.SystemColors.Control
		Me.lblPowerCenter.Enabled = True
		Me.lblPowerCenter.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPowerCenter.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPowerCenter.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPowerCenter.UseMnemonic = True
		Me.lblPowerCenter.Visible = True
		Me.lblPowerCenter.AutoSize = False
		Me.lblPowerCenter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblPowerCenter.Name = "lblPowerCenter"
		Me.Label21.Text = "Alignment:"
		Me.Label21.Size = New System.Drawing.Size(49, 13)
		Me.Label21.Location = New System.Drawing.Point(4, 52)
		Me.Label21.TabIndex = 7
		Me.Label21.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label21.BackColor = System.Drawing.SystemColors.Control
		Me.Label21.Enabled = True
		Me.Label21.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label21.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label21.UseMnemonic = True
		Me.Label21.Visible = True
		Me.Label21.AutoSize = True
		Me.Label21.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label21.Name = "Label21"
		Me.lblAlignment.Size = New System.Drawing.Size(105, 17)
		Me.lblAlignment.Location = New System.Drawing.Point(84, 52)
		Me.lblAlignment.TabIndex = 6
		Me.lblAlignment.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblAlignment.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblAlignment.BackColor = System.Drawing.SystemColors.Control
		Me.lblAlignment.Enabled = True
		Me.lblAlignment.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblAlignment.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblAlignment.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblAlignment.UseMnemonic = True
		Me.lblAlignment.Visible = True
		Me.lblAlignment.AutoSize = False
		Me.lblAlignment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblAlignment.Name = "lblAlignment"
		Me.Label3.Text = "sq. mi."
		Me.Label3.Size = New System.Drawing.Size(30, 13)
		Me.Label3.Location = New System.Drawing.Point(260, 6)
		Me.Label3.TabIndex = 5
		Me.Label3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = True
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.lblArea.Text = "1"
		Me.lblArea.Size = New System.Drawing.Size(69, 17)
		Me.lblArea.Location = New System.Drawing.Point(180, 4)
		Me.lblArea.TabIndex = 4
		Me.lblArea.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblArea.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblArea.BackColor = System.Drawing.SystemColors.Control
		Me.lblArea.Enabled = True
		Me.lblArea.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblArea.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblArea.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblArea.UseMnemonic = True
		Me.lblArea.Visible = True
		Me.lblArea.AutoSize = False
		Me.lblArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblArea.Name = "lblArea"
		Me.Label2.Text = "Area:"
		Me.Label2.Size = New System.Drawing.Size(25, 13)
		Me.Label2.Location = New System.Drawing.Point(152, 6)
		Me.Label2.TabIndex = 3
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = True
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.lblPopulation.Text = "1"
		Me.lblPopulation.Size = New System.Drawing.Size(65, 17)
		Me.lblPopulation.Location = New System.Drawing.Point(60, 4)
		Me.lblPopulation.TabIndex = 1
		Me.lblPopulation.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblPopulation.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblPopulation.BackColor = System.Drawing.SystemColors.Control
		Me.lblPopulation.Enabled = True
		Me.lblPopulation.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblPopulation.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblPopulation.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblPopulation.UseMnemonic = True
		Me.lblPopulation.Visible = True
		Me.lblPopulation.AutoSize = False
		Me.lblPopulation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lblPopulation.Name = "lblPopulation"
		Me.Label1.Text = "Population:"
		Me.Label1.Size = New System.Drawing.Size(53, 13)
		Me.Label1.Location = New System.Drawing.Point(4, 6)
		Me.Label1.TabIndex = 0
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = True
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(lstBusinesses)
		Me.Controls.Add(Label18)
		Me.Controls.Add(lblPowerCenter)
		Me.Controls.Add(Label21)
		Me.Controls.Add(lblAlignment)
		Me.Controls.Add(Label3)
		Me.Controls.Add(lblArea)
		Me.Controls.Add(Label2)
		Me.Controls.Add(lblPopulation)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class