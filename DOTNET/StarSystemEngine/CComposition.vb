Option Strict Off
Option Explicit On
Friend Class CComposition
	
	Private mvarCarbon As Single 'local copy
	Private mvarHydrogen As Single 'local copy
	Private mvarFayalite As Single 'local copy
	Private mvarNickel As Single 'local copy
	Private mvarIron As Single 'local copy
	Private mvarIce As Single 'local copy
	Private mvarMethane As Single 'local copy
	Private mvarNickelIron As Single 'local copy
	Private mvarNickelIronRatio As Single 'local copy
	
	
	
	Public Property NickelIronRatio() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.NickelIronRatio
			NickelIronRatio = mvarNickelIronRatio
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.NickelIronRatio = 5
			mvarNickelIronRatio = Value
		End Set
	End Property
	
	
	
	
	
	Public Property NickelIron() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.NickelIron
			NickelIron = mvarNickelIron
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.NickelIron = 5
			mvarNickelIron = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property Methane() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Methane
			Methane = mvarMethane
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Methane = 5
			mvarMethane = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Ice() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Ice
			Ice = mvarIce
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Ice = 5
			mvarIce = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Iron() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Iron
			Iron = mvarIron
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Iron = 5
			mvarIron = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Nickel() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Nickel
			Nickel = mvarNickel
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Nickel = 5
			mvarNickel = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Fayalite() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Fayalite
			Fayalite = mvarFayalite
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Fayalite = 5
			mvarFayalite = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Hydrogen() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Hydrogen
			Hydrogen = mvarHydrogen
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Hydrogen = 5
			mvarHydrogen = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Carbon() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Carbon
			Carbon = mvarCarbon
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Carbon = 5
			mvarCarbon = Value
		End Set
	End Property
	
	
	
	Public Function CalculateMass(ByRef RadiusM As Double) As Double
		
		CalculateMass = (mvarCarbon * CDensityKgM3 + mvarFayalite * FayaliteDensityKgM3 + mvarHydrogen * HDensityKgM3 + mvarIce * IceDensityKgM3 + mvarIron * FeDensityKgM3 + mvarMethane * (CDensityKgM3 + 4 * HDensityKgM3) + mvarNickel * NiDensityKgM3 + mvarNickelIron * (NiDensityKgM3 * mvarNickelIronRatio + FeDensityKgM3 * (100 - mvarNickelIronRatio))) * 4 / 3 * PI * RadiusM ^ 3
	End Function
End Class