Option Strict Off
Option Explicit On
<System.Runtime.InteropServices.ComVisible(False)> Friend Class CDB

    Public rs As ADODB.Recordset

    Public Sub MoveFirst()
        rs.MoveFirst()
    End Sub

    Public Sub MoveNext()
        rs.MoveNext()
    End Sub

    'UPGRADE_NOTE: EOF was upgraded to EOF_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Public ReadOnly Property EOF_Renamed() As Integer
        Get
            If Not (rs Is Nothing) Then
                EOF_Renamed = rs.EOF
            Else
                EOF_Renamed = -1
            End If
        End Get
    End Property

    Public ReadOnly Property BOF() As Integer
        Get
            If Not (rs Is Nothing) Then
                BOF = rs.BOF
            Else
                BOF = -1
            End If
        End Get
    End Property

    Public ReadOnly Property State() As Integer
        Get
            If Not (rs Is Nothing) Then
                State = CInt(rs.State)
            Else
                State = -1
            End If
        End Get
    End Property


    Public ReadOnly Property RecordCount() As Integer
        Get
            If Not (rs Is Nothing) Then
                RecordCount = CInt(rs.RecordCount)
            Else
                RecordCount = -1
            End If
        End Get
    End Property

    Public Sub Update()
        rs.Update()
    End Sub

    Public Sub ConnectDB()

        If Conn Is Nothing Then Conn = New ADODB.Connection

        If Conn.State = 0 Then
            Conn = New ADODB.Connection

            Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Mode=ReadWrite;" & "Extended Properties=""DBQ=" & My.Application.Info.DirectoryPath & "\WorldBuilder.mdb;DefaultDir=" & My.Application.Info.DirectoryPath & ";" & "Driver={Microsoft Access Driver (*.mdb)};DriverId=25;FIL=MS Access;" & "FILEDSN=" & My.Application.Info.DirectoryPath & "\WorldBuilder.dsn;MaxBufferSize=2048;MaxScanRows=8;" & "PageTimeout=5;SafeTransactions=0;Threads=3;UID=admin;UserCommitSync=Yes;"";" & "Initial Catalog=" & My.Application.Info.DirectoryPath & "\WorldBuilder"

            Conn.Open()
        End If
    End Sub

    Public Sub OpenDB(ByRef strSQL As String, Optional ByRef blnReadWrite As Boolean = False)

        ' check for connection
        If Conn Is Nothing Then ConnectDB()

        If Conn.State > 0 Then
            If rs Is Nothing Then rs = New ADODB.Recordset

            ' close the recordset if it's already open
            If rs.State > 0 Then
                rs.Close()
            End If

            If blnReadWrite = False Then
                rs.Open(strSQL, Conn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
            Else
                rs.Open(strSQL, Conn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
            End If
        End If
    End Sub

    Public Sub CloseDB()
        If rs.State > 0 Then
            rs.Close()
        End If
    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        If rs Is Nothing Then rs = New ADODB.Recordset
        ConnectDB()
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        If Not (rs Is Nothing) Then
            If rs.State > 0 Then
                rs.Close()
            End If
        End If
        'UPGRADE_NOTE: Object rs may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        rs = Nothing
        If Not (Conn Is Nothing) Then
            If Conn.State > 0 Then
                Conn.Close()
            End If
            'UPGRADE_NOTE: Object Conn may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            Conn = Nothing
        End If
    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
End Class