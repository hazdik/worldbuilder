Option Strict Off
Option Explicit On
Friend Class CRGBColor
	
	Private mvarRed As Single 'local copy
	Private mvarGreen As Single 'local copy
	Private mvarBlue As Single 'local copy
	
	
	Public Property Blue() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Blue
			Blue = mvarBlue
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Blue = 5
			mvarBlue = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Green() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Green
			Green = mvarGreen
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Green = 5
			mvarGreen = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Red() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Red
			Red = mvarRed
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Red = 5
			mvarRed = Value
		End Set
	End Property
End Class