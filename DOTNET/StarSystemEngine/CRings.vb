Option Strict Off
Option Explicit On
Friend Class CRings
	
	Private mvarInner As Double 'local copy
	Private mvarOuter As Double 'local copy
	Private mvarTexture As String 'local copy
	'local variable(s) to hold property value(s)
	Private mvarColor As CRGBColor 'local copy
	
	
	Public Property Color() As CRGBColor
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Color
			Color = mvarColor
		End Get
		Set(ByVal Value As CRGBColor)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.Color = Form1
			mvarColor = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property Texture() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Texture
			Texture = mvarTexture
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Texture = 5
			mvarTexture = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Outer() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Outer
			Outer = mvarOuter
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Outer = 5
			mvarOuter = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Inner() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Inner
			Inner = mvarInner
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Inner = 5
			mvarInner = Value
		End Set
	End Property
	
	
	
	Public Sub Generate(ByRef RadiusM As Double)
		Dim temp As Double
		Dim a As Short
		
		mvarInner = System.Math.Abs(randn) * 0.3717 * RadiusM / 1000 + 1.6825 * RadiusM / 1000
		mvarOuter = System.Math.Abs(randn) * 0.3282 * RadiusM / 1000 + 2.54175 * RadiusM / 1000
		
		If Inner > Outer Then
			temp = Inner
			mvarInner = mvarOuter
			mvarOuter = temp
		End If
		
		a = Int(Rnd() * 4) + 1
		Select Case a
			Case Is <= 1
				mvarTexture = "saturn-rings.png"
			Case 2
				mvarTexture = "neptune-rings.png"
			Case 3
				mvarTexture = "uranus-rings.png"
			Case Else
				' do nothing
		End Select
		
		mvarColor.Blue = Rnd()
		mvarColor.Red = Rnd()
		mvarColor.Green = Rnd()
		
	End Sub
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		Color = New CRGBColor
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'UPGRADE_NOTE: Object Color may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		Color = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class