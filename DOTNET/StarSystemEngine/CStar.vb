Option Strict Off
Option Explicit On
Friend Class CStar
	
	
	Private Pctr As Integer
	
	
	'local variable(s) to hold property value(s)
	Private mvarAbsMag As Single 'local copy
	Private mvarAlbedo As Single 'local copy
	Private mvarAppMag As Single 'local copy
	Private mvarDistanceLY As Double 'local copy
	Private mvarDecDeg As Single 'local copy
	Private mvarDensityKgM3 As Double 'local copy
	Private mvarMassKg As Double 'local copy
	Private mvarTag As String 'local copy
	Private mvarHabitableZoneMaxAU As Single 'local copy
	Private mvarHabitableZoneMinAU As Single 'local copy
	Private mvarMassSol As Single 'local copy
	Private mvarRightAscension As Single 'local copy
	Private mvarRadiusSol As Single 'local copy
	Private mvarRadiusM As Single 'local copy
	Private mvarDiameterMi As Integer 'local copy
	Private mvarSurfaceGravityG As Single 'local copy
	Private mvarSpectralType As String 'local copy
	Private mvarLuminosityClass As String 'local copy
	Private mvarStarType As String 'local copy
	
	Private mvarSurfaceTemperatureK As Double 'local copy
	Private mvarComposition As CComposition 'local copy
	Private mvarOrbitalData As COrbit 'local copy
	Private mvarOrbitingBodies As CStars 'local copy
	Private mvarSizeClass As String
	Private mvarShortName As String
	Private mvarIsHabitable As Boolean
	Private mvarInStarHZ As Boolean
	Private mvarobliquity As Single 'local copy
	Private mvarRotationPeriod As Single 'local copy
	Private mvarRingSystem As CRings 'local copy
	Private mvarAtmosphere As CAtmosphere 'local copy
	Private mvarRotationOffset As Single 'local copy
	Private mvarColor As CRGBColor 'local copy
	Private mvarHazeColor As CRGBColor 'local copy
	Private mvarHazeDensity As Single 'local copy
	Private mvarSpecularColor As CRGBColor 'local copy
	Private mvarSpecularPower As Single 'local copy
	Private mvarbHasHabitableZone As Boolean 'local copy
	Private mvarOblateness As Single 'local copy
	Private mvarPctWater As Byte 'local copy
	Private mvarPctIce As Byte 'local copy
	
    'Public g_cDecoders As GDIPlusWrapper.GDIPImageDecoderList
    'Public g_cEncoders As GDIPlusWrapper.GDIPImageEncoderList
	
	Public g_cSaveOptions As New cSaveOptions
	
	
	
	Public Property PctIce() As Byte
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.PctIce
			PctIce = mvarPctIce
		End Get
		Set(ByVal Value As Byte)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.PctIce = 5
			mvarPctIce = Value
		End Set
	End Property
	
	
	
	
	
	Public Property PctWater() As Byte
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.PctWater
			PctWater = mvarPctWater
		End Get
		Set(ByVal Value As Byte)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.PctWater = 5
			mvarPctWater = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property Oblateness() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Oblateness
			Oblateness = mvarOblateness
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Oblateness = 5
			mvarOblateness = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property bHasHabitableZone() As Boolean
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.bHasHabitableZone
			bHasHabitableZone = mvarbHasHabitableZone
		End Get
		Set(ByVal Value As Boolean)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.bHasHabitableZone = 5
			mvarbHasHabitableZone = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property SpecularPower() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SpecularPower
			SpecularPower = mvarSpecularPower
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.SpecularPower = 5
			mvarSpecularPower = Value
		End Set
	End Property
	
	
	
	
	
	Public Property SpecularColor() As CRGBColor
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SpecularColor
			SpecularColor = mvarSpecularColor
		End Get
		Set(ByVal Value As CRGBColor)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.SpecularColor = Form1
			mvarSpecularColor = Value
		End Set
	End Property
	
	
	
	
	
	Public Property HazeDensity() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.HazeDensity
			HazeDensity = mvarHazeDensity
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.HazeDensity = 5
			mvarHazeDensity = Value
		End Set
	End Property
	
	
	
	
	
	Public Property HazeColor() As CRGBColor
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.HazeColor
			HazeColor = mvarHazeColor
		End Get
		Set(ByVal Value As CRGBColor)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.HazeColor = Form1
			mvarHazeColor = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Color() As CRGBColor
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Color
			Color = mvarColor
		End Get
		Set(ByVal Value As CRGBColor)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.Color = Form1
			mvarColor = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property RotationOffset() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RotationOffset
			RotationOffset = mvarRotationOffset
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.RotationOffset = 5
			mvarRotationOffset = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property Atmosphere() As CAtmosphere
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Atmosphere
			Atmosphere = mvarAtmosphere
		End Get
		Set(ByVal Value As CAtmosphere)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.Atmosphere = Form1
			mvarAtmosphere = Value
		End Set
	End Property
	
	
	
	
	
	Public Property RingSystem() As CRings
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RingSystem
			RingSystem = mvarRingSystem
		End Get
		Set(ByVal Value As CRings)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.RingSystem = Form1
			mvarRingSystem = Value
		End Set
	End Property
	
	
	
	
	
	Public Property RotationPeriod() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RotationPeriod
			RotationPeriod = mvarRotationPeriod
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.RotationPeriod = 5
			mvarRotationPeriod = Value
		End Set
	End Property
	
	
	
	
	
	Public Property obliquity() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.obliquity
			obliquity = mvarobliquity
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.obliquity = 5
			mvarobliquity = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property OrbitingBodies() As CStars
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.OrbitingBodies
			OrbitingBodies = mvarOrbitingBodies
		End Get
		Set(ByVal Value As CStars)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.OrbitingBodies = Form1
			mvarOrbitingBodies = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property OrbitalData() As COrbit
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.OrbitalData
			OrbitalData = mvarOrbitalData
		End Get
		Set(ByVal Value As COrbit)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.OrbitalData = Form1
			mvarOrbitalData = Value
		End Set
	End Property
	
	
	
	
	
	Public Property composition() As CComposition
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Composition
			composition = mvarComposition
		End Get
		Set(ByVal Value As CComposition)
			'used when assigning an Object to the property, on the left side of a Set statement.
			'Syntax: Set x.Composition = Form1
			mvarComposition = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property SurfaceTemperatureK() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SurfaceTemperatureK
			SurfaceTemperatureK = mvarSurfaceTemperatureK
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.SurfaceTemperatureK = 5
			mvarSurfaceTemperatureK = Value
		End Set
	End Property
	
	
	
	Public Property IsHabitable() As Boolean
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.StarType
			IsHabitable = mvarIsHabitable
		End Get
		Set(ByVal Value As Boolean)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.StarType = 5
			mvarIsHabitable = Value
		End Set
	End Property
	
	
	
	Public Property InStarHZ() As Boolean
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.StarType
			InStarHZ = mvarInStarHZ
		End Get
		Set(ByVal Value As Boolean)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.StarType = 5
			mvarInStarHZ = Value
		End Set
	End Property
	
	
	
	
	Public Property ShortName() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.StarType
			ShortName = mvarShortName
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.StarType = 5
			mvarShortName = Value
		End Set
	End Property
	
	
	
	
	Public Property StarType() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.StarType
			StarType = mvarStarType
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.StarType = 5
			mvarStarType = Value
		End Set
	End Property
	
	
	
	Public Property SizeClass() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.StarType
			SizeClass = mvarSizeClass
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.StarType = 5
			mvarSizeClass = Value
		End Set
	End Property
	
	
	
	
	Public Property SpectralType() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SpectralType
			SpectralType = mvarSpectralType
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.SpectralType = 5
			mvarSpectralType = Value
		End Set
	End Property
	
	
	
	Public Property LuminosityClass() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SpectralType
			LuminosityClass = mvarLuminosityClass
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.SpectralType = 5
			mvarLuminosityClass = Value
		End Set
	End Property
	
	
	
	
	Public Property SurfaceGravityG() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.SurfaceGravityG
			SurfaceGravityG = mvarSurfaceGravityG
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.SurfaceGravityG = 5
			mvarSurfaceGravityG = Value
		End Set
	End Property
	
	
	
	
	
	Public Property DiameterMi() As Integer
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.DiameterMi
			DiameterMi = mvarDiameterMi
		End Get
		Set(ByVal Value As Integer)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.DiameterMi = 5
			mvarDiameterMi = Value
			mvarRadiusM = Value / 2 * 1609
			mvarRadiusSol = mvarRadiusM / RSol
			LookupSizeClass()
		End Set
	End Property
	
	
	
	
	
	Public Property RadiusM() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RadiusKm
			RadiusM = mvarRadiusM
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.RadiusKm = 5
			mvarRadiusM = Value
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			LookupSizeClass()
		End Set
	End Property
	
	
	
	
	
	Public Property RadiusSol() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RadiusSol
			RadiusSol = mvarRadiusSol
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.RadiusSol = 5
			mvarRadiusSol = Value
			mvarRadiusM = mvarRadiusSol * RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			LookupSizeClass()
		End Set
	End Property
	
	
	
	
	
	Public Property RightAscension() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.RightAscension
			RightAscension = mvarRightAscension
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.RightAscension = 5
			mvarRightAscension = Value
		End Set
	End Property
	
	
	
	
	
	Public Property MassSol() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.MassSol
			MassSol = mvarMassSol
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.MassSol = 5
			mvarMassSol = Value
			mvarMassKg = mvarMassSol * MSol
		End Set
	End Property
	
	
	
	
	
	Public Property HabitableZoneMinAU() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.HabitableZoneMinAU
			HabitableZoneMinAU = mvarHabitableZoneMinAU
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.HabitableZoneMinAU = 5
			mvarHabitableZoneMinAU = Value
		End Set
	End Property
	
	
	
	
	
	Public Property HabitableZoneMaxAU() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.HabitableZoneMaxAU
			HabitableZoneMaxAU = mvarHabitableZoneMaxAU
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.HabitableZoneMaxAU = 5
			mvarHabitableZoneMaxAU = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Tag() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Tag
			Tag = mvarTag
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Tag = 5
			mvarTag = Value
		End Set
	End Property
	
	
	
	
	
	Public Property MassKg() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.MassKg
			MassKg = mvarMassKg
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.MassKg = 5
			mvarMassKg = Value
			mvarMassSol = mvarMassKg / MSol
		End Set
	End Property
	
	
	
	
	
	Public Property DensityKgM3() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.DensityKgM3
			DensityKgM3 = mvarDensityKgM3
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.DensityKgM3 = 5
			mvarDensityKgM3 = Value
		End Set
	End Property
	
	
	
	
	
	Public Property DecDeg() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.DecDeg
			DecDeg = mvarDecDeg
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.DecDeg = 5
			mvarDecDeg = Value
		End Set
	End Property
	
	
	
	
	
	Public Property DistanceLY() As Double
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.DistanceLY
			DistanceLY = mvarDistanceLY
		End Get
		Set(ByVal Value As Double)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.DistanceLY = 5
			mvarDistanceLY = Value
		End Set
	End Property
	
	
	
	
	
	Public Property AppMag() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.AppMag
			AppMag = mvarAppMag
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.AppMag = 5
			mvarAppMag = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Albedo() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Albedo
			Albedo = mvarAlbedo
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Albedo = 5
			mvarAlbedo = Value
		End Set
	End Property
	
	
	
	
	
	Public Property AbsMag() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.AbsMag
			AbsMag = mvarAbsMag
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.AbsMag = 5
			mvarAbsMag = Value
		End Set
	End Property
	
	
	
	
	Public Function dice(ByRef nsides As Integer) As Integer
		dice = CInt(Rnd() * nsides) + 1
		If dice <= 0 Then dice = 1
		If dice >= nsides Then dice = nsides
	End Function
	
	
	Public Sub RollStar()
		Dim a As Short
		a = Int(Rnd() * 100) + 1
		
		Select Case a
			Case Is < 7
				' White dwarf
				mvarSpectralType = "WD"
				mvarStarType = "White Dwarf"
				
			Case 8 To 99
				mvarSpectralType = RollSubType(RollSpectralType())
				mvarLuminosityClass = RollLuminosityClass(mvarSpectralType)
				
				mvarStarType = "Star"
				
			Case Else
				mvarStarType = "Neutron Star"
				mvarSpectralType = "NS0"
				mvarLuminosityClass = RollLuminosityClass("NS0")
				If dice(100) = 1 Then mvarStarType = "Black Hole"
				
		End Select
		
		mvarTag = mvarSpectralType & mvarLuminosityClass
		
	End Sub
	
	Public Function RollSpectralType() As String
		Dim a As Integer
		Dim SpecType As String
		
		a = Int(Rnd() * 100000) + 1
		Select Case a
			Case Is < 16
				SpecType = "O"
			Case 17 To 2959
				SpecType = "B"
			Case 2960 To 14566
				SpecType = "A"
			Case 14567 To 49043
				SpecType = "F"
			Case 49044 To 69778
				SpecType = "G"
			Case 69779 To 94689
				SpecType = "K"
			Case 94690 To 99525
				SpecType = "M"
			Case 99526 To 99529
				SpecType = "R"
			Case 99530 To 99531
				SpecType = "S"
			Case 99532
				SpecType = "N"
			Case 99533
				SpecType = "WC"
			Case 99534 To 99535
				SpecType = "WN"
			Case 99536 To 99998
				SpecType = "R"
			Case 99999
				SpecType = "L"
			Case 100000
				SpecType = "T"
			Case Else
				SpecType = "T"
		End Select
		
		RollSpectralType = SpecType
	End Function
	
	Public Function RollSubType(ByRef SpecType As String) As String
		Dim a As Short
		a = Int(Rnd() * 100) + 1
		
		Select Case SpecType
			Case "O"
				Select Case a
					Case 1
						RollSubType = "O0"
					Case 2
						RollSubType = "O4"
					Case 3 To 8
						RollSubType = "O5"
					Case 9 To 16
						RollSubType = "O6"
					Case 17 To 28
						RollSubType = "O7"
					Case 29 To 46
						RollSubType = "O8"
					Case Else
						RollSubType = "O9"
				End Select
			Case "B"
				Select Case a
					Case 1
						RollSubType = "B0"
					Case 2 To 3
						RollSubType = "B1"
					Case 4 To 6
						RollSubType = "B2"
					Case 7 To 13
						RollSubType = "B3"
					Case 14 To 21
						RollSubType = "B4"
					Case 22 To 32
						RollSubType = "B5"
					Case 33 To 43
						RollSubType = "B6"
					Case 44 To 56
						RollSubType = "B7"
					Case 57 To 76
						RollSubType = "B8"
					Case Else
						RollSubType = "B9"
				End Select
			Case "A"
				Select Case a
					Case 1 To 9
						RollSubType = "A0"
					Case 10 To 16
						RollSubType = "A1"
					Case 17 To 25
						RollSubType = "A2"
					Case 26 To 34
						RollSubType = "A3"
					Case 35 To 43
						RollSubType = "A4"
					Case 44 To 54
						RollSubType = "A5"
					Case 55 To 64
						RollSubType = "A6"
					Case 65 To 75
						RollSubType = "A7"
					Case 76 To 87
						RollSubType = "A8"
					Case Else
						RollSubType = "A9"
				End Select
			Case "F"
				Select Case a
					Case 1 To 7
						RollSubType = "F0"
					Case 8 To 14
						RollSubType = "F1"
					Case 15 To 23
						RollSubType = "F2"
					Case 24 To 31
						RollSubType = "F3"
					Case 32 To 40
						RollSubType = "F4"
					Case 41 To 52
						RollSubType = "F5"
					Case 53 To 63
						RollSubType = "F6"
					Case 64 To 76
						RollSubType = "F7"
					Case 77 To 88
						RollSubType = "F8"
					Case Else
						RollSubType = "F9"
				End Select
				
			Case "G"
				Select Case a
					Case 1 To 15
						RollSubType = "G0"
					Case 16 To 27
						RollSubType = "G1"
					Case 28 To 39
						RollSubType = "G2"
					Case 40 To 50
						RollSubType = "G3"
					Case 51 To 59
						RollSubType = "G4"
					Case 60 To 70
						RollSubType = "G5"
					Case 71 To 78
						RollSubType = "G6"
					Case 79 To 86
						RollSubType = "G7"
					Case 87 To 94
						RollSubType = "G8"
					Case Else
						RollSubType = "G9"
				End Select
			Case "K"
				Select Case a
					Case 1 To 15
						RollSubType = "K0"
					Case 16 To 27
						RollSubType = "K1"
					Case 28 To 41
						RollSubType = "K2"
					Case 42 To 54
						RollSubType = "K3"
					Case 55 To 65
						RollSubType = "K4"
					Case 66 To 75
						RollSubType = "K5"
					Case 76 To 83
						RollSubType = "K6"
					Case 84 To 90
						RollSubType = "K7"
					Case 91 To 95
						RollSubType = "K8"
					Case Else
						RollSubType = "K9"
				End Select
			Case "M"
				Select Case a
					Case 1 To 36
						RollSubType = "M0"
					Case 37 To 70
						RollSubType = "M1"
					Case 71 To 86
						RollSubType = "M2"
					Case 87 To 94
						RollSubType = "M3"
					Case 95 To 98
						RollSubType = "M4"
					Case Else
						a = Int(Rnd() * 100) + 1
						Select Case a
							Case 1 To 84
								RollSubType = "M5"
							Case 85 To 90
								RollSubType = "M6"
							Case 91 To 93
								RollSubType = "M7"
							Case 94 To 95
								RollSubType = "M8"
							Case Else
								RollSubType = "M9"
						End Select
				End Select
			Case "R"
				Select Case a
					Case 1
						RollSubType = "R0"
					Case 2
						RollSubType = "R1"
					Case 3
						RollSubType = "R2"
					Case 4 To 6
						RollSubType = "R3"
					Case 7 To 14
						RollSubType = "R4"
					Case 15 To 86
						RollSubType = "R5"
					Case 87 To 94
						RollSubType = "R6"
					Case 95
						RollSubType = "R7"
					Case 96 To 99
						RollSubType = "R8"
					Case Else
						RollSubType = "R9"
				End Select
			Case "S"
				Select Case a
					Case 1 To 70
						RollSubType = "S0"
					Case 71
						RollSubType = "S1"
					Case 72 To 73
						RollSubType = "S2"
					Case 74 To 75
						RollSubType = "S3"
					Case 76 To 95
						RollSubType = "S4"
					Case 96
						RollSubType = "S5"
					Case 97
						RollSubType = "S6"
					Case 98
						RollSubType = "S7"
					Case 99
						RollSubType = "S8"
					Case Else
						RollSubType = "S9"
				End Select
			Case "N"
				Select Case a
					Case 1
						RollSubType = "N0"
					Case 2 To 6
						RollSubType = "N1"
					Case 7
						RollSubType = "N2"
					Case 8
						RollSubType = "N3"
					Case 9 To 28
						RollSubType = "N4"
					Case 29 To 78
						RollSubType = "N5"
					Case 79 To 89
						RollSubType = "N6"
					Case 90 To 98
						RollSubType = "N7"
					Case 99
						RollSubType = "N8"
					Case Else
						RollSubType = "N9"
				End Select
				
			Case "WC"
				Select Case a
					Case 1
						RollSubType = "WC0"
					Case 2
						RollSubType = "WC1"
					Case 3
						RollSubType = "WC2"
					Case 4
						RollSubType = "WC3"
					Case 5 To 8
						RollSubType = "WC4"
					Case 9 To 86
						RollSubType = "WC5"
					Case 87 To 90
						RollSubType = "WC6"
					Case 91
						RollSubType = "WC7"
					Case 92 To 97
						RollSubType = "WC8"
					Case Else
						RollSubType = "WC9"
				End Select
			Case "WN"
				Select Case a
					Case 1
						RollSubType = "WN0"
					Case 2
						RollSubType = "WN1"
					Case 3 To 4
						RollSubType = "WN2"
					Case 5 To 7
						RollSubType = "WN3"
					Case 8 To 14
						RollSubType = "WN4"
					Case 15 To 69
						RollSubType = "WN5"
					Case 70 To 83
						RollSubType = "WN6"
					Case 84 To 93
						RollSubType = "WN7"
					Case 94 To 99
						RollSubType = "WN8"
					Case Else
						RollSubType = "WN9"
				End Select
				
			Case "Unk"
				Select Case a
					Case 1 To 30
						RollSubType = "Unk0"
					Case 31 To 50
						RollSubType = "Unk1"
					Case 51 To 75
						RollSubType = "Unk2"
					Case 76 To 90
						RollSubType = "Unk3"
					Case 91 To 92
						RollSubType = "Unk4"
					Case 93 To 94
						RollSubType = "Unk5"
					Case 95 To 96
						RollSubType = "Unk6"
					Case 97 To 98
						RollSubType = "Unk7"
					Case 99
						RollSubType = "Unk8"
					Case Else
						RollSubType = "Unk9"
						
				End Select
				
			Case "L"
				RollSubType = SpecType & CStr(Int(Rnd() * 10))
			Case "T"
                RollSubType = SpecType & CStr(Int(Rnd() * 10))
            Case Else
                RollSubType = SpecType & "2"
        End Select
	End Function
	
	Public Function RollLuminosityClass(ByRef SpecType As String) As String
		Dim lc As String
		Dim a As Integer
		
		' I-a0 I-a I-b II III IV V VI Total
		lc = "V"
		Select Case SpecType
			Case "O5"
				a = dice(21)
				If a = 1 Then
					lc = "Ib"
				End If
				
			Case "O6"
				a = dice(28)
				If a = 1 Then
					lc = "III"
				End If
				
			Case "O7"
				a = dice(39)
				Select Case a
					Case Is <= 2
						lc = "Ia"
					Case 3
						lc = "Ib"
					Case 4
						lc = "II"
					Case 5 To 7
						lc = "III"
					Case 8
						lc = "IV"
				End Select
				
			Case "O8"
				
				a = dice(58)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2
						lc = "Ib"
					Case 3
						lc = "II"
					Case 4
						lc = "III"
					Case 5
						lc = "IV"
				End Select
				
			Case "O9"
				a = dice(182)
				Select Case a
					Case 1 To 20
						lc = "Ia"
					Case 21 To 35
						lc = "Ib"
					Case 36 To 44
						lc = "II"
					Case 45 To 60
						lc = "III"
					Case 61 To 68
						lc = "IV"
					Case 69 To 177
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "B0"
				a = dice(447)
				Select Case a
					Case 1 To 35
						lc = "Ia"
					Case 36 To 70
						lc = "Ib"
					Case 71 To 96
						lc = "II"
					Case 97 To 175
						lc = "III"
					Case 176 To 222
						lc = "IV"
				End Select
				
			Case "B1"
				a = dice(936)
				Select Case a
					Case 1 To 45
						lc = "Ia"
					Case 46 To 123
						lc = "Ib"
					Case 124 To 166
						lc = "II"
					Case 167 To 224
						lc = "III"
					Case 225 To 261
						lc = "IV"
				End Select
				
			Case "B2"
				a = dice(2396)
				Select Case a
					Case 1 To 15
						lc = "Ia"
					Case 16 To 48
						lc = "Ib"
					Case 49 To 106
						lc = "II"
					Case 107 To 245
						lc = "III"
					Case 246 To 421
						lc = "IV"
				End Select
				
			Case "B3"
				a = dice(3919)
				Select Case a
					Case 1 To 15
						lc = "Ia"
					Case 16 To 26
						lc = "Ib"
					Case 27 To 66
						lc = "II"
					Case 67 To 164
						lc = "III"
					Case 165 To 259
						lc = "IV"
				End Select
				
			Case "B4"
				a = dice(4954)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 5
						lc = "Ib"
					Case 6 To 14
						lc = "II"
					Case 15 To 66
						lc = "III"
					Case 67 To 95
						lc = "IV"
				End Select
				
			Case "B5"
				a = dice(6643)
				Select Case a
					Case 1 To 13
						lc = "Ia"
					Case 14 To 27
						lc = "Ib"
					Case 28 To 67
						lc = "II"
					Case 68 To 219
						lc = "III"
					Case 220 To 278
						lc = "IV"
					Case 279 To 6641
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "B6"
				a = dice(7036)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 11
						lc = "Ib"
					Case 12 To 24
						lc = "II"
					Case 25 To 107
						lc = "III"
					Case 108 To 143
						lc = "IV"
				End Select
				
			Case "B7"
				a = dice(7885)
				Select Case a
					Case 1 To 6
						lc = "Ia"
					Case 7 To 13
						lc = "Ib"
					Case 14 To 71
						lc = "II"
					Case 72 To 205
						lc = "III"
					Case 206 To 271
						lc = "IV"
				End Select
				
			Case "B8"
				a = dice(12170)
				Select Case a
					Case 1 To 19
						lc = "Ia"
					Case 20 To 51
						lc = "Ib"
					Case 52 To 221
						lc = "II"
					Case 222 To 592
						lc = "III"
					Case 593 To 762
						lc = "IV"
				End Select
				
			Case "B9"
				a = dice(14613)
				Select Case a
					Case 1 To 19
						lc = "Ia"
					Case 20 To 39
						lc = "Ib"
					Case 40 To 105
						lc = "II"
					Case 106 To 539
						lc = "III"
					Case 540 To 1000
						lc = "IV"
				End Select
				
			Case "A0"
				a = dice(20929)
				Select Case a
					Case 1 To 19
						lc = "Ia"
					Case 20 To 30
						lc = "Ib"
					Case 31 To 50
						lc = "II"
					Case 51 To 265
						lc = "III"
					Case 266 To 493
						lc = "IV"
				End Select
				
			Case "A1"
				a = dice(16849)
				Select Case a
					Case 1 To 6
						lc = "Ia"
					Case 7 To 10
						lc = "Ib"
					Case 11 To 19
						lc = "II"
					Case 20 To 197
						lc = "III"
					Case 198 To 388
						lc = "IV"
				End Select
				
			Case "A2"
				a = dice(21617)
				Select Case a
					Case 1 To 11
						lc = "Ia"
					Case 12 To 19
						lc = "Ib"
					Case 20 To 43
						lc = "II"
					Case 44 To 329
						lc = "III"
					Case 330 To 525
						lc = "IV"
					Case 526 To 21616
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "A3"
				a = dice(21966)
				Select Case a
					Case 1 To 9
						lc = "Ia"
					Case 10 To 16
						lc = "Ib"
					Case 17 To 38
						lc = "II"
					Case 39 To 393
						lc = "III"
					Case 394 To 541
						lc = "IV"
				End Select
				
			Case "A4"
				a = dice(21811)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2 To 14
						lc = "II"
					Case 15 To 302
						lc = "III"
					Case 303 To 380
						lc = "IV"
				End Select
				
			Case "A5"
				a = dice(25901)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 5
						lc = "Ib"
					Case 6 To 26
						lc = "II"
					Case 27 To 329
						lc = "III"
					Case 330 To 466
						lc = "IV"
				End Select
				
			Case "A6"
				a = dice(25016)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 5
						lc = "Ib"
					Case 6 To 15
						lc = "II"
					Case 16 To 303
						lc = "III"
					Case 304 To 356
						lc = "IV"
				End Select
				
			Case "A7"
				a = dice(27533)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 4
						lc = "Ib"
					Case 5 To 20
						lc = "II"
					Case 21 To 386
						lc = "III"
					Case 387 To 481
						lc = "IV"
				End Select
				
			Case "A8"
				a = dice(28640)
				Select Case a
					Case 1
						lc = "Ib"
					Case 2 To 5
						lc = "II"
					Case 6 To 358
						lc = "III"
					Case 359 To 439
						lc = "IV"
				End Select
				
			Case "A9"
				a = dice(30328)
				Select Case a
					Case 1 To 4
						lc = "Ia"
					Case 5 To 6
						lc = "Ib"
					Case 7 To 15
						lc = "II"
					Case 17 To 386
						lc = "III"
					Case 387 To 529
						lc = "IV"
				End Select
				
			Case "F0"
				a = dice(51733)
				Select Case a
					Case 1 To 6
						lc = "Ia"
					Case 7 To 14
						lc = "Ib"
					Case 15 To 40
						lc = "II"
					Case 41 To 715
						lc = "III"
					Case 716 To 1116
						lc = "IV"
					Case 1117 To 51730
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F1"
				a = dice(50621)
				Select Case a
					Case 1 To 2
						lc = "II"
					Case 2 To 518
						lc = "III"
					Case 519 To 533
						lc = "IV"
					Case Else
						lc = "V"
				End Select
				
			Case "F2"
				a = dice(58755)
				Select Case a
					Case 1 To 7
						lc = "Ia"
					Case 8 To 25
						lc = "Ib"
					Case 26 To 76
						lc = "II"
					Case 77 To 806
						lc = "III"
					Case 807 To 1145
						lc = "IV"
					Case 1146 To 58751
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F3"
				a = dice(61839)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 14
						lc = "Ib"
					Case 15 To 41
						lc = "II"
					Case 42 To 732
						lc = "III"
					Case 733 To 1150
						lc = "IV"
					Case 1151 To 61837
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F4"
				a = dice(66242)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 5
						lc = "Ib"
					Case 6 To 13
						lc = "II"
					Case 14 To 787
						lc = "III"
					Case 788 To 814
						lc = "IV"
					Case 815 To 66241
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F5"
				a = dice(81463)
				Select Case a
					Case 1 To 9
						lc = "Ia"
					Case 10 To 29
						lc = "Ib"
					Case 30 To 62
						lc = "II"
					Case 63 To 1062
						lc = "III"
					Case 1063 To 1402
						lc = "IV"
					Case 1403 To 81450
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F6"
				a = dice(80280)
				Select Case a
					Case 1 To 5
						lc = "Ia"
					Case 6 To 13
						lc = "Ib"
					Case 14 To 28
						lc = "II"
					Case 29 To 1205
						lc = "III"
					Case 1206 To 1371
						lc = "IV"
					Case 1372 To 80277
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
				
			Case "F7"
				a = dice(88719)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2 To 16
						lc = "Ib"
					Case 17 To 31
						lc = "II"
					Case 32 To 97
						lc = "III"
					Case 98 To 215
						lc = "IV"
					Case 216 To 88717
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "F8"
				a = dice(92131)
				Select Case a
					Case 1 To 11
						lc = "Ia"
					Case 12 To 44
						lc = "Ib"
					Case 45 To 62
						lc = "II"
					Case 63 To 92
						lc = "III"
					Case 93 To 203
						lc = "IV"
					Case 204 To 92122
						lc = "V"
					Case 92123 To 92131
						lc = "VI"
				End Select
				
			Case "F9"
				a = dice(82890)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 5
						lc = "Ib"
					Case 6
						lc = "II"
					Case 7 To 9
						lc = "III"
					Case 10 To 16
						lc = "IV"
					Case 17 To 82889
						lc = "V"
					Case 82890
						lc = "VI"
				End Select
				
			Case "G0"
				a = dice(64318)
				Select Case a
					Case 1 To 2
						lc = "Ia0"
					Case 3 To 9
						lc = "Ia"
					Case 10 To 33
						lc = "Ib"
					Case 34 To 50
						lc = "II"
					Case 51 To 102
						lc = "III"
					Case 103 To 190
						lc = "IV"
					Case 191 To 64316
						lc = "V"
					Case 64317 To 64318
						lc = "VI"
				End Select
				
			Case "G1"
				a = dice(53331)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 11
						lc = "Ib"
					Case 12 To 14
						lc = "II"
					Case 15 To 23
						lc = "III"
					Case 24 To 61
						lc = "IV"
					Case 62 To 53330
						lc = "V"
					Case 53331
						lc = "VI"
				End Select
				
			Case "G2"
				a = dice(50064)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 27
						lc = "Ib"
					Case 28 To 40
						lc = "II"
					Case 41 To 95
						lc = "III"
					Case 96 To 190
						lc = "IV"
					Case 191 To 50057
						lc = "V"
					Case 50058 To 50064
						lc = "VI"
				End Select
				
			Case "G3"
				a = dice(46010)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 27
						lc = "Ib"
					Case 28 To 39
						lc = "II"
					Case 40 To 255
						lc = "III"
					Case 256 To 377
						lc = "IV"
					Case 378 To 46008
						lc = "V"
					Case 46009 To 46010
						lc = "VI"
				End Select
				
			Case "G4"
				a = dice(41982)
				Select Case a
					Case 1 To 3
						lc = "Ib"
					Case 4 To 6
						lc = "II"
					Case 7 To 34047
						lc = "III"
					Case 34048 To 34060
						lc = "IV"
					Case 34061 To 41981
						lc = "V"
					Case 41982
						lc = "VI"
				End Select
				
			Case "G5"
				a = dice(45777)
				Select Case a
					Case 1
						lc = "Ia0"
					Case 2 To 9
						lc = "Ia"
					Case 10 To 35
						lc = "Ib"
					Case 36 To 98
						lc = "II"
					Case 99 To 31293
						lc = "III"
					Case 31294 To 31488
						lc = "IV"
					Case 31489 To 45774
						lc = "V"
					Case 45775 To 45777
						lc = "VI"
				End Select
				
			Case "G6"
				a = dice(35414)
				Select Case a
					'0
					Case 1
						lc = "Ia"
					Case 2 To 6
						lc = "Ib"
					Case 7 To 37
						lc = "II"
					Case 38 To 28929
						lc = "III"
					Case 28930 To 29134
						lc = "IV"
					Case 29135 To 35413
						lc = "V"
					Case 35414
						lc = "VI"
				End Select
				
			Case "G7"
				a = dice(31562)
				Select Case a
					Case 1 To 5
						lc = "II"
					Case 6 To 26339
						lc = "III"
					Case 26340 To 26347
						lc = "VI"
					Case 26348 To 31561
						lc = "V"
					Case 31562
						lc = "VI"
				End Select
				
			Case "G8"
				a = dice(33488)
				Select Case a
					'0
					Case 1 To 3
						lc = "Ia"
					Case 4 To 27
						lc = "Ib"
					Case 28 To 209
						lc = "II"
					Case 210 To 27170
						lc = "III"
					Case 27171 To 27795
						lc = "IV"
					Case 27796 To 33484
						lc = "V"
					Case 33485 To 33488
						lc = "VI"
				End Select
				
			Case "G9"
				a = dice(27853)
				Select Case a
					Case 1
						lc = "Ib"
					Case 2 To 9
						lc = "II"
					Case 10 To 23333
						lc = "III"
					Case 23334 To 23348
						lc = "IV"
					Case 23349 To 27852
						lc = "V"
					Case 27853
						lc = "VI"
				End Select
				
			Case "K0"
				a = dice(76050)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 15
						lc = "Ib"
					Case 16 To 166
						lc = "II"
					Case 167 To 55788
						lc = "III"
					Case 55789 To 56034
						lc = "IV"
					Case 56035 To 76049
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "K1"
				a = dice(65768)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2 To 8
						lc = "Ib"
					Case 9 To 135
						lc = "II"
					Case 136 To 52536
						lc = "III"
					Case 52537 To 52705
						lc = "IV"
				End Select
				
			Case "K2"
				a = dice(71793)
				Select Case a
					Case 1 To 13
						lc = "Ib"
					Case 14 To 108
						lc = "II"
					Case 109 To 52826
						lc = "III"
					Case 52827 To 52886
						lc = "IV"
				End Select
				
			Case "K3"
				a = dice(65367)
				Select Case a
					Case 1
						lc = "Ia0"
					Case 2 To 6
						lc = "Ia"
					Case 7 To 29
						lc = "Ib"
					Case 30 To 74
						lc = "II"
					Case 75 To 49520
						lc = "III"
					Case 49521 To 49554
						lc = "IV"
				End Select
				
			Case "K4"
				a = dice(57848)
				Select Case a
					Case 1 To 2
						lc = "Ia"
					Case 3 To 6
						lc = "Ib"
					Case 7 To 27
						lc = "II"
					Case 28 To 43589
						lc = "III"
					Case 43590 To 43599
						lc = "IV"
				End Select
				
			Case "K5"
				a = dice(51985)
				Select Case a
					Case 1
						lc = "Ia0"
					Case 2 To 5
						lc = "Ia"
					Case 6 To 22
						lc = "Ib"
					Case 23 To 43
						lc = "II"
					Case 44 To 36619
						lc = "III"
					Case 36620 To 36624
						lc = "IV"
				End Select
				
			Case "K6"
				a = dice(40492)
				Select Case a
					Case 1
						lc = "II"
					Case 2 To 30671
						lc = "III"
					Case 136 To 30671
						lc = "III"
					Case 30672
						lc = "IV"
				End Select
				
			Case "K7"
				a = dice(65768)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2 To 8
						lc = "Ib"
					Case 9 To 135
						lc = "II"
					Case 136 To 52536
						lc = "III"
					Case 52537 To 52705
						lc = "IV"
				End Select
				
			Case "K8"
				a = dice(28354)
				If a <= 22328 Then lc = "III"
				
			Case "K9"
				a = dice(24016)
				If a <= 19519 Then lc = "III"
				
			Case "M0"
				a = dice(35989)
				Select Case a
					Case 1 To 3
						lc = "Ia"
					Case 4 To 7
						lc = "Ib"
					Case 8 To 15
						lc = "II"
					Case 16 To 29990
						lc = "III"
					Case 29991 To 29992
						lc = "IV"
				End Select
				
			Case "M1"
				a = dice(34146)
				Select Case a
					Case 1 To 4
						lc = "Ia"
					Case 5 To 11
						lc = "Ib"
					Case 12 To 22
						lc = "II"
					Case 23 To 32178
						lc = "III"
				End Select
				
			Case "M2"
				a = dice(15728)
				Select Case a
					Case 1
						lc = "Ia0"
					Case 2 To 14
						lc = "Ia"
					Case 15 To 23
						lc = "Ib"
					Case 24 To 40
						lc = "II"
					Case 417 To 14992
						lc = "III"
					Case 14993
						lc = "IV"
				End Select
				
			Case "M3"
				a = dice(8156)
				Select Case a
					Case 1
						lc = "Ia0"
					Case 2 To 7
						lc = "Ia"
					Case 8 To 13
						lc = "Ib"
					Case 14 To 26
						lc = "II"
					Case 27 To 7817
						lc = "III"
				End Select
				
			Case "M4"
				a = dice(3797)
				Select Case a
					Case 1
						lc = "Ia"
					Case 2 To 4
						lc = "Ib"
					Case 5 To 16
						lc = "II"
					Case 17 To 3622
						lc = "III"
					Case 3623
						lc = "IV"
					Case 3624 To 3796
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "M5"
				a = dice(2049)
				Select Case a
					Case 1 To 3
						lc = "Ib"
					Case 4 To 28
						lc = "II"
					Case 29 To 1266
						lc = "III"
					Case 1267
						lc = "IV"
					Case 1268 To 2048
						lc = "V"
					Case Else
						lc = "VI"
				End Select
				
			Case "M6"
				a = dice(154)
				Select Case a
					Case 1
						lc = "Ib"
					Case 2 To 5
						lc = "II"
					Case 6 To 50
						lc = "III"
				End Select
				
			Case "M7"
				a = dice(65)
				Select Case a
					Case 1
						lc = "II"
					Case 2 To 20
						lc = "III"
				End Select
				
			Case "M8"
				a = dice(47)
				If a <= 10 Then lc = "III"
				
			Case "M9"
				a = dice(115)
				If a <= 7 Then lc = "III"
				
				
			Case "WC6" ' carbon sequence
				lc = "II"
				
			Case "WC8"
				a = dice(2)
				If a = 1 Then lc = "II"
				
			Case "WN6"
				a = dice(6)
				If a = 1 Then lc = "III"
				
			Case "Unk5"
				a = dice(931)
				If a = 1 Then lc = "II"
				
			Case Else
				lc = "V"
		End Select
		
		RollLuminosityClass = lc
	End Function
	
	
	Public Sub LookupStar()
		' References the database for temperature
		
		Dim db As New CDB
		
		If mvarStarType Like "White Dwarf" Then
			mvarMassSol = Rnd() * 2 + 0.5
			mvarMassKg = mvarMassSol * MSol
			
			mvarRadiusSol = 0.06533359758 / (mvarMassSol ^ (1 / 3)) ' a whole bunch of constants thrown together
			mvarRadiusM = mvarRadiusSol * RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			
			mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
			
			mvarSurfaceTemperatureK = dice(50000) ' same as Celestia
			
		ElseIf mvarStarType Like "Neutron Star" Then 
			mvarMassSol = Rnd() * (3 - 1.44) + 1.44
			mvarMassKg = mvarMassSol * MSol
			
			mvarSurfaceTemperatureK = CDbl(CInt(Rnd() * 100000000# - 10000#))
			
			mvarRadiusSol = 0.000001515878 / (mvarMassSol ^ (1 / 3))
			mvarRadiusM = mvarRadiusSol * RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
			mvarRotationPeriod = Rnd() / 60
			
		ElseIf mvarStarType Like "Black Hole" Then 
			mvarMassSol = dice(50) + 3 ' Most stars over 3 solar masses
			mvarMassKg = mvarMassSol * MSol
			
			mvarRadiusSol = mvarMassSol / 235714.399 ' Schwarzchild radius in solar units
			mvarRadiusM = mvarRadiusSol * RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			
			mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
			mvarSurfaceTemperatureK = 0.0000000772 * mvarMassSol ' Hawking temperature
			
		Else
			
			If mvarLuminosityClass Like "V" Then ' main sequence
				db.OpenDB("SELECT * FROM StarsTypeV WHERE Class = '" & mvarSpectralType & "'")
			ElseIf mvarLuminosityClass Like "III" Then  ' giants
				db.OpenDB("SELECT * FROM StarsTypeIII WHERE Class = '" & mvarSpectralType & "'")
			ElseIf mvarLuminosityClass Like "I*" Then  ' supergiants
				db.OpenDB("SELECT * FROM StarsTypeI WHERE Class = '" & mvarSpectralType & "'")
			Else
				Exit Sub
			End If
			
			If db.rs.State > 0 Then
				mvarSurfaceTemperatureK = CInt(db.rs.Fields("temperature").Value)
				mvarRadiusM = db.rs.Fields("RADIUS").Value
				mvarMassKg = db.rs.Fields("mass").Value
				
				mvarRadiusSol = mvarRadiusM / RSol
				mvarMassSol = mvarMassKg / MSol
				mvarDiameterMi = mvarRadiusM * 2 / 1609
				
				db.CloseDB()
			End If
		End If
		
		LookupSizeClass()
		mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
		mvarSurfaceGravityG = 6.80415E-12 * mvarMassKg / mvarRadiusM ^ 2
		mvarAbsMag = -2.5 * System.Math.Log(mvarRadiusSol ^ 2 * mvarSurfaceTemperatureK ^ 4 / (5780 ^ 4)) / System.Math.Log(10) + 4.83
		
		mvarHabitableZoneMinAU = 0.958 * mvarMassSol ^ 2.61 '32.6 * 10 ^ ((-26.8524194 - mvarAbsMag) / 5) * 63239.713959
		mvarHabitableZoneMaxAU = 1.004 * mvarMassSol ^ 2.87 '32.6 * 10 ^ ((-26.0574346 - mvarAbsMag) / 5) * 63239.713959
		
		If mvarMassSol > 1.1 Then
			mvarHabitableZoneMinAU = 0.927 * mvarMassSol ^ 3.05
		End If
		
		mvarbHasHabitableZone = True
		If mvarHabitableZoneMinAU > mvarHabitableZoneMaxAU Or mvarHabitableZoneMaxAU < (mvarRadiusM / (AU2km * 1000)) Then
			
			mvarbHasHabitableZone = False
		Else
			mvarbHasHabitableZone = True
		End If
		
	End Sub
	
	
	Public Sub LookupGasGiant()
		' References the database for temperature
		
		Dim db As New CDB
		
		db.OpenDB("SELECT * FROM StarsGasGiant WHERE Class = '" & mvarSpectralType & "'")
		
		If db.rs.State > 0 Then
			mvarRadiusM = db.rs.Fields("RADIUS").Value
			
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			
			'mvarMassKg = db.rs!Mass
			'mvarMassSol = mvarMassKg / MSol
			
			db.CloseDB()
		End If
		
		mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
		mvarSurfaceGravityG = 6.80415E-12 * mvarMassKg / mvarRadiusM ^ 2
		
	End Sub
	
	Public Sub RandAsteroid()
		' asteroid is an output here, but should have its diameter already
		
		Dim a As Byte

		a = dice(100)
		
		If mvarComposition Is Nothing Then mvarComposition = New CComposition
		
		With mvarComposition
			If a < 75 Then ' type c: albedo 0.03, carbon
				.Carbon = dice(100) / 100
				.Ice = dice(100 - .Carbon * 100) / 100
				.Methane = 1 - .Carbon - .Ice
				mvarAlbedo = 0.03
				mvarSpectralType = "C"
				
			ElseIf a < 75 + 17 Then  ' type S: albedo .1 to .22, nickel-iron, pure iron, magnesium silicate
				' Iron-nickel ratio
				.NickelIronRatio = (dice(75 - 6) + 6) / 100
				.NickelIron = dice(100) / 100
				.Iron = dice(100 - 100 * .NickelIron) / 100
				.Fayalite = 1 - .NickelIron - .Iron
				mvarAlbedo = dice(22) / 10
				mvarSpectralType = "S"
				
			Else ' type M: albedo .10-.18, pure nickel and iron
				.Nickel = dice(100) / 100
				.Iron = 1 - .Nickel
				mvarAlbedo = (dice(9) + 9) / 10
				mvarSpectralType = "M"
			End If
		End With
		
		If mvarRadiusM > 0 Then
			mvarMassKg = mvarComposition.CalculateMass(CDbl(mvarRadiusM))
			mvarMassSol = mvarMassKg / MSol
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			
		ElseIf mvarMassKg > 0 Then 
			mvarDensityKgM3 = mvarComposition.CalculateMass(0.620350490899386)
			
			mvarMassSol = mvarMassKg / MSol
			mvarRadiusM = (mvarMassKg / (4 / 3 * PI * mvarDensityKgM3)) ^ (1 / 3)
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
		End If
		
		mvarDensityKgM3 = mvarMassKg / (4 / 3 * PI * mvarRadiusM ^ 3)
		mvarSurfaceGravityG = 6.80415E-12 * mvarMassKg / mvarRadiusM ^ 2
		
		If mvarOrbitalData Is Nothing Then mvarOrbitalData = New COrbit
		
		With mvarOrbitalData
			.iDeg = .iDeg = randn * 30
			.AscendingNode = Rnd() * 360
			.eccentricity = Rnd() / 5 ' between 0.00 and 0.20
			If .eccentricity = 1 Then .eccentricity = 0.999
			
		End With
	End Sub
	
	
	Public Sub RandComet()
		Dim a As Byte
		
		a = dice(100)
		
		Select Case a
			Case 1 To 33
				mvarDensityKgM3 = CarbonaceousMetalRichCometDensityKgM3
				mvarSpectralType = "Carbonaceous, Metal-Rich"
			Case 34 To 66
				mvarDensityKgM3 = CarbonaceousMatrixRichCometDensityKgM3
				mvarSpectralType = "Carbonaceous, Matrix-Rich"
			Case Else
				mvarDensityKgM3 = ChondriteCometDensityKgM3
				mvarSpectralType = "Chondrite"
		End Select
		
		If mvarMassKg > 0 Then
			mvarRadiusM = (mvarMassKg / (4 / 3 * mvarDensityKgM3 * PI)) ^ (1 / 3)
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			
		ElseIf mvarMassKg = 0 And mvarRadiusM = 0 Then 
			mvarRadiusM = dice(10)
			mvarRadiusSol = mvarRadiusM / RSol
			mvarDiameterMi = mvarRadiusM * 2 / 1609
			mvarMassKg = mvarDensityKgM3 * 4 / 3 * PI * mvarRadiusM ^ 3
			mvarMassSol = mvarMassKg / MSol
			
		Else ' mvarRadiusM > 0
			mvarMassKg = mvarDensityKgM3 * 4 / 3 * PI * mvarRadiusM ^ 3
			mvarMassSol = mvarMassKg / MSol
		End If
		
		mvarSurfaceGravityG = 6.80415E-12 * mvarMassKg / mvarRadiusM ^ 2
		
		mvarAlbedo = 0.04
		If mvarOrbitalData Is Nothing Then mvarOrbitalData = New COrbit
		
		With mvarOrbitalData
			.eccentricity = CDbl(dice(4) + 95) / 100 ' .95-1.0 e
			.iDeg = Rnd() * 180 - 90
			.AscendingNode = Rnd() * 360
			If .eccentricity = 1 Then .eccentricity = 0.999
			
		End With
	End Sub
	
	
	
	Public Sub RollOrbitingBodies(ByRef intLevel As Short)
		
		Dim i As Integer
		Dim nplanets As Integer
		Dim a As Integer
		Dim tempstar As New CStar
		Dim NCtr As Integer
		Dim minRadAU As Double
		Dim d As Double
		Dim lastN As Integer
		Dim ii As Integer
		Dim bFoundIt As Boolean
		Dim logf As New CLogAndIniRoutines
		
		lastN = 0
		
		
		Select Case mvarMassKg
			Case Is < 1E+24 ' Small planets
				If Rnd() < 0.9 Then
					nplanets = 0
				Else
					nplanets = dice(2) - 1
				End If
			Case 1E+23 To 1.19484E+26 ' Terrestrial
				nplanets = dice(13) - 1
			Case 1.19484E+26 To 0.1 * MSol ' Jovians
				nplanets = dice(100) - 1
			Case 0.1 * MSol To MSol ' Planets
				nplanets = CInt(dice(10)) - 1
			Case Is > MSol
				nplanets = CInt(dice(10 * mvarMassSol)) - 1
		End Select
		
		'UPGRADE_NOTE: Object mvarOrbitingBodies may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarOrbitingBodies = Nothing
		mvarOrbitingBodies = New CStars
		
		NCtr = -1
		
		For i = 1 To nplanets
			mvarOrbitingBodies.Add()
			
			Pctr = Pctr + 1
			
			
			With mvarOrbitingBodies(i)
				
				If intLevel > 0 Then
					.ShortName = "Moon " & CStr(Pctr)
				Else
					.ShortName = "Planet " & CStr(Pctr)
				End If
				
				.Tag = mvarTag & "_" & .ShortName
				
				.obliquity = System.Math.Abs(randn * 20 + 10)
				
				
				'      If logf.FileExist(App.Path & "\Textures\hires\" & .Tag & ".jpg") = True Then
				'        Kill App.Path & "\Textures\hires\" & .Tag & ".jpg"
				'        Kill App.Path & "\gif\" & .Tag & ".gif"
				'      End If
				
				If intLevel < 1 Then
					.MassKg = Rnd() * 10 ^ (dice(Int(System.Math.Log(mvarMassKg) / System.Math.Log(10)) - 20) + 19)
				Else
					.MassKg = Rnd() * 1.5 * 10 ^ 23 'mvarMassKg
				End If
				
				Select Case .MassKg
					Case Is < 9E+18 ' asteroids and comets
						a = dice(100)
						If a < 50 Then
							.RandAsteroid()
							
							.StarType = "Asteroid"
							.IsHabitable = False
							
						Else
							.RandComet()
							.StarType = "Comet"
							.IsHabitable = False
						End If
						
					Case 9E+18 To 1E+21 ' planetoids
						.RandAsteroid()
						.StarType = "Planetoid"
						.IsHabitable = False
						
					Case 1E+21 To 1.19484E+26 ' terrestrial bodies
						.DensityKgM3 = 5500 + dice(5000) - 2500 ' Earth normal +- 2500
						.RadiusM = (.MassKg / (4 / 3 * PI * .DensityKgM3)) ^ (1 / 3)
						.StarType = "Terrestrial"
						
						If .OrbitalData Is Nothing Then .OrbitalData = New COrbit
						
						With .OrbitalData
							.eccentricity = Rnd() / 5
							.iDeg = randn * 5 + mvarOrbitalData.iDeg
							.AscendingNode = Rnd() * 360
							If .eccentricity = 1 Then .eccentricity = 0.999
						End With
						
						
					Case 1.19484E+26 To 2.46831E+28 ' jovians
						.LookupJovianClass()
						.LookupGasGiant()
						.StarType = "Jovian"
						
						If .OrbitalData Is Nothing Then .OrbitalData = New COrbit
						With .OrbitalData
							.eccentricity = Rnd() / 5
							.iDeg = randn * 5 + mvarOrbitalData.iDeg
							.AscendingNode = Rnd() * 360
							If .eccentricity = 1 Then .eccentricity = 0.999
						End With
						.Albedo = 0.6
						.IsHabitable = False
						
					Case 2.46831E+28 To 1.5E+29 ' brown dwarfs
						.StarType = "Brown Dwarf"
						.LookUpBrownDwarfClass()
						.LuminosityClass = "V"
						.LookupStar()
						
						.Albedo = 1
						
						If .OrbitalData Is Nothing Then .OrbitalData = New COrbit
						With .OrbitalData
							.eccentricity = Rnd() / 5
							.iDeg = randn * 5 + mvarOrbitalData.iDeg
							.AscendingNode = Rnd() * 360
							If .eccentricity = 1 Then .eccentricity = 0.999
						End With
						.IsHabitable = False
						
					Case Is > 1.5E+29 ' another star
						.StarType = "Star"
						tempstar.LookupStarType((.MassKg))
						tempstar.LookupStar()
						
						.SpectralType = tempstar.SpectralType
						.LuminosityClass = tempstar.LuminosityClass
						
						.MassKg = tempstar.MassKg
						.RadiusM = tempstar.RadiusM
						.SurfaceTemperatureK = tempstar.SurfaceTemperatureK
						.LookupStar()
						.LookupSizeClass()
						.SurfaceGravityG = 6.80415E-12 * .MassKg / .RadiusM ^ 2
						
						With .OrbitalData
							.eccentricity = Rnd() / 5
							.iDeg = Rnd() * 40 - 20
							.AscendingNode = Rnd() * 360
							If .eccentricity = 1 Then .eccentricity = 0.999
							
						End With
						
						.Albedo = 1
						.IsHabitable = False
						
				End Select
				
				If mvarMassSol > 0.5 Then
					minRadAU = (mvarRadiusM + .RadiusM + 50000000000#) / (1000 * AU2km)
				Else
					minRadAU = (mvarRadiusM + .RadiusM + 700000000#) / (1000 * AU2km)
				End If
				
				d = (mvarRadiusM * mvarSurfaceTemperatureK ^ 2 * System.Math.Sqrt(PI) / 1000000) / (AU2km * 1000)
				
				If minRadAU < d Then minRadAU = d
				
				' Crank out the rest of the orbital parameters
				Do 
					.OrbitalData.lN = .RollN
					
					If mvarMassSol * (.OrbitalData.lN / 4.83) ^ 2 / (AU2km * 1000) < minRadAU Then
						.OrbitalData.lN = Int(4.83 * System.Math.Sqrt(minRadAU / mvarMassSol)) + 1
						.OrbitalData.SemiMajorAxisAU = minRadAU
						
						bFoundIt = True
						
						Do While (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)) < minRadAU Or bFoundIt = True
							.OrbitalData.lN = .OrbitalData.lN + 1
							.OrbitalData.SemiMajorAxisAU = (.OrbitalData.lN / 4.83) ^ 2 * mvarMassSol
							
							' Make sure this level isn't filled
							bFoundIt = False
							For ii = 1 To mvarOrbitingBodies.Count
								If Not (mvarOrbitingBodies(ii).Tag Like .Tag) Then
									If mvarOrbitingBodies(ii).OrbitalData.lN = .OrbitalData.lN Then
										bFoundIt = True
										Exit For
									End If
								End If
							Next ii
							
							System.Windows.Forms.Application.DoEvents()
						Loop 
						
						
						' randomize it a little
						.OrbitalData.SemiMajorAxisAU = .OrbitalData.SemiMajorAxisAU + randn * (.OrbitalData.SemiMajorAxisAU / 20)
						
					End If
					
					
					
					'If .OrbitalData.lN > lastN Then
					'NCtr = NCtr + 1
					'.OrbitalData.lN = NCtr
					
					lastN = .OrbitalData.lN
					If .OrbitalData.eccentricity < 0.7 Then
						.RollK()
						.OrbitalData.eccentricity = .OrbitalData.lK / .OrbitalData.lN
					Else
						.OrbitalData.lK = .OrbitalData.eccentricity * .OrbitalData.lN
					End If
					
					'.OrbitalData.PeriodYr = mvarMassSol * (.OrbitalData.lN * .OrbitalData.EllipsePerimeterE(.OrbitalData.eccentricity) / w0) ^ 3
					'.OrbitalData.SemiMajorAxisAU = (mvarMassSol * .OrbitalData.PeriodYr ^ 2) ^ (1 / 3)
					
					.OrbitalData.SemiMajorAxisAU = mvarMassSol * (.OrbitalData.lN / 4.83) ^ 2
					.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
					
					.OrbitalData.MeanMotion = System.Math.Sqrt(G * (mvarMassKg + .MassKg) / .OrbitalData.SemiMajorAxisAU ^ 3)
					
					System.Windows.Forms.Application.DoEvents()
					'End If
				Loop While (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)) < minRadAU
				
				'      If .OrbitalData.SemiMajorAxisAU < 1 Then
				'        Debug.Print CStr(.OrbitalData.SemiMajorAxisAU * AU2km * 1000) & " m"
				'      Else
				'        Debug.Print CStr(.OrbitalData.SemiMajorAxisAU) & " AU"
				'      End If
				
				.LookupSizeClass()
				.SurfaceGravityG = 6.80415E-12 * .MassKg / .RadiusM ^ 2
				
				If (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity) >= mvarHabitableZoneMinAU And .OrbitalData.SemiMajorAxisAU * (1 + .OrbitalData.eccentricity) <= mvarHabitableZoneMaxAU) Or mvarInStarHZ = True Then
					
					.InStarHZ = True
				End If
				
				If .StarType Like "Terrestrial" Then
					.RollTerrestrialBody()
				End If
				
				If .Albedo > 1 Then .Albedo = 1
				If .Albedo < 0 Then .Albedo = 0
				
				If .SurfaceTemperatureK = 0 Then
					.SurfaceTemperatureK = ((.RadiusM * mvarRadiusM) / (4 * (.OrbitalData.SemiMajorAxisAU * AU2km * 1000) ^ 2)) ^ (1 / 4) * mvarSurfaceTemperatureK * (1 - .Albedo) ^ (1 / 4)
				End If
				
				If .SurfaceTemperatureK <= 2.17 And Not (.StarType Like "Black Hole") Then
					.SurfaceTemperatureK = 2.17
				End If
				
				
				.RingSystem.Generate(.RadiusM)
				.Atmosphere.Generate(.StarType, .SpectralType, .RadiusM)
				.CalculateHaze()
				
				.RotationPeriod = Rnd() * 18 + 18 '100
				.RotationOffset = Rnd() * 360
				
				.Oblateness = ((2 * PI) / (.RotationPeriod * 3600)) ^ 2 * .RadiusM ^ 3 / (2 * G * .MassKg)
				
				.OrbitalData.LongOfPericenter = Rnd() * 360
				.OrbitalData.ArgumentOfPericenter = Rnd() * 360
				.OrbitalData.PericenterDistance = .OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)
				
				If .StarType Like "Asteroid" Then
					.Oblateness = Rnd()
				End If
				
				
				If .OrbitalData.PeriodYr = 0 Then
					.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
				End If
				
				
				'If .MassKg > 1E+23 And intLevel < 1 Then
				'Debug.Print intLevel
				.RollOrbitingBodies(intLevel + 1)
				'End If
				
			End With
		Next i
	End Sub
	
	Public Sub LookupJovianClass()
		Select Case mvarMassKg
			Case Is > 2.367E+28
				mvarSpectralType = "MJ0"
			Case 2.265E+28 To 2.367E+28
				mvarSpectralType = "MJ0"
			Case 2.162E+28 To 2.265E+28
				mvarSpectralType = "MJ1"
			Case 2.059E+28 To 2.162E+28
				mvarSpectralType = "MJ2"
			Case 1.957E+28 To 2.059E+28
				mvarSpectralType = "MJ3"
			Case 1.854E+28 To 1.957E+28
				mvarSpectralType = "MJ4"
			Case 1.752E+28 To 1.854E+28
				mvarSpectralType = "MJ5"
			Case 1.645E+28 To 1.752E+28
				mvarSpectralType = "MJ6"
			Case 1.546E+28 To 1.645E+28
				mvarSpectralType = "MJ7"
			Case 1.444E+28 To 1.546E+28
				mvarSpectralType = "MJ8"
			Case 1.351E+28 To 1.444E+28
				mvarSpectralType = "MJ9"
			Case 1.277E+28 To 1.351E+28
				mvarSpectralType = "SJ0"
			Case 1.203E+28 To 1.277E+28
				mvarSpectralType = "SJ1"
			Case 1.128E+28 To 1.203E+28
				mvarSpectralType = "SJ2"
			Case 1.054E+28 To 1.128E+28
				mvarSpectralType = "SJ3"
			Case 9.803E+27 To 1.054E+28
				mvarSpectralType = "SJ4"
			Case 9.062E+27 To 9.803E+27
				mvarSpectralType = "SJ5"
			Case 8.321E+27 To 9.062E+27
				mvarSpectralType = "SJ6"
			Case 7.58E+27 To 8.321E+27
				mvarSpectralType = "SJ7"
			Case 6.839E+27 To 7.58E+27
				mvarSpectralType = "SJ8"
			Case 6.649E+27 To 6.839E+27
				mvarSpectralType = "SJ9"
			Case 5.589E+27 To 6.649E+27
				mvarSpectralType = "EJ0"
			Case 5.059E+27 To 5.589E+27
				mvarSpectralType = "EJ1"
			Case 4.529E+27 To 5.059E+27
				mvarSpectralType = "EJ2"
			Case 3.999E+27 To 4.529E+27
				mvarSpectralType = "EJ3"
			Case 3.469E+27 To 3.999E+27
				mvarSpectralType = "EJ4"
			Case 2.939E+27 To 3.469E+27
				mvarSpectralType = "EJ5"
			Case 2.409E+27 To 2.939E+27
				mvarSpectralType = "EJ6"
			Case 1.879E+27 To 2.409E+27
				mvarSpectralType = "EJ7"
			Case 1.349E+27 To 1.879E+27
				mvarSpectralType = "EJ8"
			Case 1.33E+27 To 1.349E+27
				mvarSpectralType = "EJ9"
			Case 1.245E+27 To 1.33E+27
				mvarSpectralType = "BJ0"
			Case 1.161E+27 To 1.245E+27
				mvarSpectralType = "BJ1"
			Case 1.077E+27 To 1.161E+27
				mvarSpectralType = "BJ2"
			Case 9.921E+26 To 1.077E+27
				mvarSpectralType = "BJ3"
			Case 9.077E+26 To 9.921E+26
				mvarSpectralType = "BJ4"
			Case 8.232E+26 To 9.077E+26
				mvarSpectralType = "BJ5"
			Case 7.388E+26 To 8.232E+26
				mvarSpectralType = "BJ6"
			Case 6.544E+26 To 7.388E+26
				mvarSpectralType = "BJ7"
			Case 5.699E+26 To 6.544E+26
				mvarSpectralType = "BJ8"
			Case 5.25E+26 To 5.699E+26
				mvarSpectralType = "BJ9"
			Case 4.8E+26 To 5.25E+26
				mvarSpectralType = "CJ0"
			Case 4.35E+26 To 4.8E+26
				mvarSpectralType = "CJ1"
			Case 3.9E+26 To 4.35E+26
				mvarSpectralType = "CJ2"
			Case 3.45E+26 To 3.9E+26
				mvarSpectralType = "CJ3"
			Case 3E+26 To 3.45E+26
				mvarSpectralType = "CJ4"
			Case 2.55E+26 To 3E+26
				mvarSpectralType = "CJ5"
			Case 2.1E+26 To 2.55E+26
				mvarSpectralType = "CJ6"
			Case 1.65E+26 To 2.1E+26
				mvarSpectralType = "CJ7"
			Case 1.2E+26 To 1.65E+26
				mvarSpectralType = "CJ8"
			Case Is < 1.2E+26
				mvarSpectralType = "CJ9"
		End Select
	End Sub
	
	
	Public Sub LookUpBrownDwarfClass()
		
		Select Case mvarMassKg
			Case Is > 1.59114E+29
				mvarSpectralType = "E0"
			Case 1.43202E+29 To 1.59114E+29
				mvarSpectralType = "E0"
			Case 1.37832E+29 To 1.43202E+29
				mvarSpectralType = "E1"
			Case 1.32595E+29 To 1.37832E+29
				mvarSpectralType = "E2"
			Case 1.27291E+29 To 1.32595E+29
				mvarSpectralType = "E3"
			Case 1.16352E+29 To 1.27291E+29
				mvarSpectralType = "E4"
			Case 1.05413E+29 To 1.16352E+29
				mvarSpectralType = "E5"
			Case 9.24848E+28 To 1.05413E+29
				mvarSpectralType = "E6"
			Case 7.95568E+28 To 9.24848E+28
				mvarSpectralType = "E7"
			Case 3.98898E+28 To 7.95568E+28
				mvarSpectralType = "E8"
			Case Is < 3.98898E+28
				mvarSpectralType = "E9"
				
		End Select
		
		
	End Sub
	
	Public Sub LookupSizeClass()
		Select Case mvarDiameterMi
			Case Is < 10 '"A"
				mvarSizeClass = "A"
			Case 11 To 100 '"B"
				mvarSizeClass = "B"
			Case 101 To 1000 '"C"
				mvarSizeClass = "C"
			Case 1001 To 4000 '"D"
				mvarSizeClass = "D"
			Case 4001 To 10000 '"E"
				mvarSizeClass = "E"
			Case 10001 To 40000 '"F"
				mvarSizeClass = "F"
			Case 40001 To 100000 '"G"
				mvarSizeClass = "G"
			Case 100001 To 1000000 '"H"
				mvarSizeClass = "H"
			Case 1000001 To 10000000 '"I"
				mvarSizeClass = "I"
			Case 10000000 To 100000000 '"J"
				mvarSizeClass = "J"
			Case Is > 100000000 ' "K"
				mvarSizeClass = "K"
		End Select
	End Sub
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		mvarOrbitalData = New COrbit
		mvarComposition = New CComposition
		mvarOrbitingBodies = New CStars
		mvarAtmosphere = New CAtmosphere
		mvarRingSystem = New CRings
		mvarSpecularColor = New CRGBColor
		mvarHazeColor = New CRGBColor
		mvarColor = New CRGBColor
		mvarbHasHabitableZone = True
		
    End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'UPGRADE_NOTE: Object mvarOrbitalData may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarOrbitalData = Nothing
		'UPGRADE_NOTE: Object mvarComposition may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarComposition = Nothing
		'UPGRADE_NOTE: Object mvarOrbitingBodies may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarOrbitingBodies = Nothing
		'UPGRADE_NOTE: Object mvarAtmosphere may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarAtmosphere = Nothing
		'UPGRADE_NOTE: Object mvarRingSystem may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarRingSystem = Nothing
		'UPGRADE_NOTE: Object mvarSpecularColor may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarSpecularColor = Nothing
		'UPGRADE_NOTE: Object mvarHazeColor may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarHazeColor = Nothing
		'UPGRADE_NOTE: Object mvarColor may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarColor = Nothing
		

	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	
	Public Sub LookupStarType(ByRef MassKg As Double)
		
		Dim db As New CDB
		Dim tempstar As New CStar

		Dim ClosestMass As Double
		
		
		db.OpenDB("SELECT * FROM StarsAllStars ORDER BY Mass")
		
		ClosestMass = 0
		If db.rs.State > 0 Then
			With db.rs
				.MoveFirst()
				
				Do While Not .EOF
					If System.Math.Abs(MassKg - .Fields("mass").Value) < System.Math.Abs(MassKg - ClosestMass) Then
						ClosestMass = .Fields("mass").Value
						mvarSpectralType = .Fields("Class").Value
						mvarLuminosityClass = .Fields("Luminosity").Value
					End If
					.MoveNext()
					System.Windows.Forms.Application.DoEvents()
				Loop 
				
			End With
			db.CloseDB()
		End If
		
		
	End Sub
	
	
	
	Public Sub RollTerrestrialBody()
		Dim a As Integer
		
		mvarIsHabitable = False
		
		' M < 3 MEarth
		
		' M > 3 MEarth
		mvarDensityKgM3 = 5500
		
		Select Case mvarMassKg
			
			Case Is < 3 * MEarth
				a = dice(12)
				Select Case a
					Case Is <= 1
						mvarSpectralType = "Hephaestian" ' Bodies with largely molten surfaces. The degree of |              geologic activity will vary according to why the planet is being made so active. |              In most cases, however, surface activity is frequent and widespread enough to pose |              a changing surface morphology in a short geological time span.  Internal composition |              includes a nickel-iron core, surrounded by a silicate mantle.  This mantle extends |              directly to the surface, with only a very thin rocky crust that is in a constant |              state of flux.  The atmospheres of these worlds vary, depending on the nature of |              the planet itself.  Most such worlds have some sort of sulphur dioxide atmosphere, |              its density depending on the mass of the planet.  In extreme cases, quite a dense |              atmosphere may form, but will quickly erode if the volcanism ceases or decreases |              substantially.  Some Hephaestian worlds are formed due to tidal stretching, as is |              the case with the Solar System's Io.  Other worlds are classified this due to their |              early stages of formation;  these worlds are only transitory, however.
						
						' Two layers: NiFe core, Fayalite mantle/crust, Io stats
						' rho = 3587 kg/m3
						mvarDensityKgM3 = 3587
						mvarAlbedo = 0.61
						
					Case 2
						
						mvarSpectralType = "Ferrinian" ' Heavy metal bodies that form in systems with high |              metallic content, and often close to their parent star, where fierce stellar winds |              strip away the lighter elements.  These worlds are exceedingly rare, as most |              developing star systems do not possess enough heavy metal content to make an |              entire Terrestrial world.  The core and mantle are the most metal-rich zones, |              with a thin rocky crust that is usually poor in metal content.  Atmospheres are |              almost always trace, being constantly supplied with helium by the solar wind, |              as well as having that same wind rip it away.
						
						' Very rare, < Nickel core, NiFe mantle, Fayalite crust, Mercury stats (rho >= 5500)
						mvarAlbedo = 0.06
					Case 3
						mvarSpectralType = "Hermian" 'A rock world with a large metallic core, and a relatively |              thin crust.  Almost certainly these worlds are formed by a major impact early in |              the history of the planet, which strips away much of the lighter crust, leaving |              behind the metal core.  Surprisingly, these worlds are common, and indicate that |              cataclysmic impacts during the first few hundred million years of a planet's life |              may be routine.  Often, the metallic core may be partially molten, allowing for |              the presence of a magnetic field, albeit a relatively weak one.  The surface is |              usually a mixture of craters and lava plains, remnants from the formation period. |              Any atmosphere present will be supplied with helium from the solar wind, but the |              further from the parent sun, the more tenuous an atmosphere is likely to be.
						
						' Nickel/NiFe core, very thin Fayalite crust (rho = 5500)
						
					Case 4
						mvarSpectralType = "Selenian" ' A rock world with little or no geological activity, |              and a low metal content. While heavy volcanism and meteoric impacts marked an |              active ancient geology, the relatively small iron core will have quickly seen |              periods.  Most of these worlds are marked by dark maria regions, or areas that |              have seen major lava outpourings.  The sizes and shapes of these maria vary widely, |              and almost always uniformly date towards the end of the bombardment period. |              Geologic activity is possible on these worlds, however, often in the form of |              volatile deposits, formed during major volcanic events in the far past, that |              have been working their way towards the surface for billions of years.  When |              close enough to the surface, they briefly and violently erupt, expelling |              incandescent gases that are swiftly doused and scattered in the vacuum. |              Such eruptive events are quite rare, however.  Selenian worlds have no |              atmospheres.  These are perhaps among the most common of Terrestrial worlds.
						
						' Most common, Luna stats (rho = 3589)
						mvarDensityKgM3 = 3589
						mvarAlbedo = 0.12
					Case 5
						mvarSpectralType = "EoGaian" ' These are terrestrial worlds that have largely ceased |              their accretion as they orbit their young sun, and which have grown massive enough |              to begin forming an atmosphere through retention of volatiles provided by impacting |              comets and other bodies, as well as through gases released from within the body by |              massive amounts of volcanism.  The planet Type covers the period of atmospheric |              building from the initial buildup of atmospheric gases, during which time the |              surface may still be partially molten in some areas, to the point where a massive |              atmosphere has formed, and has reached the point where the first rains begin to |              deluge the land, forming the proto-oceans.  In these early conditions, there is |              almost always the presence of organic materials, including amino acids, but the |              emergence of life may not have yet occurred.  But these worlds are certainly on |              the track to evolving into MesoGaian Type worlds.
						mvarPctWater = dice(30)
						mvarPctIce = 0
						
						' Earth stats (rho = 5500)
						mvarAlbedo = 0.3
					Case 6
						mvarSpectralType = "MesoGaian" 'These worlds tend to be geologically active and quite |              young, and as a result often have surface water, substantial atmospheres, and the |              presence of primitive life.  As with most terrestrial worlds, the internal |              morphology is divided up between a molten or semi-molten iron core, a dense |              mantle, and a silicate crust.  The atmospheres are almost always similar, being |              heavy in carbon dioxide, sulphuric oxide, methane, and so forth.  Clouds cover |              the planet uniformly and thickly, and are typically reddish-brown.  Surface |              conditions vary widely, with some worlds being frigid and in the grips of massive |              ice sheets, while others are hot and subject to massive storms.  Oceans and seas |              are young and low in salt content, due to the lack of an extended period of |              erosional forces.  Geologic activity is also usually quite elevated, and major |              impacts can be quite common.  Life will likely form as soon as conditions allow, |              but typically remain stunted in a primitive monocellular microbial form for |              hundreds of millions of years, even billions of years.  In the end these planets |              will evolve into different Types.  A very few will become Gaian worlds, while |              others may experience runaway greenhouse effects and become Cytherean in nature. |              Lower mass worlds will lose their geological cycles fairly swiftly, and thus the |              atmospheres will erode, the oceans freeze, and the worlds will become Arean Types.
						
						' Earth stats (rho = 5500)
						mvarPctWater = dice(100)
						
						mvarAlbedo = 0.3
						If mvarInStarHZ = True And Rnd() * 100 < 5 Then
							mvarIsHabitable = True
						End If
						
					Case 7
						mvarSpectralType = "Arean" ' A so-called "mature" terrestrial world with little |              geological activity, often tenuous atmospheres, and which tend to be lower in mass. |              It is because of this lower mass that the worlds initially lose their "inner fires" |              as the molten core cools, bringing a halt to major volcanic activity.  This in turn |              brings about the slow but inevitable erosion of the atmosphere, the loss of a protective |              magnetic field, and other effects besides.  Larger massed worlds may retain thicker |              atmospheres, but a lack of geological activity does not allow for a significant |              difference in form from their smaller cousins.  Frozen surface water amounts vary, |              and at rare times under rare conditions may be able to exist in a liquid form under |              the open sky.  But for the most part, water is held in the form of massive frozen |              permafrost reserves, and / or in polar ice caps.  The geology of these worlds can be |              quite fascinating, providing an insight both into its livelier past, as well as the |              periods of its youth when it was being pummeled by meteoric impacts.  Often the |              surfaces are regarded as geological fossils.  It is possible, however, for more |              active geologies to continue, and indeed many of these worlds have what is termed |              a Sisyphean Cycle, where the planet vacillates between the cold, inert, mars-like |              environment, and a more active, warmer, wetter environment, much like a "Juvenile |              Biocompatible" Type.  Life is often found on these worlds, and is typically microbial |              extremophiles, existing where they can.  more rarely, there may be more advanced |              monocellular forms, or even isolated populations of multicellular microbes and near |              microbes, living in sheltered underground environments near geothermal vents.
						
						' Mars stats (rho = 3930)
						mvarDensityKgM3 = 3930
						
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.15
					Case 8
						mvarSpectralType = "Cytherean" ' One of the more common fates for a "Juvenile |              Biocompatible" world is for its atmosphere, working with the planet's mass or |              proximity to the primary sun, to continually thicken and initiate a runaway |              greenhouse effect.  In very short order, surface temperatures can rise well |              above 500 degrees Fahrenheit, while the atmospheric pressure can easily become |              so great as to preclude any effective exploration.  Geologically, these worlds |              often do not lose any of their internal fires.  However, with the loss of |              surface water to act as a lubricant, tectonic plates will grind to a halt, and |              all volcanic activity becomes static.  Over millions of years, the crust will |              thicken, trapping the planet's internal heat, until finally (roughly every 500 |              million years, but this may vary widely) the entire planetary surface will |              rupture and succumb to volcanic fires.  For a few million years this activity |              will continue, thickening the atmosphere even more, until finally a new crust |              forms and thickens, once again trapping the heat, and beginning the cycle all |              over again.  Intermediate volcanism is still possible, though rare.  However, |              due to the lack of significant erosional forces, over time the surface will |              become peppered with volcanoes.  Atmospheric composition is almost always |              primarily carbon dioxide, though depending on the state of the planet, there may |              be significant levels of sulphuric dioxide, carbon monoxide, or even methane. |              Life is theoretically possible in certain upper atmospheric environments, albeit |              at a primitive, anaerobic microbial level.  however, this is assuming that life |              formed initially during the juvenile stage of planetary development, and the |              shift into a Cytherean state was gradual enough for biological adaptation to |              take effect.
						
						' Venus stats (rho = 5500)
						mvarAlbedo = 0.77
					Case 9
						mvarSpectralType = "Moist Greenhouse" ' These worlds are a common world when it |              comes to planetary evolution, but they are so short lived in this state that |              they are actually fairly uncommon.  A moist greenhouse world develops when a |              planet accrues enough greenhouse gases to initiate a runaway greenhouse event. |              This causes the evaporation of oceanic environments, and the break up of water |              into hydrogen, which is lost to space, and oxygen, which forms compounds with |              other elements and is incorporated into the crust.  But the oceans remain intact |              for hundreds of millions of years, due to the high proportion of carbon dioxide |              in the atmosphere.  The oceans themselves remain at an average temperature of |              200 to 300 Fahrenheit, but the immense atmospheric pressure keeps them liquid. |              A breaking point is eventually reached when enough hydrogen is lost to space, |              and the atmospheric pressure drops low enough to allow a cascading loss of surface |              water to evaporation.  These worlds form in both young and old systems, when |              solar output reaches a point that a planet's watery surface begins to evaporate, |              and the atmosphere thickens and increases its pressure.  In both cases, all the |              water is eventually lost, and in older systems the entire atmosphere is eventually |              lost as the aging and expanding star completely destroys it.  A moist greenhouse |              world, at its height, will have an almost imperceptible transition between the |              depths of its atmosphere and the surface of its ocean.  Life may be present, but |              even extremophile forms are almost always found in the depths of the ocean, where |              the heat and the destructive effects of the atmosphere are lessened.
						
						mvarPctWater = dice(50)
						
						' Venus stats pre-cytherean (rho = 5500)
						If mvarInStarHZ = True And Rnd() * 100 < 5 Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.77
						
					Case 10
						mvarSpectralType = "Gaian" ' Gaian worlds have two primary and essential |              characteristics.  The less obvious but every bit as defining characteristic |              would be a self-sustaining geological cycle that aids in recycling silicates, |              water, even atmospheric components.  In nearly every case, it is a tectonic |              system consisting of moving plates of continental crust atop a more dense crustal |              layer.  Volcanic action, mountain building, earthquake zones, all are integral |              components to a cycle that maintains the habitability of these worlds.  The |              second, and more obvious component is, of course, the presence of liquid water |              on the surface.  Without ample oceans, plate tectonics would literally grind to |              a halt, lacking the lubrication provided as the liquid is subducted by the |              tectonic plates, along with the plates themselves.  The atmosphere is directly |              related to these characteristics, and further factor makes the atmosphere unique |              among planets -- life.  The presence of life produces huge amounts of free oxygen, |              a gas which would normally break down and combine with other compounds in the |              planetary crust.  This oxygen helps provide further energy and respiration for |              most forms of life, which in turn creates more oxygen, all self contained in a |              largely sustainable cycle.  From space, Gaian worlds are unmistakable:  blue |              and white marbles flecked with green and brown.  This is the universal sign of |              life, though by no means is life confined to Gaian planets.  These worlds also |              have a wide variety of surface conditions, ranging from barely Human-habitable |              surfaces with unbreathable trace gases and low surface water amounts, to lush |              worlds green and verdant, to cold and harsh planets largely covered in glacial |              conditions.  Some worlds are even found orbiting close to red dwarf stars, their |              surfaces made habitable by a variety of atmospheric and oceanic conditions.  But |              they all have geological cycles, and in one way or another maintain a degree of
						' habitability for billions of years.
						
						' Earth stats (rho = 5500)
						mvarPctWater = dice(100)
						
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.3
						
					Case 11
						mvarSpectralType = "LithicGelidian" ' The worlds are marked by a composition with substantial |              amounts of ice, due to their formation either in the outer regions of a solar |              system, or in orbit of cool, low mass stars.  During the formation process, most |              rocky material will sink towards the core, while the less dense ices remain in |              the outer volumes.  The ratio of ice to rock can vary widely, depending on the |              amount of material initially available.  Some worlds may be almost entirely ice. |              Regardless, many of these planets have experienced geological activity well after |              their formation was completed.  This so-called cryo-volcanism often leaves behind |              distinctive scarps and rift valleys on the planetary surfaces, features which may |              remain essentially unchanged for hundreds of millions of years after the activity |              has ceased.  In some instances, this activity may be ongoing to such a degree as |              to completely resurface a world in a few hundreds of thousands of years, leaving |              behind an initially enigmatic lack of impact craters.  Organic compounds, often
						' found in large abundances in the outer solar system, may also be released through _
						'this activity, leaving dark stains on the surface.  The atmospheres of these worlds _
						'may be non-existent, or so substantial as to create pressures more than than felt _
						'on Earth's surface.  Nitrogen and methane are the most common components, although _
						'other compounds may be found as well.
						
						mvarPctWater = dice(100)
						mvarPctIce = dice(50) + 50
						
						
						mvarDensityKgM3 = 1873
						mvarAlbedo = 0.2
						' Callisto (rho = 1873)
					Case 12
						mvarSpectralType = "Europan" ' These are LithicGelidian bodies that have surface or |              subsurface oceans due to |              tidal flexing or solar heating.  From space, most of these worlds appear unassuming, |              and little different from the more common LithicGelidian Type worlds.  Their |              surfaces may be very heavily cratered, or they may be exceedingly smooth. |              Usually there are surface indications of their unique subsurface feature:  oceans. |              These oceans, ranging from relatively thin layers of salty liquid, to water oceans |              hundreds of kilometers deep, often cause surface ice movement that obliterates impact craters, create odd geological patterns, or produce weak magnetic fields.  Regardless, there is typically several tens of kilometers of surface ice separating these oceans from the surface, and they usually rest on deeper ice layers.  Most silicate rock is found beneath this oceanic bed.  Larger massed worlds may have some atmosphere, formed from the outgassing of the subsurface ocean, but even in these cases the atmosphere is quickly lost to space. |              The oceans themselves are typically formed through tidal flexing, when the body in |              question is being pushed and pulled by other, nearby bodies.  For this reason, these |              worlds are almost exclusively found orbiting Jovian worlds.  Life may form on the |              more extreme Europan worlds, where the oceanic bed is actually a rock layer, and |              volcanic hotspots for and produce sources of energy.  However, very rarely does |              this life evolve beyond simple microbial forms.
						
						' Europa (rho = 3014)
						
						mvarPctWater = dice(100)
						mvarPctIce = dice(50) + 50
						
						mvarDensityKgM3 = 3014
						mvarAlbedo = 0.61
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						
				End Select
				
			Case Else
				mvarSpectralType = "Panthalassic" ' These terrestrial worlds tend to be very massive, |          averaging perhaps three to five times the mass of Earth.  These worlds tend to |          form beyond the "snow line", that point in the solar system where forming |          planetesimals tend to accrue very large amounts of water ice.  However, |          inward migration occurs before a massive atmosphere can form, and the planet can |          evolve into a Jovian world.  As the planet moves into and settles in the warmer |          zones, a massive layer of water forms, capable of equaling the content of over |          400 Earth oceans (although this amount, of course, varies from world to world). |          These world-girdling oceans can reach depths of well over 100 kilometers.  The |          atmosphere of these worlds, when youthful, will be rich in hydrogen, but after a |          very short time, a period of only a few million years, sunlight will have |          desiccated this supply, leaving behind a water, nitrogen, and carbon dioxide |          content.  Curiously, because of the lack of a tectonically active rocky crust |          (or any rocky crust), oxygen that has formed from the photodesiccation of water |          remains in the ambient atmosphere as well, and does not point to the presence of |          life, as it does with other terrestrial worlds.  The geologic makeup of the world |          is usually quite layered, with at least one Earth-mass worth of heavy metals |          forming the core, while up to 2 Earth-masses of rocky material make up the mantle. |          The "crust" is formed by up to three Earth-masses of icy material, upon which rests |          the world ocean.
				
				' 3 to 5 earth masses: 1 earth-mass Ni/NiFe core, 2 earth-mass Fe/Fayalite mantle, up to three _
				'earth-masses of ice, 100km deep ocean of liquid water (if warm enough)
				' mass is 1.8e25 kg to 3.62e25 kg
				mvarRadiusM = System.Math.Abs(-1.55013E-44 * mvarMassKg ^ 2 + 1.29889E-18 * mvarMassKg - 9101820#)
				mvarAlbedo = 0.3
				
				mvarPctWater = dice(10) + 90
				mvarPctIce = dice(20)
				
				If mvarInStarHZ = True And Rnd() * 100 < 5 Then
					mvarIsHabitable = True
				End If
				
				
				
		End Select
		'mvarSpectralType = "Stygian" ' When a sun evolves into a red giant, those worlds that do not find themselves _
		'consumed by the stellar fires are left as Stygian Type planets, once the sun _
		'collapses into a white dwarf.  These worlds experience massive surface melting _
		'and volcanism during the red giant phase, but when they cool off the planet _
		'becomes geologically quiescent.  In effect, it becomes a fossil.  Little or no _
		'atmosphere remains to these worlds, and as the eons drag on they tend to become _
		'highly cratered, often appearing from space much like a Hephaestian or Selenian _
		'world.
		
		' All terrestrial planets around compact objects within type III radius for _
		'the primary's mass.  Use Io or Luna for a start. (rho = 3590)
		
		'GenerateTerrestrialMap
		
	End Sub
	
	
	
	Public Sub CalculateHaze()
		
		If mvarSpectralType Like "EoGaian" Or mvarSpectralType Like "Europan" Or mvarSpectralType Like "LithicGelidian" Or mvarSpectralType Like "Panthalassic" Or mvarSpectralType Like "Gaian" Or mvarSpectralType Like "MesoGaian" Or mvarSpectralType Like "Cytherian" Or mvarSpectralType Like "Moist Greenhouse" Or mvarStarType Like "Jovian" Or mvarStarType Like "Brown Dwarf" Then
			
			mvarHazeColor.Red = 1
			mvarHazeColor.Green = 1
			mvarHazeColor.Blue = 1
			
			If mvarSpectralType Like "EoGaian" Or mvarSpectralType Like "Europan" Or mvarSpectralType Like "LithicGelidian" Or mvarSpectralType Like "Panthalassic" Or mvarSpectralType Like "Gaian" Then
				
				mvarColor.Red = 0.85
				mvarColor.Green = 0.85
				mvarColor.Blue = 1#
				
				mvarSpecularColor.Red = 0.5
				mvarSpecularColor.Green = 0.5
				mvarSpecularColor.Blue = 0.55
				
				mvarSpecularPower = 25#
				
				mvarHazeDensity = 0.3
				
			ElseIf mvarSpectralType Like "MesoGaian" Then 
				mvarColor.Red = 1#
				mvarColor.Green = 0.9
				mvarColor.Blue = 0.75
				
				mvarHazeColor.Red = 0.2
				mvarHazeColor.Green = 0.5
				mvarHazeColor.Blue = 1
				
				mvarHazeDensity = 1
				
			ElseIf mvarSpectralType Like "Cytherian" Or mvarSpectralType Like "Moist Greenhouse" Then 
				mvarHazeColor.Red = 0.5
				mvarHazeColor.Green = 0.35
				mvarHazeColor.Blue = 0.2
				mvarHazeDensity = 0.35
				
			ElseIf mvarSpectralType Like "Arean" Then 
				mvarColor.Red = 1
				mvarColor.Green = 0.75
				mvarColor.Blue = 0.7
				
				mvarHazeDensity = 0.45
			End If
			
			
			If mvarStarType Like "Jovian" Or mvarStarType Like "Brown Dwarf" Then
				If SpectralType Like "CJ*" Then
					mvarColor.Red = Rnd() / 4 + 0.75
					mvarColor.Green = Rnd() / 4 + 0.75
					mvarColor.Blue = 1
					
					mvarHazeColor.Red = Rnd() / 2 + 0.5
					mvarHazeColor.Green = Rnd() / 2 + 0.5
					mvarHazeColor.Blue = Rnd() / 4 + 0.75
					
					mvarHazeDensity = Rnd() * (0.35 - 0.2) + 0.2
				ElseIf mvarSpectralType Like "BJ*" Then 
					mvarColor.Red = 1
					mvarColor.Green = 1
					mvarColor.Blue = 0.85
					
					mvarHazeColor.Red = 0
					mvarHazeColor.Green = 0
					mvarHazeColor.Blue = 1
					
					mvarHazeDensity = 0.25
					
				Else
					mvarHazeColor.Red = 0.4
					mvarHazeColor.Green = 0.45
					mvarHazeColor.Blue = 0.5
					
					mvarHazeDensity = 0.3
					
				End If
			End If
		End If
		
		If mvarStarType Like "Star" Or mvarStarType Like "White Dwarf" Then
			mvarHazeDensity = 1
			mvarHazeColor.Blue = mvarAtmosphere.Lower.Blue
			mvarHazeColor.Green = mvarAtmosphere.Lower.Green
			mvarHazeColor.Red = mvarAtmosphere.Lower.Red
			
		End If
		
	End Sub
	
	
	
	Public Function RollN() As Integer
		Dim roll As Integer
		roll = dice(108702)
		
		Select Case roll
			Case Is < 17534
				RollN = 1
			Case 17535 To 27275
				RollN = 2
			Case 27276 To 37990
				RollN = 3
			Case 37991 To 51628
				RollN = 4
			Case 51629 To 66239
				RollN = 5
			Case 66240 To 74032
				RollN = 6
			Case 74033 To 88644
				RollN = 7
			Case 88645 To 97411
				RollN = 8
			Case 97412 To 101307
				RollN = 9
			Case 101308 To 103256
				RollN = 10
			Case 103257 To 105204
				RollN = 11
			Case 105205 To 106178
				RollN = 12
			Case 106179 To 106848
				RollN = 13
			Case 106849 To 107311
				RollN = 14
			Case 107311 To 107639
				RollN = 15
			Case 107639 To 107876
				RollN = 16
			Case 107876 To 108051
				RollN = 17
			Case 108051 To 108183
				RollN = 18
			Case 108183 To 108283
				RollN = 19
			Case 108283 To 108361
				RollN = 20
			Case 108361 To 108422
				RollN = 21
			Case 108422 To 108470
				RollN = 22
			Case 108470 To 108508
				RollN = 23
			Case 108508 To 108540
				RollN = 24
			Case 108540 To 108565
				RollN = 25
			Case 108565 To 108586
				RollN = 26
			Case 108586 To 108603
				RollN = 27
			Case 108603 To 108618
				RollN = 28
			Case 108618 To 108630
				RollN = 29
			Case 108630 To 108640
				RollN = 30
			Case 108640 To 108649
				RollN = 31
			Case 108649 To 108656
				RollN = 32
			Case 108656 To 108662
				RollN = 33
			Case 108663 To 108668
				RollN = 34
			Case 108669 To 108673
				RollN = 35
			Case 108674 To 108677
				RollN = 36
			Case 108678 To 108680
				RollN = 37
			Case 108681 To 108683
				RollN = 38
			Case 108684 To 108686
				RollN = 39
			Case 108687 To 108689
				RollN = 40
			Case 108690 To 108691
				RollN = 41
			Case 108692 To 108693
				RollN = 42
			Case 108694
				RollN = 43
			Case 108695 To 108696
				RollN = 44
			Case 108697
				RollN = 45
			Case 108698
				RollN = 46
			Case 108699
				RollN = 47
			Case 108700
				RollN = 48
			Case 108701
				RollN = 49
			Case 108702
				RollN = 50
		End Select
		mvarOrbitalData.lN = RollN
	End Function
	
	
	Public Function RollK() As Integer
		'P(k) = -0.186 * log(k + 1) + 0.3694, k < 7
		'k P(k)
		'0 37%
		'1 24%
		'2 17%
		'3 11%
		'4 7%
		'5 4%
		'6 1%
		'7 -2%
		Dim roll As Integer
		roll = dice(100)
		
		Select Case mvarOrbitalData.lN
			Case 1
				RollK = 0
			Case 2
				If roll < 70 Then
					RollK = 0
				Else
					RollK = 1
				End If
			Case 3
				roll = dice(11)
				Select Case roll
					Case Is <= 2
						RollK = 0
					Case 3 To 9
						RollK = 1
					Case Else
						RollK = 2
				End Select
			Case 4
				roll = dice(14)
				Select Case roll
					Case Is <= 2
						RollK = 0
					Case 3 To 8
						RollK = 1
					Case 9 To 13
						RollK = 2
					Case Else
						RollK = 3
				End Select
			Case 5
				roll = dice(20)
				Select Case roll
					Case Is <= 4
						RollK = 0
					Case 5 To 7
						RollK = 1
					Case 8 To 15
						RollK = 2
					Case 16 To 18
						RollK = 3
					Case Else
						RollK = 4
				End Select
			Case 6
				roll = dice(8)
				Select Case roll
					Case Is <= 1
						RollK = 0
					Case 2
						RollK = 1
					Case 3 To 4
						RollK = 2
					Case 5 To 6
						RollK = 3
					Case 7 To 8
						RollK = 4
					Case Else
						RollK = 5
				End Select
			Case 7
				roll = dice(15)
				Select Case roll
					Case Is <= 2
						RollK = 0
					Case 3
						RollK = 1
					Case 4 To 10
						RollK = 2
					Case 11 To 12
						RollK = 3
					Case 13 To 14
						RollK = 4
					Case Else
						RollK = 5
				End Select
			Case Else
				RollK = Int(System.Math.Abs(randn * 1.51 * mvarOrbitalData.lN / 7) + 3.22)
		End Select
		
		If RollK < 0 Then RollK = 0
		If RollK >= mvarOrbitalData.lN Then RollK = mvarOrbitalData.lN - 1
		
		mvarOrbitalData.lK = RollK
		
		
	End Function
	
	
	Public Sub CalculateGreenhouseEffect()
		Select Case mvarSpectralType
			Case "Cytherean"
				mvarSurfaceTemperatureK = mvarSurfaceTemperatureK + 500
			Case "Moist Greenhouse"
				mvarSurfaceTemperatureK = mvarSurfaceTemperatureK + 120
			Case "Gaian"
				mvarSurfaceTemperatureK = mvarSurfaceTemperatureK + 3
			Case "Panthalassic"
				mvarSurfaceTemperatureK = mvarSurfaceTemperatureK + 3
			Case Else
				
		End Select
		
	End Sub
	
	
	'Public Function GenerateTerrestrialMap() As String
	'  Dim ShellStatus As Double
	'  Dim log As New CLogAndIniRoutines
	'  Dim im As New GDIPBitmap
	'  Dim cEncoders As GDIPImageEncoderList
	'  Dim cCodec As GDIPImageCodec
	'
	'
	'  Set cCodec = g_cEncoders.EncoderForMimeType("image/jpeg")
	'
	'  mvarPctIce = mvarobliquity
	'
	
	' Want to generate it using the tectonic approach?
	
	' generate the map using the worldgen.c mapper
	'  ShellStatus = Shell("""" & App.Path & "\worldgen.exe"" 512 256 " & _
	''    Format(CLng(Rnd * 2000000000#), "0") & " 5000 " & CStr(mvarPctWater) & " " & _
	''    CStr(mvarPctIce) & " """ & App.Path & "\gif\" & mvarTag & """", vbMinimizedFocus)
	'
	'  Sleep 1500
	'
	'  If log.FileExist(App.Path & "\gif\" & mvarTag & ".gif") = True Then
	'
	'    ' Create the Image and load the file:
	'    im.Image.FromFile App.Path & "\gif\" & mvarTag & ".gif"
	'
	'    Dim cParamList As GDIPEncoderParameterList
	'    Dim cParam As GDIPEncoderParameter
	'    If (cCodec.MimeType = "image/jpeg") Then
	'
	'      Set cParamList = im.Image.EncoderParameterList(cCodec.CodecCLSID)
	'      Set cParam = cParamList.ParameterForGuid(EncoderQuality)
	'      cParam.ValueCount = 1
	'      cParam.Value(1) = g_cSaveOptions.JpgQuality
	'
	'      Set cParam = cParamList.ParameterForGuid(EncoderTransformation)
	'      If Not (g_cSaveOptions.JpgTransformation = 0) Then
	'        cParam.ValueCount = 1
	'        cParam.Value(1) = g_cSaveOptions.JpgTransformation
	'      Else
	'        cParam.ValueCount = 0
	'      End If
	'    End If
	'
	'im.Image.Save App.Path & "\Textures\hires\" & mvarTag & ".jpg", cCodec.CodecCLSID
	
	' install it
	'im.Image.Save "C:\Program Files\Celestia\Extras\GeneratedSystem\Textures\hires\" & mvarTag & ".jpg", cCodec.CodecCLSID
	
	'im.Dispose
	
	'  End If
	'
	'  GenerateTerrestrialMap = App.Path & "\Textures\hires\" & mvarTag & ".jpg"
	'
	'End Function
	
	
	
	Public Sub RollCometsAndAsteroidSystems()
		
		Dim i As Integer
		Dim nplanets As Integer
		Dim a As Integer
		Dim tempstar As New CStar
		Dim NCtr As Integer
		Dim minRadAU As Double
		Dim d As Double
		Dim lastN As Integer
		Dim ii As Integer
		Dim bFoundIt As Boolean
		Dim logf As New CLogAndIniRoutines
		
		
		lastN = 0
		
		
		nplanets = CInt(dice(100)) - 1
		
		NCtr = -1
		
		For i = 1 To nplanets
			mvarOrbitingBodies.Add()
			With mvarOrbitingBodies((mvarOrbitingBodies.Count))
				
				.MassKg = Rnd() * 10 ^ (dice(6) + 13)
				
				a = dice(100)
				If a < 50 Then
					.RandAsteroid()
					
					.StarType = "Asteroid"
					.IsHabitable = False
					
				Else
					.RandComet()
					.StarType = "Comet"
					.IsHabitable = False
				End If
				
				.ShortName = .StarType & " " & CStr(i)
				.Tag = mvarTag & "_" & .ShortName
				
				.obliquity = System.Math.Abs(randn * 20 + 10)
				
				
				
				If mvarMassSol > 0.5 Then
					minRadAU = (mvarRadiusM + .RadiusM + 50000000000#) / (1000 * AU2km)
				Else
					minRadAU = (mvarRadiusM + .RadiusM + 700000000#) / (1000 * AU2km)
				End If
				
				d = (mvarRadiusM * mvarSurfaceTemperatureK ^ 2 * System.Math.Sqrt(PI) / 1000000) / (AU2km * 1000)
				
				If minRadAU < d Then minRadAU = d
				
				' Crank out the rest of the orbital parameters
				Do 
					.OrbitalData.lN = .RollN
					
					If mvarMassSol * (.OrbitalData.lN / 4.83) ^ 2 / (AU2km * 1000) < minRadAU Then
						.OrbitalData.lN = Int(4.83 * System.Math.Sqrt(minRadAU / mvarMassSol)) + 1
						.OrbitalData.SemiMajorAxisAU = minRadAU
						
						bFoundIt = True
						
						Do While (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)) < minRadAU Or bFoundIt = True
							.OrbitalData.lN = .OrbitalData.lN + 1
							.OrbitalData.SemiMajorAxisAU = (.OrbitalData.lN / 4.83) ^ 2 * mvarMassSol
							
							' Make sure this level isn't filled
							bFoundIt = False
							For ii = 1 To mvarOrbitingBodies.Count
								If Not (mvarOrbitingBodies(ii).Tag Like .Tag) Then
									If mvarOrbitingBodies(ii).OrbitalData.lN = .OrbitalData.lN Then
										bFoundIt = True
										Exit For
									End If
								End If
							Next ii
							
							System.Windows.Forms.Application.DoEvents()
						Loop 
						
						
						' randomize it a little
						.OrbitalData.SemiMajorAxisAU = .OrbitalData.SemiMajorAxisAU + randn * (.OrbitalData.SemiMajorAxisAU / 20)
						
					End If
					
					
					lastN = .OrbitalData.lN
					If .OrbitalData.eccentricity < 0.7 Then
						.RollK()
						.OrbitalData.eccentricity = .OrbitalData.lK / .OrbitalData.lN
					Else
						.OrbitalData.lK = .OrbitalData.eccentricity * .OrbitalData.lN
					End If
					
					
					.OrbitalData.SemiMajorAxisAU = mvarMassSol * (.OrbitalData.lN / 4.83) ^ 2
					.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
					
					.OrbitalData.MeanMotion = System.Math.Sqrt(G * (mvarMassKg + .MassKg) / .OrbitalData.SemiMajorAxisAU ^ 3)
					
					System.Windows.Forms.Application.DoEvents()
				Loop While (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)) < minRadAU
				
				
				
				.LookupSizeClass()
				.SurfaceGravityG = 6.80415E-12 * .MassKg / .RadiusM ^ 2
				
				If (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity) >= mvarHabitableZoneMinAU And .OrbitalData.SemiMajorAxisAU * (1 + .OrbitalData.eccentricity) <= mvarHabitableZoneMaxAU) Or mvarInStarHZ = True Then
					
					.InStarHZ = True
				End If
				
				If .Albedo > 1 Then .Albedo = 1
				If .Albedo < 0 Then .Albedo = 0
				
				If .SurfaceTemperatureK = 0 Then
					.SurfaceTemperatureK = ((.RadiusM * mvarRadiusM) / (4 * (.OrbitalData.SemiMajorAxisAU * AU2km * 1000) ^ 2)) ^ (1 / 4) * mvarSurfaceTemperatureK * (1 - .Albedo) ^ (1 / 4)
				End If
				
				If .SurfaceTemperatureK <= 2.17 And Not (.StarType Like "Black Hole") Then
					.SurfaceTemperatureK = 2.17
				End If
				
				.RotationPeriod = Rnd() * 100
				.RotationOffset = Rnd() * 360
				
				.Oblateness = ((2 * PI) / (.RotationPeriod * 3600)) ^ 2 * .RadiusM ^ 3 / (2 * G * .MassKg)
				
				.OrbitalData.LongOfPericenter = Rnd() * 360
				.OrbitalData.ArgumentOfPericenter = Rnd() * 360
				.OrbitalData.PericenterDistance = .OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)
				
				.Oblateness = Rnd()
				
				If .OrbitalData.PeriodYr = 0 Then
					.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
				End If
			End With
		Next i
	End Sub
	
	
	Public Sub RollHabitableBody()
		Dim a As Integer
		' Like RollTerrestrialBody except it's always habitable.
		
		
		mvarDensityKgM3 = 5500
		
		Select Case mvarMassKg
			
			Case Is < 3 * MEarth
				a = dice(12)
				Select Case a
					Case Is <= 1
						mvarSpectralType = "MesoGaian" 'These worlds tend to be geologically active and quite |              young, and as a result often have surface water, substantial atmospheres, and the |              presence of primitive life.  As with most terrestrial worlds, the internal |              morphology is divided up between a molten or semi-molten iron core, a dense |              mantle, and a silicate crust.  The atmospheres are almost always similar, being |              heavy in carbon dioxide, sulphuric oxide, methane, and so forth.  Clouds cover |              the planet uniformly and thickly, and are typically reddish-brown.  Surface |              conditions vary widely, with some worlds being frigid and in the grips of massive |              ice sheets, while others are hot and subject to massive storms.  Oceans and seas |              are young and low in salt content, due to the lack of an extended period of |              erosional forces.  Geologic activity is also usually quite elevated, and major |              impacts can be quite common.  Life will likely form as soon as conditions allow, |              but typically remain stunted in a primitive monocellular microbial form for |              hundreds of millions of years, even billions of years.  In the end these planets |              will evolve into different Types.  A very few will become Gaian worlds, while |              others may experience runaway greenhouse effects and become Cytherean in nature. |              Lower mass worlds will lose their geological cycles fairly swiftly, and thus the |              atmospheres will erode, the oceans freeze, and the worlds will become Arean Types.
						
						' Earth stats (rho = 5500)
						mvarPctWater = dice(100)
						
						mvarAlbedo = 0.3
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						
					Case 2
						mvarSpectralType = "Arean" ' A so-called "mature" terrestrial world with little |              geological activity, often tenuous atmospheres, and which tend to be lower in mass. |              It is because of this lower mass that the worlds initially lose their "inner fires" |              as the molten core cools, bringing a halt to major volcanic activity.  This in turn |              brings about the slow but inevitable erosion of the atmosphere, the loss of a protective |              magnetic field, and other effects besides.  Larger massed worlds may retain thicker |              atmospheres, but a lack of geological activity does not allow for a significant |              difference in form from their smaller cousins.  Frozen surface water amounts vary, |              and at rare times under rare conditions may be able to exist in a liquid form under |              the open sky.  But for the most part, water is held in the form of massive frozen |              permafrost reserves, and / or in polar ice caps.  The geology of these worlds can be |              quite fascinating, providing an insight both into its livelier past, as well as the |              periods of its youth when it was being pummeled by meteoric impacts.  Often the |              surfaces are regarded as geological fossils.  It is possible, however, for more |              active geologies to continue, and indeed many of these worlds have what is termed |              a Sisyphean Cycle, where the planet vacillates between the cold, inert, mars-like |              environment, and a more active, warmer, wetter environment, much like a "Juvenile |              Biocompatible" Type.  Life is often found on these worlds, and is typically microbial |              extremophiles, existing where they can.  more rarely, there may be more advanced |              monocellular forms, or even isolated populations of multicellular microbes and near |              microbes, living in sheltered underground environments near geothermal vents.
						
						' Mars stats (rho = 3930)
						mvarDensityKgM3 = 3930
						
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.15
					Case 3
						mvarSpectralType = "Moist Greenhouse" ' These worlds are a common world when it |              comes to planetary evolution, but they are so short lived in this state that |              they are actually fairly uncommon.  A moist greenhouse world develops when a |              planet accrues enough greenhouse gases to initiate a runaway greenhouse event. |              This causes the evaporation of oceanic environments, and the break up of water |              into hydrogen, which is lost to space, and oxygen, which forms compounds with |              other elements and is incorporated into the crust.  But the oceans remain intact |              for hundreds of millions of years, due to the high proportion of carbon dioxide |              in the atmosphere.  The oceans themselves remain at an average temperature of |              200 to 300 Fahrenheit, but the immense atmospheric pressure keeps them liquid. |              A breaking point is eventually reached when enough hydrogen is lost to space, |              and the atmospheric pressure drops low enough to allow a cascading loss of surface |              water to evaporation.  These worlds form in both young and old systems, when |              solar output reaches a point that a planet's watery surface begins to evaporate, |              and the atmosphere thickens and increases its pressure.  In both cases, all the |              water is eventually lost, and in older systems the entire atmosphere is eventually |              lost as the aging and expanding star completely destroys it.  A moist greenhouse |              world, at its height, will have an almost imperceptible transition between the |              depths of its atmosphere and the surface of its ocean.  Life may be present, but |              even extremophile forms are almost always found in the depths of the ocean, where |              the heat and the destructive effects of the atmosphere are lessened.
						
						mvarPctWater = dice(50)
						
						' Venus stats pre-cytherean (rho = 5500)
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.77
						
					Case 4 To 11
						mvarSpectralType = "Gaian" ' Gaian worlds have two primary and essential |              characteristics.  The less obvious but every bit as defining characteristic |              would be a self-sustaining geological cycle that aids in recycling silicates, |              water, even atmospheric components.  In nearly every case, it is a tectonic |              system consisting of moving plates of continental crust atop a more dense crustal |              layer.  Volcanic action, mountain building, earthquake zones, all are integral |              components to a cycle that maintains the habitability of these worlds.  The |              second, and more obvious component is, of course, the presence of liquid water |              on the surface.  Without ample oceans, plate tectonics would literally grind to |              a halt, lacking the lubrication provided as the liquid is subducted by the |              tectonic plates, along with the plates themselves.  The atmosphere is directly |              related to these characteristics, and further factor makes the atmosphere unique |              among planets -- life.  The presence of life produces huge amounts of free oxygen, |              a gas which would normally break down and combine with other compounds in the |              planetary crust.  This oxygen helps provide further energy and respiration for |              most forms of life, which in turn creates more oxygen, all self contained in a |              largely sustainable cycle.  From space, Gaian worlds are unmistakable:  blue |              and white marbles flecked with green and brown.  This is the universal sign of |              life, though by no means is life confined to Gaian planets.  These worlds also |              have a wide variety of surface conditions, ranging from barely Human-habitable |              surfaces with unbreathable trace gases and low surface water amounts, to lush |              worlds green and verdant, to cold and harsh planets largely covered in glacial |              conditions.  Some worlds are even found orbiting close to red dwarf stars, their |              surfaces made habitable by a variety of atmospheric and oceanic conditions.  But |              they all have geological cycles, and in one way or another maintain a degree of
						' habitability for billions of years.
						
						' Earth stats (rho = 5500)
						mvarPctWater = dice(100)
						
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						mvarAlbedo = 0.3
						
					Case 12
						mvarSpectralType = "Europan" ' These are LithicGelidian bodies that have surface or |              subsurface oceans due to |              tidal flexing or solar heating.  From space, most of these worlds appear unassuming, |              and little different from the more common LithicGelidian Type worlds.  Their |              surfaces may be very heavily cratered, or they may be exceedingly smooth. |              Usually there are surface indications of their unique subsurface feature:  oceans. |              These oceans, ranging from relatively thin layers of salty liquid, to water oceans |              hundreds of kilometers deep, often cause surface ice movement that obliterates impact craters, create odd geological patterns, or produce weak magnetic fields.  Regardless, there is typically several tens of kilometers of surface ice separating these oceans from the surface, and they usually rest on deeper ice layers.  Most silicate rock is found beneath this oceanic bed.  Larger massed worlds may have some atmosphere, formed from the outgassing of the subsurface ocean, but even in these cases the atmosphere is quickly lost to space. |              The oceans themselves are typically formed through tidal flexing, when the body in |              question is being pushed and pulled by other, nearby bodies.  For this reason, these |              worlds are almost exclusively found orbiting Jovian worlds.  Life may form on the |              more extreme Europan worlds, where the oceanic bed is actually a rock layer, and |              volcanic hotspots for and produce sources of energy.  However, very rarely does |              this life evolve beyond simple microbial forms.
						
						' Europa (rho = 3014)
						
						mvarPctWater = dice(100)
						mvarPctIce = dice(50) + 50
						
						mvarDensityKgM3 = 3014
						mvarAlbedo = 0.61
						If mvarInStarHZ = True Then
							mvarIsHabitable = True
						End If
						
				End Select
				
			Case Else
				mvarSpectralType = "Panthalassic" ' These terrestrial worlds tend to be very massive, |          averaging perhaps three to five times the mass of Earth.  These worlds tend to |          form beyond the "snow line", that point in the solar system where forming |          planetesimals tend to accrue very large amounts of water ice.  However, |          inward migration occurs before a massive atmosphere can form, and the planet can |          evolve into a Jovian world.  As the planet moves into and settles in the warmer |          zones, a massive layer of water forms, capable of equaling the content of over |          400 Earth oceans (although this amount, of course, varies from world to world). |          These world-girdling oceans can reach depths of well over 100 kilometers.  The |          atmosphere of these worlds, when youthful, will be rich in hydrogen, but after a |          very short time, a period of only a few million years, sunlight will have |          desiccated this supply, leaving behind a water, nitrogen, and carbon dioxide |          content.  Curiously, because of the lack of a tectonically active rocky crust |          (or any rocky crust), oxygen that has formed from the photodesiccation of water |          remains in the ambient atmosphere as well, and does not point to the presence of |          life, as it does with other terrestrial worlds.  The geologic makeup of the world |          is usually quite layered, with at least one Earth-mass worth of heavy metals |          forming the core, while up to 2 Earth-masses of rocky material make up the mantle. |          The "crust" is formed by up to three Earth-masses of icy material, upon which rests |          the world ocean.
				
				' 3 to 5 earth masses: 1 earth-mass Ni/NiFe core, 2 earth-mass Fe/Fayalite mantle, up to three _
				'earth-masses of ice, 100km deep ocean of liquid water (if warm enough)
				' mass is 1.8e25 kg to 3.62e25 kg
				mvarRadiusM = System.Math.Abs(-1.55013E-44 * mvarMassKg ^ 2 + 1.29889E-18 * mvarMassKg - 9101820#)
				mvarAlbedo = 0.3
				
				mvarPctWater = dice(10) + 90
				mvarPctIce = dice(20)
				
				If mvarInStarHZ = True Then
					mvarIsHabitable = True
				End If
				
				
				
		End Select
		
	End Sub
	
	
	
	Public Sub RollHabitableOrbitingBody(ByRef intLevel As Short)
		
        Dim tempstar As New CStar
		Dim NCtr As Integer
		Dim minRadAU As Double
		Dim d As Double
		Dim lastN As Integer
        Dim logf As New CLogAndIniRoutines
		Dim MinSMA, MaxSMA As Single
		lastN = 0
		
		
		
		NCtr = -1
		mvarOrbitingBodies.Add()
		
		Pctr = Pctr + 1
		
		
		With mvarOrbitingBodies((mvarOrbitingBodies.Count))
			
			If intLevel > 0 Then
				.ShortName = "Moon " & CStr(Pctr)
			Else
				.ShortName = "Planet " & CStr(Pctr)
			End If
			
			.Tag = mvarTag & "_" & .ShortName
			
			.obliquity = System.Math.Abs(randn * 20 + 10)
			.MassKg = Rnd() * (1.79226E+25 - 1E+21) + 1E+21
			.DensityKgM3 = 5500 + dice(5000) - 2500 ' Earth normal +- 2500
			.RadiusM = (.MassKg / (4 / 3 * PI * .DensityKgM3)) ^ (1 / 3)
			.StarType = "Terrestrial"
			
			
			
			If .OrbitalData Is Nothing Then .OrbitalData = New COrbit
			
			With .OrbitalData
				.eccentricity = Rnd() * 0.0005
				.iDeg = randn * 5 + mvarOrbitalData.iDeg
				.AscendingNode = Rnd() * 360
				If .eccentricity = 1 Then .eccentricity = 0.999
			End With
			
			minRadAU = (mvarRadiusM + .RadiusM + 700000000#) / (1000 * AU2km)
			
			d = (mvarRadiusM * mvarSurfaceTemperatureK ^ 2 * System.Math.Sqrt(PI) / 1000000) / (AU2km * 1000)
			
			If minRadAU < d Then minRadAU = d
			
			' Crank out the rest of the orbital parameters
			Do 
				.OrbitalData.eccentricity = Rnd() * 0.005
				MaxSMA = mvarHabitableZoneMinAU
				MinSMA = mvarHabitableZoneMaxAU
				
				.OrbitalData.SemiMajorAxisAU = Rnd() * (MaxSMA - MinSMA) + MinSMA
				.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
				
				.OrbitalData.MeanMotion = System.Math.Sqrt(G * (mvarMassKg + .MassKg) / .OrbitalData.SemiMajorAxisAU ^ 3)
				
				System.Windows.Forms.Application.DoEvents()
			Loop While (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)) < minRadAU
			
			
			
			.LookupSizeClass()
			.SurfaceGravityG = 6.80415E-12 * .MassKg / .RadiusM ^ 2
			
			
			If (.OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity) >= mvarHabitableZoneMinAU And .OrbitalData.SemiMajorAxisAU * (1 + .OrbitalData.eccentricity) <= mvarHabitableZoneMaxAU) Or mvarInStarHZ = True Then
				
				.InStarHZ = True
			End If
			
			If .StarType Like "Terrestrial" Then
				.RollHabitableBody()
			End If
			
			If .Albedo > 1 Then .Albedo = 1
			If .Albedo < 0 Then .Albedo = 0
			
			If .SurfaceTemperatureK = 0 Then
				.SurfaceTemperatureK = ((.RadiusM * mvarRadiusM) / (4 * (.OrbitalData.SemiMajorAxisAU * AU2km * 1000) ^ 2)) ^ (1 / 4) * mvarSurfaceTemperatureK * (1 - .Albedo) ^ (1 / 4)
			End If
			
			If .SurfaceTemperatureK <= 2.17 And Not (.StarType Like "Black Hole") Then
				.SurfaceTemperatureK = 2.17
			End If
			
			
			.RingSystem.Generate(.RadiusM)
			.Atmosphere.Generate(.StarType, .SpectralType, .RadiusM)
			.CalculateHaze()
			
			If .RotationPeriod = 0 Then .RotationPeriod = EPSILON_SNG
			If 2 * PI * .RadiusM / (.RotationPeriod * 3600) >= SpeedOfLight Then
				.RotationPeriod = (2 * PI * .RadiusM / (SpeedOfLight * 0.001 * Rnd())) / 3600
			Else
				Do 
					.RotationPeriod = Rnd() * 18 + 18
				Loop While 2 * PI * .RadiusM / (.RotationPeriod * 3600) >= SpeedOfLight
			End If
			
			' force a 24-hour day
			.RotationPeriod = 24
			
			.RotationOffset = Rnd() * 360
			
			.Oblateness = ((2 * PI) / (.RotationPeriod * 3600)) ^ 2 * .RadiusM ^ 3 / (2 * G * .MassKg)
			
			.OrbitalData.LongOfPericenter = Rnd() * 360
			.OrbitalData.ArgumentOfPericenter = Rnd() * 360
			.OrbitalData.PericenterDistance = .OrbitalData.SemiMajorAxisAU * (1 - .OrbitalData.eccentricity)
			
			If .StarType Like "Asteroid" Then
				.Oblateness = Rnd()
			End If
			
			If .OrbitalData.PeriodYr = 0 Then
				.OrbitalData.PeriodYr = System.Math.Sqrt(mvarMassSol * .OrbitalData.SemiMajorAxisAU ^ 3)
			End If
			
			.RollOrbitingBodies(intLevel + 1)
			
		End With
	End Sub
End Class