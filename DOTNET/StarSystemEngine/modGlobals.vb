Option Strict Off
Option Explicit On

Imports Microsoft.VisualBasic.Compatibility

Module modGlobals

    Public Conn As ADODB.Connection
    Public Const gcblnOGL As Boolean = False

    Public Const EPSILON_SNG As Single = 1.4013E-45


    Function FileExist(ByVal strFileName As String) As Boolean

        On Error GoTo errDoesFileExist

        'check to see if the file exist in the specified directory
        'first check to see if strFileName is just a directory.
        'directories will return true
        If Mid(strFileName, Len(strFileName)) = "\" Then
            'this is just a directory
            FileExist = False
            Exit Function
        End If

        'then check to see if the file exists.
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        FileExist = IIf(Dir(strFileName) <> "", True, False)

        Exit Function

errDoesFileExist:

    End Function

    Function DirExist(ByVal strFileName As String) As Boolean

        On Error GoTo errDoesFileExist

        'check to see if the file exist in the specified directory
        'first check to see if strFileName is just a directory.
        'directories will return true

        'then check to see if the file exists.
        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        DirExist = IIf(Dir(strFileName, FileAttribute.Directory) <> "", True, False)

        Exit Function

errDoesFileExist:
    End Function


    Function ceil(ByRef n As Single) As Integer
        If n - Fix(n) > 0 Then
            ceil = Fix(n) + 1
        Else
            ceil = Fix(n)
        End If
    End Function

    'Returns a normally distributed deviate with zero mean and unit variance, using ran1(idum)
    'as the source of uniform deviates.
    Public Function randn() As Single

        Static gset As Single

        Dim v1, fac, rsq, v2 As Single

        Do While (rsq >= 1.0# Or rsq = 0.0#)
            v1 = 2.0# * Rnd() - 1.0# 'pick two uniform numbers in the square extending
            'from -1 to +1 in each direction,
            v2 = 2.0# * Rnd() - 1.0#
            rsq = v1 * v1 + v2 * v2 'see if they are in the unit circle,
        Loop
        fac = System.Math.Sqrt(-2.0# * System.Math.Log(rsq) / rsq)
        gset = v1 * fac
        randn = v2 * fac
    End Function


    Function dN(ByRef n As Integer, ByRef s As Integer) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * s)
        Next i
        dN = res

    End Function

    Function d100(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 100)
        Next i
        d100 = res
    End Function

    Function d2(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 2)
        Next i
        d2 = res
    End Function

    Function d3(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 3)
        Next i
        d3 = res
    End Function


    Function d4(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 4)
        Next i
        d4 = res
    End Function

    Function d6(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 6)
        Next i
        d6 = res
    End Function

    Function d8(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 6)
        Next i
        d8 = res
    End Function

    Function d10(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 6)
        Next i
        d10 = res
    End Function

    Function d12(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 12)
        Next i
        d12 = res
    End Function

    Function d20(Optional ByRef n As Integer = 1) As Integer
        Dim i As Integer
        Dim res As Integer
        res = 0
        For i = 0 To n - 1
            res = res + ceil(Rnd() * 20)
        Next i
        d20 = res
    End Function

    Function Shuffle(ByRef n As Integer) As Integer()

        ' Generates a randomly-ordered array of longs from with
        ' non-repeating values from 1 to N

        Dim j, i, k As Integer
        Dim lngArray() As Integer

        ReDim lngArray(n)

        For i = 0 To n
            lngArray(i) = i
        Next i

        For i = 0 To n
            j = CInt(Rnd() * 2147483647.0#) Mod n
            k = lngArray(i)
            lngArray(i) = lngArray(j)
            lngArray(j) = k
        Next i

        Shuffle = VB6.CopyArray(lngArray)

    End Function



    Function Apostrify(ByRef strSQL As String) As String
        Dim tempstring() As String
        Dim outstring As String
        Dim i As Integer
        If Len(strSQL) > 0 Then
            tempstring = Split(strSQL, "'")

            outstring = tempstring(0)
            For i = 1 To UBound(tempstring)
                outstring = outstring & "''" & tempstring(i)
            Next i
            Apostrify = outstring
        Else
            Apostrify = strSQL
        End If
    End Function
End Module