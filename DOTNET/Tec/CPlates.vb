Option Strict Off
Option Explicit On
Friend Class CPlates
	
	'local variable to hold collection
	Private mCol As Collection
	
	Public Function Add(Optional ByRef dx As Integer = 0, Optional ByRef dy As Integer = 0, Optional ByRef odx As Integer = 0, Optional ByRef ody As Integer = 0, Optional ByRef rx As Integer = 0, Optional ByRef ry As Integer = 0, Optional ByRef age As Integer = 0, Optional ByRef area As Integer = 0, Optional ByRef id As Integer = 0, Optional ByRef sKey As String = "") As CPlate
		
		'create a new object
		Dim objNewMember As CPlate
		objNewMember = New CPlate
		
		
		'set the properties passed into the method
		objNewMember.dx = dx
		objNewMember.dy = dy
		objNewMember.odx = odx
		objNewMember.ody = ody
		objNewMember.rx = rx
		objNewMember.ry = ry
		objNewMember.age = age
		objNewMember.area = area
		objNewMember.id = id
		
		If Len(sKey) = 0 Then
			mCol.Add(objNewMember)
		Else
			mCol.Add(objNewMember, sKey)
		End If
		
		
		'return the object created
		Add = objNewMember
		'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		objNewMember = Nothing
		
		
	End Function
	
	Default Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CPlate
		Get
			'used when referencing an element in the collection
			'vntIndexKey contains either the Index or Key to the collection,
			'this is why it is declared as a Variant
			'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey + 1)
		End Get
	End Property
	
	
	
	Public ReadOnly Property Count() As Integer
		Get
			'used when retrieving the number of elements in the
			'collection. Syntax: Debug.Print x.Count
			Count = mCol.Count()
		End Get
	End Property
	
	
	Public Sub Remove(ByRef vntIndexKey As Object)
		'used when removing an element from the collection
		'vntIndexKey contains either the Index or Key, which is why
		'it is declared as a Variant
		'Syntax: x.Remove(xyz)

        mCol.Remove(vntIndexKey + 1)
	End Sub
	
	
	'Public Property Get NewEnum() As IUnknown
	'    'this property allows you to enumerate
	'    'this collection with the For...Each syntax
	'    Set NewEnum = mCol.(_NewEnum)
	'End Property
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		'creates the collection when this class is created
		mCol = New Collection
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'destroys collection when this class is terminated
        mCol = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class