Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modTecGlobals
	
	
	' Declares for randomad DLL
	Public Declare Sub TRandomInit Lib "randomad.dll" (ByVal seed As Integer)
	Public Declare Sub TRandomInitByArray Lib "randomad.dll" (ByRef seeds() As Integer, ByRef length As Integer)
	Public Declare Function TBRandom Lib "randomad.dll" () As Double
	Public Declare Function TIRandom Lib "randomad.dll" (ByVal min As Integer, ByVal max As Integer) As Double
	Public Declare Function TRandom Lib "randomad.dll" () As Double
	Public Declare Function TRandom2 Lib "randomad.dll" () As Double
	Public Declare Function TRandom3 Lib "randomad.dll" () As Double
	
	Public Const PI As Double = 3.1415926
	
	Public color_values(31) As Integer
	
	Public MAXMOUNTAINHEIGHT As Integer ' maximum mountain height = 32,700 feet
	
	Public MAXX As Integer
	Public MAXY As Integer
	
	Public Const MAXPRESS As Short = 255
	
	Public Const SIZE As Short = 1
	Public WINDOW_HEIGHT As Integer
	Public WINDOW_WIDTH As Integer
	Public Const MAXPLATE As Short = 500
	
	Public landcols(256) As Integer
	Public greycols(256) As Integer
	Public tecols() As Integer
	Public platecols(MAXPLATE) As Integer
	Public heatcols(256) As Integer
	Public popcols(256) As Integer
	
	Public XWinScale As Double
	Public YWinScale As Double
	
	Enum PrintMode_E
		PRINTMODE_NONE = 0
		PRINTMODE_LONG
		PRINTMODE_SCALE
		PRINTMODE_SHORT
		PRINTMODE_GREY
		PRINTMODE_CLIM
	End Enum
	
	Enum DrawType_E
		DRAW_GREY = 1
		DRAW_LAND
		DRAW_CLIM
		DRAW_TEC
		DRAW_PLATE
		DRAW_JET
		DRAW_TRUE
		DRAW_DND
		DRAW_POP
	End Enum
	
	Enum LineType_E
		LINE_DIAG = 1
		LINE_CORN
		LINE_NONE
	End Enum
	
	
	'Global Const PI = 3.14159265359
	Public Const TWOPI As Double = 6.28318530718
	Public Const TWOMILLIPI As Double = 0.00628318
	Public Const FREEZING As Double = 273#
	Public Const DEG2RAD As Double = (PI / 180)
	Public n As Integer
	Public area As Integer
	Public Const RADIUS As Short = 45
	Public Const HALFPI As Double = (PI / 2#)
	
	Public Const MAXB As Short = 12 '4
	
	'Global Const LINE_DIAG = 1
	'Global Const LINE_CORN = 2
	
	Public Const LINE_0V As Short = 1
	Public Const LINE_1V As Short = 2
	Public Const LINE_0H As Short = 4
	Public Const LINE_1H As Short = 8
	
	Public Const North As Short = LINE_0V
	Public Const South As Short = LINE_1V
	Public Const East As Short = LINE_0H
	Public Const West As Short = LINE_1H
	Public Const Northwest As Boolean = North Or West
	Public Const Southwest As Boolean = South Or West
	Public Const Northeast As Boolean = North Or East
	Public Const Southeast As Boolean = South Or East
	
	
	Public Const TREELINE_FT As Short = 10000
	Public Const SNOWLINE_FT As Short = 12000
	
	Public Enum Pressure_E
		PR_LOW = 1
		PR_HIGH
		PR_HEQ
	End Enum
	
	Public Enum MainType_E
		M_MAIN = 0
		M_HEAT
		M_PRESS
		M_WIND
		M_RAIN
		M_CLIM
	End Enum
	
	Public Enum Climate_E
		C_OCEAN = 0
		C_OCEANICE
		C_SWAMP
		C_BARELAND
		C_LAKERIVER
		C_MOUNTAIN
		C_JUNGLE ' Af
		C_DECID ' Cf
		C_BOREAL ' Df
		C_TROPICWETDRY ' Aw
		C_MONSOON ' Am
		C_GRASSLAND ' Cw
		C_MEDITERRANEAN ' Cs
		C_COLDDRYWINTER ' Dw
		C_SAVANA
		C_DESERT ' Bw
		C_STEPPE
		C_TUNDRA ' Et
		C_LANDICE
	End Enum
	
	Public Enum DNDCLIMATE_E
		D_WARM_SWAMP
		D_WARM_FOREST
		D_WARM_PLAIN
		D_WARM_DESERT
		D_WARM_HILL
		D_WARM_MOUNTAIN
		D_WARM_AQUATIC
		D_TEMP_SWAMP
		D_TEMP_FOREST
		D_TEMP_PLAIN
		D_TEMP_DESERT
		D_TEMP_HILL
		D_TEMP_MOUNTAIN
		D_TEMP_AQUATIC
		D_COLD_SWAMP
		D_COLD_FOREST
		D_COLD_PLAIN
		D_COLD_DESERT
		D_COLD_HILL
		D_COLD_MOUNTAIN
		D_COLD_AQUATIC
		D_OCEANICE
	End Enum
	
	Public Enum MONTH_E
		JANUARY
		FEBRUARY
		MARCH
		APRIL
		MAY
		JUNE
		JULY
		AUGUST
		SEPTEMBER
		OCTOBER
		NOVEMBER
		DECEMBER
	End Enum
	
	Public Enum COLOR_E
		clr_black
		clr_white
		clr_grey75
		clr_grey50
		clr_MediumPurple1
		clr_purple4
		clr_purple3
		clr_purple2
		clr_purple1
		clr_blue4
		clr_blue3
		clr_blue2
		clr_blue1
		clr_DarkGreen
		clr_green4
		clr_green3
		clr_green2
		clr_green1
		clr_DarkGoldenrod4
		clr_yellow4
		clr_yellow3
		clr_yellow2
		clr_yellow1
		clr_orange4
		clr_orange3
		clr_orange2
		clr_orange1
		clr_brown4
		clr_red4
		clr_red3
		clr_red2
		clr_red1
	End Enum
	
	Public climcols(Climate_E.C_LANDICE) As Integer
	Public truecols(Climate_E.C_LANDICE) As Integer
	Public dndcols(DNDCLIMATE_E.D_OCEANICE) As Integer
	
	Function CMP(ByRef s As String, ByRef X As String) As String
		CMP = CStr(Not StrComp(s, X))
	End Function
	
	
	
	
	
	
	
	
	
	
	
	
	Sub panic(ByRef s As String)
		'frmTec.StatusBar1.Panels(1).Text = "PANIC: " & s
		
		'Set frmTec.tec = Nothing
		
		'Unload frmTec
		'Beep
		
	End Sub
	
	
	
	
	Function random(ByRef top As Integer) As Integer
		' produces a random number from 0 to top
		random = CInt(TRandom * top)
	End Function
	
	Sub testran1()
		Dim i As Short
		Dim an As Double
		
		TRandomInit(VB.Timer())
		
		'  Debug.Print ("Random integers in interval from 0 to 99:")
		'  For i = 1 To 40
		'    an = TIRandom(0, 99)
		'    Debug.Print Format(an, "00")
		'    If i Mod 10 = 0 Then
		'        Debug.Print
		'    End If
		'  Next i
		'  Debug.Print
		
		Debug.Print("Random floating point numbers in interval from 0 to 1:")
		For i = 1 To 32
			an = TRandom
			Debug.Print(VB6.Format(an, "0.000000 "))
			If i Mod 8 = 0 Then
				Debug.Print("")
			End If
		Next i
		Debug.Print("")
		
	End Sub
	
    'Sub DrawGrid()
    '	' Draws lat/lon grid
    '	Dim i As Integer


    '	' Latitude

    '	'  For i = 0 To Picture1.ScaleHeight Step Picture1.ScaleHeight / 9
    '	'    Picture1.Line (0, i)-(Picture1.ScaleWidth, i)
    '	'  Next i
    '	With frmTec
    '		' 15 degree longitude
    '		'For i = 0 To .Picture1.ScaleWidth Step .Picture1.ScaleWidth / 24
    '		'  .Picture1.Line (i, 0)-(i, .Picture1.ScaleHeight)
    '		'Next i

    '		' Equator
    '		'.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF

    '		' Tropics
    '		'.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 + .clim.Tilt / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
    '		'.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 - .clim.Tilt / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF

    '		' Arctics
    '		'.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 + (90 - .clim.Tilt) / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
    '		'.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 - (90 - .clim.Tilt) / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
    '	End With

    'End Sub
	
    'Sub DrawRegionGrid()
    '	' Draws lat/lon grid
    '	Dim i As Integer


    '	' Latitude
    '	With frmTec
    '		' 15 degree longitude
    '		For i = 0 To VB6.PixelsToTwipsX(.Picture1.ClientRectangle.Width) Step VB6.PixelsToTwipsX(.Picture1.ClientRectangle.Width) / 16
    '			'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '			.Picture1.Line (i, 0) - (i, VB6.PixelsToTwipsY(.Picture1.ClientRectangle.Height))
    '		Next i

    '		For i = 0 To VB6.PixelsToTwipsY(.Picture1.ClientRectangle.Height) Step VB6.PixelsToTwipsY(.Picture1.ClientRectangle.Height) / 8
    '			'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '			.Picture1.Line (0, i) - (VB6.PixelsToTwipsX(.Picture1.ClientRectangle.Width), i)
    '		Next i


    '	End With

    'End Sub
	
	Function randnDLL() As Single
		Static iset As Integer
		Static gset As Single
		Dim v1, fac, rsq, v2 As Single
		
		If iset = 0 Then
			Do 
				v1 = 2# * TRandom - 1# 'pick two uniform numbers in the square extending from -1 to +1 in each direction,
				v2 = 2# * TRandom - 1#
				rsq = v1 * v1 + v2 * v2
			Loop While (rsq >= 1# Or rsq = 0#) ' and if they are not, try again.
			fac = System.Math.Sqrt(-2# * System.Math.Log(rsq) / rsq)
			' Now make the Box-Muller transformation to get two normal deviates. Return one and
			' save the other for next time.
			gset = v1 * fac
			iset = 1 ' Set flag.
			randnDLL = v2 * fac
		Else ' We have an extra deviate handy,
			iset = 0 ' so unset the flag,
			randnDLL = gset ' and return it.
		End If
		
	End Function
	
	'Returns a normally distributed deviate with zero mean and unit variance, using ran1(idum)
	'as the source of uniform deviates.
	Public Function randn() As Single
		
		Static gset As Single
		
		Dim v1, fac, rsq, v2 As Single
		
		Do While (rsq >= 1# Or rsq = 0#)
			v1 = 2# * Rnd() - 1# 'pick two uniform numbers in the square extending
			'from -1 to +1 in each direction,
			v2 = 2# * Rnd() - 1#
			rsq = v1 * v1 + v2 * v2 'see if they are in the unit circle,
		Loop 
		fac = System.Math.Sqrt(-2# * System.Math.Log(rsq) / rsq)
		gset = v1 * fac
		randn = v2 * fac
	End Function
	
    Function MaxMax(ByRef Z(,) As Short) As Integer
        Dim j, i, maxZ As Integer
        maxZ = -2000000000.0#

        For i = 0 To UBound(Z, 1)
            For j = 0 To UBound(Z, 2)
                If maxZ <= Z(i, j) Then maxZ = Z(i, j)
            Next
        Next
        MaxMax = maxZ

    End Function
	
    Function MinMin(ByRef Z(,) As Short) As Integer
        Dim j, i, minZ As Integer
        minZ = 2000000000.0#

        For i = 0 To UBound(Z, 1)
            For j = 0 To UBound(Z, 2)
                If minZ >= Z(i, j) Then minZ = Z(i, j)
            Next
        Next
        MinMin = minZ

    End Function
	
	
	Function ceil(ByRef n As Single) As Integer
		If n - Fix(n) > 0 Then
			ceil = Fix(n) + 1
		Else
			ceil = Fix(n)
		End If
	End Function
	
	Function ndims(ByRef L() As Integer) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndims = i - 1
		
	End Function
	
	Function ndimsB(ByRef L() As Byte) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndimsB = i - 1
		
	End Function
	
	Function ndimsI(ByRef L() As Short) As Integer
		Dim i As Integer
		Dim lim As Integer
		On Error GoTo exitfun
		i = 1
		Do 
			lim = UBound(L, i)
			i = i + 1
		Loop 
exitfun: 
		ndimsI = i - 1
		
	End Function
	
	
    'Function EmptySingle(ByRef rows As Integer, Optional ByRef cols As Integer = 0, Optional ByRef Z As Integer = 0) As Single()
    '	' creates an empty array of size (rows, cols, z)
    '       Dim sngO(,) As Single
    '	If rows > 0 Then
    '		If cols > 0 Then
    '			If Z > 0 Then
    '				ReDim sngO(rows, cols, Z)
    '			Else
    '				ReDim sngO(rows, cols)
    '			End If
    '		Else
    '			ReDim sngO(rows)
    '		End If
    '	End If
    '	EmptySingle = VB6.CopyArray(sngO)
    'End Function
	
    'Function Gradient(ByRef f() As Integer, ByRef X() As Single, ByRef Y() As Single, ByRef Z() As Single, ByRef dfdx() As Single, ByRef dfdy() As Single, ByRef dfdz() As Single) As Single()

    '	' [df/dx, df/dy, df/dz] = Gradient(f, x, y, z)

    '	Dim i As Integer
    '	Dim j As Integer
    '	Dim k As Integer
    '	Dim G() As Single
    '	Dim h As Single

    '	Dim ndim As Integer
    '	Dim p, n, L As Integer

    '	ndim = ndims(f)

    '	If ndim = 1 Then
    '		n = UBound(f, 1)
    '		ReDim G(n - 1)
    '		For i = 0 To n - 1
    '			G(i, j) = CSng(f(i + 1) - f(i)) / CSng(X(i + 1) - X(i))
    '		Next i

    '	ElseIf ndim = 2 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		ReDim dfdx(n - 1, p - 1)
    '		ReDim dfdy(n - 1, p - 1)
    '		ReDim G(n - 1, p - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				dfdx(i, j) = CSng(f(i, j + 1) - f(i, j)) / CSng(X(j + 1) - X(j))
    '				dfdy(i, j) = CSng(f(i + 1, j) - f(i, j)) / CSng(Y(i + 1) - Y(i))
    '				G(i, j) = System.Math.Sqrt(dfdx(i, j) ^ 2 + dfdy(i, j) ^ 2)
    '			Next j
    '		Next i
    '	ElseIf ndim = 3 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		L = UBound(f, 3)
    '		ReDim dfdx(n - 1, p - 1, L - 1)
    '		ReDim dfdy(p - 1, p - 1, L - 1)
    '		ReDim dfdz(L - 1, p - 1, L - 1)
    '		ReDim G(n - 1, p - 1, L - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				For k = 0 To L - 1
    '					dfdx(i, j, k) = CSng(f(i, j + 1, k) - f(i, j, k)) / CSng(X(j + 1) - X(j))
    '					dfdy(i, j, k) = CSng(f(i + 1, j, k) - f(i, j, k)) / CSng(Y(i + 1) - Y(i))
    '					dfdy(i, j, k) = CSng(f(i, j, k + 1) - f(i, j, k)) / CSng(Z(k) - Z(k + 1))
    '					G(i, j, k) = System.Math.Sqrt(dfdx(i, j, k) ^ 2 + dfdy(i, j, k) ^ 2 + dfdz(i, j, k) ^ 2)
    '				Next k
    '			Next j
    '		Next i
    '	End If
    '	Gradient = VB6.CopyArray(G)
    'End Function
	
	
    'Function QuickGradient(ByRef f() As Short) As Short()
    '	' like gradient, only returns magnitude, and assumes all h=1

    '	Dim i As Integer
    '	Dim j As Integer
    '	Dim k As Integer
    '	Dim G() As Short
    '	Dim h As Integer
    '	Dim dfdy, dfdx, dfdz As Integer
    '	Dim ndim As Integer
    '	Dim p, n, L As Integer

    '	ndim = ndimsI(f)

    '	If ndim = 1 Then
    '		n = UBound(f, 1)
    '		ReDim G(n - 1)
    '		For i = 0 To n - 1
    '			G(i, j) = CSng(f(i + 1) - f(i))
    '		Next i

    '	ElseIf ndim = 2 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		ReDim G(n - 1, p - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				dfdx = CSng(f(i, j + 1) - f(i, j))
    '				dfdy = CSng(f(i + 1, j) - f(i, j))
    '				G(i, j) = System.Math.Sqrt(dfdx ^ 2 + dfdy ^ 2)
    '			Next j
    '		Next i

    '	ElseIf ndim = 3 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		L = UBound(f, 3)
    '		ReDim G(n - 1, p - 1, L - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				For k = 0 To L - 1
    '					dfdx = CSng(f(i, j + 1, k) - f(i, j, k))
    '					dfdy = CSng(f(i + 1, j, k) - f(i, j, k))
    '					dfdy = CSng(f(i, j, k + 1) - f(i, j, k))
    '					G(i, j, k) = System.Math.Sqrt(dfdx ^ 2 + dfdy ^ 2 + dfdz ^ 2)
    '				Next k
    '			Next j
    '		Next i
    '	End If
    '	QuickGradient = VB6.CopyArray(G)
    'End Function
	
    'Function Div(ByRef f() As Integer, ByRef X() As Single, ByRef Y() As Single, ByRef Z() As Single) As Single()

    '	Dim i As Integer
    '	Dim j As Integer
    '	Dim k As Integer
    '	Dim G() As Single
    '	Dim h As Single
    '	Dim dfdy, dfdx, dfdz As Single

    '	Dim ndim As Integer
    '	Dim p, n, L As Integer

    '	ndim = ndims(f)

    '	If ndim = 1 Then
    '		n = UBound(f, 1)
    '		ReDim G(n - 1)
    '		For i = 0 To n - 1
    '			G(i, j) = CSng(f(i + 1) - f(i)) / CSng(X(i + 1) - X(i))
    '		Next i

    '	ElseIf ndim = 2 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		ReDim G(n - 1, p - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				dfdx = CSng(f(i, j + 1) - f(i, j)) / CSng(X(j + 1) - X(j))
    '				dfdy = CSng(f(i + 1, j) - f(i, j)) / CSng(Y(i + 1) - Y(i))
    '				G(i, j) = dfdx + dfdy
    '			Next j
    '		Next i

    '	ElseIf ndim = 3 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		L = UBound(f, 3)
    '		ReDim G(n - 1, p - 1, L - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				For k = 0 To L - 1
    '					dfdx = CSng(f(i, j + 1, k) - f(i, j, k)) / CSng(X(j + 1) - X(j))
    '					dfdy = CSng(f(i + 1, j, k) - f(i, j, k)) / CSng(Y(i + 1) - Y(i))
    '					dfdy = CSng(f(i, j, k + 1) - f(i, j, k)) / CSng(Z(k) - Z(k + 1))
    '					G(i, j, k) = dfdx + dfdy + dfdz
    '				Next k
    '			Next j
    '		Next i
    '	End If
    '	Div = VB6.CopyArray(G)
    'End Function

    'Function QuickDiv(ByRef f() As Integer) As Integer()
    '	' returns divergence field

    '	Dim i As Integer
    '	Dim j As Integer
    '	Dim k As Integer
    '	Dim G() As Integer
    '	Dim h As Integer
    '	Dim dfdy, dfdx, dfdz As Integer
    '	Dim ndim As Integer
    '	Dim p, n, L As Integer

    '	ndim = ndims(f)

    '	If ndim = 1 Then
    '		n = UBound(f, 1)
    '		ReDim G(n - 1)
    '		For i = 0 To n - 1
    '			dfdy = CSng(f(i + 1) - f(i))
    '			G(i) = dfdx
    '		Next i

    '	ElseIf ndim = 2 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		ReDim G(n - 1, p - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				dfdx = CSng(f(i, j + 1) - f(i, j))
    '				dfdy = CSng(f(i + 1, j) - f(i, j))
    '				G(i, j) = dfdx + dfdy
    '			Next j
    '		Next i

    '	ElseIf ndim = 3 Then 
    '		n = UBound(f, 1)
    '		p = UBound(f, 2)
    '		L = UBound(f, 3)
    '		ReDim G(n - 1, p - 1, L - 1)
    '		For i = 0 To n - 1
    '			For j = 0 To p - 1
    '				For k = 0 To L - 1
    '					dfdx = CSng(f(i, j + 1, k) - f(i, j, k))
    '					dfdy = CSng(f(i + 1, j, k) - f(i, j, k))
    '					dfdy = CSng(f(i, j, k + 1) - f(i, j, k))
    '					G(i, j, k) = dfdx + dfdy + dfdz
    '				Next k
    '			Next j
    '		Next i
    '	End If

    '	QuickDiv = VB6.CopyArray(G)
    'End Function
	
	
	
	Sub assign_colors(ByRef mvarZShelf As Integer, ByRef mvarZCoast As Integer)
		Dim i As Integer
		
		FillX11ColorTable()
		
		color_values(COLOR_E.clr_black) = &H0s
		color_values(COLOR_E.clr_white) = &HFFFFFF
		color_values(COLOR_E.clr_grey75) = &HBFBFBF
		color_values(COLOR_E.clr_grey50) = &H7F7F7F
		color_values(COLOR_E.clr_MediumPurple1) = RGB(&HABs, &H82s, &HFFs)
		color_values(COLOR_E.clr_purple4) = RGB(&H55s, &H1As, &H8Bs)
		color_values(COLOR_E.clr_purple3) = RGB(&H7Ds, &H26s, &HCDs)
		color_values(COLOR_E.clr_purple2) = RGB(&H91s, &H2Cs, &HEEs)
		color_values(COLOR_E.clr_purple1) = RGB(&H9Bs, &H30s, &HFFs)
		color_values(COLOR_E.clr_blue4) = RGB(0, 0, &H8Bs)
		color_values(COLOR_E.clr_blue3) = RGB(0, 0, &HCDs)
		color_values(COLOR_E.clr_blue2) = RGB(0, 0, &HEEs)
		color_values(COLOR_E.clr_blue1) = RGB(0, 0, &HFFs)
		color_values(COLOR_E.clr_DarkGreen) = RGB(0, &H64s, 0)
		color_values(COLOR_E.clr_green4) = RGB(0, &H8Bs, 0)
		color_values(COLOR_E.clr_green3) = RGB(0, &HCDs, 0)
		color_values(COLOR_E.clr_green2) = RGB(0, &HEEs, 0)
		color_values(COLOR_E.clr_green1) = RGB(0, &HFFs, 0)
		color_values(COLOR_E.clr_DarkGoldenrod4) = RGB(&H8Bs, &H65s, &H8s)
		color_values(COLOR_E.clr_yellow4) = RGB(&H8Bs, &H8Bs, &H0s)
		color_values(COLOR_E.clr_yellow3) = RGB(&HCDs, &HCDs, &H0s)
		color_values(COLOR_E.clr_yellow2) = RGB(&HEEs, &HEEs, &H0s)
		color_values(COLOR_E.clr_yellow1) = RGB(&HFFs, &HFFs, &H0s)
		color_values(COLOR_E.clr_orange4) = RGB(&H8Bs, &H5As, &H0s)
		color_values(COLOR_E.clr_orange3) = RGB(&HCDs, &H85s, &H0s)
		color_values(COLOR_E.clr_orange2) = RGB(&HEEs, &H9As, &H0s)
		color_values(COLOR_E.clr_orange1) = RGB(&HFFs, &HA5s, &H0s)
		color_values(COLOR_E.clr_brown4) = RGB(&H8Bs, &H23s, &H23s)
		color_values(COLOR_E.clr_red4) = RGB(&H8Bs, &H0s, &H0s)
		color_values(COLOR_E.clr_red3) = RGB(&HCDs, &H0s, &H0s)
		color_values(COLOR_E.clr_red2) = RGB(&HEEs, &H0s, &H0s)
		color_values(COLOR_E.clr_red1) = RGB(&HFFs, &H0s, &H0s)
		
		' Ocean
		climcols(Climate_E.C_OCEANICE) = color_values(COLOR_E.clr_grey75)
		climcols(Climate_E.C_OCEAN) = color_values(COLOR_E.clr_blue4)
		
		
		' Dry year-round
		climcols(Climate_E.C_DESERT) = color_values(COLOR_E.clr_yellow1) ' R < 25cm   (Somalia)
		climcols(Climate_E.C_SAVANA) = color_values(COLOR_E.clr_orange1) ' 25 cm < R < 50 cm  (Africa)
		climcols(Climate_E.C_STEPPE) = color_values(COLOR_E.clr_DarkGoldenrod4) ' Temperate savannah  (Colorado)
		
		' Wet/Dry and Grasslands
		climcols(Climate_E.C_MONSOON) = color_values(COLOR_E.clr_purple4)
		climcols(Climate_E.C_TROPICWETDRY) = color_values(COLOR_E.clr_yellow2)
		
		climcols(Climate_E.C_GRASSLAND) = color_values(COLOR_E.clr_green1) ' Wet summer, dry winter
		climcols(Climate_E.C_MEDITERRANEAN) = color_values(COLOR_E.clr_orange3) 'RGB(105, 112, 70)    ' Dry summer, wet winter
		
		climcols(Climate_E.C_COLDDRYWINTER) = color_values(COLOR_E.clr_green2) ' Wet summer, dry winter
		
		' Forests
		climcols(Climate_E.C_BOREAL) = color_values(COLOR_E.clr_DarkGreen) ' Coniferous
		climcols(Climate_E.C_DECID) = color_values(COLOR_E.clr_green4) ' Deciduous
		climcols(Climate_E.C_JUNGLE) = color_values(COLOR_E.clr_green3) ' Broadleaf
		
		' Bare lands
		climcols(Climate_E.C_MOUNTAIN) = color_values(COLOR_E.clr_DarkGoldenrod4)
		climcols(Climate_E.C_BARELAND) = color_values(COLOR_E.clr_yellow3)
		
		' Polar
		climcols(Climate_E.C_TUNDRA) = color_values(COLOR_E.clr_grey50)
		climcols(Climate_E.C_LANDICE) = RGB(210, 220, 222)
		
		' Other
		climcols(Climate_E.C_SWAMP) = RGB(128, 128, 255)
		climcols(Climate_E.C_LAKERIVER) = color_values(COLOR_E.clr_blue1)
		
		
		truecols(Climate_E.C_OCEAN) = RGB(3, 29, 63)
		truecols(Climate_E.C_BARELAND) = RGB(194, 164, 125)
		truecols(Climate_E.C_MOUNTAIN) = RGB(137, 122, 92)
		truecols(Climate_E.C_OCEANICE) = RGB(229, 229, 229)
		truecols(Climate_E.C_TUNDRA) = RGB(109, 100, 69)
		truecols(Climate_E.C_STEPPE) = RGB(77, 99, 45)
		truecols(Climate_E.C_DESERT) = RGB(229, 228, 129)
		truecols(Climate_E.C_SAVANA) = RGB(85, 102, 47)
		truecols(Climate_E.C_DECID) = RGB(65, 86, 29)
		truecols(Climate_E.C_JUNGLE) = RGB(50, 69, 27)
		truecols(Climate_E.C_SWAMP) = RGB(36, 71, 58)
		truecols(Climate_E.C_LAKERIVER) = RGB(3, 29, 63)
		truecols(Climate_E.C_TROPICWETDRY) = RGB(160, 150, 115)
		truecols(Climate_E.C_MONSOON) = RGB(70, 80, 37)
		truecols(Climate_E.C_GRASSLAND) = RGB(113, 115, 63)
		truecols(Climate_E.C_MEDITERRANEAN) = RGB(105, 112, 70)
		truecols(Climate_E.C_BOREAL) = RGB(52, 70, 29)
		truecols(Climate_E.C_COLDDRYWINTER) = RGB(129, 120, 86)
		truecols(Climate_E.C_LANDICE) = RGB(210, 220, 222)
		
		landcols(0) = color_values(11)
		landcols(1) = color_values(28)
		For i = 2 To 255
			landcols(i) = color_values(3)
		Next i
		
		' Hot
		For i = 0 To 32
			heatcols(i) = RGB(0, 0, (i * 4) + 128)
		Next i
		For i = 33 To 96
			heatcols(i) = RGB(0, (i - 32) * 4, 255)
		Next i
		For i = 97 To 160
			heatcols(i) = RGB((i - 96) * 8, 255, 256 - 4 * (i - 96))
		Next i
		For i = 161 To 224
			heatcols(i) = RGB(255, 256 - (4 * (i - 160)), 0)
		Next i
		For i = 225 To 255
			heatcols(i) = RGB(256 - (i - 224) * 4, 0, 0)
		Next i
		
		For i = 0 To 255
			'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
			greycols(i) = RGB(i, i, i)
		Next i
		greycols(256) = RGB(255, 255, 255)
		
		
		For i = 0 To 255
			popcols(i) = RGB(255, 255 - i, 255 - i)
		Next i
		
		
		For i = 1 To MAXPLATE
			platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
		Next i
		
		ReDim tecols(MAXMOUNTAINHEIGHT)
		' deep ocean
		For i = 0 To mvarZShelf
			tecols(i) = color_values(COLOR_E.clr_blue4) ' dark blue
		Next i
		
		' continental shelf
		For i = mvarZShelf To mvarZCoast
			tecols(i) = RGB(128, 255, 255) ' light blue
			If mvarZCoast <> mvarZShelf Then
				tecols(i) = RGB(128# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 255# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 116# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf) + &H8Bs)
			End If
		Next i
		
		' Greens for land
		For i = mvarZCoast To mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
			'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
			If i >= UBound(tecols) Then Exit For
			tecols(i) = RGB(0, i * 2 + 95, 0)
		Next i
		
		' Brown for above tree line
		For i = mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
			If i >= UBound(tecols) Then Exit For
			tecols(i) = RGB(128, (i - 73) * 40 / 16 + 40, 0)
		Next i
		
		' White for above snow line
		For i = mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
			If i >= UBound(tecols) Then Exit For
			'tecols(i) = color_values(1)
			tecols(i) = RGB(128 + i - 110, 128 + i - 110, 128 + i - 110)
		Next i
		
		For i = mvarZCoast + 200 To UBound(tecols)
			If i >= UBound(tecols) Then Exit For
			'tecols(i) = color_values(1)
			tecols(i) = RGB(255, 255, 255)
		Next i
		
		' minimum temperature > 18 C
		dndcols(DNDCLIMATE_E.D_WARM_SWAMP) = color_values(COLOR_E.clr_purple2)
		dndcols(DNDCLIMATE_E.D_WARM_FOREST) = color_values(COLOR_E.clr_green3)
		dndcols(DNDCLIMATE_E.D_WARM_PLAIN) = color_values(COLOR_E.clr_orange2)
		dndcols(DNDCLIMATE_E.D_WARM_DESERT) = color_values(COLOR_E.clr_yellow1)
		dndcols(DNDCLIMATE_E.D_WARM_HILL) = color_values(COLOR_E.clr_red2)
		dndcols(DNDCLIMATE_E.D_WARM_MOUNTAIN) = RGB(137, 122, 92)
		dndcols(DNDCLIMATE_E.D_WARM_AQUATIC) = color_values(COLOR_E.clr_blue2)
		
		dndcols(DNDCLIMATE_E.D_TEMP_SWAMP) = color_values(COLOR_E.clr_purple3)
		dndcols(DNDCLIMATE_E.D_TEMP_FOREST) = color_values(COLOR_E.clr_green4)
		dndcols(DNDCLIMATE_E.D_TEMP_PLAIN) = color_values(COLOR_E.clr_orange3)
		dndcols(DNDCLIMATE_E.D_TEMP_DESERT) = color_values(COLOR_E.clr_yellow2)
		dndcols(DNDCLIMATE_E.D_TEMP_HILL) = color_values(COLOR_E.clr_red3)
		dndcols(DNDCLIMATE_E.D_TEMP_MOUNTAIN) = RGB(137 + 32, 122 + 32, 92 + 32)
		dndcols(DNDCLIMATE_E.D_TEMP_AQUATIC) = color_values(COLOR_E.clr_blue3)
		
		' Maximum temperature < 10 C
		dndcols(DNDCLIMATE_E.D_COLD_SWAMP) = color_values(COLOR_E.clr_purple4)
		dndcols(DNDCLIMATE_E.D_COLD_FOREST) = color_values(COLOR_E.clr_DarkGreen)
		dndcols(DNDCLIMATE_E.D_COLD_PLAIN) = color_values(COLOR_E.clr_orange4)
		dndcols(DNDCLIMATE_E.D_COLD_DESERT) = color_values(COLOR_E.clr_yellow3)
		dndcols(DNDCLIMATE_E.D_COLD_HILL) = color_values(COLOR_E.clr_red4)
		dndcols(DNDCLIMATE_E.D_COLD_MOUNTAIN) = RGB(137 + 64, 122 + 64, 92 + 64)
		dndcols(DNDCLIMATE_E.D_COLD_AQUATIC) = color_values(COLOR_E.clr_blue4)
		dndcols(DNDCLIMATE_E.D_OCEANICE) = color_values(COLOR_E.clr_grey75)
		
		
	End Sub
End Module