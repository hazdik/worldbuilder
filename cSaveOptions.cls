VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSaveOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private m_lJpgQuality As Long
Private m_lJpgTransformation As Long
Private m_lTiffCompression As Long
Private m_lTiffBitDepth As Long

Public Property Get JpgQuality() As Long
   JpgQuality = m_lJpgQuality
End Property
Public Property Let JpgQuality(ByVal Value As Long)
   m_lJpgQuality = Value
End Property

Public Property Get JpgTransformation() As Long
   JpgTransformation = m_lJpgTransformation
End Property
Public Property Let JpgTransformation(ByVal Value As Long)
   m_lJpgTransformation = Value
End Property

Public Property Get TiffCompression() As Long
   TiffCompression = m_lTiffCompression
End Property
Public Property Let TiffCompression(ByVal Value As Long)
   m_lTiffCompression = Value
End Property

Public Property Get TiffBitDepth() As Long
   TiffBitDepth = m_lTiffBitDepth
End Property
Public Property Let TiffBitDepth(ByVal Value As Long)
   m_lTiffBitDepth = Value
End Property

Private Sub Class_Initialize()
   m_lJpgQuality = 50
   m_lJpgTransformation = 0
   m_lTiffCompression = 6
   m_lTiffBitDepth = 24
End Sub

