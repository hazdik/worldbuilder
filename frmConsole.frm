VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmConsole 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   Caption         =   "World Builder Console"
   ClientHeight    =   4590
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   8730
   Icon            =   "frmConsole.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4590
   ScaleWidth      =   8730
   StartUpPosition =   3  'Windows Default
   Begin RichTextLib.RichTextBox Text1 
      Height          =   4575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   8070
      _Version        =   393217
      BorderStyle     =   0
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"frmConsole.frx":12FA
   End
End
Attribute VB_Name = "frmConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub Form_Resize()
  If Me.WindowState <> vbMinimized Then
    If Me.Width > 135 Then Text1.Width = Me.Width - 135
    If Me.Height > 555 Then Text1.Height = Me.Height - 555
  End If
End Sub


Public Sub WriteLine(strLine As String)
  Text1.Text = Text1.Text & Chr(10) & strLine
  Text1.SelStart = Len(Text1.Text)
End Sub


Public Sub ClearConsole()
  Text1.Text = ""
End Sub
