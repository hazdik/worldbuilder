VERSION 5.00
Begin VB.Form frmDice 
   Caption         =   "Dice Roller"
   ClientHeight    =   6345
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7260
   Icon            =   "frmDice.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6345
   ScaleWidth      =   7260
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkDropLow 
      Caption         =   "Drop lowest"
      Height          =   255
      Left            =   4980
      TabIndex        =   12
      Top             =   120
      Width           =   1155
   End
   Begin VB.ListBox lstResults 
      Height          =   5715
      Left            =   60
      TabIndex        =   11
      Top             =   480
      Width           =   7155
   End
   Begin VB.CommandButton cmdRoll 
      Caption         =   "Roll"
      Height          =   375
      Left            =   6180
      TabIndex        =   10
      Top             =   60
      Width           =   1035
   End
   Begin VB.TextBox txtT 
      Height          =   315
      Left            =   3900
      TabIndex        =   7
      Text            =   "1"
      Top             =   60
      Width           =   615
   End
   Begin VB.TextBox txtM 
      Height          =   315
      Left            =   3000
      TabIndex        =   5
      Text            =   "1"
      Top             =   60
      Width           =   615
   End
   Begin VB.TextBox txtP 
      Height          =   315
      Left            =   2220
      TabIndex        =   4
      Text            =   "0"
      Top             =   60
      Width           =   615
   End
   Begin VB.ComboBox dboD 
      Height          =   315
      ItemData        =   "frmDice.frx":014A
      Left            =   1320
      List            =   "frmDice.frx":0166
      TabIndex        =   2
      Text            =   "4"
      Top             =   60
      Width           =   735
   End
   Begin VB.TextBox txtN 
      Height          =   315
      Left            =   480
      TabIndex        =   0
      Text            =   "1"
      Top             =   60
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Roll"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   270
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "times"
      Height          =   195
      Index           =   3
      Left            =   4560
      TabIndex        =   8
      Top             =   120
      Width           =   360
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "x"
      Height          =   195
      Index           =   2
      Left            =   2880
      TabIndex        =   6
      Top             =   120
      Width           =   75
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "+"
      Height          =   195
      Index           =   1
      Left            =   2100
      TabIndex        =   3
      Top             =   120
      Width           =   90
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "d"
      Height          =   195
      Index           =   0
      Left            =   1140
      TabIndex        =   1
      Top             =   120
      Width           =   90
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopupClear 
         Caption         =   "Clear"
      End
   End
End
Attribute VB_Name = "frmDice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdRoll_Click()
  Dim n As Long
  Dim s As Long
  Dim p As Long
  Dim m As Long
  Dim t As Long
  
  
  Dim minVal As Long
  Dim i As Long, j As Long
  Dim strItem As String
  
  Dim res() As Long
  Dim total As Long
  
  'lstResults.Clear
  
  If IsNumeric(txtT.Text) And IsNumeric(txtN.Text) And _
    IsNumeric(dboD.Text) And IsNumeric(txtP.Text) And IsNumeric(txtM.Text) Then
    
    For i = 0 To CLng(txtT.Text) - 1
      minVal = 1000000000#
      ReDim res(CLng(txtN.Text) - 1)
      total = 0
      For j = 0 To CLng(txtN.Text) - 1
        res(j) = CLng(Int(Rnd * CLng(dboD.Text)) + 1)
        total = total + res(j)
        If minVal > res(j) Then minVal = res(j)
      Next j
      If chkDropLow.value = Checked Then
        total = ((total - minVal) + CLng(txtP.Text)) * CLng(txtM.Text)
      Else
        total = (total + CLng(txtP.Text)) * CLng(txtM.Text)
      End If
      
      strItem = CStr(total) & Chr(9) & "(" & txtN.Text & "d" & dboD.Text & "+" & txtP.Text & ") x " & _
        txtM.Text & ": " & Chr(9) & "("
      
      For j = 0 To CLng(txtN.Text) - 1
        strItem = strItem & CStr(res(j)) & ", "
      Next j
      strItem = LEFT(strItem, Len(strItem) - 2) & ")"
        
      lstResults.AddItem strItem
    Next i
  End If
  
End Sub

Private Sub Form_Load()
  dboD.ListIndex = 0
End Sub

Private Sub lstResults_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 Then
    PopupMenu mnuPopup
  End If
End Sub

Private Sub mnuPopupClear_Click()
  lstResults.Clear
End Sub
