VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmEncounterBuilder 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Encounter Builder"
   ClientHeight    =   7935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13215
   Icon            =   "frmEncounterBuilder.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   13215
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   7680
      Width           =   13215
      _ExtentX        =   23310
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   8
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8625
            Text            =   "Encounter Name"
            TextSave        =   "Encounter Name"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Text            =   "Minimum EL = 99.9"
            TextSave        =   "Minimum EL = 99.9"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Text            =   "Average EL = 99.9"
            TextSave        =   "Average EL = 99.9"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   2619
            Text            =   "Maximum EL = 99.9"
            TextSave        =   "Maximum EL = 99.9"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1411
            MinWidth        =   1411
            Text            =   "ML = 0"
            TextSave        =   "ML = 0"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1411
            MinWidth        =   1411
            Text            =   "PL = 0"
            TextSave        =   "PL = 0"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1411
            MinWidth        =   1411
            Text            =   "PsiL = 0"
            TextSave        =   "PsiL = 0"
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Very Rare"
            TextSave        =   "Very Rare"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7575
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   13361
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "General Info"
      TabPicture(0)   =   "frmEncounterBuilder.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Label6"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cboCategory"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdDuplicate"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdRemove"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdAddEncouter"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cboEncounterName"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmdRename"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).ControlCount=   9
      TabCaption(1)   =   "Monsters"
      TabPicture(1)   =   "frmEncounterBuilder.frx":0166
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label7"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "tvMonsterLibrary"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lstMonsterTitlesAdded"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "SSTab2"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.CommandButton cmdRename 
         Caption         =   "Rename"
         Height          =   315
         Left            =   -72660
         TabIndex        =   109
         Top             =   900
         Width           =   795
      End
      Begin TabDlg.SSTab SSTab2 
         Height          =   5415
         Left            =   5520
         TabIndex        =   63
         Top             =   2040
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   9551
         _Version        =   393216
         Style           =   1
         Tabs            =   4
         TabsPerRow      =   6
         TabHeight       =   520
         TabCaption(0)   =   "General Info"
         TabPicture(0)   =   "frmEncounterBuilder.frx":0182
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label8"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label9"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label10"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label11"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label12"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label13"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblCR"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Label21"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Label22"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtMonsterTitle"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboMonsterName"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtMinAppearing"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtMaxAppearing"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPctChance"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtNonCombatant"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Final Statistics"
         TabPicture(1)   =   "frmEncounterBuilder.frx":019E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lblMonsterSizeTypeDescriptors"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Advancement"
         TabPicture(2)   =   "frmEncounterBuilder.frx":01BA
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "SSTab3"
         Tab(2).Control(1)=   "Frame4"
         Tab(2).Control(2)=   "Frame1"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   "Special Abilities"
         TabPicture(3)   =   "frmEncounterBuilder.frx":01D6
         Tab(3).ControlEnabled=   0   'False
         Tab(3).ControlCount=   0
         Begin VB.TextBox txtNonCombatant 
            Height          =   285
            Left            =   1800
            TabIndex        =   110
            Text            =   "0"
            Top             =   2340
            Width           =   375
         End
         Begin TabDlg.SSTab SSTab3 
            Height          =   2235
            Left            =   -74880
            TabIndex        =   98
            Top             =   3060
            Width           =   6855
            _ExtentX        =   12091
            _ExtentY        =   3942
            _Version        =   393216
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Templates"
            TabPicture(0)   =   "frmEncounterBuilder.frx":01F2
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Label25"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Label24"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "Label29"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "cmdAddTemplate"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "cboTemplatesAvailable"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lstSelectedTemplates"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtTemplateStack"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).ControlCount=   7
            TabCaption(1)   =   "Classes"
            TabPicture(1)   =   "frmEncounterBuilder.frx":020E
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtMaxLvl"
            Tab(1).Control(1)=   "txtMinLvl"
            Tab(1).Control(2)=   "lstClassLevels"
            Tab(1).Control(3)=   "cboClassesAvailable"
            Tab(1).Control(4)=   "cmdAddClassLevel"
            Tab(1).Control(5)=   "Label28"
            Tab(1).Control(6)=   "Label23"
            Tab(1).Control(7)=   "Label26"
            Tab(1).Control(8)=   "Label27"
            Tab(1).ControlCount=   9
            Begin VB.TextBox txtTemplateStack 
               Height          =   285
               Left            =   6180
               TabIndex        =   118
               Text            =   "1"
               Top             =   435
               Width           =   495
            End
            Begin VB.TextBox txtMaxLvl 
               Height          =   315
               Left            =   -68940
               TabIndex        =   116
               Text            =   "1"
               Top             =   1380
               Width           =   375
            End
            Begin VB.TextBox txtMinLvl 
               Height          =   315
               Left            =   -68940
               TabIndex        =   115
               Text            =   "1"
               Top             =   1020
               Width           =   375
            End
            Begin VB.ListBox lstClassLevels 
               Columns         =   2
               Height          =   840
               Left            =   -74940
               TabIndex        =   106
               Top             =   1020
               Width           =   4995
            End
            Begin VB.ComboBox cboClassesAvailable 
               Height          =   315
               Left            =   -73260
               Style           =   2  'Dropdown List
               TabIndex        =   105
               Top             =   420
               Width           =   2175
            End
            Begin VB.CommandButton cmdAddClassLevel 
               Caption         =   "Add"
               Height          =   315
               Left            =   -71040
               TabIndex        =   104
               Top             =   420
               Width           =   675
            End
            Begin VB.ListBox lstSelectedTemplates 
               Columns         =   2
               Height          =   1035
               Left            =   120
               TabIndex        =   101
               Top             =   1020
               Width           =   6555
            End
            Begin VB.ComboBox cboTemplatesAvailable 
               Height          =   315
               Left            =   1800
               Style           =   2  'Dropdown List
               TabIndex        =   100
               Top             =   420
               Width           =   2175
            End
            Begin VB.CommandButton cmdAddTemplate 
               Caption         =   "Add"
               Height          =   315
               Left            =   4020
               TabIndex        =   99
               Top             =   420
               Width           =   675
            End
            Begin VB.Label Label29 
               AutoSize        =   -1  'True
               Caption         =   "Template stack:"
               Height          =   195
               Left            =   4980
               TabIndex        =   117
               Top             =   480
               Width           =   1140
            End
            Begin VB.Label Label28 
               AutoSize        =   -1  'True
               Caption         =   "Max. Level"
               Height          =   195
               Left            =   -69840
               TabIndex        =   114
               Top             =   1440
               Width           =   780
            End
            Begin VB.Label Label23 
               AutoSize        =   -1  'True
               Caption         =   "Min. Level"
               Height          =   195
               Left            =   -69780
               TabIndex        =   113
               Top             =   1080
               Width           =   735
            End
            Begin VB.Label Label26 
               AutoSize        =   -1  'True
               Caption         =   "Available Class Levels:"
               Height          =   195
               Left            =   -74940
               TabIndex        =   108
               Top             =   480
               Width           =   1620
            End
            Begin VB.Label Label27 
               AutoSize        =   -1  'True
               Caption         =   "Selected Class Levels:"
               Height          =   195
               Left            =   -74940
               TabIndex        =   107
               Top             =   780
               Width           =   1605
            End
            Begin VB.Label Label24 
               AutoSize        =   -1  'True
               Caption         =   "Available Templates:"
               Height          =   195
               Left            =   120
               TabIndex        =   103
               Top             =   480
               Width           =   1470
            End
            Begin VB.Label Label25 
               AutoSize        =   -1  'True
               Caption         =   "Selected Templates:"
               Height          =   195
               Left            =   120
               TabIndex        =   102
               Top             =   780
               Width           =   1455
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Hit Dice"
            Height          =   795
            Left            =   -73860
            TabIndex        =   93
            Top             =   2220
            Width           =   4395
            Begin VB.TextBox txtExtraHD 
               Height          =   285
               Left            =   960
               TabIndex        =   95
               Text            =   "0"
               Top             =   285
               Width           =   1215
            End
            Begin VB.CommandButton cmdMaxExtraHD 
               Caption         =   "Max"
               Height          =   255
               Left            =   2220
               TabIndex        =   94
               Top             =   300
               Width           =   555
            End
            Begin VB.Label Label20 
               Caption         =   "Extra HD:"
               Height          =   195
               Left            =   180
               TabIndex        =   97
               Top             =   330
               Width           =   690
            End
            Begin VB.Label lblNewSize 
               Caption         =   "Size"
               Height          =   195
               Left            =   2880
               TabIndex        =   96
               Top             =   330
               Width           =   840
            End
         End
         Begin VB.Frame Frame1 
            Caption         =   "Ability Scores"
            Height          =   1515
            Left            =   -74700
            TabIndex        =   77
            Top             =   660
            Width           =   6435
            Begin VB.OptionButton optAbilityScores 
               Caption         =   "Base (11, 11, 11, 10, 10, 10)"
               Height          =   255
               Left            =   120
               TabIndex        =   80
               Top             =   300
               Value           =   -1  'True
               Width           =   2415
            End
            Begin VB.OptionButton optNonElite 
               Caption         =   "Non-Elite (13, 12, 11, 10, 9, 8)"
               Height          =   255
               Left            =   120
               TabIndex        =   79
               Top             =   600
               Width           =   2535
            End
            Begin VB.OptionButton Option1 
               Caption         =   "Elite (15, 14, 13, 12, 10, 8)"
               Height          =   255
               Left            =   120
               TabIndex        =   78
               Top             =   900
               Width           =   2535
            End
            Begin VB.Label Label14 
               AutoSize        =   -1  'True
               Caption         =   "Strength:"
               Height          =   195
               Left            =   3240
               TabIndex        =   92
               Top             =   285
               Width           =   645
            End
            Begin VB.Label Label15 
               AutoSize        =   -1  'True
               Caption         =   "Dexterity:"
               Height          =   195
               Left            =   3240
               TabIndex        =   91
               Top             =   645
               Width           =   660
            End
            Begin VB.Label Label16 
               AutoSize        =   -1  'True
               Caption         =   "Constitution:"
               Height          =   195
               Left            =   3240
               TabIndex        =   90
               Top             =   1005
               Width           =   870
            End
            Begin VB.Label Label17 
               AutoSize        =   -1  'True
               Caption         =   "Intelligence:"
               Height          =   195
               Left            =   4680
               TabIndex        =   89
               Top             =   285
               Width           =   855
            End
            Begin VB.Label Label18 
               AutoSize        =   -1  'True
               Caption         =   "Wisdom:"
               Height          =   195
               Left            =   4680
               TabIndex        =   88
               Top             =   645
               Width           =   615
            End
            Begin VB.Label Label19 
               AutoSize        =   -1  'True
               Caption         =   "Charisma:"
               Height          =   195
               Left            =   4680
               TabIndex        =   87
               Top             =   1005
               Width           =   690
            End
            Begin VB.Label lblStr 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   4260
               TabIndex        =   86
               Top             =   255
               Width           =   240
            End
            Begin VB.Label lblDex 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   4260
               TabIndex        =   85
               Top             =   615
               Width           =   240
            End
            Begin VB.Label lblCon 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   4260
               TabIndex        =   84
               Top             =   975
               Width           =   240
            End
            Begin VB.Label lblInt 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   5760
               TabIndex        =   83
               Top             =   255
               Width           =   240
            End
            Begin VB.Label lblWis 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   5760
               TabIndex        =   82
               Top             =   615
               Width           =   240
            End
            Begin VB.Label lblCha 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "10"
               Height          =   255
               Left            =   5760
               TabIndex        =   81
               Top             =   975
               Width           =   240
            End
         End
         Begin VB.TextBox txtPctChance 
            Height          =   285
            Left            =   1800
            TabIndex        =   73
            Text            =   "100"
            Top             =   1980
            Width           =   375
         End
         Begin VB.TextBox txtMaxAppearing 
            Height          =   285
            Left            =   5100
            TabIndex        =   71
            Text            =   "1"
            Top             =   1515
            Width           =   555
         End
         Begin VB.TextBox txtMinAppearing 
            Height          =   285
            Left            =   2220
            TabIndex        =   69
            Text            =   "1"
            Top             =   1515
            Width           =   555
         End
         Begin VB.ComboBox cboMonsterName 
            Height          =   315
            Left            =   1320
            Style           =   2  'Dropdown List
            TabIndex        =   67
            Top             =   1080
            Width           =   5775
         End
         Begin VB.TextBox txtMonsterTitle 
            Height          =   315
            Left            =   1320
            TabIndex        =   65
            Top             =   720
            Width           =   5775
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            Caption         =   "Non-combatants:"
            Height          =   195
            Left            =   180
            TabIndex        =   112
            Top             =   2385
            Width           =   1215
         End
         Begin VB.Label Label21 
            AutoSize        =   -1  'True
            Caption         =   "percent"
            Height          =   195
            Left            =   2220
            TabIndex        =   111
            Top             =   2385
            Width           =   540
         End
         Begin VB.Label lblCR 
            AutoSize        =   -1  'True
            Caption         =   "Challenge Rating: 0"
            Height          =   195
            Left            =   3180
            TabIndex        =   76
            Top             =   2040
            Width           =   1395
         End
         Begin VB.Label lblMonsterSizeTypeDescriptors 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Size Type [subtype] (Descriptor)"
            Height          =   495
            Left            =   -74760
            TabIndex        =   75
            Top             =   660
            Width           =   5340
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "percent"
            Height          =   195
            Left            =   2220
            TabIndex        =   74
            Top             =   2025
            Width           =   540
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Chance of appearing:"
            Height          =   195
            Left            =   180
            TabIndex        =   72
            Top             =   2025
            Width           =   1530
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Maximum quantity appearing:"
            Height          =   195
            Left            =   3000
            TabIndex        =   70
            Top             =   1560
            Width           =   2055
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "Minimum quantity appearing:"
            Height          =   195
            Left            =   180
            TabIndex        =   68
            Top             =   1560
            Width           =   2010
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Monster Name:"
            Height          =   195
            Left            =   180
            TabIndex        =   66
            Top             =   1140
            Width           =   1080
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Monster Title:"
            Height          =   195
            Left            =   180
            TabIndex        =   64
            Top             =   780
            Width           =   960
         End
      End
      Begin VB.ListBox lstMonsterTitlesAdded 
         Columns         =   1
         Height          =   1230
         Left            =   6600
         TabIndex        =   61
         Top             =   660
         Width           =   6135
      End
      Begin VB.ComboBox cboEncounterName 
         Height          =   315
         Left            =   -74880
         Style           =   2  'Dropdown List
         TabIndex        =   57
         Top             =   540
         Width           =   9675
      End
      Begin VB.CommandButton cmdAddEncouter 
         Caption         =   "Add"
         Height          =   315
         Left            =   -74880
         TabIndex        =   56
         Top             =   900
         Width           =   495
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "Remove"
         Height          =   315
         Left            =   -74340
         TabIndex        =   55
         Top             =   900
         Width           =   795
      End
      Begin VB.CommandButton cmdDuplicate 
         Caption         =   "Duplicate"
         Height          =   315
         Left            =   -73500
         TabIndex        =   54
         Top             =   900
         Width           =   795
      End
      Begin VB.Frame Frame2 
         Caption         =   "Habitats"
         Height          =   6015
         Left            =   -69960
         TabIndex        =   13
         Top             =   1320
         Width           =   4755
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Aquatic"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   53
            Top             =   240
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Desert"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   52
            Top             =   480
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Forest"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   51
            Top             =   720
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Hills"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   50
            Top             =   960
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Marsh"
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   49
            Top             =   1200
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Mountain"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   48
            Top             =   1440
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Cold Plain"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   47
            Top             =   1680
            Width           =   1335
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Aquatic"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   46
            Top             =   2160
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Desert"
            Height          =   255
            Index           =   8
            Left            =   120
            TabIndex        =   45
            Top             =   2400
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Forest"
            Height          =   255
            Index           =   9
            Left            =   120
            TabIndex        =   44
            Top             =   2640
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Hills"
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   43
            Top             =   2880
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Marsh"
            Height          =   255
            Index           =   11
            Left            =   120
            TabIndex        =   42
            Top             =   3120
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Mountain"
            Height          =   255
            Index           =   12
            Left            =   120
            TabIndex        =   41
            Top             =   3360
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Temperate Plain"
            Height          =   255
            Index           =   13
            Left            =   120
            TabIndex        =   40
            Top             =   3600
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Aquatic"
            Height          =   255
            Index           =   14
            Left            =   120
            TabIndex        =   39
            Top             =   4020
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Desert"
            Height          =   255
            Index           =   15
            Left            =   120
            TabIndex        =   38
            Top             =   4260
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Forest"
            Height          =   255
            Index           =   16
            Left            =   120
            TabIndex        =   37
            Top             =   4500
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Hills"
            Height          =   255
            Index           =   17
            Left            =   120
            TabIndex        =   36
            Top             =   4740
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Marsh"
            Height          =   255
            Index           =   18
            Left            =   120
            TabIndex        =   35
            Top             =   4980
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Mountain"
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   34
            Top             =   5220
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Warm Plain"
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   33
            Top             =   5460
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Stoney Barrens"
            Height          =   255
            Index           =   21
            Left            =   2220
            TabIndex        =   32
            Top             =   180
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Sandy Wastes"
            Height          =   255
            Index           =   22
            Left            =   2220
            TabIndex        =   31
            Top             =   420
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Rocky Badlands"
            Height          =   255
            Index           =   23
            Left            =   2220
            TabIndex        =   30
            Top             =   660
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Salt Flats"
            Height          =   255
            Index           =   24
            Left            =   2220
            TabIndex        =   29
            Top             =   900
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Silt Basin"
            Height          =   255
            Index           =   25
            Left            =   2220
            TabIndex        =   28
            Top             =   1140
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Mudflats"
            Height          =   255
            Index           =   26
            Left            =   2220
            TabIndex        =   27
            Top             =   1380
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Urban"
            Height          =   255
            Index           =   34
            Left            =   2220
            TabIndex        =   26
            Top             =   4020
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Urban Sewers"
            Height          =   255
            Index           =   35
            Left            =   2220
            TabIndex        =   25
            Top             =   4260
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Ruins"
            Height          =   255
            Index           =   36
            Left            =   2220
            TabIndex        =   24
            Top             =   4500
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Sky"
            Height          =   255
            Index           =   27
            Left            =   2220
            TabIndex        =   23
            Top             =   2160
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Underground"
            Height          =   255
            Index           =   28
            Left            =   2220
            TabIndex        =   22
            Top             =   2400
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Upperdark"
            Height          =   255
            Index           =   29
            Left            =   2220
            TabIndex        =   21
            Top             =   2640
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Middledark"
            Height          =   255
            Index           =   30
            Left            =   2220
            TabIndex        =   20
            Top             =   2880
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Lowerdark"
            Height          =   255
            Index           =   31
            Left            =   2220
            TabIndex        =   19
            Top             =   3120
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Wildspace"
            Height          =   255
            Index           =   32
            Left            =   2220
            TabIndex        =   18
            Top             =   3360
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Phlogiston"
            Height          =   255
            Index           =   33
            Left            =   2220
            TabIndex        =   17
            Top             =   3600
            Width           =   1815
         End
         Begin VB.CheckBox chkHabitat 
            Caption         =   "Volcanic"
            Height          =   255
            Index           =   37
            Left            =   2220
            TabIndex        =   16
            Top             =   1620
            Width           =   1815
         End
         Begin VB.CommandButton cmdCheckAll 
            Caption         =   "Check All"
            Height          =   315
            Left            =   2220
            TabIndex        =   15
            Top             =   4980
            Width           =   1215
         End
         Begin VB.CommandButton cmdClearAll 
            Caption         =   "Clear All"
            Height          =   315
            Left            =   2220
            TabIndex        =   14
            Top             =   5340
            Width           =   1215
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Encounter Info"
         Height          =   2355
         Left            =   -74520
         TabIndex        =   3
         Top             =   1560
         Width           =   4155
         Begin VB.TextBox txtMinPL 
            Height          =   285
            Left            =   3060
            TabIndex        =   8
            Text            =   "0"
            Top             =   315
            Width           =   795
         End
         Begin VB.TextBox txtMinML 
            Height          =   285
            Left            =   3060
            TabIndex        =   7
            Text            =   "1"
            Top             =   660
            Width           =   795
         End
         Begin VB.TextBox Text2 
            Height          =   285
            Left            =   3060
            TabIndex        =   6
            Text            =   "0"
            Top             =   1020
            Width           =   795
         End
         Begin VB.ComboBox cboFrequency 
            Height          =   315
            ItemData        =   "frmEncounterBuilder.frx":022A
            Left            =   1920
            List            =   "frmEncounterBuilder.frx":023D
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   1380
            Width           =   1935
         End
         Begin VB.CheckBox chkRandomTable 
            Caption         =   "Include in random table"
            Height          =   255
            Left            =   180
            TabIndex        =   4
            Top             =   1800
            Width           =   2295
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Required Technological Progress Level:"
            Height          =   195
            Left            =   180
            TabIndex        =   12
            Top             =   360
            Width           =   2835
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Required Magical Spell Level:"
            Height          =   195
            Left            =   180
            TabIndex        =   11
            Top             =   705
            Width           =   2115
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Required Psionic Power Level:"
            Height          =   195
            Left            =   180
            TabIndex        =   10
            Top             =   1065
            Width           =   2175
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Encounter Frequency:"
            Height          =   195
            Left            =   180
            TabIndex        =   9
            Top             =   1440
            Width           =   1575
         End
      End
      Begin VB.ComboBox cboCategory 
         Height          =   315
         Left            =   -70980
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   900
         Width           =   5775
      End
      Begin MSComctlLib.TreeView tvMonsterLibrary 
         Height          =   6795
         Left            =   60
         TabIndex        =   58
         Top             =   600
         Width           =   5235
         _ExtentX        =   9234
         _ExtentY        =   11986
         _Version        =   393217
         Indentation     =   706
         LineStyle       =   1
         Style           =   7
         SingleSel       =   -1  'True
         Appearance      =   1
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Creatures Encountered:"
         Height          =   195
         Left            =   5520
         TabIndex        =   62
         Top             =   420
         Width           =   1680
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Category:"
         Height          =   195
         Left            =   -71760
         TabIndex        =   60
         Top             =   960
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Monster Library"
         Height          =   195
         Left            =   120
         TabIndex        =   59
         Top             =   360
         Width           =   1080
      End
   End
End
Attribute VB_Name = "frmEncounterBuilder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cboEncounterName_Click()
  Dim a As New CDB
  Dim i As Long, j As Long, k As Long
  Dim strTemp() As String
  Dim strTemp1 As String
  
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  ' Clear the GUI
  
  ' Load up the GUI
  a.OpenDB "SELECT * FROM MonsterEncounter WHERE EncounterName = '" & Apostrify(cboEncounterName.Text) & "'"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      cboCategory.Text = a.rs!category
      StatusBar1.Panels(1).Text = a.rs!EncounterName
      StatusBar1.Panels(2).Text = "Minimum EL = " & Format(a.rs!minEL, "0.0")
      StatusBar1.Panels(3).Text = "Average EL = " & Format(a.rs!AvgEL, "0.0")
      StatusBar1.Panels(4).Text = "Maximum EL = " & Format(a.rs!maxEL, "0.0")
      StatusBar1.Panels(5).Text = "ML = " & CStr(a.rs!ML)
      StatusBar1.Panels(6).Text = "PL = " & CStr(a.rs!PL)
      txtMinPL.Text = CStr(a.rs!PL)
      txtMinML.Text = CStr(a.rs!ML)
      
      StatusBar1.Panels(7).Text = "PsiL = " & CStr(a.rs!ML)
      If Not IsNull(a.rs!Frequency) Then
        StatusBar1.Panels(8).Text = a.rs!Frequency
        cboFrequency.Text = a.rs!Frequency
      Else
        StatusBar1.Panels(8).Text = ""
        cboFrequency.ListIndex = -1
      End If
      
      If a.rs!RandomTable = True Then
        chkRandomTable.Value = Checked
      Else
        chkRandomTable.Value = Unchecked
      End If
      
      ' Do this for all the 37 habitats
      For i = 0 To 37
        strTemp = Split(chkHabitat(i).Caption, " ")
        strTemp1 = ""
        For j = 0 To UBound(strTemp)
          strTemp1 = strTemp1 & strTemp(j)
        Next j
        
        For k = 1 To a.rs.Fields.Count
          If a.rs.Fields(k).Name Like strTemp1 Then Exit For
        Next k
        
        If a.rs.Fields(k) = True Then
          chkHabitat(i).Value = Checked
        Else
          chkHabitat(i).Value = Unchecked
        End If
      Next i
    End If
    a.CloseDB
  End If
  
  ' Now add the monster list
  lstMonsterTitlesAdded.Clear
  a.OpenDB "SELECT MonsterTitle FROM MonsterEncounterData WHERE EncounterName = '" & _
    Apostrify(cboEncounterName.Text) & "'"
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        lstMonsterTitlesAdded.AddItem a.rs!MonsterTitle
        a.rs.MoveNext
      Loop
    End If
    a.CloseDB
  End If
  
  lstMonsterTitlesAdded.ListIndex = 0
  lstMonsterTitlesAdded_Click
  
End Sub

Private Sub cmdCheckAll_Click()
  Dim i As Long
  For i = 0 To 37
    chkHabitat(i).Value = Checked
  Next i
End Sub

Private Sub cmdClearAll_Click()
  Dim i As Long
  For i = 0 To 37
    chkHabitat(i).Value = Unchecked
  Next i

End Sub

Private Sub Form_Load()
  MousePointer = vbHourglass
  DoEvents
  PopulateGUI
  MousePointer = vbDefault
End Sub


Private Sub PopulateGUI()
  ' EncounterName
  PopulateEncounterName
  PopulateCategories
  PopulateTemplateNames
  PopulateMonsterNames
  PopulateClassNames
  ' Monster Library Tree
  PopulateMonsterLibrary
End Sub

Private Sub PopulateEncounterName()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  cboEncounterName.Clear
  a.OpenDB "SELECT EncounterName FROM MonsterEncounter ORDER BY Category, AvgEL"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        cboEncounterName.AddItem a.rs!EncounterName
        a.rs.MoveNext
      Loop
    End If
    a.rs.Close
  End If

End Sub

Private Sub PopulateTemplateNames()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  cboTemplatesAvailable.Clear
  a.OpenDB "SELECT DISTINCT Name FROM MonsterTemplates"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        cboTemplatesAvailable.AddItem a.rs!Name
        a.rs.MoveNext
      Loop
    End If
    a.rs.Close
  End If

End Sub

Private Sub PopulateMonsterNames()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  cboMonsterName.Clear
  a.OpenDB "SELECT DISTINCT Name FROM MonsterStats"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        cboMonsterName.AddItem a.rs!Name
        a.rs.MoveNext
      Loop
    End If
    a.rs.Close
  End If

End Sub


Private Sub PopulateClassNames()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  cboClassesAvailable.Clear
  a.OpenDB "SELECT DISTINCT ClassName FROM Classes WHERE IsMonsterType = FALSE"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        cboClassesAvailable.AddItem a.rs!ClassName
        a.rs.MoveNext
      Loop
    End If
    a.rs.Close
  End If

End Sub

Private Sub PopulateCategories()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  cboCategory.Clear
  a.OpenDB "SELECT DISTINCT Category FROM MonsterStats ORDER BY Category"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        cboCategory.AddItem a.rs!category
        a.rs.MoveNext
      Loop
    End If
    a.rs.Close
  End If

End Sub



Private Sub PopulateMonsterLibrary()
  Dim a As New CDB
  Dim strCat As String
  Dim strName As String
  
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State > 0 Then a.ConnectDB
  
  tvMonsterLibrary.Nodes.Clear
  
  tvMonsterLibrary.Nodes.Add , , "TYPE", "By Type"
  tvMonsterLibrary.Nodes("TYPE").Expanded = True
  
  a.OpenDB "SELECT DISTINCT type FROM MonsterStats WHERE Type IS NOT NULL ORDER BY Type"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        tvMonsterLibrary.Nodes.Add "TYPE", tvwChild, _
          "TYPE-" & a.rs!type, a.rs!type
        
        a.rs.MoveNext
      Loop
    End If
    a.CloseDB
  End If
  
  tvMonsterLibrary.Nodes.Add "TYPE", tvwChild, "TYPE-NONE", "Not assigned"
  
  
  a.OpenDB "SELECT DISTINCT Category, type FROM MonsterStats ORDER BY Category"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        strCat = Trim(a.rs!category)
        If Not IsNull(a.rs!type) Then
          tvMonsterLibrary.Nodes.Add "TYPE-" & a.rs!type, tvwChild, _
            "TYPE-" & a.rs!type & "-" & strCat, strCat
        Else
          tvMonsterLibrary.Nodes.Add "TYPE-NONE", tvwChild, _
            "TYPE-NONE-" & strCat, strCat
        End If
        
        cboCategory.AddItem strCat
        a.rs.MoveNext
      Loop
    End If
    a.CloseDB
  End If
  
  DoEvents
  
  
  a.OpenDB "SELECT category, name, type FROM MonsterStats ORDER BY category, CR, name"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        strCat = Trim(a.rs!category)
        strName = Trim(a.rs!Name)
        
        If strCat Like "Tohr-Kreen" And strName Like "Tohr-Kreen" Then
          If Not IsNull(a.rs!type) Then
            'tvMonsterLibrary.Nodes.Add "TYPE-" & a.rs!Type, tvwChild, _
              "TYPE-" & a.rs!Type & "-" & strCat, strCat
          Else
            'tvMonsterLibrary.Nodes.Add "TYPE-NONE", tvwChild, _
              "TYPE-NONE-" & strCat, strCat
          End If
        End If
        
        If Not IsNull(a.rs!type) Then
          tvMonsterLibrary.Nodes.Add "TYPE-" & a.rs!type & "-" & strCat, tvwChild, _
            "TYPE-" & a.rs!type & "-" & strCat & "-" & strName, strName
        Else
          tvMonsterLibrary.Nodes.Add "TYPE-NONE-" & strCat, tvwChild, _
            "TYPE-NONE-" & strCat & "-" & strName, strName
        End If
        a.rs.MoveNext
      Loop
    End If
    a.CloseDB
  End If
  
  
End Sub

Private Sub lstClassLevels_Click()
  ' get the minimum and maximum levels
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State = 0 Then a.ConnectDB
  
  a.OpenDB "SELECT MinClassLevel, MaxClassLevel FROM MonsterEncounterClasses WHERE EncounterName = '" & _
    Apostrify(cboEncounterName.Text) & "' AND MonsterName = '" & _
    Apostrify(cboMonsterName.Text) & "' AND MonsterTitle = '" & _
    Apostrify(txtMonsterTitle.Text) & "' AND ClassName = '" & _
    Apostrify(lstClassLevels.list(lstClassLevels.ListIndex)) & "'"
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      txtMinLvl.Text = CStr(a.rs!MinClassLevel)
      txtMaxLvl.Text = CStr(a.rs!MaxClassLevel)
    End If
    a.CloseDB
  End If
End Sub

Private Sub lstMonsterTitlesAdded_Click()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State = 0 Then a.ConnectDB
  
  ' Clear the monster info
  
  ' Update the monster info
  
  
  ' Templates, advancement, etc
  a.OpenDB "SELECT * FROM MonsterEncounterData WHERE EncounterName = '" & _
    Apostrify(cboEncounterName.Text) & "' AND MonsterTitle = '" & _
    Apostrify(lstMonsterTitlesAdded.list(lstMonsterTitlesAdded.ListIndex)) & "'"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      txtMonsterTitle.Text = a.rs!MonsterTitle
      cboMonsterName.Text = a.rs!monstername
      txtMinAppearing.Text = CStr(a.rs!MinAppearing)
      txtMaxAppearing.Text = CStr(a.rs!MaxAppearing)
      txtPctChance.Text = CStr(a.rs!ChanceOfAppearing)
      txtNonCombatant.Text = CStr(a.rs!NonCombatant)
      If Not IsNull(a.rs!ExtraHD) Then txtExtraHD.Text = CStr(a.rs!ExtraHD)
    End If
    a.CloseDB
  End If

  ' Now the Monster templates and classes
  lstSelectedTemplates.Clear
  lstClassLevels.Clear
  a.OpenDB "SELECT * FROM MonsterEncounterClasses WHERE EncounterName = '" & _
    Apostrify(cboEncounterName.Text) & "' AND MonsterName = '" & _
    Apostrify(cboMonsterName.Text) & "' AND MonsterTitle = '" & _
    Apostrify(txtMonsterTitle.Text) & "'"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        If Len(a.rs!TemplateName) > 1 Then
          lstSelectedTemplates.AddItem a.rs!TemplateName
        End If
        If Len(a.rs!ClassName) > 1 Then
          lstClassLevels.AddItem a.rs!ClassName
        End If
        a.rs.MoveNext
      Loop
    End If
    a.CloseDB
  End If
  
  
  
End Sub

Private Sub lstSelectedTemplates_Click()
  Dim a As New CDB
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  If Conn.State = 0 Then a.ConnectDB
  
  ' get the number of times the template stacks
  a.OpenDB "SELECT TemplateStack FROM MonsterEncounterClasses WHERE EncounterName = '" & _
    Apostrify(cboEncounterName.Text) & "' AND MonsterName = '" & _
    Apostrify(cboMonsterName.Text) & "' AND MonsterTitle = '" & _
    Apostrify(txtMonsterTitle.Text) & "' AND TemplateName = '" & _
    Apostrify(lstSelectedTemplates.list(lstSelectedTemplates.ListIndex)) & "'"
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      txtTemplateStack.Text = CStr(a.rs!TemplateStack)
    End If
    a.CloseDB
  End If

End Sub


