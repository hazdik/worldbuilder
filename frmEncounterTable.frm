VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmEncounterTable 
   Caption         =   "Encounter Table"
   ClientHeight    =   9105
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12840
   Icon            =   "frmEncounterTable.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9105
   ScaleWidth      =   12840
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Encounter Tables"
      Height          =   9075
      Left            =   3900
      TabIndex        =   12
      Top             =   0
      Width           =   8895
      Begin VB.ListBox lstEncounterTableUnique 
         Height          =   8055
         ItemData        =   "frmEncounterTable.frx":18BA
         Left            =   8940
         List            =   "frmEncounterTable.frx":18BC
         TabIndex        =   25
         Top             =   480
         Width           =   4155
      End
      Begin VB.ListBox lstEncounterTableVeryRare 
         Height          =   3960
         ItemData        =   "frmEncounterTable.frx":18BE
         Left            =   4620
         List            =   "frmEncounterTable.frx":18C0
         TabIndex        =   17
         Top             =   4440
         Width           =   4155
      End
      Begin VB.ListBox lstEncounterTableRare 
         Height          =   3960
         ItemData        =   "frmEncounterTable.frx":18C2
         Left            =   120
         List            =   "frmEncounterTable.frx":18C4
         TabIndex        =   16
         Top             =   4455
         Width           =   4335
      End
      Begin VB.ListBox lstEncounterTableUncommon 
         Height          =   3570
         ItemData        =   "frmEncounterTable.frx":18C6
         Left            =   4620
         List            =   "frmEncounterTable.frx":18C8
         TabIndex        =   15
         Top             =   480
         Width           =   4155
      End
      Begin VB.CommandButton cmdRollEncounter 
         Caption         =   "Roll Encounter"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4380
         TabIndex        =   14
         Top             =   8580
         Width           =   2775
      End
      Begin VB.ListBox lstEncounterTableCommon 
         Height          =   3570
         ItemData        =   "frmEncounterTable.frx":18CA
         Left            =   120
         List            =   "frmEncounterTable.frx":18CC
         TabIndex        =   13
         Top             =   480
         Width           =   4335
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Unique Encounters"
         Height          =   195
         Left            =   8940
         TabIndex        =   26
         Top             =   240
         Width           =   1365
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Very Rare Encounters"
         Height          =   195
         Left            =   4620
         TabIndex        =   22
         Top             =   4200
         Width           =   1560
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Rare Encounters"
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   4200
         Width           =   1200
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Uncommon Encounters"
         Height          =   195
         Left            =   4620
         TabIndex        =   20
         Top             =   240
         Width           =   1665
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Common Encounters"
         Height          =   195
         Left            =   180
         TabIndex        =   19
         Top             =   240
         Width           =   1470
      End
      Begin VB.Label lblRecordsReturned 
         AutoSize        =   -1  'True
         Caption         =   "0 records returned."
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   8640
         Width           =   1335
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Encounter Data"
      Height          =   9075
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
      Begin VB.TextBox txtMaxML 
         Height          =   315
         Left            =   3180
         TabIndex        =   34
         Text            =   "10"
         Top             =   1500
         Width           =   315
      End
      Begin VB.TextBox txtMinML 
         Height          =   315
         Left            =   2640
         TabIndex        =   32
         Text            =   "0"
         Top             =   1500
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox txtMaxPL 
         Height          =   315
         Left            =   3180
         TabIndex        =   30
         Text            =   "3"
         Top             =   1140
         Width           =   315
      End
      Begin VB.TextBox txtMinPL 
         Height          =   315
         Left            =   2640
         TabIndex        =   28
         Text            =   "0"
         Top             =   1140
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CheckBox chkUseAllEnc 
         Caption         =   "Use all encounters"
         Height          =   375
         Left            =   2280
         TabIndex        =   24
         Top             =   240
         Width           =   1455
      End
      Begin MSComctlLib.TreeView TreeView1 
         Height          =   6555
         Left            =   60
         TabIndex        =   23
         Top             =   1920
         Width           =   3675
         _ExtentX        =   6482
         _ExtentY        =   11562
         _Version        =   393217
         Indentation     =   794
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         Appearance      =   1
      End
      Begin VB.CommandButton cmdCheckAll 
         Caption         =   "Check All"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   8580
         Width           =   975
      End
      Begin VB.CommandButton cmdClearAll 
         Caption         =   "Clear All"
         Height          =   375
         Left            =   1140
         TabIndex        =   10
         Top             =   8580
         Width           =   1035
      End
      Begin VB.CommandButton cmdGetTable 
         Caption         =   "Get Table"
         Height          =   375
         Left            =   2220
         TabIndex        =   9
         Top             =   8580
         Width           =   1455
      End
      Begin VB.CheckBox chkOGL 
         Caption         =   "Open Game Content Only"
         BeginProperty DataFormat 
            Type            =   4
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   8
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   300
         Value           =   1  'Checked
         Width           =   2115
      End
      Begin VB.Frame Frame1 
         Caption         =   "Query Mode"
         Height          =   555
         Left            =   120
         TabIndex        =   3
         Top             =   540
         Width           =   2475
         Begin VB.OptionButton optQueryMode 
            Caption         =   "Encounters"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   5
            Top             =   240
            Value           =   -1  'True
            Width           =   1215
         End
         Begin VB.OptionButton optQueryMode 
            Caption         =   "Monsters"
            Height          =   195
            Index           =   1
            Left            =   1380
            TabIndex        =   4
            Top             =   240
            Width           =   1035
         End
      End
      Begin VB.TextBox txtCRLow 
         Height          =   285
         Left            =   480
         TabIndex        =   2
         Text            =   "0"
         Top             =   1140
         Width           =   675
      End
      Begin VB.TextBox txtCRHigh 
         Height          =   285
         Left            =   1380
         TabIndex        =   1
         Text            =   "1000"
         Top             =   1140
         Width           =   675
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "to"
         Height          =   195
         Left            =   3000
         TabIndex        =   33
         Top             =   1560
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "ML:"
         Height          =   195
         Left            =   2340
         TabIndex        =   31
         Top             =   1560
         Width           =   270
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "to"
         Height          =   195
         Left            =   3000
         TabIndex        =   29
         Top             =   1200
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "PL:"
         Height          =   195
         Left            =   2340
         TabIndex        =   27
         Top             =   1200
         Width           =   240
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "EL:"
         Height          =   195
         Left            =   180
         TabIndex        =   8
         Top             =   1185
         Width           =   240
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "to"
         Height          =   195
         Left            =   1200
         TabIndex        =   7
         Top             =   1185
         Width           =   135
      End
   End
End
Attribute VB_Name = "frmEncounterTable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkOGL_Click()
  PopulateTreeNodes
  'cmdGetTable_Click
  
End Sub

Private Sub chkUseAllEnc_Click()
  Select Case chkUseAllEnc.value
    Case Checked
      Frame4.Width = 13215
      Me.Width = 17280
    Case Unchecked
      Frame4.Width = 8895
      Me.Width = 12960
  End Select
  DoEvents
  'cmdGetTable_Click
End Sub

Private Sub cmdCheckAll_Click()
  Dim i As Long
  For i = 1 To TreeView1.Nodes.Count
    TreeView1.Nodes(i).Checked = True
  Next i
End Sub

Private Sub cmdClearAll_Click()
  Dim i As Long
  For i = 1 To TreeView1.Nodes.Count
    TreeView1.Nodes(i).Checked = False
  Next i
End Sub

Private Sub cmdGetTable_Click()
  Dim db As New CDB
  Dim strQuery As String
  Dim strTemp As String
  
  lstEncounterTableCommon.Clear
  lstEncounterTableUncommon.Clear
  lstEncounterTableRare.Clear
  lstEncounterTableVeryRare.Clear
  lstEncounterTableUnique.Clear
  
  db.ConnectDB
  
  If optQueryMode(0).value = True Then
    ' The base query
    strQuery = "SELECT * FROM MonsterEncounter WHERE " & _
      "AvgEL <= " & CStr(txtCRHigh.Text) & _
      " And AvgEL >= " & CStr(txtCRLow.Text) & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " And Frequency = 'Common'"
    
    If chkUseAllEnc.value = Unchecked Then
      strQuery = strQuery & " AND RandomTable = TRUE"
    End If
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND OGLCompliant = TRUE"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY Frequency, AvgEL, EncounterName"
    
    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableCommon.Clear
          Do While Not .EOF
            If CLng(!minEL) <> CLng(!maxEL) Then
              strTemp = !EncounterName & _
              " (EL " & Format(!minEL, "0") & " to " & Format(!maxEL, "0") & _
              ", avg EL = " & Format(!AvgEL, "0.0") & ")"
            ElseIf !AvgEL < 1 Then
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0.0") & ")"
            Else
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0") & ")"
            End If
            
            If Not lstEncounterTableCommon.List(lstEncounterTableCommon.ListCount - 1) Like strTemp Then
              lstEncounterTableCommon.AddItem strTemp
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If

    ' The base query
    strQuery = "SELECT * FROM MonsterEncounter WHERE " & _
      "AvgEL <= " & CStr(txtCRHigh.Text) & _
      " And AvgEL >= " & CStr(txtCRLow.Text) & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " And Frequency = 'Uncommon'"
    
    If chkUseAllEnc.value = Unchecked Then
      strQuery = strQuery & " AND RandomTable = TRUE"
    End If
    
    
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND OGLCompliant = TRUE"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY Frequency, AvgEL, EncounterName"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableUncommon.Clear
          Do While Not .EOF
            If CLng(!minEL) <> CLng(!maxEL) Then
              strTemp = !EncounterName & _
                " (EL " & Format(!minEL, "0") & " to " & Format(!maxEL, "0") & _
                ", avg EL = " & Format(!AvgEL, "0.0") & ")"
            ElseIf !AvgEL < 1 Then
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0.0") & ")"
            Else
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0") & ")"
            End If
            
            If Not lstEncounterTableUncommon.List(lstEncounterTableUncommon.ListCount - 1) Like strTemp Then
              lstEncounterTableUncommon.AddItem strTemp
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If

    ' The base query
    strQuery = "SELECT * FROM MonsterEncounter WHERE " & _
      "AvgEL <= " & CStr(txtCRHigh.Text) & _
      " And AvgEL >= " & CStr(txtCRLow.Text) & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " And Frequency = 'Rare'"
    
    If chkUseAllEnc.value = Unchecked Then
      strQuery = strQuery & " AND RandomTable = TRUE"
    End If
    
    
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND OGLCompliant = TRUE"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY Frequency, AvgEL, EncounterName"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableRare.Clear
          Do While Not .EOF
            If CLng(!minEL) <> CLng(!maxEL) Then
              strTemp = !EncounterName & _
                " (EL " & Format(!minEL, "0") & " to " & Format(!maxEL, "0") & _
                ", avg EL = " & Format(!AvgEL, "0.0") & ")"
            ElseIf !AvgEL < 1 Then
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0.0") & ")"
            Else
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0") & ")"
            End If
            
            If Not lstEncounterTableRare.List(lstEncounterTableRare.ListCount - 1) Like strTemp Then
              lstEncounterTableRare.AddItem strTemp
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If

    ' The base query
    strQuery = "SELECT * FROM MonsterEncounter WHERE " & _
      "AvgEL <= " & CStr(txtCRHigh.Text) & _
      " And AvgEL >= " & CStr(txtCRLow.Text) & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " And Frequency = 'Very Rare'"
    
    If chkUseAllEnc.value = Unchecked Then
      strQuery = strQuery & " AND RandomTable = TRUE"
    End If
    
    
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND OGLCompliant = TRUE"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY Frequency, AvgEL, EncounterName"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableVeryRare.Clear
          Do While Not .EOF
            If CLng(!minEL) <> CLng(!maxEL) Then
              strTemp = !EncounterName & _
                " (EL " & Format(!minEL, "0") & " to " & Format(!maxEL, "0") & _
                ", avg EL = " & Format(!AvgEL, "0.0") & ")"
            ElseIf !AvgEL < 1 Then
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0.0") & ")"
            Else
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0") & ")"
            End If
            
            If Not lstEncounterTableVeryRare.List(lstEncounterTableVeryRare.ListCount - 1) Like strTemp Then
              lstEncounterTableVeryRare.AddItem strTemp
            End If
            .MoveNext
            DoEvents
          Loop
        End If
      End With
      db.CloseDB
    End If
  
  
    ' The base query
    strQuery = "SELECT * FROM MonsterEncounter WHERE " & _
      "AvgEL <= " & CStr(txtCRHigh.Text) & _
      " And AvgEL >= " & CStr(txtCRLow.Text) & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " And Frequency = 'Unique'"
    
    If chkUseAllEnc.value = Unchecked Then
      strQuery = strQuery & " AND RandomTable = TRUE"
    End If
    
    
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND OGLCompliant = TRUE"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY Frequency, AvgEL, EncounterName"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableUnique.Clear
          Do While Not .EOF
            If CLng(!minEL) <> CLng(!maxEL) Then
              strTemp = !EncounterName & _
                " (EL " & Format(!minEL, "0") & " to " & Format(!maxEL, "0") & _
                ", avg EL = " & Format(!AvgEL, "0.0") & ")"
            ElseIf !AvgEL < 1 Then
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0.0") & ")"
            Else
              strTemp = !EncounterName & " (EL " & Format(!AvgEL, "0") & ")"
            End If
            
            If Not lstEncounterTableUnique.List(lstEncounterTableUnique.ListCount - 1) Like strTemp Then
              lstEncounterTableUnique.AddItem strTemp
            End If
            .MoveNext
            DoEvents
          Loop
        End If
      End With
      db.CloseDB
    End If
  
  
  Else
    ' The base query
    strQuery = "SELECT * FROM MonsterStats WHERE CR >= " & CStr(txtCRLow.Text) & _
      " AND CR <= " & CStr(txtCRHigh.Text) & " AND Frequency = 'Common'" & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " AND RandomTable = True"
 
    If chkOGL.value = Checked Then strQuery = strQuery & " AND Reference IS NOT NULL"
    
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString & " ORDER BY CR, Name"
     
    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          Do While Not .EOF
            If Not (lstEncounterTableCommon.List(lstEncounterTableCommon.ListCount - 1) Like !Name & " (CR " & Format(!CR, "0") & ")") Then
              lstEncounterTableCommon.AddItem !Name & " (CR " & Format(!CR, "0") & ")"
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If
  
    ' The base query
    strQuery = "SELECT * FROM MonsterStats WHERE CR >= " & CStr(txtCRLow.Text) & _
      " AND CR <= " & CStr(txtCRHigh.Text) & " AND Frequency = 'Uncommon'" & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " AND RandomTable = True"
 
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND Reference IS NOT NULL"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY CR, Name"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableUncommon.Clear
          Do While Not .EOF
            If Not (lstEncounterTableUncommon.List(lstEncounterTableUncommon.ListCount - 1) Like !Name & " (CR " & Format(!CR, "0") & ")") Then
              lstEncounterTableUncommon.AddItem !Name & " (CR " & Format(!CR, "0") & ")"
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If
  
    ' The base query
    strQuery = "SELECT * FROM MonsterStats WHERE CR >= " & CStr(txtCRLow.Text) & _
      " AND CR <= " & CStr(txtCRHigh.Text) & " AND Frequency = 'Rare'" & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " AND RandomTable = True"
 
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND Reference IS NOT NULL"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY CR, Name"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableRare.Clear
          Do While Not .EOF
            If Not (lstEncounterTableRare.List(lstEncounterTableRare.ListCount - 1) Like !Name & " (CR " & Format(!CR, "0") & ")") Then
              lstEncounterTableRare.AddItem !Name & " (CR " & Format(!CR, "0") & ")"
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If


    ' The base query
    strQuery = "SELECT * FROM MonsterStats WHERE CR >= " & CStr(txtCRLow.Text) & _
      " AND CR <= " & CStr(txtCRHigh.Text) & " AND Frequency = 'Very Rare'" & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " AND RandomTable = True"
      
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND Reference IS NOT NULL"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY  CR, Name"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableVeryRare.Clear
          Do While Not .EOF
            If Not (lstEncounterTableVeryRare.List(lstEncounterTableVeryRare.ListCount - 1) Like !Name & " (CR " & Format(!CR, "0") & ")") Then
              lstEncounterTableVeryRare.AddItem !Name & " (CR " & Format(!CR, "0") & ")"
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If

  
    ' The base query
    strQuery = "SELECT * FROM MonsterStats WHERE CR >= " & CStr(txtCRLow.Text) & _
      " AND CR <= " & CStr(txtCRHigh.Text) & " AND Frequency = 'Unique'" & _
      " AND PL <= " & CStr(txtMaxPL.Text) & _
      " AND ML <= " & CStr(txtMaxML.Text) & _
      " AND RandomTable = True"
      
    ' filter through the habitats
    strQuery = strQuery & CreateHabitatQueryString
    
    If chkOGL.value = Checked Then strQuery = strQuery & " AND Reference IS NOT NULL"
    
    ' Finish off with the sort
    strQuery = strQuery & " ORDER BY  CR, Name"

    db.OpenDB strQuery
    
    If db.rs.State > 0 Then
      With db.rs
        If .RecordCount > 0 Then
          .MoveFirst
          lstEncounterTableUnique.Clear
          Do While Not .EOF
            If Not (lstEncounterTableUnique.List(lstEncounterTableUnique.ListCount - 1) Like !Name & " (CR " & Format(!CR, "0") & ")") Then
              lstEncounterTableUnique.AddItem !Name & " (CR " & Format(!CR, "0") & ")"
            End If
            .MoveNext
          Loop
        End If
      End With
      db.CloseDB
    End If
  
  End If

  Conn.Close
  
  If lstEncounterTableCommon.ListCount > 0 Or lstEncounterTableUncommon.ListCount > 0 _
    Or lstEncounterTableRare.ListCount > 0 Or lstEncounterTableVeryRare.ListCount > 0 Then
      cmdRollEncounter.Enabled = True
  End If
    
  lblRecordsReturned.Caption = CStr(lstEncounterTableCommon.ListCount + lstEncounterTableUncommon.ListCount + _
    lstEncounterTableRare.ListCount + lstEncounterTableVeryRare.ListCount) & " records returned."
    
  If lstEncounterTableCommon.ListCount = 0 Then lstEncounterTableCommon.AddItem "No encounter"
  If lstEncounterTableUncommon.ListCount = 0 Then lstEncounterTableUncommon.AddItem "No encounter"
  If lstEncounterTableRare.ListCount = 0 Then lstEncounterTableRare.AddItem "No encounter"
  If lstEncounterTableVeryRare.ListCount = 0 Then lstEncounterTableVeryRare.AddItem "No encounter"
  If lstEncounterTableUnique.ListCount = 0 Then lstEncounterTableUnique.AddItem "No encounter"
  
End Sub


Private Function CreateHabitatQueryString() As String
  Dim i As Long
  Dim strQuery
  
  strQuery = ""
  If TreeView1.Nodes.Count > 0 Then
    For i = 1 To TreeView1.Nodes.Count
      If TreeView1.Nodes(i).Checked = True Then
        Select Case TreeView1.Nodes(i).Text
          Case "Cold Aquatic"
            strQuery = strQuery & " OR ColdAquatic = TRUE"
          Case "Cold Desert"
            strQuery = strQuery & " OR ColdDesert = TRUE"
          Case "Cold Forest"
            strQuery = strQuery & " OR ColdForest = TRUE"
          Case "Cold Hills"
            strQuery = strQuery & " OR ColdHills = TRUE"
          Case "Cold Marsh"
            strQuery = strQuery & " OR ColdMarsh = TRUE"
          Case "Cold Mountain"
            strQuery = strQuery & " OR ColdMountain = TRUE"
          Case "Cold Plains"
            strQuery = strQuery & " OR ColdPlain = TRUE"
          
          Case "Temperate Aquatic"
            strQuery = strQuery & " OR TemperateAquatic = TRUE"
          Case "Temperate Desert"
            strQuery = strQuery & " OR TemperateDesert = TRUE"
          Case "Temperate Forest"
            strQuery = strQuery & " OR TemperateForest = TRUE"
          Case "Temperate Hills"
            strQuery = strQuery & " OR TemperateHills = TRUE"
          Case "Temperate Marsh"
            strQuery = strQuery & " OR TemperateMarsh = TRUE"
          Case "Temperate Mountain"
            strQuery = strQuery & " OR TemperateMountain = TRUE"
          Case "Temperate Plain"
            strQuery = strQuery & " OR TemperatePlain = TRUE"
          
          Case "Warm Aquatic"
            strQuery = strQuery & " OR WarmAquatic = TRUE"
          Case "Warm Desert"
            strQuery = strQuery & " OR WarmDesert = TRUE"
          Case "Warm Forest"
            strQuery = strQuery & " OR WarmForest = TRUE"
          Case "Warm Hills"
            strQuery = strQuery & " OR WarmHills = TRUE"
          Case "Warm Marsh"
            strQuery = strQuery & " OR WarmMarsh = TRUE"
          Case "Warm Mountain"
            strQuery = strQuery & " OR WarmMountain = TRUE"
          Case "Warm Plain"
            strQuery = strQuery & " OR WarmPlain = TRUE"
          
          
          Case "Urban"
            strQuery = strQuery & " OR Urban = TRUE"
          Case "Urban Sewers"
            strQuery = strQuery & " OR UrbanSewers = TRUE"
          Case "Ruins"
            strQuery = strQuery & " OR Ruins = TRUE"
          
          Case "Rocky Badlands"
            strQuery = strQuery & " OR RockyBadlands = TRUE"
          Case "Salt Flat"
            strQuery = strQuery & " OR SaltFlats = TRUE"
          Case "Sandy Wastes"
            strQuery = strQuery & " OR SandyWastes = TRUE"
          Case "Silt Basin"
            strQuery = strQuery & " OR SiltBasin = TRUE"
          Case "Stoney Barrens"
            strQuery = strQuery & " OR StoneyBarrens = TRUE"
          Case "Mudflat"
            strQuery = strQuery & " OR Mudflats = TRUE"
          Case "Volcanic Regions"
            strQuery = strQuery & " OR Volcanic = TRUE"
          
          Case "Realm of Dreams"
            strQuery = strQuery & " OR Dreams = TRUE"
          
          Case "Sky"
            strQuery = strQuery & " OR Sky = TRUE"
          Case "Wildspace"
            strQuery = strQuery & " OR Wildspace = TRUE"
          Case "Phlogiston"
            strQuery = strQuery & " OR Phlogiston = TRUE"
          
          
          Case "Underground"
            strQuery = strQuery & " OR Underground = TRUE"
          Case "Upperdark"
            strQuery = strQuery & " OR Upperdark = TRUE"
          Case "Middledark"
            strQuery = strQuery & " OR Middledark = TRUE"
          Case "Lowerdark"
            strQuery = strQuery & " OR Lowerdark = TRUE"
          
          Case "Astral Plane"
            strQuery = strQuery & " OR Astral = TRUE"
          Case "Ethereal Plane"
            strQuery = strQuery & " OR Ethereal = TRUE"
          Case "Plane of Shadow"
            strQuery = strQuery & " OR Shadow = TRUE"
          
          
          Case "Air"
            strQuery = strQuery & " OR Air = TRUE"
          Case "Earth"
            strQuery = strQuery & " OR Earth = TRUE"
          Case "Fire"
            strQuery = strQuery & " OR Fire = TRUE"
          Case "Water"
            strQuery = strQuery & " OR Water = TRUE"
          
          Case "Ice"
            strQuery = strQuery & " OR Ice = TRUE"
          Case "Magma"
            strQuery = strQuery & " OR Magma = TRUE"
          Case "Ooze"
            strQuery = strQuery & " OR Ooze = TRUE"
          Case "Smoke"
            strQuery = strQuery & " OR Smoke = TRUE"
          
          Case "Positive Energy Plane"
            strQuery = strQuery & " OR Positive = TRUE"
          Case "Negative Energy Plane"
            strQuery = strQuery & " OR Negative = TRUE"
          Case "Temporal Energy Plane"
            strQuery = strQuery & " OR Temporal = TRUE"
          
          Case "Ash"
            strQuery = strQuery & " OR Ash = TRUE"
          Case "Dust"
            strQuery = strQuery & " OR Dust = TRUE"
          Case "Lightning"
            strQuery = strQuery & " OR Lightning = TRUE"
          Case "Minerals"
            strQuery = strQuery & " OR Mineral = TRUE"
          Case "Radiance"
            strQuery = strQuery & " OR Radiance = TRUE"
          Case "Salt"
            strQuery = strQuery & " OR Salt = TRUE"
          Case "Steam"
            strQuery = strQuery & " OR Steam = TRUE"
          Case "Vacuum"
            strQuery = strQuery & " OR Vacuum = TRUE"
          
          
          Case "A lawful-evil aligned plane"
            strQuery = strQuery & " OR LEPlane = TRUE"
          Case "A true-neutral aligned plane"
            strQuery = strQuery & " OR TNPlane = TRUE"
          Case "A neutral-good aligned plane"
            strQuery = strQuery & " OR NGPlane = TRUE"
          Case "A chaotic-neutral aligned plane"
            strQuery = strQuery & " OR CNPlane = TRUE"
          Case "A lawful-neutral aligned plane"
            strQuery = strQuery & " OR LNPlane = TRUE"
          Case "A lawful-good aligned plane"
            strQuery = strQuery & " OR LGPlane = TRUE"
          Case "A chaotic-good aligned plane"
            strQuery = strQuery & " OR CGPlane = TRUE"
          Case "A chaotic-evil aligned plane"
            strQuery = strQuery & " OR CEPlane = TRUE"
          Case "A neutral-evil aligned plane"
            strQuery = strQuery & " OR NEPlane = TRUE"
          
          Case "The Far Realm"
            strQuery = strQuery & " OR FarRealm = TRUE"
            
          
  
          
        End Select
      End If
    Next i
    If LEFT(strQuery, 2) Like " O" Then
      strQuery = " AND (" & Right(strQuery, Len(strQuery) - 3) & ")"
    ElseIf LEFT(strQuery, 2) Like " A" Then
      strQuery = " AND (" & Right(strQuery, Len(strQuery) - 4) & ")"
    End If
  End If
  CreateHabitatQueryString = strQuery
End Function

Private Sub cmdRollEncounter_Click()
  Dim val As Byte
  
  ' Uncomment the loop to force encounters
  'Do
    val = Int(Rnd * 100) + 1
'    If Not (lstEncounterTableCommon.List(0) Like "No encounter") And val < 65 Then Exit Do
'    If Not (lstEncounterTableUncommon.List(0) Like "No encounter") And val >= 65 And val <= 85 Then Exit Do
'    If Not (lstEncounterTableRare.List(0) Like "No encounter") And val >= 85 And val <= 96 Then Exit Do
'    If Not (lstEncounterTableVeryRare.List(0) Like "No encounter") And val > 96 Then Exit Do
'    DoEvents
'  Loop
  
  Select Case val
    Case Is < 65
      val = Int(Rnd * lstEncounterTableCommon.ListCount)
      MsgBox lstEncounterTableCommon.List(val)
    Case 65 To 85
      val = Int(Rnd * lstEncounterTableUncommon.ListCount)
      MsgBox lstEncounterTableUncommon.List(val)
    Case 86 To 96
      val = Int(Rnd * lstEncounterTableRare.ListCount)
      MsgBox lstEncounterTableRare.List(val)
    Case 97 To 100
      val = Int(Rnd * lstEncounterTableVeryRare.ListCount)
      MsgBox lstEncounterTableVeryRare.List(val)
  End Select
End Sub


Private Sub Form_Load()
  
  If gcblnOGL = True Then
    chkOGL.value = Checked
    chkOGL.Visible = False
  End If
  
  lstEncounterTableCommon.Clear
  lstEncounterTableUncommon.Clear
  lstEncounterTableRare.Clear
  lstEncounterTableVeryRare.Clear
  PopulateTreeNodes
End Sub

Private Sub PopulateTreeNodes()
  TreeView1.Nodes.Clear
  With TreeView1.Nodes
    .Add , , "Cold", "Cold Climates"
    .Add "Cold", tvwChild, "Cold Aquatic", "Cold Aquatic"
    .Add "Cold", tvwChild, "Cold Desert", "Cold Desert"
    .Add "Cold", tvwChild, "Cold Forest", "Cold Forest"
    .Add "Cold", tvwChild, "Cold Hills", "Cold Hills"
    .Add "Cold", tvwChild, "Cold Marsh", "Cold Marsh"
    .Add "Cold", tvwChild, "Cold Mountain", "Cold Mountain"
    .Add "Cold", tvwChild, "Cold Plains", "Cold Plains"

    .Add , , "Temperate", "Temperate Climates"
    .Add "Temperate", tvwChild, "Temperate Aquatic", "Temperate Aquatic"
    .Add "Temperate", tvwChild, "Temperate Desert", "Temperate Desert"
    .Add "Temperate", tvwChild, "Temperate Forest", "Temperate Forest"
    .Add "Temperate", tvwChild, "Temperate Hills", "Temperate Hills"
    .Add "Temperate", tvwChild, "Temperate Marsh", "Temperate Marsh"
    .Add "Temperate", tvwChild, "Temperate Mountain", "Temperate Mountain"
    .Add "Temperate", tvwChild, "Temperate Plain", "Temperate Plain"
    
    .Add , , "Warm", "Warm Climates"
    .Add "Warm", tvwChild, "Warm Aquatic", "Warm Aquatic"
    .Add "Warm", tvwChild, "Warm Desert", "Warm Desert"
    .Add "Warm", tvwChild, "Warm Forest", "Warm Forest"
    .Add "Warm", tvwChild, "Warm Hills", "Warm Hills"
    .Add "Warm", tvwChild, "Warm Marsh", "Warm Marsh"
    .Add "Warm", tvwChild, "Warm Mountain", "Warm Mountain"
    .Add "Warm", tvwChild, "Warm Plain", "Warm Plain"
    
    
    If chkOGL.value = Unchecked Then
      .Add , , "Underground Habitats", "Underground Habitats"
      .Add "Underground Habitats", tvwChild, "Underground", "Underground"
      .Add "Underground Habitats", tvwChild, "Upperdark", "Upperdark"
      .Add "Underground Habitats", tvwChild, "Middledark", "Middledark"
      .Add "Underground Habitats", tvwChild, "Lowerdark", "Lowerdark"
    End If

    .Add , , "Civilized Habitats", "Civilized Habitats"
    .Add "Civilized Habitats", tvwChild, "Urban", "Urban"
    .Add "Civilized Habitats", tvwChild, "UrbanSewers", "Urban Sewers"
    .Add "Civilized Habitats", tvwChild, "Ruins", "Ruins"

    .Add "Warm Desert", tvwChild, "Stoney Barrens", "Stoney Barrens"
    .Add "Warm Desert", tvwChild, "Rocky Badlands", "Rocky Badlands"
    .Add "Warm Desert", tvwChild, "Sandy Wastes", "Sandy Wastes"
    .Add "Warm Desert", tvwChild, "Salt Flat", "Salt Flat"
    .Add "Warm Desert", tvwChild, "Mudflat", "Mudflat"
    .Add "Warm Desert", tvwChild, "Silt Basin", "Silt Basin"

    .Add , , "Unusual Habitats", "Unusual Habitats"
    .Add "Unusual Habitats", tvwChild, "Sky", "Sky"
    .Add "Unusual Habitats", tvwChild, "Volcanic Regions", "Volcanic Regions"
    
    If chkOGL.value = Unchecked Then
      .Add "Unusual Habitats", tvwChild, "Wildspace", "Wildspace"
      .Add "Unusual Habitats", tvwChild, "Phlogiston", "Phlogiston"
    Else
      .Add "Unusual Habitats", tvwChild, "Underground", "Underground"
    End If
    
    .Add , , "Extraplanar", "Extraplanar"
    .Add "Extraplanar", tvwChild, "Transitive Planes", "Transitive Planes"
    .Add "Transitive Planes", tvwChild, "Ethereal Plane", "Ethereal Plane"
    .Add "Transitive Planes", tvwChild, "Astral Plane", "Astral Plane"
    .Add "Transitive Planes", tvwChild, "Plane of Shadow", "Plane of Shadow"
    
    .Add "Extraplanar", tvwChild, "Inner Planes", "The Inner Planes"
    If chkOGL.value = Checked Then
      .Add "Inner Planes", tvwChild, "Air", "Air"
      .Add "Inner Planes", tvwChild, "Earth", "Earth"
      .Add "Inner Planes", tvwChild, "Fire", "Fire"
      .Add "Inner Planes", tvwChild, "Water", "Water"
      .Add "Inner Planes", tvwChild, "Positive", "Positive Energy Plane"
      .Add "Inner Planes", tvwChild, "Negative", "Negative Energy Plane"
    Else
      .Add "Inner Planes", tvwChild, "Elemental Planes", "Elemental Planes"
      .Add "Elemental Planes", tvwChild, "Air", "Air"
      .Add "Elemental Planes", tvwChild, "Earth", "Earth"
      .Add "Elemental Planes", tvwChild, "Fire", "Fire"
      .Add "Elemental Planes", tvwChild, "Water", "Water"
      
      .Add "Inner Planes", tvwChild, "Para-elemental Planes", "Para-elemental Planes"
      .Add "Para-elemental Planes", tvwChild, "Ice", "Ice"
      .Add "Para-elemental Planes", tvwChild, "Magma", "Magma"
      .Add "Para-elemental Planes", tvwChild, "Ooze", "Ooze"
      .Add "Para-elemental Planes", tvwChild, "Smoke", "Smoke"
    
      .Add "Inner Planes", tvwChild, "Energy Planes", "Energy Planes"
      .Add "Energy Planes", tvwChild, "Positive", "Positive Energy Plane"
      .Add "Energy Planes", tvwChild, "Negative", "Negative Energy Plane"
      
      .Add "Inner Planes", tvwChild, "PQuasi-elemental Planes", "Positive Quasi-elemental Planes"
      .Add "PQuasi-elemental Planes", tvwChild, "Radiance", "Radiance"
      .Add "PQuasi-elemental Planes", tvwChild, "Lightning", "Lightning"
      .Add "PQuasi-elemental Planes", tvwChild, "Steam", "Steam"
      .Add "PQuasi-elemental Planes", tvwChild, "Minerals", "Minerals"
      
      .Add "Inner Planes", tvwChild, "NQuasi-elemental Planes", "Negative Quasi-elemental Planes"
      .Add "NQuasi-elemental Planes", tvwChild, "Ash", "Ash"
      .Add "NQuasi-elemental Planes", tvwChild, "Dust", "Dust"
      .Add "NQuasi-elemental Planes", tvwChild, "Salt", "Salt"
      .Add "NQuasi-elemental Planes", tvwChild, "Vacuum", "Vacuum"
    End If
    
    .Add "Extraplanar", tvwChild, "Outer Planes", "The Outer Planes"
    
    .Add "Outer Planes", tvwChild, "Heaven", "A lawful-good aligned plane"
    .Add "Outer Planes", tvwChild, "Elysium", "A neutral-good aligned plane"
    .Add "Outer Planes", tvwChild, "Olympus", "A chaotic-good aligned plane"
    .Add "Outer Planes", tvwChild, "Mechanus", "A lawful-neutral aligned plane"
    .Add "Outer Planes", tvwChild, "Outlands", "A true-neutral aligned plane"
    .Add "Outer Planes", tvwChild, "Limbo", "A chaotic-neutral aligned plane"
    .Add "Outer Planes", tvwChild, "Baator", "A lawful-evil aligned plane"
    .Add "Outer Planes", tvwChild, "Hades", "A neutral-evil aligned plane"
    .Add "Outer Planes", tvwChild, "The Abyss", "A chaotic-evil aligned plane"
    
    If chkOGL.value = Unchecked Then
      .Add "Extraplanar", tvwChild, "Demiplanes", "Demiplanes"
    End If

    
    If chkOGL.value = Unchecked Then
      .Add "Extraplanar", tvwChild, "Variant Planes", "Variant Planes"
      .Add "Variant Planes", tvwChild, "Realm of Dreams", "Realm of Dreams"
      .Add "Energy Planes", tvwChild, "Temporal Prime", "Temporal Energy Plane"
      .Add "Variant Planes", tvwChild, "The Far Realm", "The Far Realm"
    Else
      .Add "Extraplanar", tvwChild, "The Far Realm", "The Far Realm"
    End If


  End With

End Sub

Private Sub lstEncounterTableUncommon_Click()
  If lstEncounterTableUncommon.ListIndex >= 0 Then _
    lstEncounterTableUncommon.ToolTipText = lstEncounterTableUncommon.List( _
    lstEncounterTableUncommon.ListIndex)
End Sub

Private Sub lstEncounterTableCommon_Click()
  If lstEncounterTableCommon.ListIndex >= 0 Then _
    lstEncounterTableCommon.ToolTipText = lstEncounterTableCommon.List( _
      lstEncounterTableCommon.ListIndex)
End Sub

Private Sub lstEncounterTableRare_Click()
  If lstEncounterTableRare.ListIndex >= 0 Then _
    lstEncounterTableRare.ToolTipText = lstEncounterTableRare.List( _
      lstEncounterTableRare.ListIndex)
End Sub

Private Sub lstEncounterTableVeryRare_Click()
  If lstEncounterTableVeryRare.ListIndex >= 0 Then _
    lstEncounterTableVeryRare.ToolTipText = lstEncounterTableVeryRare.List( _
      lstEncounterTableVeryRare.ListIndex)
End Sub

Private Sub lstEncounterTableUnique_Click()
  If lstEncounterTableUnique.ListIndex >= 0 Then _
    lstEncounterTableUnique.ToolTipText = lstEncounterTableUnique.List( _
      lstEncounterTableUnique.ListIndex)
End Sub


Private Sub optQueryMode_Click(Index As Integer)
  If optQueryMode(0).value = True Then
    Label1.Caption = "EL:"
  Else
    Label1.Caption = "CR:"
  End If
  'cmdGetTable_Click
End Sub

Private Sub TreeView1_NodeCheck(ByVal Node As MSComctlLib.Node)
  CheckSubNodes Node
End Sub

Private Sub CheckSubNodes(Node As MSComctlLib.Node)
  Dim i As Long
  Dim nodX As MSComctlLib.Node
  
  If Node.Children > 0 Then
    Set nodX = Node.Child.FirstSibling
    
    For i = 1 To Node.Children
      If nodX.Checked <> Node.Checked Then nodX.Checked = Node.Checked
      If nodX.Children > 0 Then CheckSubNodes nodX
      Set nodX = nodX.Next
    Next
  End If

End Sub
