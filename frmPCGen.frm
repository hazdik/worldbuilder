VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPCGen 
   Caption         =   "PC Creation"
   ClientHeight    =   7560
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10305
   LinkTopic       =   "Form1"
   ScaleHeight     =   7560
   ScaleWidth      =   10305
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   7305
      Width           =   10305
      _ExtentX        =   18177
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab tabPC 
      Height          =   7275
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10275
      _ExtentX        =   18124
      _ExtentY        =   12832
      _Version        =   393216
      Style           =   1
      Tabs            =   10
      TabsPerRow      =   10
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Summary"
      TabPicture(0)   =   "frmPCGen.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblSummaryHP"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label15"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblStatTotal"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label8"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label7"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label6"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label5"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label4"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label3"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label2"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "cmdRollHP"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Frame1"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdSummaryLevelsRemove"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "cmdSummaryLevelsAdd"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtSummaryLevelsAddRemove"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "cboClass"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cboSummaryRace"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "cboSummaryAlignment"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "txtPlayerName"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtSummaryTabLabel"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "cmdGenerateRandomName"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "txtSummaryCharacterName"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).ControlCount=   22
      TabCaption(1)   =   "Race"
      TabPicture(1)   =   "frmPCGen.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Class"
      TabPicture(2)   =   "frmPCGen.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label1"
      Tab(2).Control(1)=   "cboClassAvailable"
      Tab(2).Control(2)=   "cmdAddClassLevel"
      Tab(2).Control(3)=   "tvClass"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Skills"
      TabPicture(3)   =   "frmPCGen.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      TabCaption(4)   =   "Feats"
      TabPicture(4)   =   "frmPCGen.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).ControlCount=   0
      TabCaption(5)   =   "Domains"
      TabPicture(5)   =   "frmPCGen.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      TabCaption(6)   =   "Spells"
      TabPicture(6)   =   "frmPCGen.frx":00A8
      Tab(6).ControlEnabled=   0   'False
      Tab(6).ControlCount=   0
      TabCaption(7)   =   "Inventory"
      TabPicture(7)   =   "frmPCGen.frx":00C4
      Tab(7).ControlEnabled=   0   'False
      Tab(7).ControlCount=   0
      TabCaption(8)   =   "Description"
      TabPicture(8)   =   "frmPCGen.frx":00E0
      Tab(8).ControlEnabled=   0   'False
      Tab(8).ControlCount=   0
      TabCaption(9)   =   "Preview"
      TabPicture(9)   =   "frmPCGen.frx":00FC
      Tab(9).ControlEnabled=   0   'False
      Tab(9).ControlCount=   0
      Begin VB.ComboBox cboClassAvailable 
         Height          =   315
         Left            =   -72420
         Style           =   2  'Dropdown List
         TabIndex        =   57
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton cmdAddClassLevel 
         Caption         =   ">"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   -71280
         TabIndex        =   56
         Top             =   540
         Width           =   675
      End
      Begin VB.TextBox txtSummaryCharacterName 
         Height          =   285
         Left            =   1440
         TabIndex        =   54
         Text            =   "New1"
         Top             =   540
         Width           =   2775
      End
      Begin VB.CommandButton cmdGenerateRandomName 
         Caption         =   "Generate Random Name"
         Height          =   375
         Left            =   1440
         TabIndex        =   53
         Top             =   900
         Width           =   2775
      End
      Begin VB.TextBox txtSummaryTabLabel 
         Height          =   285
         Left            =   1440
         TabIndex        =   52
         Top             =   1440
         Width           =   2775
      End
      Begin VB.TextBox txtPlayerName 
         Height          =   285
         Left            =   1440
         TabIndex        =   51
         Top             =   1800
         Width           =   2775
      End
      Begin VB.ComboBox cboSummaryAlignment 
         Height          =   315
         ItemData        =   "frmPCGen.frx":0118
         Left            =   6120
         List            =   "frmPCGen.frx":013D
         Style           =   2  'Dropdown List
         TabIndex        =   50
         Top             =   540
         Width           =   2295
      End
      Begin VB.ComboBox cboSummaryRace 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmPCGen.frx":01D1
         Left            =   6120
         List            =   "frmPCGen.frx":01D3
         Style           =   2  'Dropdown List
         TabIndex        =   49
         Top             =   960
         Width           =   2295
      End
      Begin VB.ComboBox cboClass 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmPCGen.frx":01D5
         Left            =   6120
         List            =   "frmPCGen.frx":01D7
         Style           =   2  'Dropdown List
         TabIndex        =   48
         Top             =   1320
         Width           =   2295
      End
      Begin VB.TextBox txtSummaryLevelsAddRemove 
         Height          =   285
         Left            =   6120
         TabIndex        =   47
         Text            =   "1"
         Top             =   1800
         Width           =   435
      End
      Begin VB.CommandButton cmdSummaryLevelsAdd 
         Caption         =   "+"
         Enabled         =   0   'False
         Height          =   315
         Left            =   6600
         TabIndex        =   46
         Top             =   1800
         Width           =   375
      End
      Begin VB.CommandButton cmdSummaryLevelsRemove 
         Caption         =   "-"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7020
         TabIndex        =   45
         Top             =   1800
         Width           =   375
      End
      Begin VB.Frame Frame1 
         Caption         =   "Ability Scores"
         Height          =   2895
         Left            =   180
         TabIndex        =   3
         Top             =   2280
         Width           =   6555
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   0
            Left            =   1320
            TabIndex        =   9
            Text            =   "10"
            Top             =   720
            Width           =   975
         End
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   1
            Left            =   1320
            TabIndex        =   8
            Text            =   "10"
            Top             =   1020
            Width           =   975
         End
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   2
            Left            =   1320
            TabIndex        =   7
            Text            =   "10"
            Top             =   1320
            Width           =   975
         End
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   3
            Left            =   1320
            TabIndex        =   6
            Text            =   "10"
            Top             =   1620
            Width           =   975
         End
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   4
            Left            =   1320
            TabIndex        =   5
            Text            =   "10"
            Top             =   1920
            Width           =   975
         End
         Begin VB.TextBox txtAbilityScore 
            Alignment       =   2  'Center
            Height          =   285
            Index           =   5
            Left            =   1320
            TabIndex        =   4
            Text            =   "10"
            Top             =   2220
            Width           =   975
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Strength"
            Height          =   195
            Index           =   0
            Left            =   180
            TabIndex        =   44
            Top             =   765
            Width           =   600
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Dexterity"
            Height          =   195
            Index           =   1
            Left            =   180
            TabIndex        =   43
            Top             =   1065
            Width           =   615
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Constitution"
            Height          =   195
            Index           =   2
            Left            =   180
            TabIndex        =   42
            Top             =   1365
            Width           =   825
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Intelligence"
            Height          =   195
            Index           =   3
            Left            =   180
            TabIndex        =   41
            Top             =   1665
            Width           =   810
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Wisdom"
            Height          =   195
            Index           =   4
            Left            =   180
            TabIndex        =   40
            Top             =   1965
            Width           =   570
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Charisma"
            Height          =   195
            Index           =   5
            Left            =   180
            TabIndex        =   39
            Top             =   2265
            Width           =   645
         End
         Begin VB.Label Label10 
            Alignment       =   2  'Center
            Caption         =   "Score"
            Height          =   195
            Left            =   1440
            TabIndex        =   38
            Top             =   420
            Width           =   735
         End
         Begin VB.Label Label11 
            Alignment       =   2  'Center
            Caption         =   "Race Adj"
            Height          =   195
            Left            =   2460
            TabIndex        =   37
            Top             =   420
            Width           =   735
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   0
            Left            =   2340
            TabIndex        =   36
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   1
            Left            =   2340
            TabIndex        =   35
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   2
            Left            =   2340
            TabIndex        =   34
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   3
            Left            =   2340
            TabIndex        =   33
            Top             =   1620
            Width           =   975
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   4
            Left            =   2340
            TabIndex        =   32
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblRaceAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   5
            Left            =   2340
            TabIndex        =   31
            Top             =   2220
            Width           =   975
         End
         Begin VB.Label Label12 
            Alignment       =   2  'Center
            Caption         =   "Other Adj"
            Height          =   195
            Left            =   3480
            TabIndex        =   30
            Top             =   420
            Width           =   735
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   0
            Left            =   3360
            TabIndex        =   29
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   1
            Left            =   3360
            TabIndex        =   28
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   2
            Left            =   3360
            TabIndex        =   27
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   3
            Left            =   3360
            TabIndex        =   26
            Top             =   1620
            Width           =   975
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   4
            Left            =   3360
            TabIndex        =   25
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblOtherAdj 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   5
            Left            =   3360
            TabIndex        =   24
            Top             =   2220
            Width           =   975
         End
         Begin VB.Label Label13 
            Alignment       =   2  'Center
            Caption         =   "Total"
            Height          =   195
            Left            =   4500
            TabIndex        =   23
            Top             =   420
            Width           =   735
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   0
            Left            =   4380
            TabIndex        =   22
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   1
            Left            =   4380
            TabIndex        =   21
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   2
            Left            =   4380
            TabIndex        =   20
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   3
            Left            =   4380
            TabIndex        =   19
            Top             =   1620
            Width           =   975
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   4
            Left            =   4380
            TabIndex        =   18
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblAbilityTotal 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   5
            Left            =   4380
            TabIndex        =   17
            Top             =   2220
            Width           =   975
         End
         Begin VB.Label Label14 
            Alignment       =   2  'Center
            Caption         =   "Mod"
            Height          =   195
            Left            =   5520
            TabIndex        =   16
            Top             =   420
            Width           =   735
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   0
            Left            =   5400
            TabIndex        =   15
            Top             =   720
            Width           =   975
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   1
            Left            =   5400
            TabIndex        =   14
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   2
            Left            =   5400
            TabIndex        =   13
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   3
            Left            =   5400
            TabIndex        =   12
            Top             =   1620
            Width           =   975
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   4
            Left            =   5400
            TabIndex        =   11
            Top             =   1920
            Width           =   975
         End
         Begin VB.Label lblAbilityMod 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            Caption         =   "0"
            Height          =   315
            Index           =   5
            Left            =   5400
            TabIndex        =   10
            Top             =   2220
            Width           =   975
         End
      End
      Begin VB.CommandButton cmdRollHP 
         Caption         =   "HP"
         Height          =   375
         Left            =   4980
         TabIndex        =   2
         Top             =   5280
         Width           =   675
      End
      Begin MSComctlLib.TreeView tvClass 
         Height          =   5595
         Left            =   -74880
         TabIndex        =   55
         Top             =   1140
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   9869
         _Version        =   393217
         Indentation     =   706
         LineStyle       =   1
         Style           =   7
         Appearance      =   1
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Available"
         Height          =   195
         Left            =   -73140
         TabIndex        =   68
         Top             =   660
         Width           =   645
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Character Name:"
         Height          =   195
         Left            =   180
         TabIndex        =   67
         Top             =   600
         Width           =   1200
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Tab Label:"
         Height          =   195
         Left            =   600
         TabIndex        =   66
         Top             =   1500
         Width           =   765
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Player Name:"
         Height          =   195
         Left            =   420
         TabIndex        =   65
         Top             =   1860
         Width           =   945
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Alignment:"
         Height          =   195
         Left            =   5340
         TabIndex        =   64
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Race:"
         Height          =   195
         Left            =   5640
         TabIndex        =   63
         Top             =   1020
         Width           =   435
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Class:"
         Height          =   195
         Left            =   5655
         TabIndex        =   62
         Top             =   1380
         Width           =   420
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Levels to Add/Remove:"
         Height          =   195
         Left            =   4380
         TabIndex        =   61
         Top             =   1860
         Width           =   1695
      End
      Begin VB.Label lblStatTotal 
         AutoSize        =   -1  'True
         Caption         =   "Stat Total: 60"
         Height          =   195
         Left            =   2100
         TabIndex        =   60
         Top             =   5400
         Width           =   960
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Modifier Total: 0"
         Height          =   195
         Left            =   3360
         TabIndex        =   59
         Top             =   5400
         Width           =   1140
      End
      Begin VB.Label lblSummaryHP 
         Caption         =   "0"
         Height          =   195
         Left            =   5820
         TabIndex        =   58
         Top             =   5340
         Width           =   390
      End
   End
End
Attribute VB_Name = "frmPCGen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SSTab2_DblClick()

End Sub
