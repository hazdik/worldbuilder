VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmRandomCity 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Random City Generator"
   ClientHeight    =   9915
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11385
   Icon            =   "frmRandomCity.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9915
   ScaleWidth      =   11385
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar pbDistrict 
      Height          =   9195
      Left            =   10680
      TabIndex        =   48
      Top             =   540
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   16219
      _Version        =   393216
      Appearance      =   1
      Orientation     =   1
      Scrolling       =   1
   End
   Begin VB.ComboBox cboRegion 
      Height          =   315
      Left            =   8760
      Style           =   2  'Dropdown List
      TabIndex        =   47
      Top             =   60
      Width           =   1635
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3420
      Top             =   8160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CheckBox chkLandLocked 
      Caption         =   "Land-locked?"
      Height          =   195
      Left            =   3120
      TabIndex        =   46
      Top             =   60
      Width           =   1335
   End
   Begin VB.TextBox txtNDistricts 
      Height          =   285
      Left            =   1560
      TabIndex        =   30
      Text            =   "20"
      Top             =   180
      Width           =   555
   End
   Begin VB.TextBox txtMinLvl 
      Height          =   285
      Left            =   9420
      TabIndex        =   27
      Text            =   "1"
      Top             =   7920
      Width           =   795
   End
   Begin VB.TextBox txtMaxLvl 
      Height          =   285
      Left            =   9420
      TabIndex        =   20
      Text            =   "11"
      Top             =   9360
      Width           =   795
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Export"
      Height          =   375
      Left            =   6360
      TabIndex        =   18
      Top             =   9360
      Width           =   1575
   End
   Begin VB.Frame Frame2 
      Caption         =   "Read to Players"
      Height          =   1635
      Left            =   5100
      TabIndex        =   8
      Top             =   6180
      Width           =   5475
      Begin VB.Label lblFirstImpression 
         Height          =   1215
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   5220
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Description"
      Height          =   1635
      Left            =   120
      TabIndex        =   6
      Top             =   6180
      Width           =   4935
      Begin VB.Label lblDescription 
         Height          =   1275
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   4680
      End
   End
   Begin VB.CommandButton cmdGenerate 
      Caption         =   "Generate"
      Height          =   315
      Left            =   2220
      TabIndex        =   5
      Top             =   180
      Width           =   855
   End
   Begin VB.ListBox lstPCs 
      Height          =   5520
      Left            =   6480
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   600
      Width           =   4035
   End
   Begin VB.ListBox lstBuildings 
      Height          =   5520
      Left            =   3180
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   600
      Width           =   3195
   End
   Begin VB.ListBox lstDistrictList 
      Height          =   5520
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   2955
   End
   Begin MSComctlLib.ProgressBar pbMisc 
      Height          =   9195
      Left            =   11040
      TabIndex        =   49
      Top             =   540
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   16219
      _Version        =   393216
      Appearance      =   1
      Orientation     =   1
      Scrolling       =   1
   End
   Begin VB.Label lblAlignment 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   4680
      TabIndex        =   45
      Top             =   9480
      Width           =   1575
   End
   Begin VB.Label Label21 
      AutoSize        =   -1  'True
      Caption         =   "Alignment:"
      Height          =   195
      Left            =   3480
      TabIndex        =   44
      Top             =   9480
      Width           =   735
   End
   Begin VB.Label lblPowerCenter 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   4680
      TabIndex        =   43
      Top             =   9120
      Width           =   1575
   End
   Begin VB.Label Label18 
      AutoSize        =   -1  'True
      Caption         =   "Power Center:"
      Height          =   195
      Left            =   3480
      TabIndex        =   42
      Top             =   9120
      Width           =   1005
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      Caption         =   "sq. mi."
      Height          =   195
      Left            =   6360
      TabIndex        =   41
      Top             =   8790
      Width           =   450
   End
   Begin VB.Label lblLandArea 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "1"
      Height          =   255
      Left            =   5520
      TabIndex        =   40
      Top             =   8760
      Width           =   795
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "Area:"
      Height          =   195
      Left            =   5100
      TabIndex        =   39
      Top             =   8790
      Width           =   375
   End
   Begin VB.Label lblTotalPop 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "10000"
      Height          =   255
      Left            =   5520
      TabIndex        =   38
      Top             =   8400
      Width           =   1395
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      Caption         =   "Total Population:"
      Height          =   195
      Left            =   4260
      TabIndex        =   37
      Top             =   8400
      Width           =   1200
   End
   Begin VB.Label lblNUpper 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2"
      Height          =   255
      Left            =   1680
      TabIndex        =   36
      Top             =   9540
      Width           =   1455
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      Caption         =   "Upper Class Districts:"
      Height          =   195
      Left            =   120
      TabIndex        =   35
      Top             =   9540
      Width           =   1500
   End
   Begin VB.Label lblNMiddle 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "6"
      Height          =   255
      Left            =   1680
      TabIndex        =   34
      Top             =   9240
      Width           =   1455
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "Middle Class Districts:"
      Height          =   195
      Left            =   120
      TabIndex        =   33
      Top             =   9240
      Width           =   1530
   End
   Begin VB.Label lblNLower 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "12"
      Height          =   255
      Left            =   1680
      TabIndex        =   32
      Top             =   8940
      Width           =   1455
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "Lower Class Districts:"
      Height          =   195
      Left            =   120
      TabIndex        =   31
      Top             =   8940
      Width           =   1500
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      Caption         =   "Number of districts:"
      Height          =   195
      Left            =   180
      TabIndex        =   29
      Top             =   180
      Width           =   1350
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "Minimum level = "
      Height          =   195
      Left            =   8100
      TabIndex        =   28
      Top             =   7920
      Width           =   1170
   End
   Begin VB.Label lblQ3 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "7"
      Height          =   315
      Left            =   9420
      TabIndex        =   26
      Top             =   9000
      Width           =   795
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Third quartile level:"
      Height          =   195
      Left            =   8040
      TabIndex        =   25
      Top             =   9060
      Width           =   1335
   End
   Begin VB.Label lblQ2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "4"
      Height          =   315
      Left            =   9420
      TabIndex        =   24
      Top             =   8640
      Width           =   795
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "Median level:"
      Height          =   195
      Left            =   8460
      TabIndex        =   23
      Top             =   8700
      Width           =   945
   End
   Begin VB.Label lblQ1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2"
      Height          =   315
      Left            =   9420
      TabIndex        =   22
      Top             =   8280
      Width           =   795
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "First quartile level:"
      Height          =   195
      Left            =   8100
      TabIndex        =   21
      Top             =   8340
      Width           =   1260
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Maximum level = "
      Height          =   195
      Left            =   8100
      TabIndex        =   19
      Top             =   9360
      Width           =   1215
   End
   Begin VB.Label lblSelDistSocClass 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lower"
      Height          =   255
      Left            =   6000
      TabIndex        =   17
      Top             =   7920
      Width           =   855
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Selected district social class:"
      Height          =   195
      Left            =   3960
      TabIndex        =   16
      Top             =   7920
      Width           =   2025
   End
   Begin VB.Label lblLowerGPLim 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   1680
      TabIndex        =   15
      Top             =   8520
      Width           =   1455
   End
   Begin VB.Label lblMiddleGPLim 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   1680
      TabIndex        =   14
      Top             =   8220
      Width           =   1455
   End
   Begin VB.Label lblUpperGPLim 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      Height          =   255
      Left            =   1680
      TabIndex        =   13
      Top             =   7920
      Width           =   1455
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Lower class GP limit:"
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   8520
      Width           =   1455
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Middle class GP limit:"
      Height          =   195
      Left            =   120
      TabIndex        =   11
      Top             =   8220
      Width           =   1485
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Upper class GP limit:"
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   7920
      Width           =   1455
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "People in this district:"
      Height          =   195
      Left            =   6480
      TabIndex        =   4
      Top             =   300
      Width           =   1485
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Buildings in this district:"
      Height          =   195
      Left            =   3180
      TabIndex        =   3
      Top             =   300
      Width           =   1620
   End
End
Attribute VB_Name = "frmRandomCity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' Notes
'
' Average and wealthy residential areas have 5 people per residence.
' Slums have 7.8 people per residence, but also have one inn.
' Noble Districts have 12 people per estate (including servants).


Public RUN_DEBUG As Boolean
Private db As CDB
Private City As CCityDistricts
Private mstrfilename As String
Private MinDistrictsForLevel() As Integer

Public Sub cmdGenerate_Click()
  Dim bytUpperClassDistricts As Long
  Dim bytMiddleClassDistricts As Long
  Dim bytLowerClassDistricts As Long
  
  Dim strTmpRace As String
  
  Dim bytDistrictCtr As Long
  Dim intPCCtr As Integer
  Dim intDiceRoll As Integer
  Dim strTmpGender As String
  
  cmdGenerate.Enabled = False
  cmdReport.Enabled = False
  chkLandLocked.Enabled = False
  
  lstBuildings.Clear
  lstDistrictList.Clear
  lstPCs.Clear
  
  If db Is Nothing Then Set db = New CDB
  If db.rs Is Nothing Then Set db.rs = New ADODB.Recordset
  If db.rs.State > 0 Then db.CloseDB
  
  ' Power Center
  Select Case Int(Rnd * 20) + 1
    Case 1
      lblPowerCenter.Caption = "Monstrous"
    Case 2 To 13
      lblPowerCenter.Caption = "Conventional"
    Case 14 To 18
      lblPowerCenter.Caption = "Nonstandard"
    Case 19 To 20
      lblPowerCenter.Caption = "Magical"
  End Select
  
  Select Case Int(Rnd * 100) + 1
    Case 1 To 35
      lblAlignment.Caption = "Lawful Good"
    Case 36 To 39
      lblAlignment.Caption = "Neutral Good"
    Case 40 To 41
      lblAlignment.Caption = "Chaotic Good"
    Case 42 To 61
      lblAlignment.Caption = "Lawful Neutral"
    Case 62 To 63
      lblAlignment.Caption = "Neutral"
    Case 64
      lblAlignment.Caption = "Chaotic Neutral"
    Case 65 To 90
      lblAlignment.Caption = "Lawful Evil"
    Case 91 To 98
      lblAlignment.Caption = "Neutral Evil"
    Case 99 To 100
      lblAlignment.Caption = "Chaotic Evil"
    
  End Select
  
  ' Distribution
'  rs.Open "SELECT * FROM CitySizeDistrictDistribution WHERE CitySize = '" & cboSize.Text & "'", Conn, adOpenStatic, adLockReadOnly
'  If rs.State > 0 Then
'    If rs.RecordCount > 0 Then
'      bytUpperClassDistricts = rs!Upper
'      bytMiddleClassDistricts = rs!Middle
'      bytLowerClassDistricts = rs!Lower
'
'    End If
'  End If
'  rs.Close

  bytUpperClassDistricts = CLng(lblNUpper.Caption)
  bytMiddleClassDistricts = CLng(lblNMiddle.Caption)
  bytLowerClassDistricts = CLng(lblNLower.Caption)
  
  pbDistrict.max = bytUpperClassDistricts + bytMiddleClassDistricts + bytLowerClassDistricts + 1
  pbDistrict.Value = 1
  
  ' Upper class districts
  Set City = New CCityDistricts
  
  
  lstDistrictList.Clear
  lblTotalPop.Caption = 0
  
  db.OpenDB "SELECT * FROM CityDistricts WHERE SocialClass = 'Upper class'"
  If db.rs.State > 0 Then
    If db.rs.RecordCount > 0 Then
      For bytDistrictCtr = 1 To bytUpperClassDistricts
        City.Add
        
        Do
          intDiceRoll = dice(db.rs.RecordCount) - 1
          db.rs.MoveFirst
          db.rs.move intDiceRoll
        Loop Until chkLandLocked.Value = Unchecked Or (chkLandLocked.Value = Checked And db.rs!AllowWhenLandLocked = True)
        
        City(City.Count).strType = db.rs!DistrictType
        lstDistrictList.AddItem City(City.Count).strType
        City(City.Count).strDescription = db.rs!Description
        City(City.Count).strFirstImpression = db.rs!FirstImpression
        City(City.Count).strSocialClass = "Upper class"
        Set City(City.Count).NPCs = New CNPCs
        Set City(City.Count).Buildings = New CBuildings
        
        pbMisc.Value = 1
        pbMisc.max = db.rs!Bbn + db.rs!Brd + db.rs!Clr + db.rs!Drd + db.rs!Ftr + db.rs!Mnk + db.rs!Pal + db.rs!Rgr + db.rs!Rog + db.rs!Sor + db.rs!Wiz + db.rs!Com + db.rs!Ari + db.rs!Exp + db.rs!War + db.rs!Adp + db.rs!Art + db.rs!Arc + db.rs!Psi + db.rs!PsW + db.rs!Sol + db.rs!Wld + 1
        
        RollBuildings
        
        ' PCs
        For intPCCtr = 1 To db.rs!Bbn
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Barbarian"
            If d100 < 51 Then
              strTmpGender = "M"
            Else
              strTmpGender = "F"
            End If
            .strGender = strTmpGender
            
            strTmpRace = RollRace(City(City.Count).strType)
            .strRace = strTmpRace
            
            '.strName = RollName(.strRace, strTmpGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Brd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Bard"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Clr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Cleric"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Drd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Druid"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ftr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Fighter"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Mnk
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Monk"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Pal
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Paladin"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Rog
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Rogue"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sor
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Sorcerer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wiz
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wizard"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Adp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Adept"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ari
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Aristocrat"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Com
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Commoner"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Exp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Expert"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!War
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        
        For intPCCtr = 1 To db.rs!Psi
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psion"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!PsW
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psychic Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wld
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wilder"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sol
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Soulknife"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Art
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Artificer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Arc
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Archivist"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        lblTotalPop.Caption = CStr(CLng(lblTotalPop.Caption) + City(City.Count).NPCs.Count)
      
        pbDistrict.Value = pbDistrict.Value + 1
        DoEvents
      
      Next bytDistrictCtr
    End If
  End If
  db.CloseDB
  
  
  db.OpenDB "SELECT * FROM CityDistricts WHERE SocialClass = 'Middle class'"
  If db.rs.State > 0 Then
    If db.rs.RecordCount > 0 Then
      For bytDistrictCtr = 1 To bytMiddleClassDistricts
        
        City.Add
        Do
          intDiceRoll = dice(db.rs.RecordCount) - 1
          db.rs.MoveFirst
          db.rs.move intDiceRoll
        Loop Until chkLandLocked.Value = Unchecked Or (chkLandLocked.Value = Checked And db.rs!AllowWhenLandLocked = True)

        pbMisc.Value = 1
        pbMisc.max = db.rs!Bbn + db.rs!Brd + db.rs!Clr + db.rs!Drd + db.rs!Ftr + db.rs!Mnk + db.rs!Pal + db.rs!Rgr + db.rs!Rog + db.rs!Sor + db.rs!Wiz + db.rs!Com + db.rs!Ari + db.rs!Exp + db.rs!War + db.rs!Adp + db.rs!Art + db.rs!Arc + db.rs!Psi + db.rs!PsW + db.rs!Sol + db.rs!Wld + 1
        City(City.Count).strType = db.rs!DistrictType
        lstDistrictList.AddItem City(City.Count).strType
        City(City.Count).strDescription = db.rs!Description
        City(City.Count).strFirstImpression = db.rs!FirstImpression
        City(City.Count).strSocialClass = "Middle class"
        
        Set City(City.Count).NPCs = New CNPCs
        Set City(City.Count).Buildings = New CBuildings

        RollBuildings

        For intPCCtr = 1 To db.rs!Bbn
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Barbarian"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Brd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Bard"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Clr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Cleric"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Drd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Druid"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ftr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Fighter"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Mnk
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Monk"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Pal
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Paladin"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Rog
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Rogue"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sor
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Sorcerer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wiz
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wizard"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Adp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Adept"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ari
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Aristocrat"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Com
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Commoner"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Exp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Expert"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!War
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        
        For intPCCtr = 1 To db.rs!Psi
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psion"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!PsW
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psychic Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wld
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wilder"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sol
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Soulknife"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        
        For intPCCtr = 1 To db.rs!Art
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Artificer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Arc
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Archivist"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        
        lblTotalPop.Caption = CStr(CLng(lblTotalPop.Caption) + City(City.Count).NPCs.Count)
        pbDistrict.Value = pbDistrict.Value + 1
        DoEvents
        
      Next bytDistrictCtr
    End If
  End If
  db.CloseDB
  
  db.OpenDB "SELECT * FROM CityDistricts WHERE SocialClass = 'Lower class'"
  If db.rs.State > 0 Then
    If db.rs.RecordCount > 0 Then
      For bytDistrictCtr = 1 To bytLowerClassDistricts
        
        City.Add
        Do
          Do
            intDiceRoll = dice(db.rs.RecordCount) - 1
            db.rs.MoveFirst
            db.rs.move intDiceRoll
            City(City.Count).strType = db.rs!DistrictType
          Loop While db.rs!DistrictType Like "Slave quarter" And lblAlignment Like "*Good"
        Loop Until chkLandLocked.Value = Unchecked Or (chkLandLocked.Value = Checked And db.rs!AllowWhenLandLocked = True)
        pbMisc.Value = 1
        pbMisc.max = db.rs!Bbn + db.rs!Brd + db.rs!Clr + db.rs!Drd + db.rs!Ftr + db.rs!Mnk + db.rs!Pal + db.rs!Rgr + db.rs!Rog + db.rs!Sor + db.rs!Wiz + db.rs!Com + db.rs!Ari + db.rs!Exp + db.rs!War + db.rs!Adp + db.rs!Art + db.rs!Arc + db.rs!Psi + db.rs!PsW + db.rs!Sol + db.rs!Wld + 1
       
        lstDistrictList.AddItem City(City.Count).strType
        City(City.Count).strDescription = db.rs!Description
        City(City.Count).strFirstImpression = db.rs!FirstImpression
        City(City.Count).strSocialClass = "Lower class"
        
        
        Set City(City.Count).NPCs = New CNPCs
        Set City(City.Count).Buildings = New CBuildings
        
        RollBuildings
        
        
        For intPCCtr = 1 To db.rs!Bbn
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Barbarian"
            If d100 < 51 Then
              strTmpGender = "M"
            Else
              strTmpGender = "F"
            End If
            strTmpRace = RollRace(City(City.Count).strType)
            '.strName = RollName(strTmpRace, strTmpGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), strTmpRace)
            .strGender = strTmpGender
            .strRace = strTmpRace
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Brd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Bard"
            If d100 < 51 Then
              strTmpGender = "M"
            Else
              strTmpGender = "F"
            End If
            strTmpRace = RollRace(City(City.Count).strType)
            '.strName = RollName(strTmpRace, strTmpGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), strTmpRace)
            .strGender = strTmpGender
            .strRace = strTmpRace
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Clr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Cleric"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Drd
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Druid"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ftr
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Fighter"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Mnk
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Monk"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Pal
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Paladin"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Rog
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Rogue"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sor
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Sorcerer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wiz
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wizard"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Adp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Adept"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Ari
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Aristocrat"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            strTmpRace = RollRace(City(City.Count).strType)
            .strRace = strTmpRace
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Com
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Commoner"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Exp
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Expert"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
          End With
        Next intPCCtr
        For intPCCtr = 1 To db.rs!War
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel(20))
            .strClass = "Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            strTmpRace = RollRace(City(City.Count).strType)
            .strRace = strTmpRace
            '.strName = RollName(.strRace, .strGender)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        
        For intPCCtr = 1 To db.rs!Psi
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psion"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!PsW
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Psychic Warrior"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Wld
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Wilder"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Sol
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Soulknife"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Art
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Artificer"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          End With
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
        Next intPCCtr
        For intPCCtr = 1 To db.rs!Arc
          City(City.Count).NPCs.Add
          With City(City.Count).NPCs(City(City.Count).NPCs.Count)
            .bytLevel = CByte(RollLevel)
            .strClass = "Archivist"
            If d100 < 51 Then
              .strGender = "M"
            Else
              .strGender = "F"
            End If
            .strRace = RollRace(City(City.Count).strType)
            '.strName = RollName(.strRace, .strGender)
            .strClass = RollPrCs(.strClass, CLng(.bytLevel), .strRace)
          pbMisc.Value = pbMisc.Value + 1
          DoEvents
          End With
        Next intPCCtr
        
        lblTotalPop.Caption = CStr(CLng(lblTotalPop.Caption) + City(City.Count).NPCs.Count)
        
        pbDistrict.Value = pbDistrict.Value + 1
        DoEvents
        
      Next bytDistrictCtr
    End If
  End If
  db.CloseDB
  
  lblTotalPop.Caption = Format(CLng(lblTotalPop.Caption), "#,###,##0")
  cmdGenerate.Enabled = True
  cmdReport.Enabled = True
  chkLandLocked.Enabled = True
  
  
  ' don't forget to comment these two out
  'mstrfilename = App.Path & "\exported cities\metropolis.txt"
  'cmdReport_Click
  
  
End Sub

Private Sub cmdReport_Click()
  Dim a As Object
  Dim fso As Object
  Dim i As Long
  Dim j As Long
  Dim lngTotalPop As Long
  Dim lngTotalBuildings As Long
  Dim strFileName As String
  Dim strGender As String
  Dim strAlignment As String
  Dim lngRollAlign As Long
  
  Dim strRace As String, strName As String
  
  
  chkLandLocked.Enabled = False
  cmdReport.Enabled = False
  
  If mstrfilename = "" Then
    strFileName = ""
    With CommonDialog1
      ' Set CancelError is True
      .InitDir = App.Path & "\Exported cities"
      .Filter = "Text Files (*.txt)|*.txt"
      .filename = ""
      .ShowSave
      strFileName = .filename
    End With
  Else
    strFileName = mstrfilename
  End If
  
  
  If Len(strFileName) > 0 Then
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set a = fso.CreateTextFile(strFileName, True)
    
    
    a.WriteLine "City generated " & Format(Now, "hh:mm:ss mmm dd, yyyy")
    a.WriteLine
    
    'a.writeline txtNDistricts.Text   'cboSize.Text
    a.WriteLine CStr(lstDistrictList.ListCount) & " districts"
    a.WriteLine "Upper class gp limit = " & lblUpperGPLim
    a.WriteLine "Middle class gp limit = " & lblMiddleGPLim
    a.WriteLine "Lower class gp limit = " & lblLowerGPLim
    
    a.WriteLine "Land Area: " & lblLandArea.Caption & " sq. mi."
    a.WriteLine "Power Center: " & lblPowerCenter.Caption
    a.WriteLine "Alignment: " & lblAlignment.Caption
    a.WriteLine "First quartile level: " & lblQ1.Caption
    a.WriteLine "Median Level: " & lblQ2.Caption
    a.WriteLine "Third quartile level: " & lblQ3.Caption
    a.WriteLine "Maximum Level: " & txtMaxLvl.Text
    
    
    lngTotalBuildings = 0
    lngTotalPop = 0
    
    a.WriteLine
    a.WriteLine "LIST OF DISTRICTS"
    a.WriteLine
    
    For i = 1 To City.Count
      a.WriteLine City(i).strType
    Next i
    
    a.WriteLine
    
    a.WriteLine "DISTRICT DETAILS"
    a.WriteLine
    
    pbDistrict.Value = 1
    pbDistrict.max = City.Count
    
    cmdGenerate.Enabled = False
    For i = 1 To City.Count
      pbDistrict.Value = i
      
      a.WriteLine
      a.WriteLine City(i).strType
      a.WriteLine
      a.WriteLine "Description: " & City(i).strDescription
      a.WriteLine
      a.WriteLine "First Impressions: " & City(i).strFirstImpression
      a.WriteLine
      a.WriteLine "Social class: " & City(i).strSocialClass
      
      a.WriteLine
      lngTotalBuildings = lngTotalBuildings + City(i).Buildings.Count
      a.WriteLine "BUILDINGS:"
      For j = 1 To City(i).Buildings.Count
        a.WriteLine City(i).Buildings(j).strBuildingType
      Next j
      
      a.WriteLine
      a.WriteLine "NPCs:"
      lngTotalPop = lngTotalPop + City(i).NPCs.Count
      
      pbMisc.Value = 1
      pbMisc.max = City(i).NPCs.Count
      
      For j = 1 To City(i).NPCs.Count
        pbMisc.Value = j
        DoEvents
        
        ' roll race
        If d100 < 51 Then
          strGender = "Male"
        Else
          strGender = "Female"
        End If
        strRace = RollRace(City(i).strType)
        ' roll name
        strName = RollName(strRace, City(i).NPCs(j).strGender)
        
        ' roll alignment
        lngRollAlign = dN(1, 9)
        Select Case lngRollAlign
          Case 1
            strAlignment = "Lawful Good"
          Case 2
            strAlignment = "Lawful Neutral"
          Case 3
            strAlignment = "Lawful Evil"
          Case 4
            strAlignment = "Neutral Good"
          Case 5
            strAlignment = "True Neutral"
          Case 6
            strAlignment = "Neutral Evil"
          Case 7
            strAlignment = "Chaotic Good"
          Case 8
            strAlignment = "Chaotic Neutral"
          Case 9
            strAlignment = "Chaotic Evil"
        End Select
        
        ' TODO: roll ability scores
        ' TODO: set rolled scores in order of importance for class
        ' TODO: roll equipment
        
        
        a.WriteLine strName & ", " & strAlignment & " " & strRace & " " & strGender & " " & City(i).NPCs(j).strClass & " (Character Level " & City(i).NPCs(j).bytLevel & ")"
      Next j
    
    Next i
    
    a.WriteLine
    a.WriteLine "Total population = " & Format(lngTotalPop, "#,###,##0")
    a.WriteLine "Total number of buildings = " & Format(lngTotalBuildings, "###,##0")
    a.Close
  End If
    cmdGenerate.Enabled = True
  cmdReport.Enabled = True
  chkLandLocked.Enabled = True
End Sub

Private Sub Form_Load()
  RUN_DEBUG = False
  Randomize Timer
  Set City = New CCityDistricts
  Set db = New CDB
  
  
  ReDim MinDistrictsForLevel(40) As Integer
  MinDistrictsForLevel(0) = 1
  MinDistrictsForLevel(1) = 2
  MinDistrictsForLevel(2) = 3
  MinDistrictsForLevel(3) = 5
  MinDistrictsForLevel(4) = 8
  MinDistrictsForLevel(5) = 11
  MinDistrictsForLevel(6) = 13
  MinDistrictsForLevel(7) = 13
  MinDistrictsForLevel(8) = 13
  MinDistrictsForLevel(9) = 16
  MinDistrictsForLevel(10) = 20
  MinDistrictsForLevel(11) = 26
  MinDistrictsForLevel(12) = 31
  MinDistrictsForLevel(13) = 39
  MinDistrictsForLevel(14) = 49
  MinDistrictsForLevel(15) = 59
  MinDistrictsForLevel(16) = 72
  MinDistrictsForLevel(17) = 86
  MinDistrictsForLevel(18) = 103
  MinDistrictsForLevel(19) = 123
  MinDistrictsForLevel(20) = 144
  MinDistrictsForLevel(21) = 164
  MinDistrictsForLevel(22) = 187
  MinDistrictsForLevel(23) = 209
  MinDistrictsForLevel(24) = 229
  MinDistrictsForLevel(25) = 253
  MinDistrictsForLevel(26) = 276
  MinDistrictsForLevel(27) = 297
  MinDistrictsForLevel(28) = 322
  MinDistrictsForLevel(29) = 345
  MinDistrictsForLevel(30) = 371
  MinDistrictsForLevel(31) = 400
  MinDistrictsForLevel(32) = 427
  MinDistrictsForLevel(33) = 456
  MinDistrictsForLevel(34) = 483
  MinDistrictsForLevel(35) = 512
  MinDistrictsForLevel(36) = 546
  MinDistrictsForLevel(37) = 578
  MinDistrictsForLevel(38) = 614
  MinDistrictsForLevel(39) = 648
  MinDistrictsForLevel(40) = 648
  
  Dim ddb As New CDB
  ddb.OpenDB "SELECT DISTINCT REGION FROM NameTableByRegionAndRace ORDER BY REGION"
  
  If ddb.rs.State > 0 Then
    ddb.MoveFirst
    cboRegion.Clear
    Do While Not ddb.rs.EOF
      cboRegion.AddItem ddb.rs!Region
      ddb.rs.MoveNext
    Loop
    ddb.CloseDB
  End If
  
  'frmDemographics.Show
  'cboSize.ListIndex = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
  cmdGenerate.Enabled = False
  cmdReport.Enabled = False
  
  Set db = Nothing
  'Set City = Nothing
End Sub

Private Function dice(nsides) As Integer
  dice = Int(Rnd * nsides) + 1
End Function

Private Function RollLevel(Optional lngMaxLvl As Long = -1) As Integer

  ' Thse distributions have the form P(x) = m x + b where
  '       m = -2/(L^2-2L+1) and b = 2L/(L^2-2L+1),
  ' where L-1  is the maximum level attainable.
  ' In other words,
  '     P(x) = 2 * (M + 1 - X) / (M ^ 2)
  ' where L = M + 1
  
  ' The cumulative distribution from 1 to any level X is given by
  '       (2L-X-1)(X-1)/(L^2-2L+1).
  ' or
  '       (2L-X-1)(X-1)/(L-1)^2
  ' which equals
  '       -(X-1)(X-2M-1)/(M^2)
  
  ' The cumulative distribution from 1 to L is always 1.  P(L) = 0.
  
  ' The P-th percentile can be calculated with the formula:
  '       Lp = -(L * Sqr(1 - P) - 1) - Sqr(1 - P)
  
  ' The mid-level boundary (L/7) is approximately the first 25th percentile.
  ' The high-level boundary (0.3*L) is appriximately the median.
  ' The epic-level boundary (L/2) is around the 75th percentile.
  ' The "Cream" (0.75*L) is around the 95th percentile.
  
  
  ' The logic behind allowing epic NPCs to take up about 25% of all NPCs is as follows:
  '   (1)  Morrowind has a linear progression.  This means that people
  '        practicing their skills will steadily increase in level.
  '   (2)  Lower level characters are more common than upper level, because
  '        characters die or retire after a while.
  '   (3)  In D20, even though the experience scale is quadratic, the XP awards
  '        increase accordingly.  If NPCs are assumed to be facing challenges
  '        at their same level or closer, then they will advance linearly.
  '        Hence, the same distribution (scaled down to 40th level) is used.
  '   (4)  Game balance at high levels is needed.
  '   (5)  It really keeps players on their toes as to who they mug.  Even commoners
  '        can annihilate a high-level player, although it becomes much more rare.
  
  ' The DMG uses a table based on population for the highest level PC, then uses
  ' a 1/x distribution.
  
  
  Dim CumulativeDistribution As Double
  Dim lngMinLvl As Long
  Dim m As Double, b As Double
  Dim X As Double
  Dim i As Integer
  
  lngMinLvl = CLng(txtMinLvl)
  If lngMaxLvl = -1 Then
    lngMaxLvl = CLng(txtMaxLvl) + 1
  Else
    lngMaxLvl = lngMaxLvl + 1
  End If
    
  'RollLevel = RollFixedSurvivalRateLevel(lngMaxLvl, 0.15)
  RollLevel = RollGeometric(lngMaxLvl)
        
'    X = Rnd
'    For i = lngMinLvl To lngMaxLvl
'      'CumulativeDistribution = (2 * lngMaxLvl - i - 1) * (i - 1) / (lngMaxLvl ^ 2 - 2 * lngMaxLvl + 1)
'
'      CumulativeDistribution = -1 / (lngMaxLvl - lngMinLvl) ^ 2 * _
'        (i ^ 2 - 2 * i * lngMaxLvl + (2 * lngMaxLvl - lngMinLvl) * lngMinLvl)
'
'      If X < CumulativeDistribution Then Exit For
'
'    Next i
'
'  RollLevel = i - 1
  
End Function

Private Function RollGeometric(lngMaxLvl As Long) As Integer
  Dim CumulativeDistribution As Double
  Dim lngMinLvl As Long
  Dim m As Double, b As Double
  Dim X As Double
  Dim i As Integer
  Dim Nlvl As Single
  
  ' N = maxlevel/level
  
  lngMinLvl = 1
  'lngMaxLvl = lngMaxLvl - 1
  lngMaxLvl = CLng(txtMaxLvl)

  Nlvl = 0
  For i = lngMinLvl To lngMaxLvl
    Nlvl = Nlvl + Int(lngMaxLvl / i)
  Next i
  'Nlvl = lngMaxLvl * log(lngMaxLvl)
  
  CumulativeDistribution = 0
  
  Do
      X = Int(Rnd * Nlvl)
  Loop While X > Nlvl
  
  For i = lngMinLvl To lngMaxLvl
    'CumulativeDistribution = (2 * lngMaxLvl - i - 1) * (i - 1) / (lngMaxLvl ^ 2 - 2 * lngMaxLvl + 1)
      
    'CumulativeDistribution = -1 / (lngMaxLvl - lngMinLvl) ^ 2 * _
      (i ^ 2 - 2 * i * lngMaxLvl + (2 * lngMaxLvl - lngMinLvl) * lngMinLvl)
      
    CumulativeDistribution = CumulativeDistribution + Int(lngMaxLvl / i)
      
    If X < CumulativeDistribution Then Exit For

  Next i
   ' Debug.Print i
  RollGeometric = i ' - 1
End Function

Private Function RollFixedSurvivalRateLevel(lngMaxLvl As Long, sngPercentage As Single)
    Dim sngRoll As Single
    
    Dim lvl As Long
    
    lvl = 1
    
    Do
        sngRoll = Rnd
        If sngRoll <= sngPercentage Then
            lvl = lvl + 1
        End If
    Loop Until sngRoll > sngPercentage
    
    RollFixedSurvivalRateLevel = lvl
    
End Function

Private Sub lstDistrictList_Click()
  Dim i As Integer
  Dim j As Integer
  
  lstPCs.Clear
  lstBuildings.Clear
  For i = 0 To lstDistrictList.ListCount - 1
    If lstDistrictList.Selected(i) = True Then
      lblSelDistSocClass.Caption = City(i + 1).strSocialClass
      For j = 1 To City(i + 1).NPCs.Count
      
        lstPCs.AddItem City(i + 1).NPCs(j).strName & _
          " (" & City(i + 1).NPCs(j).strGender & " " & _
          City(i + 1).NPCs(j).strRace & " " & _
          City(i + 1).NPCs(j).strClass & " (Level " & _
          Format(City(i + 1).NPCs(j).bytLevel, "00") & "))"
          
        lblFirstImpression = City(i + 1).strFirstImpression
        lblDescription = City(i + 1).strDescription
      Next j
      For j = 1 To City(i + 1).Buildings.Count
        lstBuildings.AddItem City(i + 1).Buildings(j).strBuildingType
      Next j
    End If
  Next i
End Sub


Public Function RollTable(strTableName As String) As String

  Dim rs2 As New ADODB.Recordset
  
  rs2.Open "SELECT * FROM " & strTableName, Conn, adOpenStatic, adLockReadOnly
  
  If rs2.State > 0 Then
    If rs2.RecordCount > 0 Then
      rs2.MoveFirst
      rs2.move CLng(dice(rs2.RecordCount) - 1)
      RollTable = rs2!Name
    End If
  End If
  
  rs2.Close
  

End Function


Private Sub RollBuildings()
  Dim intBuildingCtr As Integer
  
  ' Buildings
  For intBuildingCtr = 1 To db.rs!SpecialBuilding1Number
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      If db.rs!SpecialBuilding1 Like "Dungeon with at least 10 rooms" Then
        .strBuildingType = "Dungeon with " & CStr(dice(11) + 9) & " rooms"
      Else
        .strBuildingType = db.rs!SpecialBuilding1
      End If
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!SpecialBuilding2Number
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = db.rs!SpecialBuilding2
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!SpecialBuilding3Number
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = db.rs!SpecialBuilding3
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!SpecialBuilding4Number
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = db.rs!SpecialBuilding4
    End With
  Next intBuildingCtr
  
  For intBuildingCtr = 1 To db.rs!ImperialTemple
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Imperial Temple"
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!TribunalTemple
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Tribunal Temple"
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!Shrine
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Shrine"
    End With
  Next intBuildingCtr
  
  For intBuildingCtr = 1 To db.rs!UpscaleLodging
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Upscale " & RollTable("CityBuildingsLodging")
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!AverageLodging
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Average " & RollTable("CityBuildingsLodging")
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!PoorLodging
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Poor " & RollTable("CityBuildingsLodging")
    End With
  Next intBuildingCtr
  
  For intBuildingCtr = 1 To db.rs!UpscaleFood
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Upscale " & RollTable("CityBuildingsFood")
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!AverageFood
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Average " & RollTable("CityBuildingsFood")
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!PoorFood
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Poor " & RollTable("CityBuildingsFood")
    End With
  Next intBuildingCtr
  
  If db.rs!DistrictType Like "University" Then
    For intBuildingCtr = 1 To db.rs!UpscaleTrades
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsTradesUniversity")
      End With
    Next intBuildingCtr
    
  Else
    For intBuildingCtr = 1 To db.rs!ExoticTrades
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsTradesExotic")
      End With
    Next intBuildingCtr
    For intBuildingCtr = 1 To db.rs!UpscaleTrades
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsTradesUpscale")
      End With
    Next intBuildingCtr
    For intBuildingCtr = 1 To db.rs!AverageTrades
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsTradesAverage")
      End With
    Next intBuildingCtr
    For intBuildingCtr = 1 To db.rs!PoorTrades
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsTradesPoor")
      End With
    Next intBuildingCtr
  End If
  
  If db.rs!DistrictType Like "Red light district" Then
    For intBuildingCtr = 1 To db.rs!PoorServices
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsServicesRedLight")
      End With
    Next intBuildingCtr
  ElseIf db.rs!DistrictType Like "University" Then
    For intBuildingCtr = 1 To db.rs!UpscaleServices
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsServicesUniversity")
      End With
    Next intBuildingCtr
  Else
    For intBuildingCtr = 1 To db.rs!UpscaleServices
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsServicesUpscale")
      End With
    Next intBuildingCtr
    For intBuildingCtr = 1 To db.rs!AverageServices
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsServicesAverage")
      End With
    Next intBuildingCtr
    For intBuildingCtr = 1 To db.rs!PoorServices
      City(City.Count).Buildings.Add
      With City(City.Count).Buildings(City(City.Count).Buildings.Count)
        .strBuildingType = RollTable("CityBuildingsServicesPoor")
      End With
    Next intBuildingCtr
  
  End If
  
  For intBuildingCtr = 1 To db.rs!UpscaleResidences
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Upscale residence"
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!AverageResidences
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Average residence"
    End With
  Next intBuildingCtr
  For intBuildingCtr = 1 To db.rs!PoorResidences
    City(City.Count).Buildings.Add
    With City(City.Count).Buildings(City(City.Count).Buildings.Count)
      .strBuildingType = "Poor residence"
    End With
  Next intBuildingCtr

End Sub

Private Sub txtMaxLvl_Change()
  Dim L As Long
  Dim p As Single
  Dim Lmin As Long
  
  ' The P-th percentile can be calculated with the formula:
  '       Lp = -(L * (Sqr(1 - P) - 1) - Sqr(1 - P)
  If IsNumeric(txtMaxLvl.Text) And IsNumeric(txtMinLvl.Text) Then
    L = CLng(txtMaxLvl.Text) + 1
    Lmin = CLng(txtMinLvl.Text)
    
    p = 0.25
    lblQ1.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

    p = 0.5
    lblQ2.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

    p = 0.75
    lblQ3.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

  End If
End Sub

Private Sub txtMinLvl_Change()
  Dim L As Long
  Dim p As Single
  Dim Lmin As Long
  
  ' The P-th percentile can be calculated with the formula:
  '       Lp = -(L * (Sqr(1 - P) - 1) - Sqr(1 - P)
  If IsNumeric(txtMaxLvl.Text) And IsNumeric(txtMinLvl.Text) Then
    L = CLng(txtMaxLvl.Text) + 1
    Lmin = CLng(txtMinLvl.Text)
    
    p = 0.25
    lblQ1.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

    p = 0.5
    lblQ2.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

    p = 0.75
    lblQ3.Caption = Format(-1 * (Sqr(-p * (L - Lmin) ^ 2 + _
      L ^ 2 - 2 * L * Lmin + Lmin ^ 2) - L), "#0")

  End If
End Sub

Private Sub txtNDistricts_Change()
  Dim n As Long
  Dim lngMiddleClassDistricts As Long
  Dim lngUpperClassDistricts As Long
  Dim lngLowerClassDistricts As Long
  Dim i As Integer
  
  If IsNumeric(txtNDistricts.Text) Then
    n = CLng(txtNDistricts.Text)
    cmdGenerate.Enabled = True
    lblUpperGPLim.Caption = Format(7.0545 * n ^ 2 + 688.12 * n, "###,##0")
    lblMiddleGPLim.Caption = Format(2.8218 * n ^ 2 + 275.25 * n, "###,##0")
    lblLowerGPLim.Caption = Format(0.7054 * n ^ 2 + 68.812 * n, "###,##0")
    
    n = CLng(txtNDistricts.Text)
    lngMiddleClassDistricts = n * 0.3
    lngUpperClassDistricts = 0.2 * n - 2
    If lngUpperClassDistricts < 0 Then lngUpperClassDistricts = 0
    lngLowerClassDistricts = n - lngMiddleClassDistricts - lngUpperClassDistricts
    
    lblNLower.Caption = lngLowerClassDistricts
    lblNMiddle.Caption = lngMiddleClassDistricts
    lblNUpper.Caption = lngUpperClassDistricts
    lblTotalPop.Caption = n * 500
    lblLandArea = Format(n * 500 / 38500, "#0.00")
    For i = 0 To 40
      If n < MinDistrictsForLevel(i) Then Exit For
    Next i
    If i > 40 Then
      i = log(CDbl(lblUpperGPLim.Caption) ^ 10.4920586873) - 117.793071217
    End If
    txtMaxLvl = CStr(i)
    
  Else
    cmdGenerate.Enabled = False
  End If
End Sub


Public Function Shuffle(n As Long) As Long()

  ' Generates a randomly-ordered array of longs from with
  ' non-repeating values from 1 to N

  Dim i As Long, j As Long, k As Long
  Dim lngArray() As Long
  
  ReDim lngArray(n) As Long

  For i = 0 To n
    lngArray(i) = i
  Next i
  
  For i = 0 To n
    j = CLng(Rnd() * 2147483647#) Mod n
    k = lngArray(i)
    lngArray(i) = lngArray(j)
    lngArray(j) = k
  Next i
  
  Shuffle = lngArray
  
End Function


Private Function RollPrCs(strBaseClass As String, lngTotalLevel As Long, strRace As String) As String
  Dim rs As New ADODB.Recordset
  Dim n As Long
  Dim k As Long
  Dim strOut As String
  Dim lngPrCLevel As Long
  Dim lngCls1Lvl As Long
  Dim lngCls2Lvl As Long
  Dim lngCls3Lvl As Long
  
  rs.Open "SELECT * FROM NPCPrestigeClassBaseClasses WHERE TotalLevel < " & _
    CStr(lngTotalLevel) & " AND (Class1Class LIKE '" & strBaseClass & _
    "' or Class1Class LIKE 'Any' or (Class2Class LIKE '" & strBaseClass & _
    "' AND Class1Level = Class2Level)) AND (RaceRestriction LIKE '" & strRace & _
    "' OR RaceRestriction IS NULL) ORDER BY TotalLevel, ClassName", _
    Conn, adOpenStatic, adLockReadOnly
  
  If rs.State > 0 Then
    If rs.RecordCount > 0 Then
      n = dN(1, rs.RecordCount)
      rs.MoveFirst
      rs.move n - 1
      
      If rs!ClassName Like strBaseClass Then
        strOut = strBaseClass
        RollPrCs = strOut
      Else
        If rs!Class1Class Like "Any" Then
          strOut = strBaseClass
        Else
          strOut = rs!Class1Class
        End If
        
        If rs!Class1Level > 0 Then
          lngCls1Lvl = rs!Class1Level
        Else
          lngCls1Lvl = 0
        End If
        If rs!Class2Level > 0 Then
          lngCls2Lvl = rs!Class2Level
        Else
          lngCls2Lvl = 0
        End If
        If rs!Class3Level > 0 Then
          lngCls3Lvl = rs!Class3Level
        Else
          lngCls3Lvl = 0
        End If
        
        lngPrCLevel = lngTotalLevel - lngCls1Lvl - lngCls2Lvl - lngCls3Lvl
        If lngPrCLevel > rs!MaxLvl And rs!MaxLvl <> 0 Then
          lngCls1Lvl = lngCls1Lvl + lngPrCLevel - rs!MaxLvl
          lngPrCLevel = rs!MaxLvl
        End If
        If lngPrCLevel < 0 Then lngPrCLevel = 0
        
        
        If rs!Class1Level > 0 Then
          strOut = strOut & " " & CStr(lngCls1Lvl)
'        Else
'          k = dN(1, lngTotalLevel - 1)
'          lngPrCLevel = lngTotalLevel - k
'          strOut = strOut & " " & CStr(k)
        End If
        
        If Not IsNull(rs!Class2Class) Then
          strOut = strOut & "/" & rs!Class2Class & " " & CStr(lngCls2Lvl)
          'lngPrCLevel = lngPrCLevel - rs!Class2Level
        End If
        If Not IsNull(rs!Class3Class) Then
          strOut = strOut & "/" & rs!Class3Class & " " & CStr(lngCls3Lvl)
          'lngPrCLevel = lngPrCLevel - rs!Class3Level
        End If
        
        strOut = strOut & "/" & rs!ClassName & " " & CStr(lngPrCLevel)
        RollPrCs = strOut
      End If
    Else
      RollPrCs = strBaseClass
    End If
    rs.Close
  End If
End Function

Public Function RollRace(ByVal strType As String) As String
  Dim n As Long
  Dim strRace1 As String
  
  n = d100
  Select Case n
    Case Is <= 78
      strRace1 = "Human"
    Case 79 To 87
      strRace1 = "Halfling"
    Case 88 To 93
      strRace1 = "Elf"
    Case 94 To 96
      strRace1 = "Dwarf"
    Case 97 To 98
      strRace1 = "Gnome"
    Case 99
      strRace1 = "Half-Elf"
    Case 100
      strRace1 = "Half-Orc"
  End Select
  
  Select Case strType
    Case "Gnome neighborhood"
      If strRace1 Like "Gnome" Then
        strRace1 = "Human"
      ElseIf strRace1 Like "Human" Then
        strRace1 = "Gnome"
      End If
    Case "Elf neighborhood"
      If strRace1 Like "Elf" Then
        strRace1 = "Human"
      ElseIf strRace1 Like "Human" Then
        strRace1 = "Elf"
      End If
    Case "Halfling encampment"
      If strRace1 Like "Halfling" Then
        strRace1 = "Human"
      ElseIf strRace1 Like "Human" Then
        strRace1 = "Halfling"
      End If
    Case "Goblinoid ghetto"
      If strRace1 Like "Half-Orc" Then
        strRace1 = "Human"
      ElseIf strRace1 Like "Human" Then
        strRace1 = "Half-Orc"
      End If
    Case "Dwarf neighborhood"
      If strRace1 Like "Dwarf" Then
        strRace1 = "Human"
      ElseIf strRace1 Like "Human" Then
        strRace1 = "Dwarf"
      End If
    
  End Select
  RollRace = strRace1
End Function


Private Function RollName(ByVal strRace1 As String, ByVal strGender1 As String) As String
  Dim strNameFileList(25) As String
  Dim rn As New CRandomName
  Dim k As Long
  Dim strTemp As String
  
  Do
  
  rn.strFileName = "orc.nam"
  rn.strSourceFolder = App.Path & "\names"

  strNameFileList(0) = "hispanic_NAMES.nam"
  strNameFileList(1) = "mar_knightly_names.nam"
  strNameFileList(2) = "rw_african_names.nam"
  strNameFileList(3) = "rw_anglo_saxon_latinized_names.nam"
  strNameFileList(4) = "rw_anglo_saxon_names.nam"
  strNameFileList(5) = "rw_chinese_names.nam"
  strNameFileList(6) = "rw_english_late_period_names.nam"
  strNameFileList(7) = "rw_flemish_names.nam"
  strNameFileList(8) = "rw_french_names.nam"
  strNameFileList(9) = "rw_german_names.nam"
  strNameFileList(10) = "rw_gothic_names.nam"
  strNameFileList(11) = "rw_greek_names.nam"
  strNameFileList(12) = "rw_hebrew_names.nam"
  strNameFileList(13) = "rw_japanese_names.nam"
  strNameFileList(14) = "rw_native_american_names.nam"
  strNameFileList(15) = "rw_persian_names.nam"
  strNameFileList(16) = "rw_sumerian_names.nam"
  strNameFileList(17) = "rw_English_middle_Period_"
  strNameFileList(18) = "rw_frankish_"
  strNameFileList(19) = "rw_Irish_Gaelic_"
  strNameFileList(20) = "rw_Italian_Renaissance_"
  strNameFileList(21) = "rw_pictish_classical_names.nam"
  strNameFileList(22) = "rw_pictish_medieval_"
  strNameFileList(23) = "rw_roman_"
  strNameFileList(24) = "rw_russian_"
  strNameFileList(25) = "rw_scottish_gaelic_"
  
  
  If strRace1 Like "*Dwarf*" Then
    rn.strFileName = "dvargar.nam"
    
  ElseIf strRace1 Like "*Elf*" Then
    rn.strFileName = "alver.nam"
    
  ElseIf strRace1 Like "*Drow*" Then
    rn.strFileName = "drow_names.nam"
    
  ElseIf strRace1 Like "*Gnome*" Then
    rn.strFileName = "gnome.nam"
    
  ElseIf strRace1 Like "Halfling*" Then
    rn.strFileName = "rw_english_late_period_names.nam"
  
  ElseIf strRace1 Like "*Human*" Then
    k = dN(1, 25) - 1
    rn.strFileName = strNameFileList(k)
    'If strClass Like "*Aristocrat*" Then
    '  randomname.strFileName = "Mar_knightly_names.nam"
    'End If
  
  ElseIf strRace1 Like "*Half-Elf*" Then
    k = dN(1, 50) - 1
    If k < 26 Then
      rn.strFileName = strNameFileList(k)
    Else
      rn.strFileName = "Alver.nam"
    End If
  
  ElseIf strRace1 Like "*Half-Orc*" Then
    rn.strFileName = "orc.nam"
  End If
  
  If Not LCase(Right(rn.strFileName, 4)) Like ".nam" Then
    If strGender1 Like "F" Then
      rn.strFileName = rn.strFileName & "Female.nam"
    Else
      rn.strFileName = rn.strFileName & "Male.nam"
    End If
  End If
  
  
  
  strTemp = rn.RandomName(strGender1)
  
  Loop While Len(strTemp) = 0
  
  RollName = strTemp
  
End Function

