VERSION 5.00
Begin VB.Form frmRegion 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Region Close-up (3 mi. resolution)"
   ClientHeight    =   9405
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11535
   Icon            =   "frmRegion.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9405
   ScaleWidth      =   11535
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cboMapSelect 
      Height          =   315
      ItemData        =   "frmRegion.frx":1BB2
      Left            =   9420
      List            =   "frmRegion.frx":1BBF
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   60
      Width           =   2055
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      Height          =   9375
      Left            =   0
      ScaleHeight     =   9315
      ScaleWidth      =   9315
      TabIndex        =   0
      Top             =   0
      Width           =   9375
   End
End
Attribute VB_Name = "frmRegion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mvarZ() As Integer
Private cl() As Byte
Private wd() As Byte
Private pm() As Byte
Private temperature() As Integer
Private rn() As Integer
Private mvarZShelf As Long
Private mvarZCoast As Long

Private frac As CCollageFractal

Public Property Get ZShelf() As Long
  ZShelf = mvarZShelf
End Property
Public Property Let ZShelf(vData As Long)
  mvarZShelf = vData
End Property

Public Property Get ZCoast() As Long
  ZCoast = mvarZCoast
End Property
Public Property Let ZCoast(vData As Long)
  mvarZCoast = vData
End Property

Public Sub LetDEM(vData() As Integer)
  ReDim mvarZ(0, 0) As Integer
  mvarZ = vData
  draw_ DRAW_TEC, mvarZ
End Sub
Public Function GetDEM() As Integer()
  GetDEM = mvarZ
End Function

Public Sub LetClimate(vData() As Byte)
  cl = vData
End Sub
Public Function GetClimate() As Byte()
  GetClimate = cl
End Function

Public Sub LetRain(vData() As Integer)
  rn = vData
End Sub
Public Function GetRain() As Integer()
  GetRain = rn
End Function

Public Sub LetTemperature(vData() As Integer)
  temperature = vData
End Sub
Public Function GetTemperature() As Integer()
  GetTemperature = temperature
End Function

Public Sub LetWind(vData() As Byte)
  wd = vData
End Sub
Public Function GetWind() As Byte()
  GetWind = wd
End Function

Public Sub LetPressure(vData() As Byte)
  pm = vData
End Sub
Public Function GetPressure() As Byte()
  GetPressure = pm
End Function


'Private Sub assignTecColors()
'  Dim i As Long
'  Dim mvarZShelf As Long, mvarZCoast As Long
'
'  mvarZShelf = frmTec.tec.ZShelf
'  mvarZCoast = frmTec.tec.ZCoast
'
'  FillX11ColorTable
'
'  color_values(0) = &H0 'black
'  color_values(1) = &HFFFFFF 'white
'  color_values(2) = &HBFBFBF 'grey75
'  color_values(3) = &H7F7F7F 'grey50
'  color_values(4) = &HAB82FF 'MediumPurple1
'  color_values(5) = &H551A8B 'purple4
'  color_values(6) = &H7D26CD 'purple3
'  color_values(7) = &H912CEE 'purple2
'  color_values(8) = &H9B30FF 'purple1
'  color_values(9) = RGB(0, 0, &H8B) 'blue4
'  color_values(10) = RGB(0, 0, &HCD) 'blue3
'  color_values(11) = RGB(0, 0, &HEE) 'blue2
'  color_values(12) = RGB(0, 0, &HFF)     'blue1
'  color_values(13) = RGB(0, &H64, 0) 'DarkGreen
'  color_values(14) = RGB(0, &H8B, 0) 'green4
'  color_values(15) = RGB(0, &HCD, 0) 'green3
'  color_values(16) = RGB(0, &HEE, 0) 'green2
'  color_values(17) = RGB(0, &HFF, 0) 'green1
'  color_values(18) = &H8B6508 'DarkGoldenrod4
'  color_values(19) = &H8B8B00 'yellow4
'  color_values(20) = &HCDCD00 'yellow3
'  color_values(21) = &HEEEE00 'yellow2
'  color_values(22) = &HFFFF00 'yellow1
'  color_values(23) = &H8B5A00 'orange4
'  color_values(24) = &HCD8500 'orange3
'  color_values(25) = &HEE9A00 'orange2
'  color_values(26) = &HFFA500 'orange1
'  color_values(27) = &H8B2323 'brown4
'  color_values(28) = &H8B0000 'red4
'  color_values(29) = &HCD0000 'red3
'  color_values(30) = &HEE0000 'red2
'  color_values(31) = &HFF0000 'red1
'
'  climcols(0) = color_values(9)
'  climcols(1) = color_values(20)
'  climcols(2) = color_values(18)
'  climcols(3) = color_values(2)
'  climcols(4) = color_values(3)
'  climcols(5) = color_values(18)
'  climcols(6) = color_values(22)
'  climcols(7) = color_values(26)
'  climcols(8) = color_values(14)
'  climcols(9) = color_values(17)
'  climcols(10) = color_values(12)
'
'  landcols(0) = color_values(11)
'  landcols(1) = color_values(28)
'  landcols(2) = color_values(3)
'
'
'  For i = 0 To 255
'    'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
'    greycols(i) = RGB(i, i, i)
'  Next i
'
'  For i = 1 To MAXPLATE
'    platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
'  Next i
'
'  ReDim tecols(MAXMOUNTAINHEIGHT) As Long
'
'  ' deep ocean
'  For i = 0 To mvarZShelf
'    tecols(i) = color_values(9) ' dark blue
'  Next i
'
'  ' continental shelf
'  For i = mvarZShelf To mvarZCoast
'    tecols(i) = RGB(128, 255, 255) ' light blue
'  Next i
'
'  ' Greens for land
'  For i = mvarZCoast To mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255)
'    'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
'    If i >= UBound(tecols) Then Exit For
'    tecols(i) = RGB(0, i * 2 + 95, 0)
'  Next i
'
'  ' Brown for above tree line
'  For i = mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255)
'    If i >= UBound(tecols) Then Exit For
'    tecols(i) = RGB(128, i, i)
'  Next i
'
'  ' White for above snow line
'  For i = mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
'    If i >= UBound(tecols) Then Exit For
'    'tecols(i) = color_values(1)
'    tecols(i) = RGB(150 + i / 2, 150 + i / 2, 150 + i / 2)
'  Next i
'
'  For i = mvarZCoast + 200 To UBound(tecols)
'    If i >= UBound(tecols) Then Exit For
'    'tecols(i) = color_values(1)
'    tecols(i) = RGB(255, 255, 255)
'  Next i
'End Sub

Private Sub draw_(ctype As DrawType_E, cra() As Integer)
  
  Dim i As Long, j As Long, k As Long, X As Long
  Dim lut() As Long
  Dim XSize As Long, YSize As Long
  Dim clrinx As Long
  
  XSize = UBound(cra, 1)
  YSize = UBound(cra, 2)

  Debug.Print CStr(XSize) & " x " & CStr(YSize)

  assign_colors mvarZShelf, mvarZCoast
  Select Case ctype
    Case DRAW_GREY
      lut = greycols
    Case DRAW_LAND
      lut = landcols
    Case DRAW_CLIM
      lut = climcols
    Case DRAW_TEC
      lut = tecols
    Case DRAW_PLATE
      lut = platecols
    Case DRAW_JET
      lut = heatcols
  End Select
  
  For j = 0 To YSize
    For i = 0 To XSize - 1
      clrinx = cra(i, j) / (MAXMOUNTAINHEIGHT * 100# / 255)
      X = CLng(lut(clrinx))
      k = i + 1
      
      Do While ((CLng(lut(cra(k, j) / (MAXMOUNTAINHEIGHT * 100# / 255))) = X) And (k < XSize - 1))
        k = k + 1
      Loop
      
      Picture1.Line _
        (i * SIZE * Picture1.ScaleWidth / XSize, _
        j * SIZE * Picture1.ScaleHeight / YSize _
        )-Step((k - i) * SIZE * Picture1.ScaleWidth / XSize, _
        SIZE * Picture1.ScaleHeight / YSize), _
        X, BF
      
      i = k - 1
      
    Next i
  
    DoEvents
  Next j
  

End Sub

Private Sub cboMapSelect_Change()
  Select Case cboMapSelect.ListIndex
    Case 0
      ' draw elevation
    Case 1
      ' draw climate
    Case 2
      ' draw population
  End Select
End Sub
