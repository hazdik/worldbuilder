VERSION 5.00
Begin VB.Form frmSV 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Town/Village Statistics"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4665
   Icon            =   "frmSV.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   4665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstBusinesses 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5310
      Left            =   60
      TabIndex        =   2
      Top             =   1140
      Width           =   4515
   End
   Begin VB.Label Label18 
      AutoSize        =   -1  'True
      Caption         =   "Power Center:"
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   420
      Width           =   1005
   End
   Begin VB.Label lblPowerCenter 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1260
      TabIndex        =   8
      Top             =   420
      Width           =   1575
   End
   Begin VB.Label Label21 
      AutoSize        =   -1  'True
      Caption         =   "Alignment:"
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   780
      Width           =   735
   End
   Begin VB.Label lblAlignment 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   1260
      TabIndex        =   6
      Top             =   780
      Width           =   1575
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "sq. mi."
      Height          =   195
      Left            =   3900
      TabIndex        =   5
      Top             =   90
      Width           =   450
   End
   Begin VB.Label lblArea 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "1"
      Height          =   255
      Left            =   2700
      TabIndex        =   4
      Top             =   60
      Width           =   1035
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Area:"
      Height          =   195
      Left            =   2280
      TabIndex        =   3
      Top             =   90
      Width           =   375
   End
   Begin VB.Label lblPopulation 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "1"
      Height          =   255
      Left            =   900
      TabIndex        =   1
      Top             =   60
      Width           =   975
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Population:"
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   795
   End
End
Attribute VB_Name = "frmSV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub CalculateSV()
  lstBusinesses.Clear
  AddListItem "Shoemakers          ", 150
  AddListItem "Furriers            ", 250
  AddListItem "Maidservants        ", 250
  AddListItem "Tailors             ", 250
  AddListItem "Barbers             ", 350
  AddListItem "Unlicensed doctors  ", 350
  AddListItem "Jewelers            ", 400
  AddListItem "Taverns/Restaurants ", 400
  AddListItem "Old-Clothes         ", 400
  AddListItem "Pastrycooks         ", 500
  AddListItem "Masons              ", 500
  AddListItem "Carpenters          ", 550
  AddListItem "Mercers             ", 700
  AddListItem "Coopers             ", 700
  AddListItem "Bakers              ", 800
  AddListItem "Watercarriers       ", 850
  AddListItem "Scabbardmakers      ", 850
  AddListItem "Wine-Sellers        ", 900
  AddListItem "Hatmakers           ", 950
  AddListItem "Saddlers            ", 1000
  AddListItem "Chicken Butchers    ", 1000
  AddListItem "Pursemakers         ", 1100
  AddListItem "Woodsellers         ", 2400
  AddListItem "Arcane Supply Seller", 2800
  AddListItem "Bookbinders         ", 3000
  AddListItem "Butchers            ", 1200
  AddListItem "Fishmongers         ", 1200
  AddListItem "Beer-Sellers        ", 1400
  AddListItem "Buckle Makers       ", 1400
  AddListItem "Plasterers          ", 1400
  AddListItem "Spice Merchants     ", 1400
  AddListItem "Blacksmiths         ", 1500
  AddListItem "Painters            ", 1500
  AddListItem "Licensed Doctors    ", 1700
  AddListItem "Roofers             ", 1800
  AddListItem "Locksmiths          ", 1900
  AddListItem "Bathers             ", 1900
  AddListItem "Ropemakers          ", 1900
  AddListItem "Inns                ", 2000
  AddListItem "Tanners             ", 2000
  AddListItem "Copyists            ", 2000
  AddListItem "Sculptors           ", 2000
  AddListItem "Rugmakers           ", 2000
  AddListItem "Harness-Makers      ", 2000
  AddListItem "Bleachers           ", 2100
  AddListItem "Hay Merchants       ", 2300
  AddListItem "Glovemakers         ", 2400
  AddListItem "Woodcarvers         ", 2400
  AddListItem "Booksellers         ", 6300
  AddListItem "Illuminators        ", 3900
  AddListItem "Nobles              ", 200
  AddListItem "Lawyer (Advocate)   ", 650
  AddListItem "Clergymen           ", 40
  AddListItem "Priest              ", 25 * 40
  AddListItem "Lawmen              ", 150
  lstBusinesses.AddItem "Avian Livestock     " & Chr(9) & Format(CLng(lblPopulation.Caption) * 2.2 * 0.68, "#,##0")
  lstBusinesses.AddItem "Mammalian Livestock " & Chr(9) & Format(CLng(lblPopulation.Caption) * 2.2 * 0.32, "#,##0")
  lblArea.Caption = Format(CLng(lblPopulation.Caption) / 38850, "#,##0.0")
  
  
  ' Power Center
  Select Case Int(Rnd * 20) + 1
    Case 1
      lblPowerCenter.Caption = "Monstrous"
    Case 2 To 13
      lblPowerCenter.Caption = "Conventional"
    Case 14 To 18
      lblPowerCenter.Caption = "Nonstandard"
    Case 19 To 20
      lblPowerCenter.Caption = "Magical"
  End Select
  
  Select Case Int(Rnd * 100) + 1
    Case 1 To 35
      lblAlignment.Caption = "Lawful Good"
    Case 36 To 39
      lblAlignment.Caption = "Neutral Good"
    Case 40 To 41
      lblAlignment.Caption = "Chaotic Good"
    Case 42 To 61
      lblAlignment.Caption = "Lawful Neutral"
    Case 62 To 63
      lblAlignment.Caption = "Neutral"
    Case 64
      lblAlignment.Caption = "Chaotic Neutral"
    Case 65 To 90
      lblAlignment.Caption = "Lawful Evil"
    Case 91 To 98
      lblAlignment.Caption = "Neutral Evil"
    Case 99 To 100
      lblAlignment.Caption = "Chaotic Evil"
    
  End Select
  
End Sub


Private Sub AddListItem(strCaption As String, sv As Long)
  Dim pop As Long
  Dim sngval As Single
  pop = CLng(lblPopulation.Caption)
  
  With lstBusinesses
    If pop >= sv Then
      .AddItem strCaption & Chr(9) & Format(pop / sv, "0")
    Else
      sngval = Rnd
      If sngval < CSng(pop) / CSng(sv) Then
        .AddItem strCaption & Chr(9) & "1 poor quality"
      Else
        '.AddItem strCaption & Chr(9) & "0"
      End If
      '.AddItem strCaption & Chr(9) & Format(CSng(pop) / CSng(sv), "0%") & " chance"
    End If
  End With

End Sub


Private Sub Form_Load()
  If gcblnOGL = True Then
    Label18.Visible = False
    Label21.Visible = False
    lblPowerCenter.Visible = False
    lblAlignment.Visible = False
    lstBusinesses.top = 420
    lstBusinesses.Height = 5940
  End If
End Sub
