VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmStarSystem 
   Caption         =   "Random Planetary System Generator"
   ClientHeight    =   4380
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7065
   Icon            =   "frmStarSystem.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4380
   ScaleWidth      =   7065
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkHabitable 
      Caption         =   "Habitable"
      Height          =   315
      Left            =   4500
      TabIndex        =   7
      Top             =   2820
      Value           =   1  'Checked
      Width           =   1035
   End
   Begin VB.PictureBox Picture1 
      Height          =   375
      Left            =   720
      ScaleHeight     =   315
      ScaleWidth      =   795
      TabIndex        =   6
      Top             =   3540
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Export to Celestia"
      Height          =   375
      Left            =   1740
      TabIndex        =   5
      Top             =   2760
      Width           =   1575
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   4125
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6800
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            TextSave        =   "9:41 AM"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "4/28/2006"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pbSystems 
      Height          =   375
      Left            =   5640
      TabIndex        =   3
      Top             =   2760
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.TextBox txtNumSystems 
      Height          =   285
      Left            =   3360
      TabIndex        =   2
      Text            =   "1"
      Top             =   2805
      Width           =   1095
   End
   Begin MSComctlLib.TreeView tv1 
      Height          =   2535
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   6795
      _ExtentX        =   11986
      _ExtentY        =   4471
      _Version        =   393217
      Indentation     =   441
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Roll Star System"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   2760
      Width           =   1575
   End
End
Attribute VB_Name = "frmStarSystem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Systems As New CStars

Private Sub Command2_Click()
  Command1.Enabled = False
  txtNumSystems.Enabled = False
  tv1.Enabled = False
  Command2.Enabled = False
  Me.MousePointer = vbHourglass
  
  CelestiaExport
  
  Command1.Enabled = True
  Command2.Enabled = True
  txtNumSystems.Enabled = True
  tv1.Enabled = True
  Me.MousePointer = vbDefault

End Sub

Private Sub Form_Load()
  
  Randomize Timer
  Me.Width = 7335
  Me.Height = 7665
  Form_Resize
End Sub

Private Sub Command1_Click()
  Dim i As Long
  Dim yn As VbMsgBoxResult
  
  
  Do
  
    Set Systems = Nothing
    Set Systems = New CStars
    
    Command1.Enabled = False
    txtNumSystems.Enabled = False
    tv1.Enabled = False
    Command2.Enabled = False
    Me.MousePointer = vbHourglass
    
    pbSystems.min = 0
    pbSystems.max = CLng(txtNumSystems.Text)
    pbSystems.Value = 0
    
    For i = 1 To CLng(txtNumSystems.Text)
      
      Systems.Add
      
      Systems(i).RollStar
      
      'Systems(i).SpectralType = "G" & CStr(Int(Rnd * 10) + 1)
      'Systems(i).LuminosityClass = "V"
      
      Systems(i).LookupStar
      Systems(i).Tag = "HIP " & CStr(i + 600000)
      
      If Systems(i).bHasHabitableZone = True Then
        Systems(i).RollOrbitingBodies 0
        
        If chkHabitable.Value = Checked Then
          Systems(i).RollHabitableOrbitingBody 0 ' Roll 1 earthlike planet
        End If
        
        Systems(i).RollCometsAndAsteroidSystems
      End If
      Systems(i).DistanceLY = Rnd * 6000
      Systems(i).RightAscension = Rnd * 360
      Systems(i).DecDeg = Rnd * 180 - 90
      Systems(i).AppMag = Systems(i).AbsMag + 5 * log(Systems(i).DistanceLY / 32.6) / log(10)
      
      pbSystems.Value = i
      StatusBar1.Panels(1).Text = CStr(i) & " of " & CStr(txtNumSystems.Text)
      DoEvents
    Next i
    
    
    DisplaySystemData
    
    
    For i = 1 To tv1.Nodes.Count
      If tv1.Nodes(i).BackColor = vbGreen Then Exit Do
    Next i
    
    DoEvents
    
    If chkHabitable.Value = Unchecked Then Exit Do
  
  Loop While tv1.Nodes.Count > 0
  
  
  'yn = MsgBox("Export system?", vbQuestion + vbYesNo)
  yn = vbNo
  If yn = vbYes Then
    CelestiaExport
  End If

  Command1.Enabled = True
  Command2.Enabled = True
  txtNumSystems.Enabled = True
  tv1.Enabled = True
  Me.MousePointer = vbDefault
End Sub


Private Sub Form_Resize()

  If Me.WindowState <> vbMinimized And Me.WindowState <> vbMaximized Then

    If Me.Height < 1650 Then Me.Height = 1650
    If Me.Width < 4800 Then Me.Width = 4800
    tv1.Height = Me.Height - 1350
    tv1.Width = Me.Width - 360
    pbSystems.Width = Me.Width - 5910
    pbSystems.top = Me.Height - 1200
    Command1.top = Me.Height - 1200
    Command2.top = Me.Height - 1200
    txtNumSystems.top = Me.Height - 1200
    chkHabitable.top = Me.Height - 1200
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set Systems = Nothing
End Sub

Private Sub txtNumSystems_LostFocus()
  If Not IsNumeric(txtNumSystems.Text) Then
    txtNumSystems.Text = 1
    MsgBox "Number of systems must be an integer greater than 0.", vbCritical
  ElseIf CLng(txtNumSystems.Text) < 1 Then
    txtNumSystems.Text = 1
    MsgBox "Number of systems must be an integer greater than 0.", vbCritical
  End If
End Sub


Public Sub DisplaySystemData()
  Dim sysctr As Long
  
  tv1.Nodes.Clear
  For sysctr = 1 To Systems.Count
    AddNode "", tvwLast, Systems(sysctr)
  Next sysctr
  
  
  tv1.Nodes(1).Expanded = True
  'tv1.Nodes(tv1.Nodes(1).Key & "OrbitingBodies").Expanded = True
  'For sysctr = 1 To tv1.Nodes.Count
  '  tv1.Nodes(sysctr).Expanded = True
  'Next sysctr
End Sub


Private Sub AddNode(BaseNodeKey As String, Relationship As TreeRelationshipConstants, star As CStar)
  
  Dim Pctr As Long
  Static nodectr As Long
  Dim tagstring As String
  
  With star
    nodectr = nodectr + 1
    
    If Len(.ShortName) = 0 Then .ShortName = .Tag
    
    tagstring = CStr(nodectr) & .Tag
    
    If Len(BaseNodeKey) > 0 Then
      'tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName
    
    
      If .StarType Like "Star" Or .StarType Like "White Dwarf" Or _
        .StarType Like "Neutron Star" Or .StarType Like "Brown Dwarf" Then
        
        tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " fire world)"
      
      ElseIf .StarType Like "Jovian" Then
        tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " air world)"
      
      ElseIf .SpectralType Like "Panthalassic" Then
        tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " water world)"
      
      ElseIf .StarType Like "Black Hole" Then
        tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " portal to Negative Material Plane)"
      
      Else
        tv1.Nodes.Add BaseNodeKey, Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " earth world)"
      End If
      
    Else
      If .StarType Like "Star" Or .StarType Like "White Dwarf" Or _
        .StarType Like "Neutron Star" Or .StarType Like "Brown Dwarf" Then
        
        tv1.Nodes.Add , Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " fire world)"
      
      ElseIf .StarType Like "Jovian" Then
        tv1.Nodes.Add , Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " air world)"
      
      ElseIf .SpectralType Like "Panthalassic" Then
        tv1.Nodes.Add , Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " water world)"
      
      ElseIf .StarType Like "Black Hole" Then
        tv1.Nodes.Add , Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " portal to Negative Material Plane)"
      
      Else
        tv1.Nodes.Add , Relationship, tagstring, .ShortName & _
          " (Size " & .SizeClass & " earth world)"
      End If
    End If
     
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "StarType", "Type: " & .StarType & ", " & _
      .SpectralType & .LuminosityClass


    
    
    
    If .MassSol < 1 / 100 Then
      tv1.Nodes.Add tagstring, tvwChild, tagstring & "MassSol", "Mass: " & _
        Format(.MassKg, "0.0####e+0") & " kg (" & Format(.MassSol, "0.0###e+0") & " solar masses)"
    Else
      tv1.Nodes.Add tagstring, tvwChild, tagstring & "MassSol", "Mass: " & _
        Format(.MassKg, "0.0####e+0") & " kg (" & Format(.MassSol, "0.0###") & " solar masses)"
    End If



    If .RadiusSol < 1 / 1000 Then
      tv1.Nodes.Add tagstring, tvwChild, tagstring & "RadiusSol", "Radius: " & _
        Format(.RadiusM / 1000, "#,##0") & " km (" & Format(.RadiusSol, "0.0####e+0") & " Solar radii)"
    Else
      tv1.Nodes.Add tagstring, tvwChild, tagstring & "RadiusSol", "Radius: " & _
        Format(.RadiusM / 1000, "#,##0") & " km (" & Format(.RadiusSol, "0.0####") & " Solar radii)"
    End If
    
    
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "DiamMi", "Diameter: " & _
      Format(.DiameterMi, "#,###,###,##0") & " mi"
    
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "SurfaceTempK", _
      "Surface Temperature: " & Format(.SurfaceTemperatureK, "#,##0") & " K"

    
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "SurfaceGravG", "Surface gravity: " & _
      Format(.SurfaceGravityG, "#,##0.0") & " g"
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "DensityKgM3", "Density: " & _
      Format(.DensityKgM3, "#,##0.0") & " kg/m^3"
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "StarData", "Stellar Data"
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "PlanetData", "Planetary Data"
      tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "Albedo", "Albedo: " & _
        Format(.Albedo, "0.00")
      tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "RotationPeriod", _
        "Rotation period: " & Format(.RotationPeriod, "0.00") & " hr"
      tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "RotationOffset", _
        "Rotation offset: " & Format(.RotationOffset, "0.00") & " degrees"
      tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "Obliquity", _
        "Obliquity: " & Format(.obliquity, "0.00") & " degrees"
      tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "Oblateness", _
        "Oblateness: " & Format(.Oblateness, "0.000E+0")

    
      tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "AbsMag", _
        "Absolute Magnitude: " & Format(.AbsMag, "#0.00")
      tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "AppMag", _
        "Apparent Magnitude: " & Format(.AppMag, "#0.00")
    
    
    If .bHasHabitableZone = True Then
      If .HabitableZoneMinAU < 1 / 100 Then
        tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MinHZ", _
          "Minimum Habitable Zone: " & Format(.HabitableZoneMinAU * AU2km, "0.00#e+0") & " km"
        tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MaxHZ", _
          "Maximum Habitable Zone: " & Format(.HabitableZoneMaxAU * AU2km, "0.00#e+0") & " km"
      Else
        tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MinHZ", _
          "Minimum Habitable Zone: " & Format(.HabitableZoneMinAU, "0.000") & " AU"
        tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MaxHZ", _
          "Maximum Habitable Zone: " & Format(.HabitableZoneMaxAU, "0.000") & " AU"
      End If
    Else
      tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MinHZ", _
        "Minimum Habitable Zone: None"
      tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "MaxHZ", _
        "Maximum Habitable Zone: None"
    End If
    
    tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "RA", _
      "Right Ascension: " & Format(.RightAscension, "0.00") & " degrees"
    
    tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "Dec", _
      "Declination: " & Format(.DecDeg, "0.00") & " degrees"
    
    tv1.Nodes.Add tagstring & "StarData", tvwChild, tagstring & "Distance", _
      "Distance (LY): " & Format(.DistanceLY, "#,##0.0") & " light-years"



    tv1.Nodes.Add tagstring, tvwChild, tagstring & "OrbitalData", "Orbital Data"
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataEpoch", _
        "Epoch: " & Format(.OrbitalData.epoch, "0000000.0")
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataSMA", _
        "a: " & Format(.OrbitalData.SemiMajorAxisAU, "##0.0#") & " AU (" & _
        Format(.OrbitalData.SemiMajorAxisAU * AU2km, "#,##0") & " km)"
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataeccentricity", _
        "Eccentricity: " & Format(.OrbitalData.eccentricity, "0.0##")
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataQuantumN", _
        "Quantum numbers: n = " & Format(.OrbitalData.lN, "##0") & ", k = " & _
        Format(.OrbitalData.lK, "##0")
      
      'tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDatak", "k: " & _
        Format(.OrbitalData.k, "0000000.0")
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataInclination", _
        "Inclination: " & Format(.OrbitalData.iDeg, "##0.0#") & " degrees"
      
      'tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataMeanMotion", _
        "Mean Motion: " & Format(.OrbitalData.MeanMotion, "##0.0#E+0") & " s^-2"
        
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataAscendingNode", _
        "Ascending node: " & Format(.OrbitalData.AscendingNode, "##0.0#") & " degrees"
      
      'tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataTimeOfPericenter", _
        "Time of Pericenter: " & Format(.OrbitalData.TimeOfPericenter, "##0.0#")
      
      If .OrbitalData.PeriodYr > 1 Then
        tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataPeriodYr", _
          "Period: " & Format(.OrbitalData.PeriodYr, "##0.0#") & " yr"
      Else
        tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataPeriodYr", _
          "Period: " & Format(.OrbitalData.PeriodYr * 365.25, "##0.0#") & " days"
      End If
      
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "OrbitalDataLongOfPericenter", _
        "Longitude of Pericenter: " & Format(.OrbitalData.LongOfPericenter, "##0.0#") & " degrees"
     
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & _
        "OrbitalDataPericenterDistance", "Pericenter Distance: " & _
        Format(.OrbitalData.PericenterDistance, "##0.0#") & " AU (" & _
        Format(.OrbitalData.PericenterDistance * AU2km, "0.0E+0##") & " km)"
        
      tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & _
        "OrbitalDataArgumentOfPericenter", "Argument of Pericenter: " & _
        Format(.OrbitalData.ArgumentOfPericenter, "##0.0#") & " degrees"
    
      If .IsHabitable = True Then
        tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "IsHabitable", _
          "This planet is habitable."
        tv1.Nodes(tagstring).Text = "HABITABLE " & tv1.Nodes(tagstring).Text
        tv1.Nodes(tagstring).BackColor = vbGreen
        Beep
      Else
        tv1.Nodes.Add tagstring & "OrbitalData", tvwChild, tagstring & "IsHabitable", _
          "This planet is uninabitable."
          
      End If
    
    tv1.Nodes.Add tagstring & "PlanetData", tvwChild, tagstring & "Composition", "Composition"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionCarbon", _
        "Carbon: " & Format(.composition.Carbon, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionHydrogen", _
        "Hydrogen: " & Format(.composition.Hydrogen, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionFayalite", _
        "Fayalite: " & Format(.composition.Fayalite, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionNickel", _
        "Nickel: " & Format(.composition.Nickel, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionIron", _
        "Iron: " & Format(.composition.Iron, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionIce", _
        "Ice: " & Format(.composition.Ice, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionMethane", _
        "Methane: " & Format(.composition.Methane, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionNickelIron", _
        "Nickel-Iron Alloy: " & Format(.composition.NickelIron, "##0.0#") & "%"
      
      tv1.Nodes.Add tagstring & "Composition", tvwChild, tagstring & "CompositionNickelIronRatio1", _
        "Nickel-Iron is " & Format(.composition.NickelIronRatio, "##0.0#") & "% Nickel"


    
    
    tv1.Nodes.Add tagstring, tvwChild, tagstring & "OrbitingBodies", "Orbiting Bodies"
    For Pctr = 1 To .OrbitingBodies.Count
      AddNode tagstring & "OrbitingBodies", tvwChild, .OrbitingBodies(Pctr)
    Next Pctr
  End With
  
  
End Sub


Public Sub CelestiaExport()
  Dim sysctr As Long
  Dim hipctr As Long
  Dim log As New CLogAndIniRoutines
  
  hipctr = 600000
  pbSystems.Value = 0
  
  If log.FileExist(App.Path & "\GeneratedStars.stc") = True Then
    Kill App.Path & "\GeneratedStars.stc"
  End If
  If log.FileExist(App.Path & "\GeneratedPlanets.ssc") = True Then
    Kill App.Path & "\GeneratedPlanets.ssc"
  End If
  
  For sysctr = 1 To Systems.Count
    WriteStar Systems(sysctr)
    WriteSystem Systems(sysctr), -1
    pbSystems.Value = sysctr
    DoEvents
  Next sysctr
  
  Set log = Nothing
End Sub

Public Sub WriteStar(star As CStar)
  Dim log As New CLogAndIniRoutines
  
  Static nodectr As Long
  Dim tagstring As String
  
  'log.strLogFileName = App.Path & "\GeneratedStars.stc"
  log.strLogFileName = "C:\Program Files\Celestia\extras\GeneratedSystem\GeneratedStars.stc"
  log.bWriteLog = True
  
  With star
    nodectr = nodectr + 1
    
    log.writeLogEntry "# HIP " & CStr(nodectr + 600000)
    log.writeLogEntry CStr(nodectr + 600000) & " {"
    log.writeLogEntry "   RA " & Format(.RightAscension, "0.00000")
    log.writeLogEntry "   Dec " & Format(.DecDeg, "0.00000")
    log.writeLogEntry "   Distance " & CStr(.DistanceLY)
    log.writeLogEntry "   SpectralType """ & .SpectralType & .LuminosityClass & """"
    log.writeLogEntry "   AppMag " & Format(.AppMag, "#0.00")
    log.writeLogEntry "}"
    log.writeLogEntry " "
  End With
  Set log = Nothing

End Sub

Public Sub WriteSystem(star As CStar, intLevel As Integer)
  
  Dim Pctr As Long
  Dim log As New CLogAndIniRoutines
  Dim tmp As Long
  Dim a As Long
  
  Static nodectr As Long
  Dim tagstring As String
  
  log.strLogFileName = "C:\Program Files\Celestia\extras\GeneratedSystem\GeneratedPlanets.ssc"
  'log.strLogFileName = App.Path & "\GeneratedPlanets.ssc"
  log.bWriteLog = True
  
  With star
    If intLevel >= 0 Then
      nodectr = nodectr + 1
      If Len(.ShortName) = 0 Then .ShortName = "Object " & CStr(nodectr)
      
      tagstring = .Tag
      
      ' Change the "_" to "/"
      Do
        tmp = InStr(tagstring, "_")
        If tmp > 0 Then
          tagstring = LEFT(tagstring, tmp - 1) & "/" & Right(tagstring, Len(tagstring) - tmp)
        End If
        DoEvents
      Loop Until tmp = 0
      ' take off the last part
      
      tagstring = StrReverse(tagstring)
      tmp = InStr(tagstring, "/")
      tagstring = StrReverse(tagstring)
      tagstring = LEFT(tagstring, Len(tagstring) - tmp)
      
      log.writeLogEntry """" & .ShortName & """ """ & tagstring & """"
      log.writeLogEntry "{"
      
      If intLevel = 0 Then
        If .StarType Like "Comet" Then
          log.writeLogEntry "   Class ""comet"""
        ElseIf .StarType Like "Asteroid" Or .StarType Like "Planetoid" Then
          log.writeLogEntry "   Class ""asteroid"""
        Else
          log.writeLogEntry "   Class ""planet"""
        End If
      
      Else
        log.writeLogEntry "   Class ""moon"""
      End If
      
      log.writeLogEntry "   Radius " & Format(Abs(.RadiusM / 1000), "#0.0#")
      log.writeLogEntry "   Texture """ & GetTexture(star) & """"
  
      If .Oblateness > 0 Then
        log.writeLogEntry "   Oblateness " & Format(.Oblateness, "0.000##########")
      End If
  
      If .Color.Red > 0 And .Color.Green > 0 And .Color.Blue > 0 Then
        log.writeLogEntry "   Color [ " & CStr(.Color.Red) & " " & CStr(.Color.Green) & " " & _
          CStr(.Color.Blue) & " ]"
      End If
      
      'If .MassKg > 1.3E+23 Then
  
        'If .SpectralType Like "EoGaian" Or .SpectralType Like "Europan" _
          Or .SpectralType Like "LithicGelidian" Or .SpectralType Like "Panthalassic" _
          Or .SpectralType Like "Gaian" Or .SpectralType Like "MesoGaian" _
          Or .SpectralType Like "Cytherian" Or .SpectralType Like "Moist Greenhouse" _
          Or .SpectralType Like "Arean" Or .StarType Like "Jovian" Or .StarType Like "Brown Dwarf" _
          Or .StarType Like "Star" Or .StarType Like "White Dwarf" Then
  
          If .Atmosphere.Height > 0 Then
            log.writeLogEntry "   HazeColor [ " & CStr(.HazeColor.Red) & " " & CStr(.HazeColor.Green) & " " & _
              CStr(.HazeColor.Blue) & " ]"
              
            log.writeLogEntry "   HazeDensity " & CStr(.HazeDensity)
    
    
            log.writeLogEntry " "
            log.writeLogEntry "   Atmosphere {"
            log.writeLogEntry "      Height " & CStr(CLng(.Atmosphere.Height / 1000))
            log.writeLogEntry "      Lower [ " & CStr(.Atmosphere.Lower.Red) & " " & _
              CStr(.Atmosphere.Lower.Green) & " " & CStr(.Atmosphere.Lower.Blue) & " ]"
              
            log.writeLogEntry "      Upper [ " & CStr(.Atmosphere.Upper.Red) & " " & _
              CStr(.Atmosphere.Upper.Green) & " " & CStr(.Atmosphere.Upper.Blue) & " ]"
              
            log.writeLogEntry "      Sky [ " & CStr(.Atmosphere.Sky.Red) & " " & _
              CStr(.Atmosphere.Sky.Green) & " " & CStr(.Atmosphere.Sky.Blue) & " ]"
            
            If .Atmosphere.Sunset.Red > 0 And .Atmosphere.Sunset.Green > 0 And .Atmosphere.Sunset.Blue > 0 Then
              log.writeLogEntry "      Sunset [ " & CStr(.Atmosphere.Sunset.Red) & " " & _
                CStr(.Atmosphere.Sunset.Green) & " " & CStr(.Atmosphere.Sunset.Blue) & " ]"
            End If
    
            If Not (.SpectralType Like "Arean") Then
              If Len(.Atmosphere.CloudMap) > 0 Then
                log.writeLogEntry "      CloudMap """ & .Atmosphere.CloudMap & """"
                log.writeLogEntry "      CloudHeight " & CStr(.Atmosphere.CloudHeight)
                log.writeLogEntry "      CloudSpeed " & CStr(.Atmosphere.CloudSpeed)
              End If
            End If
            log.writeLogEntry "   }"
          End If
          
          log.writeLogEntry " "
        'End If
      'End If
      
      log.writeLogEntry "   EllipticalOrbit"
      log.writeLogEntry "   {"
        log.writeLogEntry "      Epoch " & Format(.OrbitalData.epoch, "0000000.0")
        
        If intLevel = 0 Then
          log.writeLogEntry "      Period " & Format(.OrbitalData.PeriodYr, "##0.0#")
          log.writeLogEntry "      SemiMajorAxis " & Format(.OrbitalData.SemiMajorAxisAU, "##0.0#")
        Else
          log.writeLogEntry "      Period " & Format(.OrbitalData.PeriodYr * 365.25, "##0.0#")
          log.writeLogEntry "      SemiMajorAxis " & Format(.OrbitalData.SemiMajorAxisAU * AU2km, "###0.0#")
        End If
        log.writeLogEntry "      Eccentricity " & Format(.OrbitalData.eccentricity, "0.0##")
        log.writeLogEntry "      Inclination " & Format(.OrbitalData.iDeg, "##0.0#")
        log.writeLogEntry "      AscendingNode " & Format(.OrbitalData.AscendingNode, "##0.0#")
        log.writeLogEntry "      ArgOfPericenter " & Format(.OrbitalData.ArgumentOfPericenter, "##0.0#")
        log.writeLogEntry "      LongOfPericenter " & Format(.OrbitalData.LongOfPericenter, "##0.0#")
        log.writeLogEntry "      MeanAnomaly " & Format(.OrbitalData.MeanAnomaly, "##0.0###")
      
      log.writeLogEntry "   }"
      log.writeLogEntry " "
      
      log.writeLogEntry "   Albedo " & Format(.Albedo, "0.00")
      log.writeLogEntry "   RotationPeriod " & Format(.RotationPeriod, "#0.0#")
      log.writeLogEntry "   RotationOffset " & Format(.RotationOffset, "#0.0#")
      log.writeLogEntry "   Obliquity " & Format(.obliquity, "##0.0#")
      
      If .StarType Like "Star" Or .StarType Like "Brown Dwarf" Or .StarType Like "Neutron Star" _
        Or .StarType Like "White Dwarf" Then
        
        log.writeLogEntry "   Emissive true"
        
      End If
      
      If Len(.RingSystem.Texture) > 0 And .StarType Like "Jovian" Or .StarType Like "Brown Dwarf" Or Int(Rnd * 100) + 1 = 1 Then
        log.writeLogEntry "   Rings {"
        log.writeLogEntry "      Inner " & Format(.RingSystem.Inner, "##0")
        log.writeLogEntry "      Outer " & Format(.RingSystem.Outer, "##0")
        log.writeLogEntry "      Texture """ & .RingSystem.Texture & """"
    
        log.writeLogEntry "      Color [ " & Format(.RingSystem.Color.Red, "0.00") & " " & _
          Format(.RingSystem.Color.Green, "0.00") & " " & Format(.RingSystem.Color.Blue, "0.00") & " ]"
        
        log.writeLogEntry "   }"
      End If
  
      If .SpecularPower > 0 Then
        log.writeLogEntry "   SpecularPower " & CStr(.SpecularPower)
      End If
      
      If .SpecularColor.Red > 0 And .SpecularColor.Green > 0 And .SpecularColor.Blue > 0 Then
        log.writeLogEntry "   SpecularColor [ " & CStr(.SpecularColor.Red) & " " & CStr(.SpecularColor.Green) & " " & _
          CStr(.SpecularColor.Blue) & " ]"
      End If
      
      log.writeLogEntry "}"
      log.writeLogEntry " "
    End If
      
    DoEvents
    For Pctr = 1 To .OrbitingBodies.Count
      WriteSystem .OrbitingBodies(Pctr), intLevel + 1
    Next Pctr
  End With
  
End Sub


Public Function GetTexture(star As CStar) As String
  
  Dim log As New CLogAndIniRoutines
  
  Select Case star.StarType
    Case "Star"
      Select Case star.SurfaceTemperatureK
        Case Is > 33000
          GetTexture = "ostar.jpg"
        Case 10500 To 33000
          GetTexture = "bstar.jpg"
        Case 7390 To 10500
          GetTexture = "astar.jpg"
        Case 6115 To 7390
          GetTexture = "fstar.jpg"
        Case 5376 To 6115
          GetTexture = "gstar.jpg"
        Case 3877 To 5376
          GetTexture = "kstar.jpg"
        Case 2510 To 3877
          GetTexture = "mstar.jpg"
        Case Else
          GetTexture = "browndwarf.jpg"
      End Select
      
    Case "White Dwarf"
      GetTexture = "astar.jpg"
      
    Case "Neutron Star"
      GetTexture = "ostar.jpg"
      
    Case "Brown Dwarf"
      GetTexture = "browndwarf.jpg"
    
    Case "Jovian"
      Select Case LEFT(star.SpectralType, 1)
        Case "C"
          GetTexture = "neptune.jpg"
        Case "B"
          GetTexture = "saturn.jpg"
        Case "E"
          GetTexture = "jupiter.jpg"
        Case "S"
          GetTexture = "gasgiant.jpg"
        Case "M"
          GetTexture = "jupiterlike.jpg"
      End Select
      
    Case "Terrestrial"
      Select Case star.SpectralType
        Case "Hephaestian"
          GetTexture = "io.jpg"
        Case "Ferrinian"
          GetTexture = "mercury.jpg"
        Case "Hermian"
          GetTexture = "mercury.jpg"
        Case "Selenian"
          GetTexture = "moon.jpg"
        Case "EoGaian"
          GetTexture = "io.jpg"
        Case "MesoGaian"
          GetTexture = "comporellon.jpg"
        Case "Arean"
          GetTexture = "mars.jpg"
        Case "Cytherean"
          GetTexture = "venussurface.jpg"
        Case "Moist Greenhouse"
          GetTexture = "comporellon.jpg"
        Case "Gaian"
          GetTexture = "toril.jpg"
        Case "LithicGelidian"
          GetTexture = "callisto.jpg"
        Case "Europan"
          GetTexture = "europa.jpg"
        Case "Panthalassic"
        
          If star.SurfaceTemperatureK < 273 Then
            GetTexture = "ice-world.jpg"
          Else
            GetTexture = "water-world.jpg"
          End If
          
        Case "Stygian"
          GetTexture = "blackmoon.jpg"
      End Select
    
    Case "Planetoid"
      GetTexture = "asteroid.jpg"
    Case "Asteroid"
      GetTexture = "asteroid.jpg"
    Case "Comet"
      GetTexture = "asteroid.jpg"
  End Select
  
  If Len(GetTexture) = 0 Then
    GetTexture = "mstar.jpg"
  End If
  

  'If star.StarType Like "Terrestrial" Then
    'star.GenerateTerrestrialMap
    'If log.FileExist(App.Path & "\Textures\hires\" & star.Tag & ".jpg") = True Then
    '  GetTexture = star.Tag & ".jpg"
    'End If
  'End If

End Function



' To generate a habitable earthlike world, the following must be met:
' 1. It must lie in the habitable zone of the primary star.
' 2. No fire worlds can be in the system other than the primary.
' 3. Some jovian planets must exist in stable orbits in the outer system.
' 4. Gravity must be between G/3 and 3G. (m=146969167894*r^2) == 1G
' 5. The primary's apparent magnitude is optimally -26.72. (-26.83 to -26.72)



