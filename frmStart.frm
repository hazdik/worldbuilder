VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Begin VB.Form frmStart 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "World Generation Suite"
   ClientHeight    =   480
   ClientLeft      =   150
   ClientTop       =   840
   ClientWidth     =   4965
   Icon            =   "frmStart.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   480
   ScaleWidth      =   4965
   StartUpPosition =   3  'Windows Default
   Begin ComCtl3.CoolBar CoolBar1 
      Height          =   450
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   794
      BandCount       =   2
      _CBWidth        =   4935
      _CBHeight       =   450
      _Version        =   "6.7.9782"
      Child1          =   "tbGenerate"
      MinWidth1       =   1800
      MinHeight1      =   390
      Width1          =   765
      NewRow1         =   0   'False
      Child2          =   "tbAdmin"
      MinWidth2       =   405
      MinHeight2      =   390
      Width2          =   1710
      NewRow2         =   0   'False
      Begin MSComctlLib.Toolbar tbAdmin 
         Height          =   390
         Left            =   3330
         TabIndex        =   2
         Top             =   30
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         ImageList       =   "imlIcons"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "EncounterBuilder"
               Object.ToolTipText     =   "Build encounters"
               Object.Tag             =   "EncounterBuilder"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DiceRoller"
               Object.ToolTipText     =   "Dice roller"
               Object.Tag             =   "DiceRoller"
               ImageIndex      =   7
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar tbGenerate 
         Height          =   390
         Left            =   165
         TabIndex        =   1
         Top             =   30
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         ImageList       =   "imlIcons"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   8
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Stars"
               Object.ToolTipText     =   "Generate a star system"
               Object.Tag             =   "Stars"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Tec"
               Object.ToolTipText     =   "Run plate tectonics"
               Object.Tag             =   "Tec"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Kingdom"
               Object.ToolTipText     =   "Calculate kingdom demographics"
               Object.Tag             =   "Kingdom"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "City"
               Object.ToolTipText     =   "Generate a random city"
               Object.Tag             =   "City"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Encounters"
               Object.ToolTipText     =   "Generate an encounter table"
               Object.Tag             =   "Encounters"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Random Dungeon"
               Object.ToolTipText     =   "Random Dungeon"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "Treasure"
               Object.ToolTipText     =   "Treasure"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "NPCStats"
               ImageIndex      =   10
            EndProperty
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   2340
      Top             =   1080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":12FA
            Key             =   "Stars"
            Object.Tag             =   "Stars"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1454
            Key             =   "Tec"
            Object.Tag             =   "Tec"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":15AE
            Key             =   "Kingdom"
            Object.Tag             =   "Kingdom"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1708
            Key             =   "City"
            Object.Tag             =   "City"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1862
            Key             =   "Encounters"
            Object.Tag             =   "Encounters"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":19BC
            Key             =   "EncounterBuilder"
            Object.Tag             =   "EncounterBuilder"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1B16
            Key             =   "Dice"
            Object.Tag             =   "Dice"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1C70
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1DCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmStart.frx":1F24
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuFileExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuGenerate 
      Caption         =   "Generate"
      Begin VB.Menu mnuGenerateStarSystem 
         Caption         =   "Star System"
      End
      Begin VB.Menu mnuGenerateTec 
         Caption         =   "Planetary Topography and Climate"
      End
      Begin VB.Menu mnuGenerateKingdomDemographics 
         Caption         =   "Kingdom Demographics"
      End
      Begin VB.Menu mnuGenerateCity 
         Caption         =   "City"
      End
      Begin VB.Menu mnuGenerateDungeon 
         Caption         =   "Dungeon"
      End
      Begin VB.Menu mnuGenerateEncounterTable 
         Caption         =   "Encounter Table"
      End
      Begin VB.Menu mnuGenerateTreasure 
         Caption         =   "Treasure"
      End
      Begin VB.Menu mnuGenerateNPCStats 
         Caption         =   "NPC Stats"
      End
   End
   Begin VB.Menu mnuAdmin 
      Caption         =   "Admin"
      Begin VB.Menu mnuAdminEncounters 
         Caption         =   "Encounter Editor"
      End
      Begin VB.Menu mnuDiceRoller 
         Caption         =   "Dice Program"
      End
      Begin VB.Menu mnuAdminCalculateAveragesAndELs 
         Caption         =   "Calculate Encounter Averages and ELs"
      End
      Begin VB.Menu mnuAdminCalculateAllEncounterAveragesAndELs 
         Caption         =   "Calculate All Encounter Averages and ELs"
      End
      Begin VB.Menu mnuAdminCalculateMonsterDatabaseCompletion 
         Caption         =   "Calculate Monster Database Completion"
      End
      Begin VB.Menu mnuAdminGetEncounterFrequencies 
         Caption         =   "Get Encounter Frequencies"
      End
      Begin VB.Menu mnuAdminGetEncounterHabitats 
         Caption         =   "Get Encounter Habitats"
      End
      Begin VB.Menu mnuAdminGetMonFreqHist 
         Caption         =   "Get Monster Frequency Histogram"
      End
      Begin VB.Menu mnuAdminGetMonFreqHistCR 
         Caption         =   "Get Monster Frequency Histogram by CR"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "frmStart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()


  tbAdmin.Buttons(1).Visible = False
  mnuAdminEncounters.Visible = False
  
  ' if we have OGL only set to true then we need to disable some of this stuff
  If gcblnOGL = True Then
    mnuGenerateCity.Visible = False
    tbGenerate.Buttons(4).Visible = False
    'mnuAdmin.Visible = False
    tbGenerate.Buttons(8).Visible = False
    mnuGenerateNPCStats.Visible = False
  End If
End Sub

Private Sub mnuAdminCalculateAllEncounterAveragesAndELs_Click()
  CalculateAveragesAndELs True
End Sub

Private Sub mnuAdminCalculateAveragesAndELs_Click()
  CalculateAveragesAndELs
End Sub

Private Sub mnuAdminCalculateMonsterDatabaseCompletion_Click()
  CalculateMonsterDatabaseCompletion
End Sub

Private Sub mnuAdminEncounters_Click()
  frmEncounterBuilder.Show , Me
End Sub

Private Sub mnuAdminGetEncounterFrequencies_Click()
  GetEncounterFrequencies
End Sub

Private Sub mnuAdminGetEncounterHabitats_Click()
  GetEncounterHabitats
End Sub

Private Sub mnuAdminGetMonFreqHist_Click()
  GetMonsterFrequencyHistogram
End Sub

Private Sub mnuAdminGetMonFreqHistCR_Click()
  GetMonsterFrequencyHistogramCRTerrain
End Sub

Private Sub mnuDiceRoller_Click()
  frmDice.Show , Me
End Sub

Private Sub mnuFileExit_Click()
  Unload Me
End Sub

Private Sub mnuGenerateCity_Click()
  frmRandomCity.Show , Me
End Sub

Private Sub mnuGenerateDungeon_Click()
  frmRandomDungeon.Show , Me
End Sub

Private Sub mnuGenerateEncounterTable_Click()
  frmEncounterTable.Show , Me
End Sub

Private Sub mnuGenerateKingdomDemographics_Click()
  frmDemographics.Show , Me
End Sub

Private Sub mnuGenerateNPCStats_Click()
  frmDiceRollerCompact.Show , Me
End Sub

Private Sub mnuGenerateStarSystem_Click()
  frmStarSystem.Show , Me
End Sub

Private Sub mnuGenerateTec_Click()
  frmTec.Show , Me
End Sub

Private Sub mnuGenerateTreasure_Click()
  frmTreasure.Show , Me
End Sub

Private Sub mnuHelpAbout_Click()
  frmAbout.Show vbModal, Me
End Sub

Private Sub tbAdmin_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "EncounterBuilder"
      mnuAdminEncounters_Click
    Case "DiceRoller"
      mnuDiceRoller_Click
  End Select
End Sub

Private Sub tbGenerate_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Stars"
      mnuGenerateStarSystem_Click
    Case "Tec"
      mnuGenerateTec_Click
    Case "Kingdom"
      mnuGenerateKingdomDemographics_Click
    Case "City"
      mnuGenerateCity_Click
    Case "Encounters"
      mnuGenerateEncounterTable_Click
    Case "Random Dungeon"
      mnuGenerateDungeon_Click
    Case "Treasure"
      mnuGenerateTreasure_Click
    Case "NPCStats"
      mnuGenerateNPCStats_Click
  End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "Stars"
      mnuGenerateStarSystem_Click
    Case "Tec"
      mnuGenerateTec_Click
    Case "Kingdom"
      mnuGenerateKingdomDemographics_Click
    Case "City"
      mnuGenerateCity_Click
    Case "Encounters"
      mnuGenerateEncounterTable_Click
    Case "EncounterBuilder"
      mnuAdminEncounters_Click
  End Select
End Sub

