Attribute VB_Name = "modBitmap"
Option Explicit


Dim bytSingleBitArray() As Byte


'-----------------------------------------------------------------
'-------------------------- File Header  -------------------------
'-----------------------------------------------------------------
' NAME       : modBitmap.bas
' PROJECT ID : OSMD-CAS, DO-13 Open Skies
' DESCRIPTION: This file contains methods and functions for
'              manipulation of bitmap files.
' LIMITATIONS/CONSTRAINTS:
'              (1) Built and tested on Windows 2000.
'-----------------------------------------------------------------
' Public Properties:
'   None
'
' Public Methods:
'   createBMPImage
'
' MODIFICATION HISTORY:
'   B. Schneider/SAIC     10/21/03      Created file
'-----------------------------------------------------------------

'Bitmap file header, total 14 bytes
Type winBMPFileHeader
     strFileType As String * 2 'File type always 4D42h or "BM"
     lngFileSize As Long       'Size in bytes ussually 0 for uncompressed
     bytReserved1 As Integer   'Always 0
     bytReserved2 As Integer   'Always 0
     lngBitmapOffset As Long   'Starting position of image data in bytes
End Type

'Bitmap image header, total 40 bytes
Type BITMAPINFOHEADER
     biSize As Long          'Size of this header
     biWidth As Long         'Width of the image
     biHeight As Long        'Height of the image
     biPlanes As Integer     'Always 1
     byBitCount As Integer   'Number of bits per pixel 1, 4, 8, or 24
     biCompression As Long   '0 data is not compressed
     biSizeImage As Long     'Size of bitmap in bytes, typicaly 0 when uncompressed
     biXPelsPerMeter As Long 'Preferred resolution in pixels per meter
     biYPelsPerMeter As Long 'Preferred resolution in pixels per meter
     biClrUsed As Long       'Number of colors that are actually used (can be 0)
     biClrImportant As Long  'Which color is most important (0 means all of them)
End Type

'Bitmap palette, 4 bytes * 256 = 1024
Type BITMAPPalette
     lngBlue As Byte
     lngGreen As Byte
     lngRed As Byte
     lngReserved As Byte
End Type

'-----------------------------------------------------------------
'------------------------ Routine Header  ------------------------
'-----------------------------------------------------------------
' NAME       : createBMPImage
' DESCRIPTION: This routine creates an 8-bit bitmap file from the
'              passed in array of normalized pixel colors.  It fills
'              in the bitmap file header information, the information
'              header, and the color palette and then each color byte
'              into the file.  The end of each row is padded accordingly
'              to fill in odd ending bytes.
' REQUIREMENTS: 1.6, 1.6A, 5.3A
' EXTERNAL ROUTINES:  None
' PARAMETERS
' INPUTS:
'       strImageName        The bitmap file name that is created.
'       lngWidth            The width of the saved bitmap.
'       lngHeight           The height of the saved bitmap.
'       bytSingleBitArray   An array of color pixel data.
' OUTPUTS:
'       Bitmap file         The bitmap file is created with the
'                           name of the passed in argument strImageName.
'-----------------------------------------------------------------
Public Sub createBMPImage(ByVal strImageName As String, _
                            ByVal lngWidth As Long, _
                            ByVal lngHeight As Long, _
                            ByRef bytSingleBitArray() As Byte)
                                    'This was byte
'LOCAL
Dim BMPHeader As winBMPFileHeader   'The bitmap file header
Dim BMPInfo As BITMAPINFOHEADER     'The bitmap info header
Dim BMPPalette As BITMAPPalette     'The bitmap color palette
Dim bytPadding As Byte              'Padding for the end of each row
Dim lngEachColorPixel As Long       'Counter for each data point in the bitmap
Dim bytColor As Byte                'Pixel color
Dim dblCounter As Double            'Counter for loops
          
On Error GoTo ErrorHandler

    Open strImageName For Binary As #1
    
    'Load the bitmap file header
    BMPHeader.strFileType = "BM"       'File type always 4D42h or "BM"
    BMPHeader.lngFileSize = 0          'Size in bytes usually 0 for uncompressed
    BMPHeader.bytReserved1 = 0         'Always 0
    BMPHeader.bytReserved2 = 0         'Always 0
    BMPHeader.lngBitmapOffset = 1078   'Starting position of image data in bytes
                                 '= sizeof(BMPHeader) + sizeof(BMPInfo)it's 54 with
                                 'no color palette (24 bit bitmap).  It is an
                                 'offset of 1078 for an 8-bit bitmap
                                 'because of the color palette.

    'Save the bitmap file header
    Put #1, , BMPHeader
        
    'Load the bitmap Info header
    BMPInfo.biSize = 40           'Size of this header  sizeof(BMPInfo)
    BMPInfo.biWidth = lngWidth    'Width of the image in pixels
    BMPInfo.biHeight = lngHeight  'Height of the image in pixels
    BMPInfo.biPlanes = 1          'Always 1
    BMPInfo.byBitCount = 8        'Number of bits per pixel 1, 4, 8, or 24
    BMPInfo.biCompression = 0     '0, data is not compressed
    BMPInfo.biSizeImage = 0       'Size of bitmap in bytes, typicaly 0 when uncompressed
    BMPInfo.biXPelsPerMeter = 3740   'Preferred resolution in pixels per meter
    BMPInfo.biYPelsPerMeter = 3740   'Preferred resolution in pixels per meter
    BMPInfo.biClrUsed = 0         'Number of colors that are actually used (can be 0)
    BMPInfo.biClrImportant = 0    'Which color is most important (0 means all of them)
    
    'Save the bitmap info header to the file
    Put #1, , BMPInfo
     
    'Load the color palette
    For dblCounter = 0 To (BMPHeader.lngBitmapOffset - 54) / Len(BMPPalette) - 1
        BMPPalette.lngBlue = dblCounter
        BMPPalette.lngGreen = dblCounter
        BMPPalette.lngRed = dblCounter
        BMPPalette.lngReserved = 0
        
        'Write the color palette to the file
        Put #1, , BMPPalette
    Next dblCounter

    'Zero the counter, we're going to use it again
    dblCounter = 0

    'Find out how much padding there will be at the end of each data row (if any)
    bytPadding = 32 - ((BMPInfo.biWidth * BMPInfo.byBitCount) Mod 32)
    If bytPadding = 32 Then bytPadding = 0
    bytPadding = bytPadding \ BMPInfo.byBitCount
    
    'Walk though each pixel of the bitmap
    For lngEachColorPixel = 0 To UBound(bytSingleBitArray()) - 1
        
        'Write the data bit to the bitmap file
        Put #1, , bytSingleBitArray(lngEachColorPixel)

        'Increment the counter
        dblCounter = dblCounter + 1

        'If the counter has reached the end of the row then...
        If dblCounter = BMPInfo.biWidth Then
        
            'Each row of data must be divisible by 4, if not you need to pad the row.
            If BMPInfo.biWidth \ 4 <> 0 Then
                
                'Pad the end of the row the correct number of bytes
                For dblCounter = 1 To bytPadding
                    
                    bytColor = 0
                    'Write the pad to the bitmap file
                    Put #1, , bytColor

                Next dblCounter
                
                'Zero the counter to start a new row
                dblCounter = 0

            End If

        End If

    Next
    'Close the bitmap file
    Close #1
     
Exit Sub

ErrorHandler:
MsgBox "Error Writing Bitmap"
     'Call pDisplayError(vbOKOnly, "Error in pCreateBMPImage")
     Close #1
     
End Sub

