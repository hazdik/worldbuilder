Attribute VB_Name = "modD20Enums"
Option Explicit

Public Enum EGender
  G_MALE
  G_FEMALE
  G_NONE
  G_BOTH
End Enum

Public Enum EAlignment
  AL_LG
  AL_LN
  AL_LE
  AL_NG
  AL_TN
  AL_NE
  AL_CG
  AL_CN
  AL_CE
End Enum

Public Enum ESize
  SZ_FINE
  SZ_DIMINUTIVE
  SZ_TINY
  SZ_SMALL
  SZ_MEDIUM
  SZ_LARGE
  SZ_HUGE
  SZ_GARGANTUAN
  SZ_COLOSSAL
  SZ_COLOSSALPLUS
End Enum


