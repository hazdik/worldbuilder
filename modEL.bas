Attribute VB_Name = "modEL"
Option Explicit

Global db As CDB

Function GetEL(n() As Long, CR() As Single) As Single
  ' Calculates encounter level from lists of numbers of monsters and CRs
  ' N()  list of monster quantities
  ' CR() list of monster CRs
  ' Returns Encounter Level as a single

  ' EL = GetEL(N, CR)
  Dim iMonQty() As Long
  Dim iMonCR() As Single
  Dim iMonPwr() As Double
  Dim i As Long
  
  ReDim iMonPwr(UBound(n)) As Double
  iMonQty = n
  iMonCR = CR
  
  For i = 1 To UBound(n)
    iMonPwr(i) = iMonQty(i) * mCRtoPL(iMonCR(i))
    iMonPwr(0) = iMonPwr(0) + iMonPwr(i)
    iMonQty(0) = iMonQty(0) + iMonQty(i)
  Next i
  GetEL = CSng(mPLtoCR(iMonPwr(0))) 'Encounter Level
End Function


Private Function mCRtoPL(x) As Double
  If x < 2 Then
    mCRtoPL = x
  Else
    mCRtoPL = 2 ^ (x / 2)
  End If
End Function

Private Function mPLtoCR(x) As Double
  If x < 2 Then
    mPLtoCR = x
  Else
    mPLtoCR = 2 * log(x) / log(2)
  End If
End Function



Sub CalculateAveragesAndELs(Optional DoAll As Boolean = False)
  Dim Nmin() As Long, CRMin() As Single
  Dim Nmax() As Long, CRMax() As Single
  Dim Navg() As Long, CRavg() As Single
  Dim ELMin As Single, ELMax As Single, ELAvg As Single
  Dim i As Long, j As Long
  Dim blnOGL As Boolean
  Dim strTemp As String
  
  Dim FixedEncounterName As String
  Dim FixedMonsterName As String
  Dim tempstr() As String
  Dim CRTable As New ADODB.Recordset
  Dim EncounterList As New ADODB.Recordset
  Dim TemplateList As New ADODB.Recordset
  Dim sngHitDice As Single
  
  Dim db As New CDB
  
  frmConsole.Show
  DoEvents
  
  frmConsole.Text1.Text = ""
  db.ConnectDB
  
  If DoAll = False Then
    'OR Template1 IS NOT NULL OR Template2 IS NOT NULL or Template3 IS NOT NULL)
    EncounterList.Open "SELECT * FROM MonsterEncounter WHERE AvgEL = 0.0 OR AvgEL IS NULL", Conn, adOpenStatic, adLockOptimistic
  Else
    EncounterList.Open "SELECT * FROM MonsterEncounter WHERE EncounterName <> 'Mephit mob' and EncounterName <> 'Mephit gang'", Conn, adOpenStatic, adLockOptimistic
  End If
  
  If EncounterList.State > 0 Then
    If EncounterList.RecordCount > 0 Then
      EncounterList.MoveFirst
      frmConsole.WriteLine EncounterList!EncounterName
      Do While EncounterList.EOF = False
        
        blnOGL = True ' start at true; if any are not OGL-compliant, then the encounter is not
        
        FixedEncounterName = Apostrify(EncounterList!EncounterName)
'        If InStr(FixedEncounterName, "'") > 0 Then
'          tempstr = Split(FixedEncounterName, "'")
'          FixedEncounterName = tempstr(0)
'          For i = 1 To UBound(tempstr)
'            FixedEncounterName = FixedEncounterName & "''" & tempstr(i)
'            'frmConsole.WriteLine FixedEncounterName
'          Next i
'
'        End If
        
        db.OpenDB "SELECT * FROM MonsterEncounterData WHERE EncounterName Like '" & FixedEncounterName & "' AND MonsterName IS NOT NULL AND MonsterTitle IS NOT NULL", True
        If db.rs.State > 0 Then
          If db.rs.RecordCount > 0 Then
            db.rs.MoveFirst
            
            ReDim Nmin(db.rs.RecordCount) As Long
            ReDim CRMin(db.rs.RecordCount) As Single
            ReDim Nmax(db.rs.RecordCount) As Long
            ReDim CRMax(db.rs.RecordCount) As Single
            ReDim Navg(db.rs.RecordCount) As Long
            ReDim CRavg(db.rs.RecordCount) As Single
            i = 1
            Do While db.rs.EOF = False
              'frmConsole.WriteLine db.rs!monstername
              
              FixedMonsterName = db.rs!monstername
              If InStr(FixedMonsterName, "'") > 0 Then
                tempstr = Split(FixedMonsterName, "'")
                FixedMonsterName = tempstr(0)
                For j = 1 To UBound(tempstr)
                  FixedMonsterName = FixedMonsterName & "''" & tempstr(j)
                Next j
                frmConsole.WriteLine FixedMonsterName

              End If
              
              CRTable.Open "SELECT CR, Name, hit_dice, type, reference FROM MonsterStats WHERE Name LIKE '" _
                & FixedMonsterName & "' AND CR IS NOT NULL", Conn, adOpenStatic, adLockReadOnly
              
              If CRTable.State > 0 Then
                If CRTable.RecordCount > 0 Then
                  CRTable.MoveFirst
                 
                  ' get the minimum appearing
                  If db.rs!ChanceOfAppearing = 100 Then
                    Nmin(i) = db.rs!MinAppearing
                  Else
                    Nmin(i) = 0
                  End If
                  
                  CRMin(i) = CRTable!CR
                  CRMax(i) = CRTable!CR
                  
                  
                  ' Get the hit dice
                  If Not IsNull(CRTable!hit_dice) Then
                    If InStr(CRTable!hit_dice, "/") > 0 Then
                      strTemp = Mid(CRTable!hit_dice, InStr(LCase(CRTable!hit_dice), "/") + 1)
                      
                      sngHitDice = CSng(LEFT(CRTable!hit_dice, _
                        InStr(LCase(CRTable!hit_dice), "/") - 1)) / _
                        CSng(LEFT(strTemp, InStr(LCase(strTemp), "d") - 1))
                    Else
                      sngHitDice = CSng(LEFT(CRTable!hit_dice, InStr(LCase(CRTable!hit_dice), "d") - 1))
                    End If
                  Else
                    sngHitDice = 0#
                  End If

                  ' Calculate CR from extra hit dice
                  If Not IsNull(db.rs!ExtraHD) Then
                    If db.rs!ExtraHD > 0 Then
                      If Not IsNull(CRTable!Type) Then
                        Select Case LCase(CRTable!Type)
                          Case "aberration"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "construct"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "elemental"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "fey"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "giant"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "humanoid"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "ooze"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "plant"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "undead"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "vermin"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 4)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 4)
                          Case "animal"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 3)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 3)
                          Case "magical beast"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 3)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 3)
                          Case "monstrous humanoid"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 3)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 3)
                          Case "dragon"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 2)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 2)
                          Case "outsider"
                            CRMin(i) = CRMin(i) + Int(db.rs!ExtraHD / 2)
                            CRMax(i) = CRMax(i) + Int(db.rs!ExtraHD / 2)
                          
                        End Select
                      Else
                        CRMin(i) = CRMin(i) + 2 * log((sngHitDice + db.rs!ExtraHD) / sngHitDice) / log(2)
                        CRMax(i) = CRMax(i) + 2 * log((sngHitDice + db.rs!ExtraHD) / sngHitDice) / log(2)
                      End If
                    
                    
                    End If
                  End If
                  
                  
                  If GetMonsterEncounterClassTemplates(Apostrify(db.rs!EncounterName), FixedMonsterName, _
                    Apostrify(db.rs!MonsterTitle), CRMin(i), CRMax(i), sngHitDice) = False Or blnOGL = False Then
                    
                    blnOGL = False
                  Else
                    blnOGL = True
                  End If
                  
                  Nmax(i) = db.rs!MaxAppearing
                  CRavg(i) = (CRMin(i) + CRMax(i)) / 2
                  Navg(i) = (Nmax(i) + db.rs!MinAppearing) / 2 * db.rs!ChanceOfAppearing / 100
                  
                  If Not IsNull(CRTable!reference) Then
                    If Len(CRTable!reference) > 0 And blnOGL = True Then
                      blnOGL = True
                    Else
                      blnOGL = False
                    End If
                  Else
                    blnOGL = False
                  End If
                  
                  EncounterList!OGLCompliant = blnOGL
                  
                  db.rs!AvgAppearing = Navg(i)
                  'db.rs!AvgLevel = (db.rs!MinLevel + db.rs!MaxLevel) / 2
                  db.rs.Update
                End If
                i = i + 1
                CRTable.Close
              End If
              
              db.rs.MoveNext
              DoEvents
            Loop
            
            ELMin = GetEL(Nmin, CRMin)
            ELMax = GetEL(Nmax, CRMax)
            ELAvg = GetEL(Navg, CRavg)
            
            'db.rs.MoveFirst
            'Do While db.rs.EOF = False
              EncounterList!minEL = ELMin
              EncounterList!maxEL = ELMax
              EncounterList!AvgEL = ELAvg
              EncounterList!OGLCompliant = blnOGL
              EncounterList.Update
              'EncounterList.MoveNext
              
              'DoEvents
            'Loop
            db.rs.Close
          End If
        End If
        If blnOGL = True Then
          frmConsole.WriteLine EncounterList!EncounterName & " EL = " & Format(ELMin, "0.0") & " to " & Format(ELMax, "0.0") & " (Average EL = " & Format(ELAvg, "0.0") & ") - OGL"
        Else
          frmConsole.WriteLine EncounterList!EncounterName & " EL = " & Format(ELMin, "0.0") & " to " & Format(ELMax, "0.0") & " (Average EL = " & Format(ELAvg, "0.0") & ") - NON-OGL"
        End If
        EncounterList.MoveNext
        DoEvents
        
      Loop
    End If
    EncounterList.Close
  End If
  
  Conn.Close
  frmConsole.WriteLine "FINISHED!!!!!!!!!!!!!!!!!!!!!!!"
End Sub



Private Function GetMonsterEncounterClassTemplates(strEncounterName As String, strMonsterName As String, _
  strMonsterTitle As String, CRMin As Single, CRMax As Single, sngHitDice As Single) As Boolean
  
  
  Dim db As New ADODB.Recordset
  Dim TemplateList As New ADODB.Recordset
  Dim blnOGL As Boolean
  
  blnOGL = True
  
  'db.ConnectDB
  
  db.Open "SELECT * FROM MonsterEncounterClasses WHERE EncounterName = '" & strEncounterName & _
    "' AND MonsterName = '" & strMonsterName & "' AND MonsterTitle = '" & strMonsterTitle & "'", _
    Conn, adOpenStatic, adLockOptimistic
  
  If db.State > 0 Then
    If db.RecordCount > 0 Then
      db.MoveFirst

      Do While Not db.EOF
        
        ' Assumes all classes are associated except NPC classes
        If Len(db!ClassName) > 1 And db!MinClassLevel > 0 And db!MaxClassLevel > 0 Then
          
          If db!ClassName Like "Warrior" Or db!ClassName Like "Commoner" Or _
            db!ClassName Like "Aristocrat" Or db!ClassName Like "Adempt" Or _
            db!ClassName Like "Expert" Then
            
            CRMin = CRMin + Int(db!MinClassLevel / 2)
            CRMax = CRMax + Int(db!MaxClassLevel / 2)
          Else
            CRMin = CRMin + db!MinClassLevel
            CRMax = CRMax + db!MaxClassLevel
          End If
          
          If db!AvgClassLevel = 0 Then
            db!AvgClassLevel = (db!MinClassLevel + db!MaxClassLevel) / 2
            db.Update
          End If
        End If
        
        If Len(db!TemplateName) > 1 Then
          Select Case db!TemplateName
            Case "Skeleton (template)"
              Select Case sngHitDice
                Case Is < 1
                  CRMin = 1# / 6#
                  CRMax = 1# / 6#
                Case 1
                  CRMin = 1# / 3#
                  CRMax = 1# / 3#
                Case Else
                  CRMin = CSng(Int(sngHitDice / 2))
                  CRMax = CSng(Int(sngHitDice / 2))
              End Select
      
            Case "Zombie (template)"
              Select Case sngHitDice
                Case Is < 1
                  CRMin = 1# / 8#
                  CRMax = 1# / 8#
                Case 1
                  CRMin = 1# / 4#
                  CRMax = 1# / 4#
                Case 2
                  CRMin = 1# / 2#
                  CRMax = 1# / 2#
                Case 3 To 5
                  CRMin = 1
                  CRMax = 1
                Case 6 To 7
                  CRMin = 2
                  CRMax = 2
                Case 8 To 10
                  CRMin = 3
                  CRMax = 3
                Case 11 To 14
                  CRMin = 4
                  CRMax = 4
                Case 15 To 16
                  CRMin = 5
                  CRMax = 5
                Case 17 To 20
                  CRMin = 6
                  CRMax = 6
              End Select
      
            Case Else
              TemplateList.Open "SELECT CR, reference FROM MonsterTemplates WHERE Name LIKE '" & _
                db!TemplateName & "' AND CR IS NOT NULL AND MinHD <= " & CStr(sngHitDice) & _
                " AND MaxHD >= " & CStr(sngHitDice), Conn, adOpenStatic, adLockReadOnly
              
              If TemplateList.State > 0 Then
                If TemplateList.RecordCount > 0 Then
                  TemplateList.MoveFirst
                  CRMin = CRMin + TemplateList!CR * db!TemplateStack
                  CRMax = CRMax + TemplateList!CR * db!TemplateStack
                  If IsNull(TemplateList!reference) Then blnOGL = False
                End If
                TemplateList.Close
              End If
          
          End Select
        End If
        db.MoveNext
        DoEvents
      Loop
    End If
  End If
  
  db.Close
  
  GetMonsterEncounterClassTemplates = blnOGL
End Function
