Attribute VB_Name = "modGlobals"
Option Explicit

Global Conn As ADODB.Connection
Global Const gcblnOGL As Boolean = True

Global Const EPSILON_SNG As Single = 1.4013E-45


Function FileExist(ByVal strFileName As String) As Boolean
  
  On Error GoTo errDoesFileExist
  
  'check to see if the file exist in the specified directory
  'first check to see if strFileName is just a directory.
  'directories will return true
  If Mid(strFileName, Len(strFileName)) = "\" Then
    'this is just a directory
    FileExist = False
    Exit Function
  End If
  
  'then check to see if the file exists.
  FileExist = IIf(Dir$(strFileName) <> "", True, False)

  Exit Function

errDoesFileExist:

End Function

Function DirExist(ByVal strFileName As String) As Boolean
  
  On Error GoTo errDoesFileExist
  
  'check to see if the file exist in the specified directory
  'first check to see if strFileName is just a directory.
  'directories will return true
  
  'then check to see if the file exists.
  DirExist = IIf(Dir$(strFileName, vbDirectory) <> "", True, False)

  Exit Function

errDoesFileExist:
End Function




Function dN(n As Long, s As Long) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * s)
  Next i
  dN = res

End Function

Function d100(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 100)
  Next i
  d100 = res
End Function

Function d2(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 2)
  Next i
  d2 = res
End Function

Function d3(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 3)
  Next i
  d3 = res
End Function


Function d4(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 4)
  Next i
  d4 = res
End Function

Function d6(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 6)
  Next i
  d6 = res
End Function

Function d8(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 6)
  Next i
  d8 = res
End Function

Function d10(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 6)
  Next i
  d10 = res
End Function

Function d12(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 12)
  Next i
  d12 = res
End Function

Function d20(Optional n As Long = 1) As Long
  Dim i As Long
  Dim res As Long
  res = 0
  For i = 0 To n - 1
    res = res + ceil(Rnd * 20)
  Next i
  d20 = res
End Function

Function Shuffle(n As Long) As Long()

  ' Generates a randomly-ordered array of longs from with
  ' non-repeating values from 1 to N

  Dim i As Long, j As Long, k As Long
  Dim lngArray() As Long
  
  ReDim lngArray(n) As Long

  For i = 0 To n
    lngArray(i) = i
  Next i
  
  For i = 0 To n
    j = CLng(Rnd() * 2147483647#) Mod n
    k = lngArray(i)
    lngArray(i) = lngArray(j)
    lngArray(j) = k
  Next i
  
  Shuffle = lngArray
  
End Function




Function GetMonsterFrequencyHistogram()
  Dim a As New CDB
  Dim freqs(4) As Long
  Dim nrecs As Long
  
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE Frequency IS NOT NULL"
  
  
  If a.rs.State > 0 Then
    a.rs.MoveFirst
    Do While Not a.rs.EOF
      If LCase(a.rs!Frequency) Like "very rare" Then
        freqs(3) = freqs(3) + 1
      ElseIf LCase(a.rs!Frequency) Like "rare" Then
        freqs(2) = freqs(2) + 1
      ElseIf LCase(a.rs!Frequency) Like "uncommon" Then
        freqs(1) = freqs(1) + 1
      ElseIf LCase(a.rs!Frequency) Like "common" Then
        freqs(0) = freqs(0) + 1
      Else
        freqs(4) = freqs(4) + 1
      End If
      a.rs.MoveNext
      DoEvents
    Loop
    nrecs = a.rs.RecordCount
    a.CloseDB
    Conn.Close
    
    frmConsole.Show
    frmConsole.Text1.Text = ""
    DoEvents
    frmConsole.WriteLine CStr(nrecs) & " records"
    frmConsole.WriteLine "Common monster species = " & CStr(freqs(0)) & " (" & Format(100 * freqs(0) / nrecs, "0.0") & "%)"
    frmConsole.WriteLine "Uncommon monster species = " & CStr(freqs(1)) & " (" & Format(100 * freqs(1) / nrecs, "0.0") & "%)"
    frmConsole.WriteLine "Rare monster species = " & CStr(freqs(2)) & " (" & Format(100 * freqs(2) / nrecs, "0.0") & "%)"
    frmConsole.WriteLine "Very Rare monster species = " & CStr(freqs(3)) & " (" & Format(100 * freqs(3) / nrecs, "0.0") & "%)"
    frmConsole.WriteLine "Unique monsters = " & CStr(freqs(4)) & " (" & Format(100 * freqs(4) / nrecs, "0.0") & "%)"
    
    Debug.Print CStr(nrecs) & " records"
    Debug.Print "Common monster species = " & CStr(freqs(0)) & " (" & Format(100 * freqs(0) / nrecs, "0.0") & "%)"
    Debug.Print "Uncommon monster species = " & CStr(freqs(1)) & " (" & Format(100 * freqs(1) / nrecs, "0.0") & "%)"
    Debug.Print "Rare monster species = " & CStr(freqs(2)) & " (" & Format(100 * freqs(2) / nrecs, "0.0") & "%)"
    Debug.Print "Very Rare monster species = " & CStr(freqs(3)) & " (" & Format(100 * freqs(3) / nrecs, "0.0") & "%)"
    Debug.Print "Unique monsters = " & CStr(freqs(4)) & " (" & Format(100 * freqs(4) / nrecs, "0.0") & "%)"
  End If
End Function

Function GetMonsterFrequencyHistogramCRTerrain()
  Dim a As New CDB
  Dim freqs(4) As Long
  Dim nrecs As Long
  Dim CR As Single
  Dim fractionalCR
  Dim fractionalCRCounter
  
  fractionalCR = Array(0.1, 0.125, 0.1666666666666, 0.25, 1 / 3, 0.5)
  fractionalCRCounter = 0
  a.ConnectDB
  
  frmConsole.Show
  frmConsole.Text1.Text = ""
  DoEvents
  
  For CR = 0 To 40
    If fractionalCRCounter <= 5 Then
      CR = fractionalCR(fractionalCRCounter)
      fractionalCRCounter = fractionalCRCounter + 1
    ElseIf fractionalCRCounter = 6 Then
      fractionalCRCounter = fractionalCRCounter + 1
      CR = 1
    End If
    
    a.OpenDB "SELECT * FROM MonsterStats WHERE Frequency IS NOT NULL AND " & _
      "Unique = False AND RandomTable = True AND CR = " & CStr(CR)
    
    freqs(0) = 0
    freqs(1) = 0
    freqs(2) = 0
    freqs(3) = 0
    freqs(4) = 0
    
    If a.rs.State > 0 Then
      If a.rs.RecordCount > 0 Then
        a.rs.MoveFirst
        Do While Not a.rs.EOF
          If LCase(a.rs!Frequency) Like "very rare" Then
            freqs(3) = freqs(3) + 1
          ElseIf LCase(a.rs!Frequency) Like "rare" Then
            freqs(2) = freqs(2) + 1
          ElseIf LCase(a.rs!Frequency) Like "uncommon" Then
            freqs(1) = freqs(1) + 1
          ElseIf LCase(a.rs!Frequency) Like "common" Then
            freqs(0) = freqs(0) + 1
          Else
            freqs(4) = freqs(4) + 1
          End If
          a.rs.MoveNext
          DoEvents
        Loop
        nrecs = a.rs.RecordCount
        a.CloseDB
        Debug.Print CStr(CR) & "; " _
          & CStr(freqs(0)) & "; " & CStr(freqs(1)) & "; " & CStr(freqs(2)) & "; " _
          & CStr(freqs(3)) & "; " & CStr(nrecs) & "; " _
          & Format(freqs(0) / nrecs, "0.000") & "; " _
          & Format(freqs(1) / nrecs, "0.000") & "; " _
          & Format(freqs(2) / nrecs, "0.000") & "; " _
          & Format(freqs(3) / nrecs, "0.000")
        frmConsole.WriteLine CStr(CR) & "; " _
          & CStr(freqs(0)) & "; " & CStr(freqs(1)) & "; " & CStr(freqs(2)) & "; " _
          & CStr(freqs(3)) & "; " & CStr(nrecs) & "; " _
          & Format(freqs(0) / nrecs, "0.000") & "; " _
          & Format(freqs(1) / nrecs, "0.000") & "; " _
          & Format(freqs(2) / nrecs, "0.000") & "; " _
          & Format(freqs(3) / nrecs, "0.000")
      End If
    End If
    If CR < 1 Then
      CR = -1
    End If
  Next CR
  Conn.Close
End Function


Sub PopulationUnique()
  ' Sets the populations of all monsters checked as unique to 1
  Dim a As New CDB
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE Unique = TRUE", True
  If a.rs.State > 0 Then
    a.rs.MoveFirst
    Do While Not a.rs.EOF
      Debug.Print a.rs!Name
      a.rs!population = 1
      a.rs.Update
      a.rs.MoveNext
    Loop
    a.CloseDB
  End If
  Conn.Close
End Sub


Function GetEncounterFrequencies()
  Dim a As New CDB
  Dim MSRS As New ADODB.Recordset
  Dim EMRS As New ADODB.Recordset
  Dim strFreq() As String
  Dim strEncounterList() As String
  Dim i As Long
  
  frmConsole.Show
  frmConsole.Text1.Text = ""
  DoEvents
  
  a.ConnectDB
  
  If Conn.State > 0 Then
    
    a.OpenDB "SELECT DISTINCT EncounterName FROM MonsterEncounter WHERE Frequency IS NULL"
    
    If a.rs.State > 0 Then
      If a.rs.RecordCount > 0 Then
        a.rs.MoveFirst
        ReDim strEncounterList(a.rs.RecordCount) As String
        ReDim strFreq(a.rs.RecordCount) As String
        
        i = 0
        Do While Not a.rs.EOF
          strEncounterList(i) = a.rs!EncounterName
          i = i + 1
          a.rs.MoveNext
          DoEvents
        Loop
        a.CloseDB
      End If
      For i = 0 To UBound(strEncounterList) - 1
        If Len(strEncounterList(i)) > 0 Then
          
          a.OpenDB "SELECT MonsterName, Frequency FROM MonsterEncounter WHERE EncounterName = '" & Apostrify(strEncounterList(i)) & "'"
          
          a.rs.MoveFirst
          strFreq(i) = ""
          Do While Not a.rs.EOF
            MSRS.Open "SELECT DISTINCT name, Frequency FROM MonsterStats WHERE name = '" & Apostrify(a.rs!monstername) & "' AND Frequency IS NOT NULL", Conn, adOpenStatic, adLockReadOnly
            If MSRS.State > 0 Then
              If MSRS.RecordCount > 0 Then
                Select Case MSRS!Frequency
                  Case "Unique"
                    strFreq(i) = "Unique"
                  Case "Very Rare"
                    If strFreq(i) Like "Rare" Or strFreq(i) Like "Uncommon" Or strFreq(i) Like "Common" Or Len(strFreq(i)) = 0 Or strFreq(i) Like "No" Then
                      strFreq(i) = "Very Rare"
                    End If
                  Case "Rare"
                    If strFreq(i) Like "Uncommon" Or strFreq(i) Like "Common" Or Len(strFreq(i)) = 0 Or strFreq(i) Like "No" Then
                      strFreq(i) = "Rare"
                    End If
                  Case "Uncommon"
                    If strFreq(i) Like "Common" Or Len(strFreq(i)) = 0 Or strFreq(i) Like "No" Then
                      strFreq(i) = "Uncommon"
                    End If
                  Case "Common"
                    If Len(strFreq(i)) = 0 Or strFreq(i) Like "No" Then
                      strFreq(i) = "Common"
                    End If
                End Select
              End If
              MSRS.Close
            End If
            
            a.rs.MoveNext
          Loop
          a.rs.Close
        End If
        DoEvents
      Next i
          
      frmConsole.Text1.Text = ""
      For i = 0 To UBound(strFreq) - 1
        a.OpenDB "SELECT * FROM MonsterEncounter WHERE EncounterName = '" & Apostrify(strEncounterList(i)) & "'", True
        
        If a.rs.RecordCount > 0 Then
          a.rs.MoveFirst
          Do While Not a.rs.EOF
            frmConsole.WriteLine strEncounterList(i) & " (" & strFreq(i) & ")"
            a.rs!Frequency = strFreq(i)
            a.rs.Update
            'a.rs.Requery
            a.rs.MoveNext
            DoEvents
          Loop
        End If
        a.rs.Close
      Next i
            
    End If
    
    
  End If
  Conn.Close
End Function



Function Apostrify(strSQL As String) As String
  Dim tempstring() As String
  Dim outstring As String
  Dim i As Long
  If Len(strSQL) > 0 Then
    tempstring = Split(strSQL, "'")
  
    outstring = tempstring(0)
    For i = 1 To UBound(tempstring)
      outstring = outstring & "''" & tempstring(i)
    Next i
    Apostrify = outstring
  Else
    Apostrify = strSQL
  End If
End Function



Sub CalculateMonsterDatabaseCompletion()

  Dim a As New CDB
  Dim nfields As Long
  Dim nrecs As Long
  Dim nNull As Long
  
  
  a.ConnectDB
  
  ' For OGL monsters
  a.OpenDB "SELECT * FROM QOGLMonsters"
  
  If a.rs.RecordCount > 0 Then
    nNull = 0
    nrecs = a.rs.RecordCount
    nfields = 38
    
    a.rs.MoveFirst
    Do While Not a.rs.EOF
      With a.rs
        'If !Template = True Then
          If IsNull(!category) Then nNull = nNull + 1
          If IsNull(!CR) Then nNull = nNull + 1
          'If IsNull(!XPV) Then nNull = nNull + 1
          If IsNull(!Frequency) Then nNull = nNull + 1
          'If IsNull(!environment) Then nNull = nNull + 1
          If IsNull(!organization) Then nNull = nNull + 1
          If IsNull(!SIZE) Then nNull = nNull + 1
          If IsNull(!type) Then nNull = nNull + 1
          If IsNull(!hit_dice) Then nNull = nNull + 1
          If IsNull(!hit_points) Then nNull = nNull + 1
          If IsNull(!initiative) Then nNull = nNull + 1
          If IsNull(!speed) Then nNull = nNull + 1
          If IsNull(!AC) Then nNull = nNull + 1
          If IsNull(!BAB) Then nNull = nNull + 1
          If IsNull(!Grapple) Then nNull = nNull + 1
          If IsNull(!attack) Then nNull = nNull + 1
          If IsNull(!full_attack) Then nNull = nNull + 1
          If IsNull(!Space) Then nNull = nNull + 1
          If IsNull(!reach) Then nNull = nNull + 1
          If IsNull(!special_attacks) Then nNull = nNull + 1
          If IsNull(!special_qualities) Then nNull = nNull + 1
          If IsNull(!Fort) Then nNull = nNull + 1
          If IsNull(!Ref) Then nNull = nNull + 1
          If IsNull(!Will) Then nNull = nNull + 1
          If IsNull(!STREN) Then nNull = nNull + 1
          If IsNull(!DEX) Then nNull = nNull + 1
          If IsNull(!CON) Then nNull = nNull + 1
          If IsNull(!INTEL) Then nNull = nNull + 1
          If IsNull(!WIS) Then nNull = nNull + 1
          If IsNull(!CHA) Then nNull = nNull + 1
          If IsNull(!skills) Then nNull = nNull + 1
          If IsNull(!feats) Then nNull = nNull + 1
          If IsNull(!Treasure) Then nNull = nNull + 1
          If IsNull(!Alignment) Then nNull = nNull + 1
          If IsNull(!Advancement) Then nNull = nNull + 1
          If IsNull(!reference) Then nNull = nNull + 1
          If IsNull(!Source) Then nNull = nNull + 1
        'End If
      End With
      a.rs.MoveNext
      DoEvents
    Loop
    
    a.CloseDB
    
  End If
  
 
  frmConsole.Show
  frmConsole.Text1.Text = ""
  DoEvents
  
  frmConsole.WriteLine "OGL Monster Database population is " & Format(100# * (1 - (CSng(nNull) / CSng(nrecs * nfields))), "0.00") & "% complete."
  
  
  ' For non-OGL monsters
  a.OpenDB "SELECT * FROM MonsterStats WHERE reference IS NULL"
  
  If a.rs.RecordCount > 0 Then
    nNull = 0
    nrecs = a.rs.RecordCount
    nfields = 38
    
    a.rs.MoveFirst
    Do While Not a.rs.EOF
      With a.rs
        If IsNull(!category) Then nNull = nNull + 1
        If IsNull(!CR) Then nNull = nNull + 1
        'If IsNull(!XPV) Then nNull = nNull + 1
        If IsNull(!Frequency) Then nNull = nNull + 1
        'If IsNull(!environment) Then nNull = nNull + 1
        If IsNull(!organization) Then nNull = nNull + 1
        If IsNull(!SIZE) Then nNull = nNull + 1
        If IsNull(!type) Then nNull = nNull + 1
        If IsNull(!hit_dice) Then nNull = nNull + 1
        If IsNull(!hit_points) Then nNull = nNull + 1
        If IsNull(!initiative) Then nNull = nNull + 1
        If IsNull(!speed) Then nNull = nNull + 1
        If IsNull(!AC) Then nNull = nNull + 1
        If IsNull(!BAB) Then nNull = nNull + 1
        If IsNull(!Grapple) Then nNull = nNull + 1
        If IsNull(!attack) Then nNull = nNull + 1
        If IsNull(!full_attack) Then nNull = nNull + 1
        If IsNull(!Space) Then nNull = nNull + 1
        If IsNull(!reach) Then nNull = nNull + 1
        If IsNull(!special_attacks) Then nNull = nNull + 1
        If IsNull(!special_qualities) Then nNull = nNull + 1
        If IsNull(!Fort) Then nNull = nNull + 1
        If IsNull(!Ref) Then nNull = nNull + 1
        If IsNull(!Will) Then nNull = nNull + 1
        If IsNull(!STREN) Then nNull = nNull + 1
        If IsNull(!DEX) Then nNull = nNull + 1
        If IsNull(!CON) Then nNull = nNull + 1
        If IsNull(!INTEL) Then nNull = nNull + 1
        If IsNull(!WIS) Then nNull = nNull + 1
        If IsNull(!CHA) Then nNull = nNull + 1
        If IsNull(!skills) Then nNull = nNull + 1
        If IsNull(!feats) Then nNull = nNull + 1
        If IsNull(!Treasure) Then nNull = nNull + 1
        If IsNull(!Alignment) Then nNull = nNull + 1
        If IsNull(!Advancement) Then nNull = nNull + 1
        If IsNull(!reference) Then nNull = nNull + 1
        If IsNull(!Source) Then nNull = nNull + 1
      End With
      a.rs.MoveNext
      DoEvents
    Loop
    
    a.CloseDB
    
  End If
  
  frmConsole.WriteLine "Non-OGL Monster Database population is " & Format(100# * (1 - (CSng(nNull) / CSng(nrecs * nfields))), "0.00") & "% complete."
  
  
  ' For All monsters
  a.OpenDB "SELECT * FROM MonsterStats"
  
  
  If a.rs.RecordCount > 0 Then
    nNull = 0
    nrecs = a.rs.RecordCount
    nfields = 38
    
    a.rs.MoveFirst
    Do While Not a.rs.EOF
      With a.rs
        If IsNull(!category) Then nNull = nNull + 1
        If IsNull(!CR) Then nNull = nNull + 1
        'If IsNull(!XPV) Then nNull = nNull + 1
        If IsNull(!Frequency) Then nNull = nNull + 1
        'If IsNull(!environment) Then nNull = nNull + 1
        If IsNull(!organization) Then nNull = nNull + 1
        If IsNull(!SIZE) Then nNull = nNull + 1
        If IsNull(!type) Then nNull = nNull + 1
        If IsNull(!hit_dice) Then nNull = nNull + 1
        If IsNull(!hit_points) Then nNull = nNull + 1
        If IsNull(!initiative) Then nNull = nNull + 1
        If IsNull(!speed) Then nNull = nNull + 1
        If IsNull(!AC) Then nNull = nNull + 1
        If IsNull(!BAB) Then nNull = nNull + 1
        If IsNull(!Grapple) Then nNull = nNull + 1
        If IsNull(!attack) Then nNull = nNull + 1
        If IsNull(!full_attack) Then nNull = nNull + 1
        If IsNull(!Space) Then nNull = nNull + 1
        If IsNull(!reach) Then nNull = nNull + 1
        If IsNull(!special_attacks) Then nNull = nNull + 1
        If IsNull(!special_qualities) Then nNull = nNull + 1
        If IsNull(!Fort) Then nNull = nNull + 1
        If IsNull(!Ref) Then nNull = nNull + 1
        If IsNull(!Will) Then nNull = nNull + 1
        If IsNull(!STREN) Then nNull = nNull + 1
        If IsNull(!DEX) Then nNull = nNull + 1
        If IsNull(!CON) Then nNull = nNull + 1
        If IsNull(!INTEL) Then nNull = nNull + 1
        If IsNull(!WIS) Then nNull = nNull + 1
        If IsNull(!CHA) Then nNull = nNull + 1
        If IsNull(!skills) Then nNull = nNull + 1
        If IsNull(!feats) Then nNull = nNull + 1
        If IsNull(!Treasure) Then nNull = nNull + 1
        If IsNull(!Alignment) Then nNull = nNull + 1
        If IsNull(!Advancement) Then nNull = nNull + 1
        If IsNull(!reference) Then nNull = nNull + 1
        If IsNull(!Source) Then nNull = nNull + 1
      End With
      a.rs.MoveNext
      DoEvents
    Loop
    
    a.CloseDB
    
  End If
  frmConsole.WriteLine "Total Monster Database population is " & Format(100# * (1 - (CSng(nNull) / CSng(nrecs * nfields))), "0.00") & "% complete."
  frmConsole.WriteLine "Complete."
  Conn.Close
End Sub


Sub CategorizeMonsters()
  Dim a As New CDB
  Dim categories As New ADODB.Recordset
  Dim strEncounterName As String
  
  a.ConnectDB
  
  a.OpenDB "SELECT * FROM MonsterEncounters", True
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        categories.Open "SELECT category FROM MonsterStats where Name LIKE '" & Apostrify(a.rs!EncounterName) & "'", Conn, adOpenStatic, adLockReadOnly
        If categories.State > 0 Then
          If categories.RecordCount > 0 Then
            categories.MoveFirst
            a.rs!category = categories!category
            a.rs.Update
          Else
            ' try again with the last space
            categories.Close
            categories.Open "SELECT category FROM MonsterStats where Name LIKE '" & Apostrify(a.rs!monstername) & "'", Conn, adOpenStatic, adLockReadOnly
            If categories.State > 0 Then
              If categories.RecordCount > 0 Then
                categories.MoveFirst
                a.rs!category = categories!category
                a.rs.Update
              End If
            End If
          End If
          categories.Close
        End If
        Debug.Print a.rs!EncounterName
        
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
End Sub

Sub DoDragonClutches()
  Dim a As New CDB
  Dim b As New ADODB.Recordset
  
  a.ConnectDB
  
  a.OpenDB "SELECT * FROM MonsterEncounters WHERE EncounterName LIKE '% Dragon'"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        If a.rs!EncounterName Like "Wyrmling *" Or a.rs!EncounterName Like "Very Young *" _
          Or a.rs!EncounterName Like "Young *" Or a.rs!EncounterName Like "Juvenile *" Or _
          a.rs!EncounterName Like "Young Adult *" Then
          
          Debug.Print a.rs!EncounterName
          
          b.Open "SELECT * FROM MonsterEncounters WHERE EncounterName LIKE '" & a.rs!EncounterName & _
            " clutch'", Conn, adOpenStatic, adLockOptimistic
            
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!category = a.rs!category
            b!EncounterName = a.rs!EncounterName & " clutch"
            b!OGLCompliant = a.rs!OGLCompliant
            b!Frequency = a.rs!Frequency
            b!monstername = a.rs!monstername
            b!MonsterTitle = a.rs!MonsterTitle
            b!MinAppearing = 2
            b!MaxAppearing = 5
            b.Update
            b.Close
          End If
        Else
          ' Pairs
          Debug.Print a.rs!EncounterName
          
          b.Open "SELECT * FROM MonsterEncounters WHERE EncounterName LIKE '" & a.rs!EncounterName & _
            " pair'", Conn, adOpenStatic, adLockOptimistic
            
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!category = a.rs!category
            b!EncounterName = a.rs!EncounterName & " pair"
            b!OGLCompliant = a.rs!OGLCompliant
            b!Frequency = a.rs!Frequency
            b!monstername = a.rs!monstername
            b!MonsterTitle = a.rs!MonsterTitle
            b!MinAppearing = 2
            b!MaxAppearing = 2
            b.Update
            
            b.Close
          End If
          b.Open "SELECT * FROM MonsterEncounters WHERE EncounterName LIKE '" & a.rs!EncounterName & _
            " family'", Conn, adOpenStatic, adLockOptimistic
            
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!category = a.rs!category
            b!EncounterName = a.rs!EncounterName & " family"
            b!OGLCompliant = a.rs!OGLCompliant
            b!Frequency = a.rs!Frequency
            b!monstername = a.rs!monstername
            b!MonsterTitle = a.rs!MonsterTitle
            b!MinAppearing = 1
            b!MaxAppearing = 2
            b.Update
            
            b.MoveNext
            
            If b.EOF = True Then b.AddNew
            
            If a.rs!monstername Like "Adult *" Then
              b!monstername = "Wyrmling" & Mid(a.rs!monstername, Len("Adult "))
              b!MonsterTitle = "Wyrmling" & Mid(a.rs!monstername, Len("Adult "))
            ElseIf a.rs!monstername Like "Mature Adult *" Then
              b!monstername = "Very Young" & Mid(a.rs!monstername, Len("Mature Adult "))
              b!MonsterTitle = "Very Young" & Mid(a.rs!monstername, Len("Mature Adult "))
            ElseIf a.rs!monstername Like "Old *" Then
              b!monstername = "Young" & Mid(a.rs!monstername, Len("Old "))
              b!MonsterTitle = "Young" & Mid(a.rs!monstername, Len("Old "))
            ElseIf a.rs!monstername Like "Very Old *" Then
              b!monstername = "Juvenile" & Mid(a.rs!monstername, Len("Very Old "))
              b!MonsterTitle = "Juvenile" & Mid(a.rs!monstername, Len("Very Old "))
            ElseIf a.rs!monstername Like "Ancient *" Then
              b!monstername = "Young Adult" & Mid(a.rs!monstername, Len("Ancient "))
              b!MonsterTitle = "Young Adult" & Mid(a.rs!monstername, Len("Ancient "))
            ElseIf a.rs!monstername Like "Wyrm *" Then
              b!monstername = "Adult" & Mid(a.rs!monstername, Len("Wyrm "))
              b!MonsterTitle = "Adult" & Mid(a.rs!monstername, Len("Wyrm "))
            ElseIf a.rs!monstername Like "Great Wyrm *" Then
              b!monstername = "Mature Adult" & Mid(a.rs!monstername, Len("Great Wyrm "))
              b!MonsterTitle = "Mature Adult" & Mid(a.rs!monstername, Len("Great Wyrm "))
            End If
            
            b!category = a.rs!category
            b!EncounterName = a.rs!EncounterName & " family"
            b!OGLCompliant = a.rs!OGLCompliant
            b!Frequency = a.rs!Frequency
            b!MinAppearing = 2
            b!MaxAppearing = 5
            b.Update
            b.Close
          
          
          End If
        End If
        a.rs.MoveNext
        DoEvents
      Loop
    End If
  End If
  Debug.Print "Finished"
  Conn.Close
End Sub



Sub GetEncounterHabitats()

  Dim a As New CDB
  Dim b As New ADODB.Recordset
  Dim c As New ADODB.Recordset

  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterEncounter WHERE NOT (EncounterName LIKE 'Human*') ORDER BY Category, AvgEL, EncounterName", True
  
  frmConsole.Show
  frmConsole.Text1.Text = ""
  
  
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
      
        frmConsole.WriteLine a.rs!EncounterName
        
        c.Open "SELECT DISTINCT MonsterName FROM MonsterEncounterData WHERE EncounterName LIKE '" & _
          Apostrify(a.rs!EncounterName) & "'", Conn, adOpenStatic, adLockReadOnly
          
        If c.State > 0 Then
          If c.RecordCount > 0 Then
            c.MoveFirst
            
            Do While Not c.EOF
            
              b.Open "SELECT * FROM MonsterStats WHERE category LIKE '" & Apostrify(a.rs!category) & _
                "' AND name LIKE '" & Apostrify(c!monstername) & "'", Conn, adOpenStatic, adLockReadOnly
      
              If b.State > 0 Then
                If b.RecordCount > 0 Then
                  b.MoveFirst
                  
                  'Do While Not b.EOF
                  
                    a.rs!ColdAquatic = b!ColdAquatic
                    a.rs!ColdDesert = b!ColdDesert
                    a.rs!ColdForest = b!ColdForest
                    a.rs!ColdHills = b!ColdHills
                    a.rs!ColdMarsh = b!ColdMarsh
                    a.rs!ColdMountain = b!ColdMountain
                    a.rs!ColdPlain = b!ColdPlain
                    a.rs!TemperateAquatic = b!TemperateAquatic
                    a.rs!TemperateDesert = b!TemperateDesert
                    a.rs!TemperateForest = b!TemperateForest
                    a.rs!TemperateHills = b!TemperateHills
                    a.rs!TemperateMarsh = b!TemperateMarsh
                    a.rs!TemperateMountain = b!TemperateMountain
                    a.rs!TemperatePlain = b!TemperatePlain
                    a.rs!WarmAquatic = b!WarmAquatic
                    a.rs!WarmDesert = b!WarmDesert
                    a.rs!WarmForest = b!WarmForest
                    a.rs!WarmHills = b!WarmHills
                    a.rs!WarmMarsh = b!WarmMarsh
                    a.rs!WarmMountain = b!WarmMountain
                    a.rs!WarmPlain = b!WarmPlain
                    a.rs!StoneyBarrens = b!StoneyBarrens
                    a.rs!SandyWastes = b!SandyWastes
                    a.rs!RockyBadlands = b!RockyBadlands
                    a.rs!SaltFlats = b!SaltFlats
                    a.rs!Mudflats = b!Mudflats
                    a.rs!Urban = b!Urban
                    a.rs!UrbanSewers = b!UrbanSewers
                    a.rs!Ruins = b!Ruins
                    a.rs!Underground = b!Underground
                    a.rs!Upperdark = b!Upperdark
                    a.rs!Middledark = b!Middledark
                    a.rs!Lowerdark = b!Lowerdark
                    a.rs!Sky = b!Sky
                    a.rs!Wildspace = b!Wildspace
                    a.rs!Phlogiston = b!Phlogiston
                    a.rs!SiltBasin = b!SiltBasin
                    a.rs!Volcanic = b!Volcanic
                    a.rs!Ethereal = b!Ethereal
                    a.rs!Shadow = b!Shadow
                    a.rs!Temporal = b!Temporal
                    a.rs!Dreams = b!Dreams
                    a.rs!Air = b!Air
                    a.rs!Earth = b!Earth
                    a.rs!Water = b!Water
                    a.rs!Fire = b!Fire
                    a.rs!Ooze = b!Ooze
                    a.rs!Ice = b!Ice
                    a.rs!Magma = b!Magma
                    a.rs!Smoke = b!Smoke
                    a.rs!Positive = b!Positive
                    a.rs!Negative = b!Negative
                    a.rs!Radiance = b!Radiance
                    a.rs!Vacuum = b!Vacuum
                    a.rs!Mineral = b!Mineral
                    a.rs!Dust = b!Dust
                    a.rs!Lightning = b!Lightning
                    a.rs!Ash = b!Ash
                    a.rs!Steam = b!Steam
                    a.rs!salt = b!salt
                    a.rs!Astral = b!Astral
                    a.rs!LNPlane = b!LNPlane
                    a.rs!LGPlane = b!LGPlane
                    a.rs!NGPlane = b!NGPlane
                    a.rs!CGPlane = b!CGPlane
                    a.rs!CNPlane = b!CNPlane
                    a.rs!CEPlane = b!CEPlane
                    a.rs!NEPlane = b!NEPlane
                    a.rs!LEPlane = b!LEPlane
                    a.rs!TNPlane = b!TNPlane
                    a.rs!FarRealm = b!FarRealm
                    
                    a.rs.Update
            
                    DoEvents
                  'Loop ' b
                End If
                b.Close
              End If
              c.MoveNext
            Loop ' c
          End If
          c.Close
        End If
        
        a.rs.MoveNext
      Loop ' a
    End If
    a.CloseDB
  End If
  frmConsole.WriteLine "Finished."
  Conn.Close

End Sub


Sub FillInOGLWorlds()
  Dim a As New CDB
  Dim strEnv As String
  
End Sub

Sub FillInHabitats()
  Dim a As New CDB
  Dim strEnv As String
  
End Sub


Sub FillInWorlds()
  Dim a As New CDB
  Dim strEnv As String
 
End Sub

Sub RavenloftMonsters()
  Dim a As New CDB
End Sub



Sub DragonAdvancement()
  Dim a As New CDB
  Dim sngMinHitDice As Single
  Dim sngMaxHitDice As Single
  Dim strCategory As String
  Dim strWyrmlingSize As String
  Dim strGreatWyrmSize As String
  Dim intStrBonus As Integer
  
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats " & _
    "WHERE category LIKE 'Dragon, %' AND hit_dice IS NOT NULL AND size IS NOT NULL " & _
    "ORDER BY category, CR", True
    
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        Debug.Print a.rs!Name
        If a.rs!Name Like "Wyrmling *" Then strWyrmlingSize = a.rs!SIZE
        sngMinHitDice = CSng(LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1)) + 1
        strCategory = a.rs!category
        a.rs.MoveNext
        If a.rs.EOF = False Then
          If (a.rs!category Like strCategory) Then
            sngMaxHitDice = CSng(LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1)) - 1
            a.rs.MovePrevious
            a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & Format(sngMaxHitDice, "0") & _
              " HD (" & a.rs!SIZE & ")"
          Else ' Great Wyrm
            a.rs.MovePrevious
            strGreatWyrmSize = a.rs!SIZE
            If LCase(strWyrmlingSize) Like "tiny" Then
              ' lesser
              ' Colossal @ 6 HD, Colossal+ @ 12 HD
              a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
                Format(sngMinHitDice + 5, "0") & " HD (" & a.rs!SIZE & "); " & _
                Format(sngMinHitDice + 6, "0") & "-" & _
                Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
                Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"


            ElseIf LCase(strWyrmlingSize) Like "small" And _
              Not (LCase(strGreatWyrmSize) Like "colossal") And _
              Not (LCase(strGreatWyrmSize) Like "colossal+") Then
              ' ordinary
              a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
                Format(sngMinHitDice + 2, "0") & " HD (" & a.rs!SIZE & "); " & _
                Format(sngMinHitDice + 3, "0") & "-" & _
                Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
                Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"
              ' Colossal @ 3 HD, Colossal+ @ 12 HD
            
            ElseIf (LCase(strWyrmlingSize) Like "small" Or _
              LCase(strWyrmlingSize) Like "large") And _
              Not (LCase(strGreatWyrmSize) Like "colossal") And _
              Not (LCase(strGreatWyrmSize) Like "colossal+") Then
              ' greater
              ' Colossal+ @ 12 HD
              a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
                Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
                Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"
              ' Colossal @ 3 HD, Colossal+ @ 12 HD
            
            ElseIf LCase(strGreatWyrmSize) Like "colossal+" Then ' epic
              ' Always colossal+
              a.rs!Advancement = Format(sngMinHitDice, "0") & "+" & " HD (Colossal+)"
            
            End If
            
          End If
        Else
          a.rs.MovePrevious
          strGreatWyrmSize = a.rs!SIZE
          If LCase(strWyrmlingSize) Like "tiny" Then
            ' lesser
            ' Colossal @ 6 HD, Colossal+ @ 12 HD
            a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
              Format(sngMinHitDice + 5, "0") & " HD (" & a.rs!SIZE & "); " & _
              Format(sngMinHitDice + 6, "0") & "-" & _
              Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
              Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"


          ElseIf LCase(strWyrmlingSize) Like "small" And _
            Not (LCase(strGreatWyrmSize) Like "colossal") And _
            Not (LCase(strGreatWyrmSize) Like "colossal+") Then
            ' ordinary
            a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
              Format(sngMinHitDice + 2, "0") & " HD (" & a.rs!SIZE & "); " & _
              Format(sngMinHitDice + 3, "0") & "-" & _
              Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
              Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"
            ' Colossal @ 3 HD, Colossal+ @ 12 HD
          
          ElseIf (LCase(strWyrmlingSize) Like "small" Or _
            LCase(strWyrmlingSize) Like "large") And _
            Not (LCase(strGreatWyrmSize) Like "colossal") And _
            Not (LCase(strGreatWyrmSize) Like "colossal+") Then
            ' greater
            ' Colossal+ @ 12 HD
            a.rs!Advancement = Format(sngMinHitDice, "0") & "-" & _
              Format(sngMinHitDice + 11, "0") & " HD (Colossal); " & _
              Format(sngMinHitDice + 12, "0") & "+ HD (Colossal+)"
            ' Colossal @ 3 HD, Colossal+ @ 12 HD
          
          ElseIf LCase(strGreatWyrmSize) Like "colossal+" Then ' epic
            ' Always colossal+
            a.rs!Advancement = Format(sngMinHitDice, "0") & "+" & " HD (Colossal+)"
          
          End If
          
        End If
        If Not IsNull(a.rs!STREN) Then
          intStrBonus = Int((a.rs!STREN - 10) / 2)
        Else
          intStrBonus = 0
        End If
        Select Case LCase(a.rs!SIZE)
          Case "tiny"
            a.rs!Space = "2.5 ft."
            a.rs!reach = "5 ft."
            
            a.rs!attack = "Bite " & a.rs!BAB & " (1d4+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (1d4+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (1d3+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee"
            a.rs!special_attacks = "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "small"
            a.rs!Space = "5 ft."
            a.rs!reach = "5 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (1d6+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (1d6+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (1d4+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee"
            a.rs!special_attacks = "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "medium"
            a.rs!Space = "5 ft."
            a.rs!reach = "5 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (1d8+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (1d8+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (1d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (1d4+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee"
            a.rs!special_attacks = "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "large"
            a.rs!Space = "10 ft."
            a.rs!reach = "10 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (2d6+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (2d6+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (1d8+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (1d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "tail slap " & a.rs!BAB - 4 & " (1d8+" & CStr(Int(intStrBonus * 1.5)) & "/19-20) melee"
            a.rs!special_attacks = "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "huge"
            a.rs!Space = "15 ft."
            a.rs!reach = "10 ft."
            
            a.rs!attack = "Bite " & a.rs!BAB & " (2d8+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (2d8+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (2d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (1d8+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "tail slap " & a.rs!BAB - 4 & " (2d6+" & CStr(Int(intStrBonus * 1.5)) & "/19-20) melee"
            a.rs!special_attacks = "Crush 2d8+" & CStr(Int(intStrBonus * 1.5)) & " (DC 45), " & _
              "tail sweep 2d4+" & CStr(Int(intStrBonus * 1.5)) & " (DC ), " & _
              "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "gargantuan"
            a.rs!Space = "20 ft."
            a.rs!reach = "15 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (4d6+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (4d6+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (2d8+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (2d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "tail slap " & a.rs!BAB - 4 & " (2d8+" & CStr(Int(intStrBonus * 1.5)) & "/19-20) melee"
            a.rs!special_attacks = "Crush 4d6+" & CStr(Int(intStrBonus * 1.5)) & " (DC 45), " & _
              "tail sweep 2d6+" & CStr(Int(intStrBonus * 1.5)) & " (DC ), " & _
              "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "colossal"
            a.rs!Space = "30 ft."
            a.rs!reach = "20 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (4d8+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (4d8+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (4d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (2d8+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "tail slap " & a.rs!BAB - 4 & " (2d6+" & CStr(Int(intStrBonus * 1.5)) & "/19-20) melee"
            a.rs!special_attacks = "Crush 4d8+" & CStr(Int(intStrBonus * 1.5)) & " (DC 45), " & _
              "tail sweep 2d8+" & CStr(Int(intStrBonus * 1.5)) & " (DC ), " & _
              "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
          Case "colossal+"
            a.rs!Space = "30 ft."
            a.rs!reach = "20 ft."
            a.rs!attack = "Bite " & a.rs!BAB & " (8d6+" & CStr(intStrBonus) & "/19-20) melee"
            a.rs!full_attack = "Bite " & a.rs!BAB & " (8d6+" & CStr(intStrBonus) & "/19-20) melee, " & _
              "2 claws " & a.rs!BAB - 4 & " (4d8+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "2 wings " & a.rs!BAB - 4 & " (4d6+" & CStr(Int(intStrBonus / 2)) & "/19-20) melee, " & _
              "tail slap " & a.rs!BAB - 4 & " (4d8+" & CStr(Int(intStrBonus * 1.5)) & "/19-20) melee"
            a.rs!special_attacks = "Crush 8d6+" & CStr(Int(intStrBonus * 1.5)) & " (DC 45), " & _
              "tail sweep 4d6+" & CStr(Int(intStrBonus * 1.5)) & " (DC ), " & _
              "breath weapon, " & _
              "frightful presence (DC ), " & _
              "spells (caster level ), " & _
              "spell-like abilities"
        End Select
        
        a.rs.Update
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  
  Conn.Close
End Sub


Sub FillInGrapple()
  Dim a As New CDB
  
  frmConsole.Show
  frmConsole.ClearConsole
  DoEvents
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE STREN IS NOT NULL AND BAB IS NOT NULL AND size IS NOT NULL AND Grapple IS NULL ORDER BY Name", True
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        If IsNumeric(a.rs!BAB) And IsNumeric(a.rs!STREN) Then
          frmConsole.WriteLine a.rs!Name
          Select Case a.rs!SIZE
            Case "Awesome"
              a.rs!SIZE = "Colossal"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 16
            Case "Colossal+"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 16
            Case "Colossal"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 16
            Case "Gargantuan"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 12
            Case "Huge"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 8
            Case "Large"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) + 4
            Case "Medium"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2)
            Case "Small"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) - 4
            Case "Tiny"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) - 8
            Case "Diminutive"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) - 12
            Case "Fine"
              a.rs!Grapple = CLng(a.rs!BAB) + Int((a.rs!STREN - 10) / 2) - 16
          End Select
        Else
          Debug.Print "WARNING: " & a.rs!Name
        End If
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  Debug.Print "FINISHED"
End Sub

Sub CalculateBAB()
  Dim a As New CDB
  Dim intHD As Integer
  
  frmConsole.Show
  frmConsole.ClearConsole
  DoEvents
  
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE hit_dice IS NOT NULL AND Type IS NOT NULL AND BAB IS NULL", True
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        frmConsole.WriteLine a.rs!Name
        If InStr(UCase(a.rs!hit_dice), "D") = 0 Then
          intHD = Int(a.rs!hit_dice)
        ElseIf InStr(UCase(a.rs!hit_dice), "D") = 1 Then
          intHD = 1
        Else
          intHD = Int(LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1))
        End If
        Select Case LCase(a.rs!type)
          Case "aberration"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "animal"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "construct"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "dragon"
            a.rs!BAB = (intHD)
          Case "elemental"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "fey"
            a.rs!BAB = (Int(intHD * 0.5))
          Case "giant"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "humanoid"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "magical beast"
            a.rs!BAB = (intHD)
          Case "monstrous humanoid"
            a.rs!BAB = (intHD)
          Case "ooze"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "outsider"
            a.rs!BAB = (intHD)
          Case "plant"
            a.rs!BAB = (Int(intHD * 0.75))
          Case "undead"
            a.rs!BAB = (Int(intHD * 0.5))
          Case "vermin"
            a.rs!BAB = (Int(intHD * 0.75))
        End Select
        a.rs.Update
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  Debug.Print "Finished."
End Sub


Sub CalculateSkillPoints()
  Dim a As New CDB
  Dim intHD As Integer
  
  frmConsole.Show
  frmConsole.ClearConsole
  DoEvents
  
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE hit_dice IS NOT NULL AND Type IS NOT NULL AND INTEL IS NOT NULL AND SkillPoints IS NULL", True
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        frmConsole.WriteLine a.rs!Name
        If InStr(UCase(a.rs!hit_dice), "D") = 0 Then
          intHD = Int(a.rs!hit_dice)
        ElseIf InStr(UCase(a.rs!hit_dice), "D") = 1 Then
          intHD = 1
        ElseIf InStr(a.rs!hit_dice, "/") > 0 Then
          intHD = 0
        Else
          intHD = Int(LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1))
        End If
        
        Select Case LCase(a.rs!type)
          Case "aberration"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "animal"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "construct"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "dragon"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 6)
          Case "elemental"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "fey"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "giant"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "humanoid"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "magical beast"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "monstrous humanoid"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "ooze"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "outsider"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 8)
          Case "plant"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
          Case "undead"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 4)
          Case "vermin"
            a.rs!SkillPoints = intHD * (Int((a.rs!INTEL - 10) / 2) + 2)
        End Select
        If a.rs!SkillPoints < 0 Then a.rs!SkillPoints = 0
        a.rs.Update
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  Debug.Print "Finished."
End Sub

Sub FilterAC()
  Dim a As New CDB
  
  Dim strTemp As String
  Dim intTemp As Integer
  Dim intMod As Integer
  
  a.ConnectDB
  a.OpenDB "SELECT * FROM MonsterStats WHERE AC IS NOT NULL ORDER BY Name", True
  
  frmConsole.Show
  frmConsole.ClearConsole
  DoEvents
  
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        frmConsole.WriteLine a.rs!Name
        
        If InStr(a.rs!AC, " ") = 0 Then
          a.rs!ACTotal = Int(a.rs!AC)
        Else
          a.rs!ACTotal = Int(LEFT(a.rs!AC, InStr(a.rs!AC, " ")))
        End If
        
        intTemp = InStr(LCase(a.rs!AC), "natural")
        intMod = 0
        If intTemp > 0 Then
          Do Until Mid(a.rs!AC, intTemp + intMod, 1) Like "+"
            intMod = intMod - 1
          Loop
          strTemp = Mid(a.rs!AC, intTemp + intMod, -intMod)
          If InStr(strTemp, "(") > 0 Then
            a.rs!ACNatural = LEFT(strTemp, InStr(strTemp, "(") - 1)
          Else
            a.rs!ACNatural = Int(strTemp)
          End If
        Else
          a.rs!ACNatural = 0
        End If
        
        intTemp = InStr(LCase(a.rs!AC), "deflection")
        intMod = 0
        If intTemp > 0 Then
          Do Until Mid(a.rs!AC, intTemp + intMod, 1) Like "+"
            intMod = intMod - 1
          Loop
          a.rs!ACDeflection = Int(Mid(a.rs!AC, intTemp + intMod, -intMod))
        Else
          a.rs!ACDeflection = 0
        End If
        
        intTemp = InStr(LCase(a.rs!AC), "profane")
        intMod = 0
        If intTemp > 0 Then
          Do Until Mid(a.rs!AC, intTemp + intMod, 1) Like "+"
            intMod = intMod - 1
          Loop
          a.rs!ACProfane = Int(Mid(a.rs!AC, intTemp + intMod, -intMod))
        Else
          a.rs!ACProfane = 0
        End If
        
        
        intTemp = InStr(LCase(a.rs!AC), "insight")
        intMod = 0
        If intTemp > 0 Then
          Do Until Mid(a.rs!AC, intTemp + intMod, 1) Like "+"
            intMod = intMod - 1
          Loop
          a.rs!ACInsight = Int(Mid(a.rs!AC, intTemp + intMod, -intMod))
        Else
          a.rs!ACInsight = 0
        End If
        
        intTemp = InStr(LCase(a.rs!AC), "touch")
        If intTemp > 0 Then
          strTemp = Trim(Mid(a.rs!AC, intTemp + Len("touch")))
          If InStr(strTemp, ",") Then
            strTemp = Trim(LEFT(strTemp, InStr(strTemp, ",") - 1))
          End If
          a.rs!ACTouch = Int(strTemp)
        Else
          ' calculate it
          a.rs!ACTouch = a.rs!ACTotal - a.rs!ACNatural
        End If
        
        intTemp = InStr(LCase(a.rs!AC), "flat")
        If intTemp > 0 Then
          strTemp = Trim(Mid(a.rs!AC, intTemp + Len("flat-footed")))
          If InStr(strTemp, ",") Then
            strTemp = Trim(LEFT(strTemp, InStr(strTemp, ",") - 1))
          End If
          a.rs!ACFlatFooted = Int(strTemp)
        ElseIf Not IsNull(a.rs!DEX) Then
          a.rs!ACFlatFooted = a.rs!ACTotal - Int((a.rs!DEX - 10) / 2)
        End If
        If a.rs!feats Like "%ncanny dodge%" Then
          a.rs!ACFlatFooted = a.rs!AC
        End If
        
        a.rs.Update
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  frmConsole.WriteLine "Finished."
End Sub


Sub ML()
  Dim a As New CDB
  a.ConnectDB
  a.OpenDB "SELECT ML FROM MonsterStats WHERE ML IS NULL", True
  a.MoveFirst
  Do While Not a.rs.EOF
    a.rs!ML = 1
    a.rs.Update
    a.rs.MoveNext
  Loop
  a.rs.Close
  
  Conn.Close
  Debug.Print "Finished"
End Sub


Sub PLMLEncounters()
  Dim enc As New CDB ' MonsterEncounters
  Dim mon As New ADODB.Recordset  ' MonsterEncounterData
  Dim tem As New ADODB.Recordset  ' MonsterEncounterClasses
  Dim sta As New ADODB.Recordset  ' MonsterTemplates
  Dim mst As New ADODB.Recordset  ' MonsterStats
  
  Dim minPL As Byte
  Dim maxPL As Byte
  Dim minML As Byte
  Dim maxML As Byte
  
  
  enc.ConnectDB
  enc.OpenDB "SELECT EncounterName, ML, PL FROM MonsterEncounter", True
  If enc.rs.State > 0 Then
    If enc.rs.RecordCount > 0 Then
      enc.rs.MoveFirst
      Do While Not enc.rs.EOF
        minML = 0
        maxML = 0
        minPL = 0
        maxPL = 0
        
        mon.Open "SELECT * FROM MonsterEncounterData WHERE EncounterName = '" & _
          Apostrify(enc.rs!EncounterName) & "'", Conn, adOpenStatic, adLockReadOnly
          
        If mon.State > 0 Then
          If mon.RecordCount > 0 Then
            mon.MoveFirst
            Do While Not mon.EOF
              mst.Open "SELECT ML, PL FROM MonsterStats WHERE Name = '" & _
                Apostrify(mon!monstername) & "'", Conn, adOpenStatic, adLockReadOnly
              If mst.State > 0 Then
                If mst.RecordCount > 0 Then
                  mst.MoveFirst
                  If mst!PL > maxPL Then maxPL = mst!PL
                  If mst!PL > minPL Then minPL = mst!PL
                  If mst!ML > maxML Then maxML = mst!ML
                  If mst!ML > minML Then minML = mst!ML
                End If
                mst.Close
              End If
              
              tem.Open "SELECT * FROM MonsterEncounterClasses WHERE EncounterName = '" & _
                Apostrify(mon!EncounterName) & "' AND MonsterName = '" & _
                Apostrify(mon!monstername) & "' AND MonsterTitle = '" & _
                Apostrify(mon!MonsterTitle) & "'", Conn, adOpenStatic, adLockReadOnly
                
              If tem.State > 0 Then
                If tem.RecordCount > 0 Then
                  tem.MoveFirst
                  
                  Do While Not tem.EOF
                    If Len(tem!TemplateName) > 1 Then
                      sta.Open "SELECT PL, ML FROM MonsterTemplates WHERE Name = '" & _
                        Apostrify(tem!TemplateName) & "'", Conn, adOpenStatic, adLockReadOnly
                      If sta.State > 0 Then
                        If sta.RecordCount > 0 Then
                          sta.MoveFirst
                          If sta!PL > maxPL Then maxPL = sta!PL
                          If sta!PL > minPL Then minPL = sta!PL
                          If sta!ML > maxML Then maxML = sta!ML
                          If sta!ML > minML Then minML = sta!ML
                        End If
                        sta.Close
                      End If
                    End If
                    If Len(tem!ClassName) > 1 Then
                      sta.Open "SELECT PL, ML FROM Classes WHERE ClassName = '" & _
                        Apostrify(tem!ClassName) & "'", Conn, adOpenStatic, adLockReadOnly
                      If sta.State > 0 Then
                        If sta.RecordCount > 0 Then
                          sta.MoveFirst
                          If sta!PL > maxPL Then maxPL = sta!PL
                          If sta!PL > minPL Then minPL = sta!PL
                          If sta!ML > maxML Then maxML = sta!ML
                          If sta!ML > minML Then minML = sta!ML
                        End If
                        sta.Close
                      End If
                    End If
                    
                    tem.MoveNext
                  Loop
                End If
                tem.Close
              End If
              
              mon.MoveNext
            Loop
          End If
          mon.Close
        End If
        enc.rs!PL = minPL
        enc.rs!ML = minML
        enc.rs.Update
        enc.rs.MoveNext
        DoEvents
      Loop
    End If
    enc.CloseDB
  End If
  Conn.Close
  Debug.Print "Finished"
  
End Sub


Public Sub SourceShort()
  Dim a As New CDB
  a.ConnectDB
  a.OpenDB "SELECT Source FROM Classes", True
  a.rs.MoveFirst
  Do While Not a.rs.EOF
    a.rs!Source = Trim(a.rs!Source)
    a.rs.Update
    a.rs.MoveNext
    DoEvents
  Loop
  a.rs.Close
  Conn.Close
  Debug.Print "Finished"
End Sub


Public Sub FillMonsterFeats()
  Dim a As New CDB
  Dim b As New ADODB.Recordset
  Dim strFeatList() As String
  Dim i As Long
  
  a.ConnectDB
  a.OpenDB "SELECT Category, Name, feats FROM MonsterStats WHERE feats IS NOT NULL"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        Debug.Print a.rs!Name
        
        strFeatList = Split(a.rs!feats, ",")
        
        For i = 0 To UBound(strFeatList)
          b.Open "SELECT * FROM MonsterFeats WHERE Name = '" & Apostrify(a.rs!Name) & _
            "' AND Feat = '" & Apostrify(strFeatList(i)) & "'", Conn, adOpenStatic, adLockOptimistic
          If b.State > 0 Then
            If b.RecordCount = 0 Then
              b.AddNew
            Else
              b.MoveFirst
            End If
            b!category = a.rs!category
            b!Name = a.rs!Name
            b!Feat = Trim(strFeatList(i))
            b!TimesTaken = b!TimesTaken + 1
            b.Update
            b.Close
          End If
        
        Next i
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  
End Sub


Sub HitDice()
  Dim a As New CDB
  Dim sngHitDice As Single
  Dim strhit As String
  a.ConnectDB
  a.OpenDB "SELECT name, hit_dice, hit_dice_num FROM MonsterStats WHERE hit_dice LIKE '%d%'", True
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        Debug.Print a.rs!Name
        strhit = LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1)
        If IsNumeric(strhit) Then
          If InStr(strhit, "/") = 0 Then
            sngHitDice = CLng(LEFT(a.rs!hit_dice, InStr(a.rs!hit_dice, "d") - 1)) + 1
          Else
            
          End If
          a.rs!hit_dice_num = sngHitDice
          a.rs.Update
        End If
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  Conn.Close
  Debug.Print "Finished"
End Sub

Sub Subtype()
  Dim a As New CDB
  Dim b As New ADODB.Recordset
  Dim strSubtypeList() As String
  Dim i As Long
  
  a.ConnectDB
  a.OpenDB "SELECT Category, Name, descriptor FROM MonsterStats WHERE descriptor IS NOT NULL"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        Debug.Print a.rs!Name
        
        strSubtypeList = Split(a.rs!Descriptor, ",")
        
        For i = 0 To UBound(strSubtypeList)
          If Len(strSubtypeList(i)) > 1 Then
            b.Open "SELECT * FROM MonsterFeats WHERE Name = '" & Apostrify(a.rs!Name) & _
              "' AND Feat = '" & Apostrify(strSubtypeList(i)) & "'", Conn, adOpenStatic, _
              adLockOptimistic
            If b.State > 0 Then
              If b.RecordCount = 0 Then
                b.AddNew
              Else
                b.MoveFirst
              End If
              b!category = a.rs!category
              b!Name = a.rs!Name
              b!Subtype = Trim(strSubtypeList(i))
              b!Feat = "-"
              b.Update
              b.Close
            End If
          End If
        Next i
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  
  Conn.Close
End Sub


Sub AC()
  Dim a As New CDB
  a.ConnectDB
  a.OpenDB "SELECT ACBonusValue FROM MonsterFeats WHERE ACBonusValue IS NULL", True
  a.rs.MoveFirst
  Do While Not a.rs.EOF
    a.rs!ACBonusValue = 0
    a.rs.Update
    a.rs.MoveNext
  Loop
  Debug.Print "Finished"
End Sub


Sub ArmorClass()
  Dim a As New CDB
  Dim b As New ADODB.Recordset
  Dim strACType As String
  Dim i As Long
  
  a.ConnectDB
  a.OpenDB "SELECT name, category, ACNatural, ACInsight, ACDeflection, ACProfane" & _
    " FROM MonsterStats"
  If a.rs.State > 0 Then
    If a.rs.RecordCount > 0 Then
      a.rs.MoveFirst
      Do While Not a.rs.EOF
        If a.rs!ACNatural > 0 Then
          strACType = "Natural"
          b.Open "SELECT * FROM MonsterFeats WHERE " & _
            "Name = '" & Apostrify(a.rs!Name) & "' AND ACBonusName = '" & _
            strACType & "'", Conn, adOpenStatic, adLockOptimistic
          
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!ACBonusName = strACType
            b!ACBonusValue = a.rs!ACNatural
            b!Name = a.rs!Name
            b!category = a.rs!category
            b.Update
            b.Close
          End If
        
          
        End If
        
        If a.rs!ACInsight > 0 Then
          strACType = "Insight"
          b.Open "SELECT * FROM MonsterFeats WHERE " & _
            "Name = '" & Apostrify(a.rs!Name) & "' AND ACBonusName = '" & _
            strACType & "'", Conn, adOpenStatic, adLockOptimistic
          
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!ACBonusName = strACType
            b!ACBonusValue = a.rs!ACInsight
            b!Name = a.rs!Name
            b!category = a.rs!category
            b.Update
            b.Close
          End If
        End If
        
        If a.rs!ACDeflection > 0 Then
          strACType = "Deflection"
          b.Open "SELECT * FROM MonsterFeats WHERE " & _
            "Name = '" & Apostrify(a.rs!Name) & "' AND ACBonusName = '" & _
            strACType & "'", Conn, adOpenStatic, adLockOptimistic
          
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!ACBonusName = strACType
            b!ACBonusValue = a.rs!ACDeflection
            b!Name = a.rs!Name
            b!category = a.rs!category
            b.Update
            b.Close
          End If
        End If
        
        If a.rs!ACProfane > 0 Then
          strACType = "Profane"
          b.Open "SELECT * FROM MonsterFeats WHERE " & _
            "Name = '" & Apostrify(a.rs!Name) & "' AND ACBonusName = '" & _
            strACType & "'", Conn, adOpenStatic, adLockOptimistic
          
          If b.State > 0 Then
            If b.RecordCount > 0 Then
              b.MoveFirst
            Else
              b.AddNew
            End If
            b!ACBonusName = strACType
            b!ACBonusValue = a.rs!ACProfane
            b!Name = a.rs!Name
            b!category = a.rs!category
            b.Update
            b.Close
          End If
        End If
        
        a.rs.MoveNext
        DoEvents
      Loop
    End If
    a.CloseDB
  End If
  
  Conn.Close
  Debug.Print "Finished"
End Sub


Sub Expand()
  Dim a As New CDB
  a.ConnectDB
  a.OpenDB "SELECT * From MonsterFeats", True
  a.rs.MoveFirst
  Do While Not a.rs.EOF
    a.rs!SkillName = "-"
    a.rs!SkillRanks = 0
    a.rs.Update
    a.rs.MoveNext
    DoEvents
  Loop
  a.CloseDB
  Conn.Close
  Debug.Print "Done"
End Sub

'Function RollTreasureTable(strTableName As String) As String
'  Dim lngRoll As Long
'  Dim ddb As New CDB
'  Dim bytRoll As Byte
'  Dim strResult As String
'
'  ddb.OpenDB "SELECT * FROM " & strTableName & " ORDER BY Roll"
'
'  bytRoll = d100
'  strResult = ""
'  If ddb.rs.State > 0 Then
'    If ddb.rs.RecordCount > 0 Then
'      ddb.rs.MoveFirst
'      Do
'        ddb.rs.MoveNext
'        If ddb.rs.EOF = True Then Exit Do
'      Loop Until ddb.rs!roll >= bytRoll
'      ddb.rs.MovePrevious
'      If ddb.rs.EOF = True Then ddb.rs.MoveLast
'
'      strResult = ddb.rs!Name
'
'      If Not IsNull(ddb.rs!SubtableName) Then
'        strResult = strResult & "|" & RollTreasureTable("Treasure" & ddb.rs!SubtableName)
'      End If
'
'    End If
'    ddb.CloseDB
'  End If
'  RollTreasureTable = strResult
'End Function

Function RollTreasureTableMagic(strTableName As String, strFieldName As String, ByRef sngValue As Single, Optional ByRef bytBonusAdjustment As Byte = 0, Optional ByRef blnIsEpic As Boolean = False) As String
  Dim lngRoll As Long
  Dim ddb As New CDB
  Dim bytRoll As Byte
  Dim strResult As String
  
  Dim fldctr As Long
  
  Select Case strFieldName
    Case "Minor"
      ddb.OpenDB "SELECT * FROM " & strTableName & " WHERE Minor > 0 ORDER BY Minor"
    Case "Medium"
      ddb.OpenDB "SELECT * FROM " & strTableName & " WHERE Medium > 0 ORDER BY Medium"
    Case "Major"
      ddb.OpenDB "SELECT * FROM " & strTableName & " WHERE Major > 0 ORDER BY Major"
    Case "Epic"
      ddb.OpenDB "SELECT * FROM " & strTableName & " WHERE Epic > 0 ORDER BY Epic"
    Case "Roll"
      ddb.OpenDB "SELECT * FROM " & strTableName & " ORDER BY Roll"
  End Select
  
  bytRoll = d100
  strResult = ""
  If ddb.rs.State > 0 Then
    If ddb.rs.RecordCount > 0 Then
      ddb.rs.MoveFirst
      Select Case strFieldName
        Case "Roll"
          Do
            ddb.rs.MoveNext
            If ddb.rs.EOF = True Then Exit Do
          Loop Until ddb.rs!roll >= bytRoll
        Case "Minor"
          Do
            ddb.rs.MoveNext
            If ddb.rs.EOF = True Then Exit Do
          Loop Until ddb.rs!Minor >= bytRoll
        Case "Medium"
          Do
            ddb.rs.MoveNext
            If ddb.rs.EOF = True Then Exit Do
          Loop Until ddb.rs!Medium >= bytRoll
        Case "Major"
          Do
            ddb.rs.MoveNext
            If ddb.rs.EOF = True Then Exit Do
          Loop Until ddb.rs!Major >= bytRoll
        Case "Epic"
          Do
            ddb.rs.MoveNext
            If ddb.rs.EOF = True Then Exit Do
          Loop Until ddb.rs!Epic >= bytRoll
      End Select
      ddb.rs.MovePrevious
      If ddb.rs.EOF = True Then ddb.rs.MoveLast
      
      
      If ddb.rs!Name Like "Roll twice again" Or ddb.rs!Name Like "Roll again twice" Then
        strResult = RollTreasureTableMagic(strTableName, strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = strResult & "|" & RollTreasureTableMagic(strTableName, strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll twice on nonepic magic item Table: Armor Special Abilities. " Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll on nonepic magic item Table: Armor Special Abilities, then roll again on this table. " Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Epic", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll twice on nonepic magic item Table: Melee Weapon Special Abilities." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll on nonepic magic item Table: Melee Weapon Special Abilities, then roll again on this table." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Epic", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll on nonepic magic item Table: Ranged Weapon Special Abilities, then roll again on this table." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Epic", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll twice on nonepic magic item Table: Ranged Weapon Special Abilities." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll on nonepic magic item Table: Shield Special Abilities, then roll again on this table." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Epic", sngValue, bytBonusAdjustment, blnIsEpic)
      ElseIf ddb.rs!Name Like "Roll twice on nonepic magic item Table: Shield Special Abilities." Then
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
        strResult = RollTreasureTableMagic(strTableName, "Major", sngValue, bytBonusAdjustment, blnIsEpic)
      Else
        
        strResult = ddb.rs!Name
        If Not IsNull(ddb.rs!Value) Then
          sngValue = sngValue + ddb.rs!Value
        End If
        
        For fldctr = 0 To ddb.rs.Fields.Count - 1
          If ddb.rs.Fields(fldctr).Name Like "BonusAdjustment" Then
            If Not IsNull(ddb.rs!BonusAdjustment) Then
              bytBonusAdjustment = bytBonusAdjustment + ddb.rs!BonusAdjustment
            End If
            Exit For
          End If
        Next fldctr
        
        
        For fldctr = 0 To ddb.rs.Fields.Count - 1
          If ddb.rs.Fields(fldctr).Name Like "IsEpic" Then
            If Not IsNull(ddb.rs!IsEpic) Then
              If ddb.rs!IsEpic = True Then blnIsEpic = True
            End If
            Exit For
          End If
        Next fldctr
        
        
        If Not IsNull(ddb.rs!Subtablename) Then
          If ddb.rs!Subtablename Like "*SpecialAbilities*" Then
            If bytBonusAdjustment = 0 Then
              bytBonusAdjustment = bytBonusAdjustment + 1
              strResult = strResult & "|+1|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Else
              strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            End If
            
          ElseIf (ddb.rs!Subtablename Like "*Scroll*" And _
            Not (ddb.rs!Subtablename Like "*ScrollSpellLevels*") And _
            Not (ddb.rs!Subtablename Like "*ScrollType*")) Then
            
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*RandomArmor*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*RandomShield*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*WeaponCommon*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*WeaponUncommon*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*UniversalItems*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          ElseIf (ddb.rs!Subtablename Like "*PsionicPower*") Then
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, "Roll", sngValue, bytBonusAdjustment, blnIsEpic)
          Else
            strResult = strResult & "|" & RollTreasureTableMagic("Treasure" & ddb.rs!Subtablename, strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
          End If
        End If
  
        If strResult Like "*eapon|*" Then
          If strResult Like "*elee*" Then
            Select Case strFieldName
              Case "Minor"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponMelee", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Medium"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponMelee", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Major"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponMelee", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Epic"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponMelee", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            End Select
          Else
            Select Case strFieldName
              Case "Minor"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponRanged", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Medium"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponRanged", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Major"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponRanged", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
              Case "Epic"
                strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicWeaponRanged", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            End Select
          End If
          
          If bytBonusAdjustment > 10 Then blnIsEpic = True
          If blnIsEpic = True Then
            sngValue = sngValue + 20000 * bytBonusAdjustment ^ 2
          Else
            sngValue = sngValue + 2000 * bytBonusAdjustment ^ 2
          End If
        End If
        If strResult Like "Shield|*" Then
          Select Case strFieldName
            Case "Minor"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicShield", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Medium"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicShield", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Major"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicShield", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Epic"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicShield", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
          End Select
          
          If bytBonusAdjustment > 10 Then blnIsEpic = True
          If blnIsEpic = True Then
            sngValue = sngValue + 10000 * bytBonusAdjustment ^ 2
          Else
            sngValue = sngValue + 1000 * bytBonusAdjustment ^ 2
          End If
        End If
        
        If strResult Like "Armor|*" Then
          Select Case strFieldName
            Case "Minor"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicArmor", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Medium"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicArmor", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Major"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicArmor", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
            Case "Epic"
              strResult = strResult & "|" & RollTreasureTableMagic("TreasureMagicArmor", strFieldName, sngValue, bytBonusAdjustment, blnIsEpic)
          End Select
          
          If bytBonusAdjustment > 10 Then blnIsEpic = True
          If blnIsEpic = True Then
            sngValue = sngValue + 10000 * bytBonusAdjustment ^ 2
          Else
            sngValue = sngValue + 1000 * bytBonusAdjustment ^ 2
          End If
        End If
        
        
      End If
      ddb.CloseDB
    End If
  End If
  RollTreasureTableMagic = strResult
End Function


Public Sub TestRandomMagic()
  Dim i As Long
  Dim sngValue As Single
  Dim strName As String
  
  Randomize Timer
  
  For i = 1 To 10
    sngValue = 0
    strName = RollTreasureTableMagic("TreasureMundaneItem", "Roll", sngValue)
    Debug.Print strName & " (worth " & Format(sngValue, "#,###,##0") & " gp)"
  Next i
  
  For i = 1 To 10
    sngValue = 0
    strName = RollTreasureTableMagic("TreasureMagicItem", "Minor", sngValue)
    Debug.Print strName & " (worth " & Format(sngValue, "#,###,##0") & " gp)"
  Next i
  For i = 1 To 10
    sngValue = 0
    strName = RollTreasureTableMagic("TreasureMagicItem", "Medium", sngValue)
    Debug.Print strName & " (worth " & Format(sngValue, "#,###,##0") & " gp)"
  Next i
  For i = 1 To 10
    sngValue = 0
    strName = RollTreasureTableMagic("TreasureMagicItem", "Major", sngValue)
    Debug.Print strName & " (worth " & Format(sngValue, "#,###,##0") & " gp)"
  Next i
  For i = 1 To 10
    sngValue = 0
    strName = RollTreasureTableMagic("TreasureMagicItem", "Epic", sngValue)
    Debug.Print strName & " (worth " & Format(sngValue, "#,###,##0") & " gp)"
  Next i
End Sub





Sub testName()
  Dim a As New CRandomName
  Dim i As Long
  
  Randomize Timer
  
  a.strSourceFolder = App.Path & "\names"
  a.strFileName = "drow_names.nam"
  
  For i = 1 To 5
    Debug.Print a.RandomName("F")
  Next i
  
  

End Sub




Private Function RollPrCs(strBaseClass As String, lngTotalLevel As Long, strRace As String) As String
  Dim rs As New ADODB.Recordset
  Dim n As Long
  Dim k As Long
  Dim strOut As String
  Dim lngPrCLevel As Long
  
  rs.Open "SELECT * FROM NPCPrestigeClassBaseClasses WHERE TotalLevel < " & _
    CStr(lngTotalLevel) & " AND (Class1Class LIKE '" & strBaseClass & _
    "' or Class1Class LIKE 'Any' or (Class2Class LIKE '" & strBaseClass & _
    "' AND Class1Level = Class2Level)) AND (RaceRestriction LIKE '" & strRace & _
    "' OR RaceRestriction IS NULL) ORDER BY TotalLevel, ClassName", _
    Conn, adOpenStatic, adLockReadOnly
  
  If rs.State > 0 Then
    If rs.RecordCount > 0 Then
      n = dN(1, rs.RecordCount)
      rs.MoveFirst
      rs.move n - 1
      If rs!ClassName Like strBaseClass Then
        strOut = strBaseClass
        RollPrCs = strOut
      Else
        If rs!Class1Class Like "Any" Then
          strOut = strBaseClass
        Else
          strOut = rs!Class1Class
        End If
        
        If rs!Class1Level > 0 Then
          strOut = strOut & " " & CStr(rs!Class1Level)
          lngPrCLevel = lngTotalLevel - rs!Class1Level
        Else
          k = dN(1, lngTotalLevel - 1)
          lngPrCLevel = lngTotalLevel - k
          strOut = strOut & " " & CStr(k)
        End If
        
        If Not IsNull(rs!Class2Class) Then
          strOut = strOut & "/" & rs!Class2Class & " " & CStr(rs!Class2Level)
          lngPrCLevel = lngPrCLevel - rs!Class2Level
        End If
        strOut = strOut & "/" & rs!ClassName & " " & CStr(lngPrCLevel)
        RollPrCs = strOut
      End If
    Else
      RollPrCs = strBaseClass
    End If
    rs.Close
  End If
End Function

Function testRollPrCs()
  Dim i As Long
  Dim db As New CDB
  db.ConnectDB
  For i = 1 To 10
    Debug.Print RollPrCs("Fighter", 10, "Human")
  Next i
End Function



Sub FixEndLines()

  Dim strIn As String
  Dim log As New CLogAndIniRoutines
  Dim strTemp As String
  Dim fso As Object
  Dim ts As Object
  
  
  log.bWriteLog = True
  log.strLogFileName = "C:\Documents and Settings\bernardb.SAIC-US-EAST\My Documents\Dungeons and Dragons\I11 Needle\i11Out.txt"
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile("C:\Documents and Settings\bernardb.SAIC-US-EAST\My Documents\Dungeons and Dragons\I11 Needle\i11.txt")
  Do
    strTemp = ts.Readline
    log.writeLogEntry strTemp
  Loop Until ts.AtEndOfStream = True
  ts.Close

  
  
End Sub
