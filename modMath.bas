Attribute VB_Name = "modMath"
Option Explicit

'Returns a normally distributed deviate with zero mean and unit variance, using ran1(idum)
'as the source of uniform deviates.
Public Function randn() As Single

  Static gset As Single
  
  Dim fac As Single, rsq As Single, v1 As Single, v2 As Single
  
  Do While (rsq >= 1# Or rsq = 0#)
    v1 = 2# * Rnd() - 1# 'pick two uniform numbers in the square extending
    'from -1 to +1 in each direction,
    v2 = 2# * Rnd() - 1#
    rsq = v1 * v1 + v2 * v2 'see if they are in the unit circle,
  Loop
  fac = Sqr(-2# * log(rsq) / rsq)
  gset = v1 * fac
  randn = v2 * fac
End Function

