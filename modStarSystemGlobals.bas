Attribute VB_Name = "modStarSystemGlobals"
Option Explicit



' solar system constants
Global Const MSol As Double = 1.98892E+30 ' kg
Global Const RSol As Long = 695500000 ' m
Global Const TSol As Long = 5780 ' Kelvin
Global Const MvSol As Single = 4.83 ' Absolute Magnitude
Global Const MAppSol As Single = -26.77 ' Apparent Magnitude of Sol from Earth
Global Const MEarth As Double = 5.9742E+24 ' kg
Global Const REarth  As Double = 6378.1 ' km
Global Const MJupiter As Double = 1.8987E+27 ' kg

' Atomic constants
Global Const MElectron As Double = 9.10938188E-31 'kg
Global Const MHydrogen As Double = 1.67353403897E-27 'kg

' Physical and Mathematical constants
Global Const w0 As Double = 30.376077073 ' AU/yr: fundamental and universal speed of planetary accretion disk
Global Const PI As Double = 3.14159265359
Global Const G As Double = 6.67259E-11        ' m�/(s��kg)
Global Const HPlanck As Double = 6.6260755E-34 'J�s
Global Const HBar As Double = 1.0545727E-34 'J�s
Global Const UniversalGasConstantR As Double = 8.31 'K/mol�K
Global Const AvogadroConstantNA As Double = 6.02E+23 'atoms/mol
Global Const StefanBoltzmannConstant As Double = 0.0000000567 'W/(m��K4)
Global Const SpeedOfLight As Double = 299792458 ' m/s; exact
Global Const kB As Double = 1.380658E-23 ' J/K

' Conversion ratios
Global Const DiamMi2RadKm As Single = 0.8045 ' converts planetary diameter in miles to radius in km
Global Const AU2km As Double = 149597900 ' Converts astronomical units to km
Global Const mi2km As Single = 1.609344 ' miles per km

' Density Constants
Global Const HDensityKgM3 As Double = 0.09
Global Const AerogelDensityKgM3 As Double = 3
Global Const IceDensityKgM3 As Double = 900
Global Const H2ODensityKgM3 As Double = 997
Global Const NDensityKgM3 As Double = 1250
Global Const ODensityKgM3 As Double = 1429
Global Const MgDensityKgM3 As Double = 1738
Global Const SiDensityKgM3 As Double = 2330
Global Const CarbonaceousMatrixRichCometDensityKgM3 = 2600
Global Const CarbonaceousMetalRichCometDensityKgM3 = 3300
Global Const ChondriteCometDensityKgM3 = 3750
Global Const CDensityKgM3 As Double = 3510
Global Const FayaliteDensityKgM3 = 4312 ' Magnesium Silicate
Global Const FeDensityKgM3 As Double = 7874
Global Const NiDensityKgM3 As Double = 8908
Global Const PbDensityKgM3 As Double = 11340
Global Const UDensityKgM3 As Double = 19050
Global Const WhiteDwarfStarCoreDensityKgM3 As Double = 10000000000#
Global Const UraniumNucleusDensityKgM3 As Double = 3E+17
Global Const NeutronStarCoreDensityKgM3 As Double = 1E+17 'to 1e18 kg/m�
Global Const NeutronStarMeanDensityKgM3 As Double = 1E+15
Global Const BlackHoleDensityKgM3 As Double = 1E+19

Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


