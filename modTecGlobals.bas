Attribute VB_Name = "modTecGlobals"
Option Explicit


' Declares for randomad DLL
Public Declare Sub TRandomInit Lib "randomad.dll" (ByVal seed As Long)
Public Declare Sub TRandomInitByArray Lib "randomad.dll" (ByRef seeds() As Long, length As Long)
Public Declare Function TBRandom Lib "randomad.dll" () As Double
Public Declare Function TIRandom Lib "randomad.dll" (ByVal min As Long, ByVal max As Long) As Double
Public Declare Function TRandom Lib "randomad.dll" () As Double
Public Declare Function TRandom2 Lib "randomad.dll" () As Double
Public Declare Function TRandom3 Lib "randomad.dll" () As Double


Global color_values(31) As Long

Global MAXMOUNTAINHEIGHT As Long  ' maximum mountain height = 32,700 feet

Global MAXX As Long
Global MAXY As Long

Global Const MAXPRESS = 255

Global Const SIZE = 1
Global WINDOW_HEIGHT As Long
Global WINDOW_WIDTH As Long
Global Const MAXPLATE = 500

Global landcols(256) As Long
Global greycols(256) As Long
Global tecols() As Long
Global platecols(MAXPLATE) As Long
Global heatcols(256) As Long
Global popcols(256) As Long

Global XWinScale As Double
Global YWinScale As Double

Enum PrintMode_E
  PRINTMODE_NONE = 0
  PRINTMODE_LONG
  PRINTMODE_SCALE
  PRINTMODE_SHORT
  PRINTMODE_GREY
  PRINTMODE_CLIM
End Enum

Enum DrawType_E
  DRAW_GREY = 1
  DRAW_LAND
  DRAW_CLIM
  DRAW_TEC
  DRAW_PLATE
  DRAW_JET
  DRAW_TRUE
  DRAW_DND
  DRAW_POP
End Enum

Enum LineType_E
  LINE_DIAG = 1
  LINE_CORN
  LINE_NONE
End Enum


'Global Const PI = 3.14159265359
Global Const TWOPI = 6.28318530718
Global Const TWOMILLIPI = 0.00628318
Global Const FREEZING = 273#
Global Const DEG2RAD = (PI / 180)
Global n As Long
Global area As Long
Global Const RADIUS = 45
Global Const HALFPI = (PI / 2#)

Global Const MAXB = 12 '4

'Global Const LINE_DIAG = 1
'Global Const LINE_CORN = 2

Global Const LINE_0V = 1
Global Const LINE_1V = 2
Global Const LINE_0H = 4
Global Const LINE_1H = 8

Global Const North = LINE_0V
Global Const South = LINE_1V
Global Const East = LINE_0H
Global Const West = LINE_1H
Global Const Northwest = North Or West
Global Const Southwest = South Or West
Global Const Northeast = North Or East
Global Const Southeast = South Or East


Global Const TREELINE_FT = 10000
Global Const SNOWLINE_FT = 12000

Public Enum Pressure_E
  PR_LOW = 1
  PR_HIGH
  PR_HEQ
End Enum

Public Enum MainType_E
  M_MAIN = 0
  M_HEAT
  M_PRESS
  M_WIND
  M_RAIN
  M_CLIM
End Enum

Public Enum Climate_E
  C_OCEAN = 0
  C_OCEANICE
  C_SWAMP
  C_BARELAND
  C_LAKERIVER
  C_MOUNTAIN
  
  C_JUNGLE ' Af
  C_DECID ' Cf
  C_BOREAL ' Df
  
  C_TROPICWETDRY ' Aw
  C_MONSOON ' Am
  C_GRASSLAND ' Cw
  C_MEDITERRANEAN ' Cs
  C_COLDDRYWINTER ' Dw
  
  C_SAVANA
  C_DESERT ' Bw
  
  C_STEPPE
  
  C_TUNDRA ' Et
  C_LANDICE
End Enum

Public Enum DNDCLIMATE_E
  D_WARM_SWAMP
  D_WARM_FOREST
  D_WARM_PLAIN
  D_WARM_DESERT
  D_WARM_HILL
  D_WARM_MOUNTAIN
  D_WARM_AQUATIC
  D_TEMP_SWAMP
  D_TEMP_FOREST
  D_TEMP_PLAIN
  D_TEMP_DESERT
  D_TEMP_HILL
  D_TEMP_MOUNTAIN
  D_TEMP_AQUATIC
  D_COLD_SWAMP
  D_COLD_FOREST
  D_COLD_PLAIN
  D_COLD_DESERT
  D_COLD_HILL
  D_COLD_MOUNTAIN
  D_COLD_AQUATIC
  D_OCEANICE
End Enum

Public Enum MONTH_E
  JANUARY
  FEBRUARY
  MARCH
  APRIL
  MAY
  JUNE
  JULY
  AUGUST
  SEPTEMBER
  OCTOBER
  NOVEMBER
  DECEMBER
End Enum

Public Enum COLOR_E
  clr_black
  clr_white
  clr_grey75
  clr_grey50
  clr_MediumPurple1
  clr_purple4
  clr_purple3
  clr_purple2
  clr_purple1
  clr_blue4
  clr_blue3
  clr_blue2
  clr_blue1
  clr_DarkGreen
  clr_green4
  clr_green3
  clr_green2
  clr_green1
  clr_DarkGoldenrod4
  clr_yellow4
  clr_yellow3
  clr_yellow2
  clr_yellow1
  clr_orange4
  clr_orange3
  clr_orange2
  clr_orange1
  clr_brown4
  clr_red4
  clr_red3
  clr_red2
  clr_red1
End Enum

Global climcols(C_LANDICE) As Long
Global truecols(C_LANDICE) As Long
Global dndcols(D_OCEANICE) As Long

Function CMP(s As String, X As String) As String
  CMP = Not StrComp(s, X)
End Function












Sub panic(s As String)
  'frmTec.StatusBar1.Panels(1).Text = "PANIC: " & s
  
  'Set frmTec.tec = Nothing
  
  'Unload frmTec
  'Beep
  
End Sub




Function random(top As Long) As Long
  ' produces a random number from 0 to top
  random = CLng(TRandom * top)
End Function

Sub testran1()
  Dim i As Integer
  Dim an As Double
  
  TRandomInit Timer
  
'  Debug.Print ("Random integers in interval from 0 to 99:")
'  For i = 1 To 40
'    an = TIRandom(0, 99)
'    Debug.Print Format(an, "00")
'    If i Mod 10 = 0 Then
'        Debug.Print
'    End If
'  Next i
'  Debug.Print

  Debug.Print ("Random floating point numbers in interval from 0 to 1:")
  For i = 1 To 32
    an = TRandom
    Debug.Print Format(an, "0.000000 ")
    If i Mod 8 = 0 Then
        Debug.Print
    End If
  Next i
  Debug.Print

End Sub

Sub DrawGrid()
  ' Draws lat/lon grid
  Dim i As Long
  
  
  ' Latitude
  
'  For i = 0 To Picture1.ScaleHeight Step Picture1.ScaleHeight / 9
'    Picture1.Line (0, i)-(Picture1.ScaleWidth, i)
'  Next i
  With frmTec
    ' 15 degree longitude
    'For i = 0 To .Picture1.ScaleWidth Step .Picture1.ScaleWidth / 24
    '  .Picture1.Line (i, 0)-(i, .Picture1.ScaleHeight)
    'Next i
  
    ' Equator
    '.Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
  
    ' Tropics
    .Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 + .clim.Tilt / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
    .Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 - .clim.Tilt / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
  
    ' Arctics
    .Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 + (90 - .clim.Tilt) / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
    .Picture1.Line (0, .Picture1.ScaleHeight / 2 - 1 * Screen.TwipsPerPixelY / 2 - (90 - .clim.Tilt) / 180 * .Picture1.ScaleHeight)-Step(.Picture1.ScaleWidth, 1 * Screen.TwipsPerPixelY), , BF
  End With

End Sub

Sub DrawRegionGrid()
  ' Draws lat/lon grid
  Dim i As Long
  
  
  ' Latitude
  With frmTec
    ' 15 degree longitude
    For i = 0 To .Picture1.ScaleWidth Step .Picture1.ScaleWidth / 16
      .Picture1.Line (i, 0)-(i, .Picture1.ScaleHeight)
    Next i
  
    For i = 0 To .Picture1.ScaleHeight Step .Picture1.ScaleHeight / 8
      .Picture1.Line (0, i)-(.Picture1.ScaleWidth, i)
    Next i
  
  
  End With

End Sub

Function randnDLL() As Single
  Static iset As Long
  Static gset As Single
  Dim fac As Single, rsq As Single, v1 As Single, v2 As Single
    
  If iset = 0 Then
    Do
      v1 = 2# * TRandom - 1#  'pick two uniform numbers in the square extending from -1 to +1 in each direction,
      v2 = 2# * TRandom - 1#
      rsq = v1 * v1 + v2 * v2
    Loop While (rsq >= 1# Or rsq = 0#)   ' and if they are not, try again.
    fac = Sqr(-2# * log(rsq) / rsq)
    ' Now make the Box-Muller transformation to get two normal deviates. Return one and
    ' save the other for next time.
    gset = v1 * fac
    iset = 1 ' Set flag.
    randnDLL = v2 * fac
  Else ' We have an extra deviate handy,
    iset = 0 ' so unset the flag,
    randnDLL = gset ' and return it.
  End If
  
End Function

'Returns a normally distributed deviate with zero mean and unit variance, using ran1(idum)
'as the source of uniform deviates.
Public Function randn() As Single

  Static gset As Single
  
  Dim fac As Single, rsq As Single, v1 As Single, v2 As Single
  
  Do While (rsq >= 1# Or rsq = 0#)
    v1 = 2# * Rnd() - 1# 'pick two uniform numbers in the square extending
    'from -1 to +1 in each direction,
    v2 = 2# * Rnd() - 1#
    rsq = v1 * v1 + v2 * v2 'see if they are in the unit circle,
  Loop
  fac = Sqr(-2# * log(rsq) / rsq)
  gset = v1 * fac
  randn = v2 * fac
End Function

Function MaxMax(Z() As Integer) As Long
  Dim i As Long, j As Long, maxZ As Long
  maxZ = -2000000000#
  
  For i = 0 To UBound(Z, 1)
    For j = 0 To UBound(Z, 2)
      If maxZ <= Z(i, j) Then maxZ = Z(i, j)
    Next
  Next
  MaxMax = maxZ
  
End Function

Function MinMin(Z() As Integer) As Long
  Dim i As Long, j As Long, minZ As Long
  minZ = 2000000000#
  
  For i = 0 To UBound(Z, 1)
    For j = 0 To UBound(Z, 2)
      If minZ >= Z(i, j) Then minZ = Z(i, j)
    Next
  Next
  MinMin = minZ
  
End Function


Function ceil(n As Single) As Long
  If n - Fix(n) > 0 Then
    ceil = Fix(n) + 1
  Else
    ceil = Fix(n)
  End If
End Function

Function ndims(L() As Long) As Long
  Dim i As Long
  Dim lim As Long
  On Error GoTo exitfun
  i = 1
  Do
    lim = UBound(L, i)
    i = i + 1
  Loop
exitfun:
  ndims = i - 1
  
End Function

Function ndimsB(L() As Byte) As Long
  Dim i As Long
  Dim lim As Long
  On Error GoTo exitfun
  i = 1
  Do
    lim = UBound(L, i)
    i = i + 1
  Loop
exitfun:
  ndimsB = i - 1
  
End Function

Function ndimsI(L() As Integer) As Long
  Dim i As Long
  Dim lim As Long
  On Error GoTo exitfun
  i = 1
  Do
    lim = UBound(L, i)
    i = i + 1
  Loop
exitfun:
  ndimsI = i - 1
  
End Function


Function EmptySingle(rows As Long, Optional cols As Long = 0, Optional Z As Long = 0) As Single()
  ' creates an empty array of size (rows, cols, z)
  Dim sngO() As Single
  If rows > 0 Then
    If cols > 0 Then
      If Z > 0 Then
        ReDim sngO(rows, cols, Z) As Single
      Else
        ReDim sngO(rows, cols) As Single
      End If
    Else
      ReDim sngO(rows) As Single
    End If
  End If
  EmptySingle = sngO
End Function

Function Gradient(f() As Long, X() As Single, Y() As Single, Z() As Single, _
  dfdx() As Single, dfdy() As Single, dfdz() As Single) As Single()
  
  ' [df/dx, df/dy, df/dz] = Gradient(f, x, y, z)
  
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim G() As Single
  Dim h As Single
  
  Dim ndim As Long
  Dim n As Long, p As Long, L As Long
  
  ndim = ndims(f)
  
  If ndim = 1 Then
    n = UBound(f, 1)
    ReDim G(n - 1) As Single
    For i = 0 To n - 1
      G(i, j) = CSng(f(i + 1) - f(i)) / CSng(X(i + 1) - X(i))
    Next i
    
  ElseIf ndim = 2 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    ReDim dfdx(n - 1, p - 1) As Single
    ReDim dfdy(n - 1, p - 1) As Single
    ReDim G(n - 1, p - 1) As Single
    For i = 0 To n - 1
      For j = 0 To p - 1
        dfdx(i, j) = CSng(f(i, j + 1) - f(i, j)) / CSng(X(j + 1) - X(j))
        dfdy(i, j) = CSng(f(i + 1, j) - f(i, j)) / CSng(Y(i + 1) - Y(i))
        G(i, j) = Sqr(dfdx(i, j) ^ 2 + dfdy(i, j) ^ 2)
      Next j
    Next i
  ElseIf ndim = 3 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    L = UBound(f, 3)
    ReDim dfdx(n - 1, p - 1, L - 1) As Single
    ReDim dfdy(p - 1, p - 1, L - 1) As Single
    ReDim dfdz(L - 1, p - 1, L - 1) As Single
    ReDim G(n - 1, p - 1, L - 1) As Single
    For i = 0 To n - 1
      For j = 0 To p - 1
        For k = 0 To L - 1
          dfdx(i, j, k) = CSng(f(i, j + 1, k) - f(i, j, k)) / CSng(X(j + 1) - X(j))
          dfdy(i, j, k) = CSng(f(i + 1, j, k) - f(i, j, k)) / CSng(Y(i + 1) - Y(i))
          dfdy(i, j, k) = CSng(f(i, j, k + 1) - f(i, j, k)) / CSng(Z(k) - Z(k + 1))
          G(i, j, k) = Sqr(dfdx(i, j, k) ^ 2 + dfdy(i, j, k) ^ 2 + dfdz(i, j, k) ^ 2)
        Next k
      Next j
    Next i
  End If
  Gradient = G
End Function


Function QuickGradient(f() As Integer) As Integer()
  ' like gradient, only returns magnitude, and assumes all h=1
  
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim G() As Integer
  Dim h As Long
  Dim dfdx As Long, dfdy As Long, dfdz As Long
  Dim ndim As Long
  Dim n As Long, p As Long, L As Long
  
  ndim = ndimsI(f)
  
  If ndim = 1 Then
    n = UBound(f, 1)
    ReDim G(n - 1) As Integer
    For i = 0 To n - 1
      G(i, j) = CSng(f(i + 1) - f(i))
    Next i
  
  ElseIf ndim = 2 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    ReDim G(n - 1, p - 1) As Integer
    For i = 0 To n - 1
      For j = 0 To p - 1
        dfdx = CSng(f(i, j + 1) - f(i, j))
        dfdy = CSng(f(i + 1, j) - f(i, j))
        G(i, j) = Sqr(dfdx ^ 2 + dfdy ^ 2)
      Next j
    Next i
    
  ElseIf ndim = 3 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    L = UBound(f, 3)
    ReDim G(n - 1, p - 1, L - 1) As Integer
    For i = 0 To n - 1
      For j = 0 To p - 1
        For k = 0 To L - 1
          dfdx = CSng(f(i, j + 1, k) - f(i, j, k))
          dfdy = CSng(f(i + 1, j, k) - f(i, j, k))
          dfdy = CSng(f(i, j, k + 1) - f(i, j, k))
          G(i, j, k) = Sqr(dfdx ^ 2 + dfdy ^ 2 + dfdz ^ 2)
        Next k
      Next j
    Next i
  End If
  QuickGradient = G
End Function

Function Div(f() As Long, X() As Single, Y() As Single, Z() As Single) As Single()
  
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim G() As Single
  Dim h As Single
  Dim dfdx As Single, dfdy As Single, dfdz As Single
  
  Dim ndim As Long
  Dim n As Long, p As Long, L As Long
  
  ndim = ndims(f)
  
  If ndim = 1 Then
    n = UBound(f, 1)
    ReDim G(n - 1) As Single
    For i = 0 To n - 1
      G(i, j) = CSng(f(i + 1) - f(i)) / CSng(X(i + 1) - X(i))
    Next i
    
  ElseIf ndim = 2 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    ReDim G(n - 1, p - 1) As Single
    For i = 0 To n - 1
      For j = 0 To p - 1
        dfdx = CSng(f(i, j + 1) - f(i, j)) / CSng(X(j + 1) - X(j))
        dfdy = CSng(f(i + 1, j) - f(i, j)) / CSng(Y(i + 1) - Y(i))
        G(i, j) = dfdx + dfdy
      Next j
    Next i
    
  ElseIf ndim = 3 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    L = UBound(f, 3)
    ReDim G(n - 1, p - 1, L - 1) As Single
    For i = 0 To n - 1
      For j = 0 To p - 1
        For k = 0 To L - 1
          dfdx = CSng(f(i, j + 1, k) - f(i, j, k)) / CSng(X(j + 1) - X(j))
          dfdy = CSng(f(i + 1, j, k) - f(i, j, k)) / CSng(Y(i + 1) - Y(i))
          dfdy = CSng(f(i, j, k + 1) - f(i, j, k)) / CSng(Z(k) - Z(k + 1))
          G(i, j, k) = dfdx + dfdy + dfdz
        Next k
      Next j
    Next i
  End If
  Div = G
End Function

Function QuickDiv(f() As Long) As Long()
  ' returns divergence field
  
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim G() As Long
  Dim h As Long
  Dim dfdx As Long, dfdy As Long, dfdz As Long
  Dim ndim As Long
  Dim n As Long, p As Long, L As Long
  
  ndim = ndims(f)
  
  If ndim = 1 Then
    n = UBound(f, 1)
    ReDim G(n - 1) As Long
    For i = 0 To n - 1
      dfdy = CSng(f(i + 1) - f(i))
      G(i) = dfdx
    Next i
  
  ElseIf ndim = 2 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    ReDim G(n - 1, p - 1) As Long
    For i = 0 To n - 1
      For j = 0 To p - 1
        dfdx = CSng(f(i, j + 1) - f(i, j))
        dfdy = CSng(f(i + 1, j) - f(i, j))
        G(i, j) = dfdx + dfdy
      Next j
    Next i
    
  ElseIf ndim = 3 Then
    n = UBound(f, 1)
    p = UBound(f, 2)
    L = UBound(f, 3)
    ReDim G(n - 1, p - 1, L - 1) As Long
    For i = 0 To n - 1
      For j = 0 To p - 1
        For k = 0 To L - 1
          dfdx = CSng(f(i, j + 1, k) - f(i, j, k))
          dfdy = CSng(f(i + 1, j, k) - f(i, j, k))
          dfdy = CSng(f(i, j, k + 1) - f(i, j, k))
          G(i, j, k) = dfdx + dfdy + dfdz
        Next k
      Next j
    Next i
  End If
  
  QuickDiv = G
End Function



Sub assign_colors(mvarZShelf As Long, mvarZCoast As Long)
  Dim i As Long
  
  FillX11ColorTable
  
  color_values(clr_black) = &H0
  color_values(clr_white) = &HFFFFFF
  color_values(clr_grey75) = &HBFBFBF
  color_values(clr_grey50) = &H7F7F7F
  color_values(clr_MediumPurple1) = RGB(&HAB, &H82, &HFF)
  color_values(clr_purple4) = RGB(&H55, &H1A, &H8B)
  color_values(clr_purple3) = RGB(&H7D, &H26, &HCD)
  color_values(clr_purple2) = RGB(&H91, &H2C, &HEE)
  color_values(clr_purple1) = RGB(&H9B, &H30, &HFF)
  color_values(clr_blue4) = RGB(0, 0, &H8B)
  color_values(clr_blue3) = RGB(0, 0, &HCD)
  color_values(clr_blue2) = RGB(0, 0, &HEE)
  color_values(clr_blue1) = RGB(0, 0, &HFF)
  color_values(clr_DarkGreen) = RGB(0, &H64, 0)
  color_values(clr_green4) = RGB(0, &H8B, 0)
  color_values(clr_green3) = RGB(0, &HCD, 0)
  color_values(clr_green2) = RGB(0, &HEE, 0)
  color_values(clr_green1) = RGB(0, &HFF, 0)
  color_values(clr_DarkGoldenrod4) = RGB(&H8B, &H65, &H8)
  color_values(clr_yellow4) = RGB(&H8B, &H8B, &H0)
  color_values(clr_yellow3) = RGB(&HCD, &HCD, &H0)
  color_values(clr_yellow2) = RGB(&HEE, &HEE, &H0)
  color_values(clr_yellow1) = RGB(&HFF, &HFF, &H0)
  color_values(clr_orange4) = RGB(&H8B, &H5A, &H0)
  color_values(clr_orange3) = RGB(&HCD, &H85, &H0)
  color_values(clr_orange2) = RGB(&HEE, &H9A, &H0)
  color_values(clr_orange1) = RGB(&HFF, &HA5, &H0)
  color_values(clr_brown4) = RGB(&H8B, &H23, &H23)
  color_values(clr_red4) = RGB(&H8B, &H0, &H0)
  color_values(clr_red3) = RGB(&HCD, &H0, &H0)
  color_values(clr_red2) = RGB(&HEE, &H0, &H0)
  color_values(clr_red1) = RGB(&HFF, &H0, &H0)
  
  ' Ocean
  climcols(C_OCEANICE) = color_values(clr_grey75)
  climcols(C_OCEAN) = color_values(clr_blue4)
  
  
  ' Dry year-round
  climcols(C_DESERT) = color_values(clr_yellow1) ' R < 25cm   (Somalia)
  climcols(C_SAVANA) = color_values(clr_orange1) ' 25 cm < R < 50 cm  (Africa)
  climcols(C_STEPPE) = color_values(clr_DarkGoldenrod4) ' Temperate savannah  (Colorado)
  
  ' Wet/Dry and Grasslands
  climcols(C_MONSOON) = color_values(clr_purple4)
  climcols(C_TROPICWETDRY) = color_values(clr_yellow2)
  
  climcols(C_GRASSLAND) = color_values(clr_green1) ' Wet summer, dry winter
  climcols(C_MEDITERRANEAN) = color_values(clr_orange3) 'RGB(105, 112, 70)    ' Dry summer, wet winter
  
  climcols(C_COLDDRYWINTER) = color_values(clr_green2) ' Wet summer, dry winter
  
  ' Forests
  climcols(C_BOREAL) = color_values(clr_DarkGreen) ' Coniferous
  climcols(C_DECID) = color_values(clr_green4)     ' Deciduous
  climcols(C_JUNGLE) = color_values(clr_green3)    ' Broadleaf
  
  ' Bare lands
  climcols(C_MOUNTAIN) = color_values(clr_DarkGoldenrod4)
  climcols(C_BARELAND) = color_values(clr_yellow3)
  
  ' Polar
  climcols(C_TUNDRA) = color_values(clr_grey50)
  climcols(C_LANDICE) = RGB(210, 220, 222)
  
  ' Other
  climcols(C_SWAMP) = RGB(128, 128, 255)
  climcols(C_LAKERIVER) = color_values(clr_blue1)
  
  
  truecols(C_OCEAN) = RGB(3, 29, 63)
  truecols(C_BARELAND) = RGB(194, 164, 125)
  truecols(C_MOUNTAIN) = RGB(137, 122, 92)
  truecols(C_OCEANICE) = RGB(229, 229, 229)
  truecols(C_TUNDRA) = RGB(109, 100, 69)
  truecols(C_STEPPE) = RGB(77, 99, 45)
  truecols(C_DESERT) = RGB(229, 228, 129)
  truecols(C_SAVANA) = RGB(85, 102, 47)
  truecols(C_DECID) = RGB(65, 86, 29)
  truecols(C_JUNGLE) = RGB(50, 69, 27)
  truecols(C_SWAMP) = RGB(36, 71, 58)
  truecols(C_LAKERIVER) = RGB(3, 29, 63)
  truecols(C_TROPICWETDRY) = RGB(160, 150, 115)
  truecols(C_MONSOON) = RGB(70, 80, 37)
  truecols(C_GRASSLAND) = RGB(113, 115, 63)
  truecols(C_MEDITERRANEAN) = RGB(105, 112, 70)
  truecols(C_BOREAL) = RGB(52, 70, 29)
  truecols(C_COLDDRYWINTER) = RGB(129, 120, 86)
  truecols(C_LANDICE) = RGB(210, 220, 222)
  
  landcols(0) = color_values(11)
  landcols(1) = color_values(28)
  For i = 2 To 255
    landcols(i) = color_values(3)
  Next i
  
  ' Hot
  For i = 0 To 32
    heatcols(i) = RGB(0, 0, (i * 4) + 128)
  Next i
  For i = 33 To 96
    heatcols(i) = RGB(0, (i - 32) * 4, 255)
  Next i
  For i = 97 To 160
    heatcols(i) = RGB((i - 96) * 8, 255, 256 - 4 * (i - 96))
  Next i
  For i = 161 To 224
    heatcols(i) = RGB(255, 256 - (4 * (i - 160)), 0)
  Next i
  For i = 225 To 255
    heatcols(i) = RGB(256 - (i - 224) * 4, 0, 0)
  Next i

  For i = 0 To 255
    'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
    greycols(i) = RGB(i, i, i)
  Next i
  greycols(256) = RGB(255, 255, 255)
  
  
  For i = 0 To 255
    popcols(i) = RGB(255, 255 - i, 255 - i)
  Next i
  
  
  For i = 1 To MAXPLATE
    platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
  Next i
  
  ReDim tecols(MAXMOUNTAINHEIGHT) As Long
  ' deep ocean
  For i = 0 To mvarZShelf
    tecols(i) = color_values(clr_blue4) ' dark blue
  Next i
  
  ' continental shelf
  For i = mvarZShelf To mvarZCoast
    tecols(i) = RGB(128, 255, 255) ' light blue
    If mvarZCoast <> mvarZShelf Then
      tecols(i) = RGB(128# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 255# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf), 116# * (i - mvarZShelf) / (mvarZCoast - mvarZShelf) + &H8B)
    End If
  Next i
  
  ' Greens for land
  For i = mvarZCoast To mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
    'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
    If i >= UBound(tecols) Then Exit For
    tecols(i) = RGB(0, i * 2 + 95, 0)
  Next i
  
  ' Brown for above tree line
  For i = mvarZCoast + TREELINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255)
    If i >= UBound(tecols) Then Exit For
    tecols(i) = RGB(128, (i - 73) * 40 / 16 + 40, 0)
  Next i
  
  ' White for above snow line
  For i = mvarZCoast + SNOWLINE_FT / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
    If i >= UBound(tecols) Then Exit For
    'tecols(i) = color_values(1)
    tecols(i) = RGB(128 + i - 110, 128 + i - 110, 128 + i - 110)
  Next i
  
  For i = mvarZCoast + 200 To UBound(tecols)
    If i >= UBound(tecols) Then Exit For
    'tecols(i) = color_values(1)
    tecols(i) = RGB(255, 255, 255)
  Next i

  ' minimum temperature > 18 C
  dndcols(D_WARM_SWAMP) = color_values(clr_purple2)
  dndcols(D_WARM_FOREST) = color_values(clr_green3)
  dndcols(D_WARM_PLAIN) = color_values(clr_orange2)
  dndcols(D_WARM_DESERT) = color_values(clr_yellow1)
  dndcols(D_WARM_HILL) = color_values(clr_red2)
  dndcols(D_WARM_MOUNTAIN) = RGB(137, 122, 92)
  dndcols(D_WARM_AQUATIC) = color_values(clr_blue2)
  
  dndcols(D_TEMP_SWAMP) = color_values(clr_purple3)
  dndcols(D_TEMP_FOREST) = color_values(clr_green4)
  dndcols(D_TEMP_PLAIN) = color_values(clr_orange3)
  dndcols(D_TEMP_DESERT) = color_values(clr_yellow2)
  dndcols(D_TEMP_HILL) = color_values(clr_red3)
  dndcols(D_TEMP_MOUNTAIN) = RGB(137 + 32, 122 + 32, 92 + 32)
  dndcols(D_TEMP_AQUATIC) = color_values(clr_blue3)
  
  ' Maximum temperature < 10 C
  dndcols(D_COLD_SWAMP) = color_values(clr_purple4)
  dndcols(D_COLD_FOREST) = color_values(clr_DarkGreen)
  dndcols(D_COLD_PLAIN) = color_values(clr_orange4)
  dndcols(D_COLD_DESERT) = color_values(clr_yellow3)
  dndcols(D_COLD_HILL) = color_values(clr_red4)
  dndcols(D_COLD_MOUNTAIN) = RGB(137 + 64, 122 + 64, 92 + 64)
  dndcols(D_COLD_AQUATIC) = color_values(clr_blue4)
  dndcols(D_OCEANICE) = color_values(clr_grey75)

  
End Sub


